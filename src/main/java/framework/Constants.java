package framework;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 27.09.2016.
 */
public interface Constants
{

    public final static int CONSTANT_1_SECOND = 1000;
    public final static int CONSTANT_2_SECONDS = 2000;
    public final static int CONSTANT_3_SECONDS = 3000;
    public final static int CONSTANT_4_SECONDS = 4000;
    public final static int CONSTANT_5_SECONDS = 5000;
    public final static int CONSTANT_10_SECONDS = 10000;
    public final static int CONSTANT_20_SECONDS = 20000;
    public final static int CONSTANT_30_SECONDS = 30000;
    public final static int CONSTANT_60_SECONDS = 60000;
    public final static int CONSTANT_120_SECONDS = 120000;
    public final static int CONSTANT_180_SECONDS = 180000;
    public final static int CONSTANT_240_SECONDS = 240000;
    public final static int CONSTANT_300_SECONDS = 300000;
    public final static int CONSTANT_360_SECONDS = 360000;
    public final static int CONSTANT_420_SECONDS = 420000;
    public final static int CONSTANT_480_SECONDS = 480000;
    public final static int CONSTANT_540_SECONDS = 540000;

    String HTTPS = "https://";
    String LEVEL_TRAVEL_URL = "level.travel/";
    String TRAVELATA_URL = "travelata.ru/";
    String AB = "?ab=a";
    String TRAVELATA_UTM_SOURCE = "?utm_source=advcake";

    Map<String, String> hotelClass = new HashMap<String, String>()
    {{
            put("", "all");
            put("5", "7");
            put("4", "4");
            put("3", "3");
            put("2", "2");

    }};

    Map<String, String> destinationCountryEng = new HashMap<String, String>()
    {{
        put("Турция", "Turkey");
        put("Россия", "Russia");
        put("Кипр", "Cyprus");
        put("Таиланд", "Thailand");
        put("Куба", "Cuba");
        put("Греция", "Greece");
        put("Доминикана", "Dominican");
        put("Тунис", "Tunisia");
        put("Египет", "Egypt");
        put("Вьетнам", "Vietnam");
        put("Индия", "India");
        put("ОАЭ", "UAE");
        put("Танзания", "Tanzania");
        put("Абхазия", "Abkhazia");
        put("Мальдивы", "Maldives");
        put("Черногория", "Montenegro");
        put("Мексика", "Mexico");
        put("Шри-Ланка", "Sri Lanka");
        put("Венесуэла", "Venezuela");
        put("Сейшелы", "Seychelles");
        put("Армения", "Armenia");
        put("Сербия", "Serbia");
        put("Иордания", "Jordan");
        put("Азербайджан", "Azerbaijan");

    }};

    Map<String, String> destinationCountryLT = new HashMap<String, String>()
    {{
        put("Турция", "TR");
        put("Россия", "RU");
        put("Кипр", "CY");
        put("Таиланд", "TH");
        put("Куба", "CU");
        put("Греция", "GR");
        put("Доминикана", "DO");
        put("Тунис", "TN");
        put("Египет", "EG");
        put("Вьетнам", "VN");
        put("Индия", "IN");
        put("ОАЭ", "AE");
        put("Танзания", "TZ");
        put("Абхазия", "AB");
        put("Мальдивы", "MV");
        put("Черногория", "ME");
        put("Мексика", "MX");
        put("Шри-Ланка", "LK");
        put("Венесуэла", "VE");
        put("Сейшелы", "SC");
        put("Армения", "AM");
        put("Сербия", "RS");
        put("Иордания", "JO");
        put("Азербайджан", "AZ");

    }};

    String[] resortsLTTurkey = {"Alanya", "Antalya", "Belek", "Bodrum", "Kemer", "Marmaris", "Side"}; //
    String[] ResortsLTRussia = {"Sochi", "Crimean", "Krasnaya.Polyana", "Anapa", "Alushta", "Yalta", "Eupatoria", "Kavminvody", "Adler"}; //  "Sochi", "Crimean", "Anapa", "Gelendzhik"
    String[] ResortsLTCyprus = {"Ayia.Napa", "Larnaca", "Limassol", "Paphos", "Protaras"};
    String[] ResortsLTThailand = {"Phuket", "Koh.Samui", "Pattaya", "Krabi"};
    String[] ResortsLTCuba = {"Varadero", "Havana"}; //, "Cayo.Santa.Maria", "Cayo.Coco"
    String[] ResortsLTGreece = {"Crete.island", "Corfu", "Rhodes", "Chalcidice", "Peloponnes"};
    String[] ResortsLTDominicanRepublic = {"Punta.Cana", "Boca.Chica"};
    String[] ResortsLTTunisia = {"Djerba", "Sousse", "Hammamet"};
    String[] ResortsLTEgypt = {"Hurghada.City", "Sharm.el.Sheikh", "Makadi.Bay"};
    String[] ResortsLTVietnam = {"Nha.Trang", "Phu.Quoc", "Phan.Thiet"};
    String[] ResortsLTIndia = {"Goa", "North.Goa", "South.Goa"};
    String[] ResortsLTUAE = {"Abu.Dhabi", "Dubai", "Sharjah"};
    String[] ResortsLTTanzania = {"Zanzibar"};
    String[] ResortsLTAbkhazia = {"Gagra", "Picunda", "Sukhumi"};
    String[] ResortsLTMaldives = {"Male", "Ari.Atoll", "South.Male.Atoll", "North.Male.Atoll"};
    String[] ResortsLTMontenegro = {"Budva", "Zabljak", "Kolasin", "Tivat"};
    String[] ResortsLTMexico = {"Cancun", "Riviera.Maya"}; //"Ciudad.de.México",
    String[] ResortsLTSriLanka = {"Galle", "Negombo", "Unawatuna"};
    String[] ResortsLTVenezuela = {"Margarita.Island"};
    String[] ResortsLTSeychelles = {"Mahe.Island", "Praslin.Island", "Silhouette.Island"};
    String[] ResortsLTArmenia = {"Djermuk", "Dilijan", "Erevan", "Tsakhkadzor"};
    String[] ResortsLTSerbia = {"Belgrade", "Kopaonik"};
    String[] ResortsLTJordan = {"Aqaba", "Amman", "Petra"};
    String[] ResortsLTAzerbaijan = {"Baku"};

    Map<String, String[]> destinationResortLT = new HashMap<String, String[]>()
    {{
        //TR
        put("Турция", resortsLTTurkey);
        //RU
        put("Россия", ResortsLTRussia);
        //CY
        put("Кипр", ResortsLTCyprus);
        //TH
        put("Таиланд", ResortsLTThailand);
        //CU
        put("Куба", ResortsLTCuba);
        //GR
        put("Греция", ResortsLTGreece);
        //DO
        put("Доминикана", ResortsLTDominicanRepublic);
        //TN
        put("Тунис", ResortsLTTunisia);
        //EG
        put("Египет", ResortsLTEgypt);
        //VN
        put("Вьетнам", ResortsLTVietnam);
        //IN
        put("Индия", ResortsLTIndia);
        //AE
        put("ОАЭ", ResortsLTUAE);
        //TZ
        put("Танзания", ResortsLTTanzania);
        //AB
        put("Абхазия", ResortsLTAbkhazia);
        //MV
        put("Мальдивы", ResortsLTMaldives);
        //ME
        put("Черногория", ResortsLTMontenegro);
        //MX
        put("Мексика", ResortsLTMexico);
        //LK
        put("Шри-Ланка", ResortsLTSriLanka);
        //VE
        put("Венесуэла", ResortsLTVenezuela);
        //SC
        put("Сейшелы", ResortsLTSeychelles);
        //AM
        put("Армения", ResortsLTArmenia);
        //RS
        put("Сербия", ResortsLTSerbia);
        //JO
        put("Иордания", ResortsLTJordan);
        //AZ
        put("Азербайджан", ResortsLTAzerbaijan);
    }};

    Map<String, String> resortLT = new HashMap<String, String>()
    {{
        put("ANY", "Любой");
        //TR
        put("Alanya", "Аланья");
        put("Antalya", "Анталья");
        put("Belek", "Белек");
        put("Bodrum", "Бодрум");
        put("Kemer", "Кемер");
        put("Marmaris", "Мармарис");
        put("Side", "Сиде");
        //RU
        put("Sochi", "Сочи");
        put("Adler", "Адлер");
        put("Crimean", "Крым");
        put("Anapa", "Анапа");
        put("Alushta", "Алушта");
        put("Yalta", "Ялта");
        put("Eupatoria", "Евпатория");
        put("Krasnaya.Polyana", "Красная поляна");
        put("Kavminvody", "Кавказские минеральные воды");
        put("St.Petersburg", "Санкт-Петербург");
        put("Dombay", "Домбай");
        put("Sheregesh", "Шерегеш");
        put("Arkhyz", "Архыз");
        put("Elbrusskiy.District", "Приэльбрусье");
        put("Gelendzhik", "Геленджик");
        //CY
        put("Ayia.Napa", "Айа-Напа");
        put("Larnaca", "Ларнака");
        put("Limassol", "Лимасол");
        put("Paphos", "Пафос");
        put("Protaras", "Протарас");
        //TH
        put("Phuket", "о. Пхукет");
        put("Koh.Samui", "о. Самуи");
        put("Pattaya", "Паттайя");
        put("Krabi", "Провинция Краби");
        //CU
        put("Varadero", "Варадеро");
        put("Havana", "Гавана");
//        put("Cayo.Santa.Maria", "о. Кайо-Санта-Мария");
//        put("Cayo.Coco", "о. Кайо Коко");
        //GR
        put("Crete.island", "о. Крит");
        put("Corfu", "о. Корфу");
        put("Rhodes", "о. Родос");
        put("Chalcidice", "Халкидики");
        put("Peloponnes", "Пелопоннес");
        //DO
        put("Punta.Cana", "Пунта-Кана");
        put("Boca.Chica", "Бока-Чика");
        //TN
        put("Djerba", "Джерба");
        put("Sousse", "Сусс");
        put("Hammamet", "Хаммамет");
        //EG
        put("Hurghada.City", "Хургада");
        put("Sharm.el.Sheikh", "Шарм-Эль-Шейх");
        put("Makadi.Bay", "Макади Бей");
        //VN
        put("Nha.Trang", "Нячанг");
        put("Phu.Quoc", "Фукуок");
        put("Phan.Thiet", "Фантхьет");
        //IN
        put("Goa", "Гоа");
        put("North.Goa", "Северный Гоа");
        put("South.Goa", "Южный Гоа");
        //AE
        put("Abu.Dhabi", "Абу Даби");
        put("Dubai", "Дубай");
        put("Sharjah", "Шарджа");
        //TZ
        put("Zanzibar", "о. Занзибар");
        //AB
        put("Gagra", "Гагра");
        put("Picunda", "Пицунда");
        put("Sukhumi", "Сухум");
        //MV
        put("Male", "Мале");
        put("Ari.Atoll", "Ари Атолл");
        put("South.Male.Atoll", "Южный Мале Атолл");
        put("North.Male.Atoll", "Северный Мале Атолл");
        //ME
        put("Budva", "Будва");
        put("Zabljak", "Жабляк");
        put("Kolasin", "Колашин");
        put("Tivat", "Тиват");
        //MX
        put("Cancun", "Канкун");
//        put("Ciudad.de.México", "Мехико");
        put("Riviera.Maya", "Ривьера Майя");
        //LK
        put("Galle", "Галле");
        put("Negombo", "Негомбо");
        put("Unawatuna", "Унаватуна");
        //VE
        put("Margarita.Island", "о. Маргарита");
        //SC
        put("Mahe.Island", "о. Маэ");
        put("Praslin.Island", "о. Праслен");
        put("Silhouette.Island", "о. Силуэт");
        //AM
        put("Djermuk", "Джермук");
        put("Dilijan", "Дилижан");
        put("Erevan", "Ереван");
        put("Tsakhkadzor", "Цахкадзор");
        //RS
        put("Belgrade", "Белград");
        put("Kopaonik", "Копаоник");
        //JO
        put("Aqaba", "Акаба");
        put("Amman", "Амман");
        put("Petra", "Петра");
        //AZ
        put("Baku", "Баку");

    }};

    Map<String, String> departureLT = new HashMap<String, String>()
    {{
        put("Москва", "Moscow");
        put("Санкт-Петербург", "St.Petersburg");
        put("Екатеринбург", "Yekaterinburg");
        put("Казань", "Kazan");
        put("Самара", "Samara");
        put("Нижний Новгород", "Nizhny.Novgorod");
//        put("Краснодар", "Krasnodar");
//        put("Ростов-на-Дону", "Rostov.on.Don");
        put("Уфа", "Ufa");
        put("Новосибирск", "Novosibirsk");
        put("Пермь", "Perm");
        put("Тюмень", "Tyumen");
        put("Калининград", "Kaliningrad");

        put("Абакан", "");
        put("Анапа", "");
        put("Апатиты", "");
        put("Архангельск", "");
        put("Астрахань", "");
        put("Барнаул", "");
        put("Белгород", "");
        put("Благовещенск", "");
        put("Брянск", "");
        put("Владивосток", "");
        put("Владикавказ", "");
        put("Волгоград", "");
        put("Воронеж", "");
        put("Грозный", "");
        put("Иваново", "");
        put("Иркутск", "");
        put("Калуга", "");
        put("Кемерово", "");
        put("Красноярск", "");
        put("Курган", "");
        put("Курск", "");
        put("Липецк", "");
        put("Магадан", "");
        put("Магнитогорск", "");
        put("Махачкала", "");
        put("Минеральные Воды", "");
        put("Мурманск", "");
        put("Набережные Челны", "");
        put("Нальчик", "");
        put("Нижневартовск", "");
        put("Нижнекамск", "");
        put("Новокузнецк", "");
        put("Норильск", "");
        put("Омск", "");
        put("Оренбург", "");
        put("Пенза", "");
        put("Петрозаводск", "");
        put("Петропавловск-Камчатский", "");
        put("Салехард", "");
        put("Саратов", "");
        put("Сочи", "");
        put("Ставрополь", "");
        put("Сургут", "");
        put("Сыктывкар", "");
        put("Тольятти", "");
        put("Томск", "");
        put("Улан-Удэ", "");
        put("Ульяновск", "");
        put("Хабаровск", "");
        put("Ханты-Мансийск", "");
        put("Чебоксары", "");
        put("Челябинск", "");
        put("Череповец", "");
        put("Чита", "");
        put("Южно-Сахалинск", "");
        put("Якутск", "");
        put("Ярославль", "");

    }};

    Map<String, String> destinationCountryOffer = new HashMap<String, String>()
    {{
        put("Турция", "92");
        put("Россия", "76");
        put("Кипр", "43");
        put("Таиланд", "87");
        put("Куба", "48");
        put("Греция", "26");
        put("Доминикана", "28");
        put("Тунис", "91");
        put("Египет", "29");
        put("Вьетнам", "22");
        put("Индия", "33");
        put("ОАЭ", "68");
        put("Танзания", "88");
        put("Абхазия", "1");
        put("Мальдивы", "56");
        put("Черногория", "104");
        put("Мексика", "60");
        put("Шри-Ланка", "110");
        put("Венесуэла", "21");
        put("Сейшелы", "78");
        put("Армения", "6");
        put("Сербия", "81");
        put("Иордания", "35");
        put("Азербайджан", "119");

    }};

    String[] resortsOfferTurkey = {"2159" , "2161", "2162", "2163", "3839", "2178","3828"}; //
    String[] ResortsOfferRussia = {"3097", "2232", "1691", "1552", "2202", "3135", "2213", "3100", "1545"}; // "3097", "2232", "1552", "3218"
    String[] ResortsOfferCyprus = {"919", "920", "922", "2869", "926"};
    String[] ResortsOfferThailand = {"2096", "2098", "2100", "2103"};
    String[] ResortsOfferCuba = {"1001", "1004"}; //, "1014", "1011"
    String[] ResortsOfferGreece = {"3163", "497", "509", "3164", "3842"};
    String[] ResortsOfferDominicanRepublic = {"571", "561"};
    String[] ResortsOfferTunisia = {"3841", "2147", "2150", "2155"};
    String[] ResortsOfferEgypt = {"597", "598", "591"};
    String[] ResortsOfferVietnam = {"417", "429", "428"};
    String[] ResortsOfferIndia = {"3280", "643", "3278"};
    String[] ResortsOfferUAE = {"1377", "1379", "1385"};
    String[] ResortsOfferTanzania = {"2134"};
    String[] ResortsOfferAbkhazia = {"1", "5", "6"};
    String[] ResortsOfferMaldives = {"1142", "1136", "1152", "1148"};
    String[] ResortsOfferMontenegro = {"3011", "2507", "2508", "2514"};
    String[] ResortsOfferMexico = {"1210",  "1227"}; //"1216",
    String[] ResortsOfferSriLanka = {"2658", "2681", "2695"};
    String[] ResortsOfferVenezuela = {"399"};
    String[] ResortsOfferSeychelles = {"1940", "1941", "1945"};
    String[] ResortsOfferArmenia = {"101", "102", "103", "105"};
    String[] ResortsOfferSerbia = {"1952", "1955"};
    String[] ResortsOfferJordan = {"694", "695", "703"};
    String[] ResortsOfferAzerbaijan = {"2952"};

    Map<String, String[]> destinationResortOffer = new HashMap<String, String[]>()
    {{
        //TR
        put("Турция", resortsOfferTurkey);
        //RU
        put("Россия", ResortsOfferRussia);
        //CY
        put("Кипр", ResortsOfferCyprus);
        //TH
        put("Таиланд", ResortsOfferThailand);
        //CU
        put("Куба", ResortsOfferCuba);
        //GR
        put("Греция", ResortsOfferGreece);
        //DO
        put("Доминикана", ResortsOfferDominicanRepublic);
        //TN
        put("Тунис", ResortsOfferTunisia);
        //EG
        put("Египет", ResortsOfferEgypt);
        //VN
        put("Вьетнам", ResortsOfferVietnam);
        //IN
        put("Индия", ResortsOfferIndia);
        //AE
        put("ОАЭ", ResortsOfferUAE);
        //TZ
        put("Танзания", ResortsOfferTanzania);
        //AB
        put("Абхазия", ResortsOfferAbkhazia);
        //MV
        put("Мальдивы", ResortsOfferMaldives);
        //ME
        put("Черногория", ResortsOfferMontenegro);
        //MX
        put("Мексика", ResortsOfferMexico);
        //LK
        put("Шри-Ланка", ResortsOfferSriLanka);
        //VE
        put("Венесуэла", ResortsOfferVenezuela);
        //SC
        put("Сейшелы", ResortsOfferSeychelles);
        //AM
        put("Армения", ResortsOfferArmenia);
        //RS
        put("Сербия", ResortsOfferSerbia);
        //JO
        put("Иордания", ResortsOfferJordan);
        //AZ
        put("Азербайджан", ResortsOfferAzerbaijan);
    }};

    Map<String, String> resortOffer = new HashMap<String, String>()
    {{
        //TR
        put("Аланья", "2159");
        put("Анталья", "2161");
        put("Белек", "2162");
        put("Бодрум", "2163");
        put("Кемер", "3839");   //2175
        put("Мармарис", "2178");
        put("Сиде", "3828");    //2184
        put("Стамбул", "2185");
        //RU
        put("Сочи", "3097");
        put("Крым", "2232");
        put("Красная поляна", "1691");
        put("Анапа", "3974");
        put("Алушта", "2202");
        put("Ялта", "2280");
        put("Евпатория", "2213");
        put("Кавказские минеральные воды", "3100");
        put("Адлер", "1545");
        put("Кудепста", "4030");
        put("Головинка", "4016");
        put("Имеретинская бухта", "4015");
        put("Дагомыс", "1620");
        put("Лазаревское", "1704");
        put("Лоо", "3123");
        put("Хоста", "3124");
        put("Роза Хутор", "3158");
        put("Сочи, центр города", "1843");
        put("Горки Город", "3290");
        put("Красная поляна поселок", "3289");
        put("Эсто-Садок", "3288");
        put("Санкт-Петербург", "1817");
        put("Домбай", "1624");
        put("Шерегеш", "1615");
        put("Архыз", "1660");
        put("Приэльбрусье", "1798");
        put("Геленджик", "1610");
        //CY
        put("Айа-Напа", "919");
        put("Ларнака", "920");
        put("Лимасол", "922");
        put("Пафос", "2869");
        put("Протарас", "926");
        //TH
        put("о. Пхукет", "2096");
        put("о. Самуи", "2098");
        put("Паттайя", "2100");
        put("Провинция Краби", "2103");
        //CU
        put("Варадеро", "1001");
        put("Гавана", "1004");
//        put("о. Кайо-Санта-Мария", "1014");
//        put("о. Кайо Коко", "1011");
        //GR
        put("о. Крит", "3163");
        put("о. Корфу", "497");
        put("о. Родос", "509");
        put("Халкидики", "3164");
        put("Пелопоннес", "3842");   //528
        //DO
        put("Пунта-Кана", "571");
        put("Бока-Чика", "561");
        //TN
        put("Джерба", "3841");  //2142
        put("Сусс", "2150");
        put("Хаммамет", "2155");
        //EG
        put("Хургада", "597");
        put("Шарм-Эль-Шейх", "598");
        put("Макади Бей", "591");
        //VN
        put("Нячанг", "417");
        put("Фукуок", "429");
        put("Фантхьет", "428");
        //IN
        put("Гоа", "3280");
        put("Северный Гоа", "643");
        put("Южный Гоа", "3278");
        //AE
        put("Абу Даби", "1377");
        put("Дубай", "1379");
        put("Шарджа", "1385");
        //TZ
        put("о. Занзибар", "2134");
        //AB
        put("Гагра", "1");
        put("Пицунда", "5");
        put("Сухум", "6");
        //MV
        put("Мале", "1142");
        put("Ари Атолл", "1136");
        put("Южный Мале Атолл", "1152");
        put("Северный Мале Атолл", "1148");
        //ME
        put("Будва", "3011");
        put("Жабляк", "2507");
        put("Колашин", "2508");
        put("Тиват", "2514");
        //MX
        put("Канкун", "1210");
//        put("Мехико", "1216");
        put("Ривьера Майя", "1227");
        //LK
        put("Галле", "2658");
        put("Негомбо", "2681");
        put("Унаватуна", "2695");
        //VE
        put("о. Маргарита", "399");
        //SC
        put("о. Маэ", "1940");
        put("о. Праслен", "1941");
        put("о. Силуэт", "1945");
        //AM
        put("Джермук", "101");
        put("Дилижан", "102");
        put("Ереван", "103");
        put("Цахкадзор", "105");
        //RS
        put("Белград", "1952");
        put("Копаоник", "1955");
        //JO
        put("Акаба", "694");
        put("Амман", "695");
        put("Петра", "703");
        //AZ
        put("Баку", "2952");
    }};

    Map<String, String> departureOffer = new HashMap<String, String>()
    {{
        put("Москва", "2");
        put("Санкт-Петербург", "1");
        put("Екатеринбург", "25");
        put("Казань", "29");
        put("Самара", "64");
        put("Нижний Новгород", "50");
//        put("Краснодар", "36");
//        put("Ростов-На-Дону", "63");
        put("Уфа", "79");
        put("Новосибирск", "53");
        put("Пермь", "61");
        put("Тюмень", "74");
        put("Калининград", "30");

        put("Абакан", "90");
//        put("Анапа", "129"); //
        put("Апатиты", "131");
        put("Архангельск", "8"); //
//        put("Астрахань", "10"); //
        put("Барнаул", "12"); //
//        put("Белгород", "13"); //
        put("Благовещенск", "15"); //
//        put("Брянск", "18");
        put("Владивосток", "19"); //
//        put("Владикавказ", "20"); //
        put("Волгоград", "21");  //
//        put("Воронеж", "22"); //
        put("Грозный", "130"); //
        put("Иваново", "137");
        put("Иркутск", "28"); //
        put("Калуга", "128");
        put("Кемерово", "32"); //
        put("Красноярск", "37"); //
        put("Курган", "38");
//        put("Курск", "39");
//        put("Липецк", "91");
        put("Магадан", "42");
        put("Магнитогорск", "43"); //
        put("Махачкала", "92"); //
        put("Минеральные Воды", "44"); //
        put("Мурманск", "46"); //
        put("Набережные Челны", "96");
        put("Нальчик", "47");
        put("Нижневартовск", "48");
        put("Нижнекамск", "49"); //
        put("Новокузнецк", "51"); //
        put("Норильск", "135");
        put("Омск", "56"); //
        put("Оренбург", "57"); //
        put("Пенза", "60");
        put("Петрозаводск", "133");
        put("Петропавловск-Камчатский", "62");
        put("Салехард", "132");
        put("Саратов", "65"); //
        put("Сочи", "67"); //
//        put("Ставрополь", "93");
        put("Сургут", "68"); //
        put("Сыктывкар", "70");
        put("Тольятти", "71");
        put("Томск", "72"); //
        put("Улан-Удэ", "75");
        put("Ульяновск", "76"); //
        put("Хабаровск", "80"); //
        put("Ханты-Мансийск", "81"); //
        put("Чебоксары", "83");
        put("Челябинск", "84"); //
        put("Череповец", "134");
        put("Чита", "85");
        put("Южно-Сахалинск", "87"); //
        put("Якутск", "88");
        put("Ярославль", "136");

    }};

    Map<String, String> TOLT = new HashMap<String, String>()
    {{
        put("Alean", "37");
        put("AnexTour", "6");
        put("Biblio Globus", "7");
        put("Coral", "2");
        put("ICS Travel", "8");
        put("Intourist", "34");
        put("Mouzenidis", "43");
        put("One Touch Travel", "82");
        put("Paks", "21");
        put("Panteon", "44");
        put("Pegast", "4");
        put("Russian Express", "22");
        put("Space Travel", "45");
        put("Sunmar", "3");
        put("TezTour", "1");
        put("FUN&SUN", "70");
        put("Level.Travel", "78");
    }};

    String[] TOAll = new String[]{
            "Alean",
            "AnexTour",
            "Biblio Globus",
            "Coral",
            "GrandPTC",
            "ICS Travel",
            "Intourist",
            "Mouzenidis",
            "One Touch Travel",
            "Pac Group",
            "Paks",
            "Panteon",
            "Pegast",
            "Russian Express",
            "Solvex",
            "Space Travel",
            "SpectrumTravel",
            "Sunmar",
            "TezTour",
            "FUN&SUN",
            "MyAgent",
            "Level.Travel"
    };

    String[] TOPriceBy = new String[]{
            "AnexTour",
            "Biblio Globus",
            "Coral",
            "ICS Travel",
            "Intourist",
            "Pegast",
            "Sunmar",
            "TezTour",
            "FUN&SUN",
    };

    Map<String, String> TOOffer = new HashMap<String, String>()
    {{
        put("Alean",            "AleanTour");
        put("Ambotis Tours",    "Ambotis Tours");
        put("Amigo-S",          "Amigo-S");
        put("Amigo-Tours",      "Amigo-Tours");
        put("AnexTour",         "AnexTour");
        put("Biblio Globus",    "Biblio Globus");
        put("Coral",            "Coral Travel");
        put("De Visu",          "De Visu");
        put("GrandPTC",         "GrandPTC(Grand Express)");
        put("ICS Travel",       "ICS Travel Group");
        put("Intourist",        "NTK-Intourist");
        put("Mouzenidis",       "Mouzenidis Travel");
        put("Pac Group",        "PAC group");
        put("Paks",             "PAKS");
        put("Panteon",          "Panteon");
        put("Pegast",           "Pegas Touristik");
        put("Roza Vetrov",      "Roza Vetrov");
        put("Russian Express",  "RussianExpress");
        put("RusTour",          "RusTour");
        put("Solvex",           "Solvex");
        put("Space Travel",     "Space Travel");
        put("SpectrumTravel",   "SpectrumTravel");
        put("Sunmar",           "Sunmar");
        put("TezTour",          "Tez Tour");
        put("FUN&SUN (TUI)",    "FUN&SUN");
        put("FUN&SUN (ex. TUI)","FUN&SUN");
        put("FUN&SUN",          "FUN&SUN");
        put("Vand",             "Vand");
        put("Vedi TourGroup",   "Vedi TourGroup");
        put("MyAgent",          "MyAgent");
        put("One Touch Travel", "One Touch Travel");
        put("Level.Travel",     "Level.Travel");
    }};

    Map<String, String> TOOfferId = new HashMap<String, String>()
    {{
        put("Alean",            "122");
        put("Ambotis Tours",    "3");
        put("Amigo-S",          "4");
        put("Amigo-Tours",      "119");
        put("AnexTour",         "5");
        put("Biblio Globus",    "11");
        put("Coral",            "117");
        put("De Visu",          "14");
        put("GrandPTC",         "22");
        put("ICS Travel",       "26");
        put("Intourist",        "81");
        put("Mouzenidis",       "35");
        put("Pac Group",        "39");
        put("Paks",             "92");
        put("Panteon",          "121");
        put("Pegast",           "40");
        put("Roza Vetrov",      "181");
        put("Russian Express",  "98");
        put("RusTour",          "180");
        put("Solvex",           "101");
        put("Space Travel",     "46");
        put("SpectrumTravel",   "47");
        put("Sunmar",           "48");
        put("TezTour",          "50");
        put("FUN&SUN",          "55");
        put("Vand",             "190");
        put("Vedi TourGroup",   "68");
        put("MyAgent",          "226");

    }};

    Map<String, String> hotelStarsOffer = new HashMap<String, String>()
    {{
        put("2", "2");
        put("3", "3");
        put("4", "4");
        put("5", "7");
        put("APARTMENT", "5");
        put("Boutique", "Бутик-отель");
    }};

    Map<String, String> hotelMealsOffer = new HashMap<String, String>()
    {{
        put("AI", "1");
        put("BB", "2");
        put("FB", "3");
        put("HB", "5");
        put("RO", "7");
        put("AO", "7");
        put("UAI", "8");
        put("PRG", "9");
        put("LHB", "10");
        put("AI(noAlc)", "11");
        put("Lunch", "12");
        put("Dinner", "13");
        put("ROT", "14");
        put("BBT", "15");
        put("HBT", "16");
        put("FBT", "17");
        put("FB+", "4");
        put("HB+", "6");
    }};

    String[] hotelCategoriesOffer = new String[]{
            "Апартаменты",
            "Вилла",
            "Хостел",
            "Виллы",
            "Клубный отель",
            "Апарт-отель",
            "Аппарт-отель",
            "Гостиничный комп",
            "Курортный город",
            "Санаторий",
            "Пансионат",
            "Дом отдыха",
            "База отдыха",
            "Гостевой дом",
            "Гостиница",
            "Бутик-отель",
            "Экскурсионная программа",
    };

    String urlSeparator = "&";

    //SMTP
    String gmailUsername = "travelata.mail.test@gmail.com";
    String gmailPassword = "!Qazwsx123";
    String gmailHost = "smtp.gmail.com";

    String mailFrom = "travelata.mail.test@gmail.com";
    String mailHost = "smtp.travadm.org"; //mx02.travelata.ru, mx01.travelata.ru "mx.travelata.ru"; smtp.travadm.org
    String mailPort = "465";

    Map<String, String> monthsRussian = new HashMap<String, String>()
    {{
        put("января", "январь");
        put("февраля", "февраль");
        put("марта", "март");
        put("апреля", "апрель");
        put("мая", "май");
        put("июня", "июнь");
        put("июля", "июль");
        put("августа", "август");
        put("сентября", "сентябрь");
        put("октября", "октябрь");
        put("ноября", "ноябрь");
        put("декабря", "декабрь");
    }};

    Map<String, String> monthsRussianCut = new HashMap<String, String>()
    {{
        put("января", "янв");
        put("февраля", "фев");
        put("марта", "мар");
        put("апреля", "апр");
        put("мая", "май");
        put("июня", "июн");
        put("июля", "июл");
        put("августа", "авг");
        put("сентября", "сен");
        put("октября", "окт");
        put("ноября", "ноя");
        put("декабря", "дек");
    }};

    Map<String, String> monthsRussianCapital = new HashMap<String, String>()
    {{
        put("января", "Январь");
        put("февраля", "Февраль");
        put("марта", "Март");
        put("апреля", "Апрель");
        put("мая", "Май");
        put("июня", "Июнь");
        put("июля", "Июль");
        put("августа", "Август");
        put("сентября", "Сентябрь");
        put("октября", "Октябрь");
        put("ноября", "Ноябрь");
        put("декабря", "Декабрь");
    }};

    Map<String, String> monthsRussianNumber = new HashMap<String, String>()
    {{
        put("января", "1");
        put("февраля", "2");
        put("марта", "3");
        put("апреля", "4");
        put("мая", "5");
        put("июня", "6");
        put("июля", "7");
        put("августа", "8");
        put("сентября", "9");
        put("октября", "10");
        put("ноября", "11");
        put("декабря", "12");
    }};

    String[] resortsAnexRussia = {"Сочи", "Геленджик", "Анапа"};
    String[] resortsAnexTurkey = {"Анталья", "Аланья", "Белек", "Бодрум", "Кемер", "Мармарис", "Сиде"};
    Map<String, String[]> countryResortsAnex = new HashMap<String, String[]>(){
        {
            put("Турция", resortsAnexTurkey);
            put("Россия", resortsAnexRussia);
        }
    };
    Map<String, String> hotelMealsAnex = new HashMap<String, String>()
    {{
        put("AI", "All Inclusive");
        put("BB", "Bed & Breakfast");
        put("FB", "Full Board");
        put("HB", "Half Board");
        put("RO", "Ro");
        put("UAI", "Ultra Ai");
    }};

    String[] departureAnex =
    {
        "Москва",
        "Санкт-Петербург",
        "Екатеринбург",
        "Казань",
        "Самара",
        "Нижний Новгород",
//        "Краснодар",
//        "Ростов-На-Дону",
        "Уфа",
        "Новосибирск",
        "Пермь",
        "Тюмень",
        "Калининград",
//        "Анапа",
        "Архангельск",
//        "Астрахань",
        "Барнаул",
//        "Белгород",
        "Благовещенск",
        "Владивосток",
//        "Владикавказ",
        "Волгоград",
//        "Воронеж",
        "Грозный",
        "Иркутск",
        "Кемерово",
        "Красноярск",
        "Магнитогорск",
        "Махачкала",
        "Минеральные Воды",
        "Мурманск",
        "Нижнекамск",
        "Новокузнецк",
        "Омск",
        "Оренбург",
        "Саратов",
//        "Сочи",
        "Сургут",
        "Томск",
        "Ульяновск",
        "Хабаровск",
        "Ханты-Мансийск",
        "Челябинск",
        "Южно-Сахалинск",
    };

    String[] departureBG =
            {
                    "Москва",
                    "Санкт-Петербург",
                    "Абакан",
//                    "Анапа",
                    "Архангельск",
                    "Владивосток",
                    "Волгоград",
//                    "Воронеж",
                    "Грозный",
                    "Екатеринбург",
                    "Иркутск",
                    "Казань",
                    "Калининград",
                    "Кемерово",
//                    "Краснодар",
                    "Красноярск",
                    "Минеральные Воды",
                    "Мурманск",
                    "Нижневартовск",
                    "Нижний Новгород",
                    "Новокузнецк",
                    "Новосибирск",
                    "Омск",
                    "Оренбург",
                    "Пермь",
//                    "Ростов-на-Дону",
                    "Самара",
//                    "Симферополь",
//                    "Сочи",
                    "Сургут",
                    "Сыктывкар",
                    "Тюмень",
                    "Уфа",
                    "Хабаровск",
                    "Ханты-Мансийск",
                    "Челябинск",
                    "Южно-Сахалинск",
            };

    String[] departureFSTravel =
            {
                    "Москва",
                    "Санкт-Петербург",
//                    "Адлер",
//                    "Анапа",
                    "Архангельск",
//                    "Астрахань",
                    "Барнаул",
//                    "Белгород",
                    "Благовещенск",
//                    "Брянск",
                    "Владивосток",
//                    "Владикавказ",
                    "Волгоград",
//                    "Воронеж",
                    "Грозный",
                    "Екатеринбург",
                    "Иркутск",
                    "Казань",
                    "Калининград",
                    "Калуга",
                    "Кемерово",
//                    "Краснодар",
                    "Красноярск",
                    "Магнитогорск",
                    "Махачкала",
                    "Минеральные Воды",
                    "Мурманск",
                    "Нижневартовск",
                    "Нижнекамск",
                    "Нижний Новгород",
                    "Новокузнецк",
                    "Новосибирск",
                    "Омск",
                    "Оренбург",
                    "Пермь",
                    "Петрозаводск",
//                    "Ростов-на-Дону",
                    "Самара",
                    "Саранск",
                    "Саратов",
//                    "Сочи",
                    "Сургут",
                    "Сыктывкар",
                    "Тюмень",
                    "Улан-Удэ",
                    "Ульяновск",
                    "Уфа",
                    "Хабаровск",
                    "Челябинск",
                    "Череповец",
                    "Южно-Сахалинск",
                    "Якутск",
            };

    String[] departureLevelTravel =
            {
                    "Москва",
                    "Санкт-Петербург",
                    "Екатеринбург",
                    "Казань",
//                    "Краснодар",
                    "Нижний Новгород",
                    "Новосибирск",
                    "Пермь",
//                    "Ростов-на-Дону",
                    "Самара",
                    "Тюмень",
                    "Уфа",
            };

    Map<String, String> hotelMealsTui = new HashMap<String, String>()
    {{
        put("AI", "Все включено");
        put("BB", "Завтрак");
        put("HB", "Полупансион");
        put("RO", "Без питания");
        put("UAI", "Ультра все включено");
    }};

    Map<String, String> hotelMealsFSTravel = new HashMap<String, String>()
    {{
        put("AI", "Все включено"); //10004
        put("BB", "Завтрак"); //10001
        put("HB", "Полупансион"); //10002
        put("FB", "Полный пансион"); //10003
        put("RO", "Без питания"); //10005
        put("UAI", "Ультра все включено"); //8
    }};

    Map<String, String> hotelMealsPegast = new HashMap<String, String>()
    {{
        put("AI", "Все Включено");
        put("BB", "Только Завтраки");
        put("HB", "Завтрак И Ужин");
        put("RO", "Без Питания");
        put("FB", "Завтрак, Ужин, Обед Без Алкоголя");
    }};

    String[] resortsAleanRussia = {"Сочи", "Крым", "Анапа"};
    Map<String, String[]> countryResortsAlean = new HashMap<String, String[]>(){
        {
            put("Россия", resortsAleanRussia);
        }
    };

    String[] resortsBGRussia = {"Сочи", "Крым", "Анапа"};
    String[] resortsBGTurkey = {"Бодрум", "Стамбул", "Анталию"};
    Map<String, String> resortsBGOffer = new HashMap<String, String>()
    {{
        put("Бодрум", "Бодрум");
        put("Стамбул", "Стамбул");
        put("Анталию", "Анталья");
    }};

    Map<String, String> hotelCategoriesBG = new HashMap<String, String>()
    {{
        put("5", "1, 2, 3, 4, ");
        put("4", "5, 6, 7, 8, ");
        put("3", "9, 10, 11, 12, 13, ");
        put("2", "14, 15, 16, 17, ");
        put("1", "18, ");
        put("APARTMENT", "23, ");
    }};

    Map<String, String> hotelCategoriesAnex = new HashMap<String, String>()
    {{
        put("2", "10001, ");
        put("3", "10008, ");
        put("4", "10009, ");
        put("5", "10010, 3, ");
        put("Boutique", "10014, ");
    }};

    Map<String, String> hotelCategoriesTui = new HashMap<String, String>()
    {{
        put("3", "10003, ");
        put("4", "10004, ");
        put("5", "10005, 8, ");
        put("Apt", "10006, ");
        put("BOUTIQUE", "4, ");
        put("HV", "10007, ");
    }};

    Map<String, String> hotelCategoriesPegast = new HashMap<String, String>()
    {{
        put("5", "27, 29, 1388, 1389, 113035705, 388879400, 449374365, ");
        put("4", "28, 1387, 17084, 73528743, 1390, 113039649, 116931611, ");
        put("3", "329, 330, 113044873, 113047862, ");
        put("2", "1385, 1386, ");
        put("1", "1384, 6596484, ");
    }};

    Map<String, String> countriesBG = new HashMap<String, String>()
    {{
        put("Россия", "Россия");
        put("Турция", "Турция");
        put("Кипр", "Кипр");
        put("ОАЭ", "Объединенные Арабские Эмираты");
        put("Египет", "Египет");
    }};

}
