package framework;

import java.sql.*;
import java.util.ArrayList;

import static framework.Constants.*;
import static pageobjects.Travelata.OfferEmailSearchPage.getHotelIdOfferList;
import static pageobjects.Travelata.OfferEmailSearchPage.setHotelIdOfferList;
import static pageobjects.operators.Anex_new.AnexSearchPage.getCountryResortAnexList;
import static pageobjects.operators.Anex_new.AnexSearchPage.getHotelNamesAnexList;
import static pageobjects.operators.BiblioGlobus.BGTourListPage.getCountryResortBGList;
import static pageobjects.operators.BiblioGlobus.BGTourListPage.getHotelNamesBGList;
import static pageobjects.operators.FSTravel.FSTravelSearchPage.getCountryResortFSTravelList;
import static pageobjects.operators.FSTravel.FSTravelSearchPage.getHotelNamesFSTravelList;
import static pageobjects.operators.Pegast.PegastSearchPage.getCountryResortPegastList;
import static pageobjects.operators.Pegast.PegastSearchPage.getHotelNamesPegastList;
import static pageobjects.operators.TUI_new.TuiSearchPage.getCountryResortTuiList;
import static pageobjects.operators.TUI_new.TuiSearchPage.getHotelNamesTuiList;

public class JDBCPostgreSQLConnection {

    //  DB BG credentials
    static final String DB_BG_URL = "jdbc:postgresql://biblio-globus.psql.travelata.lan:6432/biblio_globus";
    static final String DB_BG_USER = "readonly_olena_tyshchenko"; //biblio_globus //readonly_olena_tyshchenko
    static final String DB_BG_PASS = "ienu7ieru2Oh"; //uas4AMeituqu //bVd73VXDI08AYP7yufhQ

    //  DB Anex credentials
    static final String DB_ANEX_URL = "jdbc:postgresql://anex.psql.travelata.lan:6432/anex";
    static final String DB_ANEX_USER = "readonly_olena_tyshchenko";
    static final String DB_ANEX_PASS = "ienu7ieru2Oh";

    //  DB Tui credentials
    static final String DB_TUI_URL = "jdbc:postgresql://samo_tui.psql.travelata.lan:6432/samo_tui";
    static final String DB_TUI_USER = "readonly_olena_tyshchenko";
    static final String DB_TUI_PASS = "ienu7ieru2Oh";

    //  DB Pegast credentials
    static final String DB_PEGAST_URL = "jdbc:postgresql://pegast.psql.travelata.lan:6432/pegast";
    static final String DB_PEGAST_USER = "readonly_olena_tyshchenko";
    static final String DB_PEGAST_PASS = "ienu7ieru2Oh";

    //  DB inv credentials
    static final String DB_INV_URL = "jdbc:postgresql://10.10.11.30:6433/inventory";
    static final String DB_INV_USER = "readonly_tyshchenko";
    static final String DB_INV_PASS = "ienaimo9AhKo";

    private static ArrayList<Long> hotelIdBGList = new ArrayList<>();
    public static ArrayList <Long> getHotelIdBGList(){
        return hotelIdBGList;
    }
    public static void setHotelIdBGList(Long hotelIdBGListValue){
        hotelIdBGList.add(hotelIdBGListValue);
    }

    private static ArrayList<Long> hotelIdAnexList = new ArrayList<>();
    public static ArrayList <Long> getHotelIdAnexList(){
        return hotelIdAnexList;
    }
    public static void setHotelIdAnexList(Long hotelIdAnexListValue){
        hotelIdAnexList.add(hotelIdAnexListValue);
    }

    private static ArrayList<Long> hotelIdTuiList = new ArrayList<>();
    public static ArrayList <Long> getHotelIdTuiList(){
        return hotelIdTuiList;
    }
    public static void setHotelIdTuiList(Long hotelIdTuiListValue){
        hotelIdTuiList.add(hotelIdTuiListValue);
    }

    private static ArrayList<Long> hotelIdFSTravelList = new ArrayList<>();
    public static ArrayList <Long> getHotelIdFSTravelList(){
        return hotelIdFSTravelList;
    }
    public static void setHotelIdFSTravelList(Long hotelIdFSTravelListValue){
        hotelIdFSTravelList.add(hotelIdFSTravelListValue);
    }

    private static ArrayList<Long> hotelIdPegastList = new ArrayList<>();
    public static ArrayList <Long> getHotelIdPegastList(){
        return hotelIdPegastList;
    }
    public static void setHotelIdPegastList(Long hotelIdPegastListValue){
        hotelIdPegastList.add(hotelIdPegastListValue);
    }

    public static void getBGOfferHotelId(ArrayList<String> starsValue, String countryValue) throws SQLException{

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        String hotelCategoriesValue = "";
        for(int starsValueCounter = 0; starsValueCounter < starsValue.size(); starsValueCounter ++){
            hotelCategoriesValue += hotelCategoriesBG.get(starsValue.get(starsValueCounter));
        }
        String hotelCategoryDBCondition = "";
        if(!hotelCategoriesValue.isEmpty()){
            hotelCategoriesValue = hotelCategoriesValue.substring(0, hotelCategoriesValue.lastIndexOf(", "));
            hotelCategoryDBCondition = " and category_id in (" + hotelCategoriesValue + ")";
        }


        if(!getHotelNamesBGList().isEmpty()){
            for(int i=0; i<getHotelNamesBGList().size(); i++){

                Connection connection = null;
                try {
                    connection = DriverManager
                            .getConnection(DB_BG_URL, DB_BG_USER, DB_BG_PASS);

                } catch (SQLException e) {
                    System.out.println("Connection Failed");
                    e.printStackTrace();
                    return;
                }

                String hotelNameValue = getHotelNamesBGList().get(i);
                if(hotelNameValue.contains("'"))
                    hotelNameValue = getHotelNamesBGList().get(i).substring(0, hotelNameValue.indexOf("'"));
                Statement statement = connection.createStatement();
                try {
                    String SQL = "select distinct id from hotels where name like '" + hotelNameValue + "%'" + hotelCategoryDBCondition;
                    if(getCountryResortBGList().get(i).equals(countryValue))
                        SQL += " and resort_id in (select id from resorts where name_ru like '%" + getCountryResortBGList().get(i) + "%')";
                    ResultSet resultSet = statement.executeQuery(SQL);

                    if (resultSet.next())
                        hotelIdBGList.add(resultSet.getLong(1));
                    else
                        hotelIdBGList.add(Long.valueOf(0));
                } finally {
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                }
            }
            }

        ///////////////////////////////////////////////////////////////////////

        for(int i=0; i<hotelIdBGList.size(); i++){

            if(hotelIdBGList.get(i) != Long.valueOf(0)){
                Connection connection = null;
                try {
                    connection = DriverManager
                            .getConnection(DB_INV_URL, DB_INV_USER, DB_INV_PASS);

                } catch (SQLException e) {
                    System.out.println("Connection Failed");
                    e.printStackTrace();
                    return;
                }
                Statement statement = connection.createStatement();
                try {
                    String SQL = "select gate_id from hotels_relations where out_system_id like '" + hotelIdBGList.get(i) + "' and source = 'biblioglobus'";
                    ResultSet resultSet = statement.executeQuery(SQL);

                    if (resultSet.next()){
                        setHotelIdOfferList(String.valueOf(resultSet.getLong(1)));
                    }
                    else
                        setHotelIdOfferList("0");
                } finally {
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                }
            }
            else
                setHotelIdOfferList("0");
        }

        for(int i=0; i<getHotelIdOfferList().size(); i++){
            System.out.println("Hotel offer id: " + getHotelIdOfferList().get(i));
        }

    }

    public static void getAnexOfferHotelId(ArrayList<String> starsValue, String countryValue) throws SQLException{

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        String hotelCategoriesValue = "";
        for(int starsValueCounter = 0; starsValueCounter < starsValue.size(); starsValueCounter ++){
            hotelCategoriesValue += hotelCategoriesAnex.get(starsValue.get(starsValueCounter));
        }
        String hotelCategoryDBCondition = "";
        if(!hotelCategoriesValue.isEmpty()){
            hotelCategoriesValue = hotelCategoriesValue.substring(0, hotelCategoriesValue.lastIndexOf(", "));
            hotelCategoryDBCondition = " and category_id in (" + hotelCategoriesValue + ")";
        }

        if(!getHotelNamesAnexList().isEmpty()){
            for(int i=0; i<getHotelNamesAnexList().size(); i++){

                Connection connection = null;
                try {
                    connection = DriverManager
                            .getConnection(DB_ANEX_URL, DB_ANEX_USER, DB_ANEX_PASS);

                } catch (SQLException e) {
                    System.out.println("Connection Failed");
                    e.printStackTrace();
                    return;
                }

                String hotelNameValue = getHotelNamesAnexList().get(i);
                if(hotelNameValue.contains("'"))
                    hotelNameValue = getHotelNamesAnexList().get(i).substring(0, hotelNameValue.indexOf("'"));
                Statement statement = connection.createStatement();
                try {
                    String SQL = "select distinct id from hotels where name like '" + hotelNameValue + "%'" + hotelCategoryDBCondition;
                    if(getCountryResortAnexList().get(i).equals(countryValue))
                        SQL += " and resort_id in (select id from resorts where name like '%" + getCountryResortAnexList().get(i) + "%')";
                    ResultSet resultSet = statement.executeQuery(SQL);

                    if (resultSet.next())
                        hotelIdAnexList.add(resultSet.getLong(1));
                    else
                        hotelIdAnexList.add(Long.valueOf(0));
                } finally {
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////

        for(int i=0; i<hotelIdAnexList.size(); i++){

            if(hotelIdAnexList.get(i) != Long.valueOf(0)){
                Connection connection = null;
                try {
                    connection = DriverManager
                            .getConnection(DB_INV_URL, DB_INV_USER, DB_INV_PASS);

                } catch (SQLException e) {
                    System.out.println("Connection Failed");
                    e.printStackTrace();
                    return;
                }
                Statement statement = connection.createStatement();
                try {
                    String SQL = "select gate_id from hotels_relations where out_system_id like '" + hotelIdAnexList.get(i) + "'  and source = 'anex'";
                    ResultSet resultSet = statement.executeQuery(SQL);

                    if (resultSet.next()){
                        setHotelIdOfferList(String.valueOf(resultSet.getLong(1)));
                    }
                    else
                        setHotelIdOfferList("0");
                } finally {
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                }
            }
            else
                setHotelIdOfferList("0");
        }

        for(int i=0; i<getHotelIdOfferList().size(); i++){
            System.out.println("Hotel offer id: " + getHotelIdOfferList().get(i));
        }

    }

    public static void getTuiOfferHotelId(ArrayList<String> starsValue, String countryValue) throws SQLException{

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        String hotelCategoriesValue = "";
        for(int starsValueCounter = 0; starsValueCounter < starsValue.size(); starsValueCounter ++){
            hotelCategoriesValue += hotelCategoriesTui.get(starsValue.get(starsValueCounter));
        }
        String hotelCategoryDBCondition = "";
        if(!hotelCategoriesValue.isEmpty()){
            hotelCategoriesValue = hotelCategoriesValue.substring(0, hotelCategoriesValue.lastIndexOf(", "));
            hotelCategoryDBCondition = " and category_id in (" + hotelCategoriesValue + ")";
        }

        if(!getHotelNamesTuiList().isEmpty()){
            for(int i=0; i<getHotelNamesTuiList().size(); i++){

                Connection connection = null;
                try {
                    connection = DriverManager
                            .getConnection(DB_TUI_URL, DB_TUI_USER, DB_TUI_PASS);

                } catch (SQLException e) {
                    System.out.println("Connection Failed");
                    e.printStackTrace();
                    return;
                }

                String hotelNameValue = getHotelNamesTuiList().get(i);
                if(hotelNameValue.contains("'"))
                    hotelNameValue = getHotelNamesTuiList().get(i).substring(0, hotelNameValue.indexOf("'"));
                Statement statement = connection.createStatement();
                try {
                    String SQL = "select distinct id from hotels where name like '" + hotelNameValue + "%'" + hotelCategoryDBCondition;
                    if(getCountryResortTuiList().get(i).equals(countryValue))
                        SQL += " and resort_id in (select id from resorts where name like '%" + getCountryResortTuiList().get(i) + "%')";
                    ResultSet resultSet = statement.executeQuery(SQL);

                    if (resultSet.next())
                        hotelIdTuiList.add(resultSet.getLong(1));
                    else
                        hotelIdTuiList.add(Long.valueOf(0));
                } finally {
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////

        for(int i=0; i<hotelIdTuiList.size(); i++){

            if(hotelIdTuiList.get(i) != Long.valueOf(0)){
                Connection connection = null;
                try {
                    connection = DriverManager
                            .getConnection(DB_INV_URL, DB_INV_USER, DB_INV_PASS);

                } catch (SQLException e) {
                    System.out.println("Connection Failed");
                    e.printStackTrace();
                    return;
                }
                Statement statement = connection.createStatement();
                try {
                    String SQL = "select gate_id from hotels_relations where out_system_id like '" + hotelIdTuiList.get(i) + "'  and source = 'samotui'";
                    ResultSet resultSet = statement.executeQuery(SQL);

                    if (resultSet.next()){
                        setHotelIdOfferList(String.valueOf(resultSet.getLong(1)));
                    }
                    else
                        setHotelIdOfferList("0");
                } finally {
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                }
            }
            else
                setHotelIdOfferList("0");
        }

        for(int i=0; i<getHotelIdOfferList().size(); i++){
            System.out.println("Hotel offer id: " + getHotelIdOfferList().get(i));
        }

    }

    public static void getFSTravelOfferHotelId(ArrayList<String> starsValue, String countryValue) throws SQLException{

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        String hotelCategoriesValue = "";
        for(int starsValueCounter = 0; starsValueCounter < starsValue.size(); starsValueCounter ++){
            hotelCategoriesValue += hotelCategoriesTui.get(starsValue.get(starsValueCounter));
        }
        String hotelCategoryDBCondition = "";
        if(!hotelCategoriesValue.isEmpty()){
            hotelCategoriesValue = hotelCategoriesValue.substring(0, hotelCategoriesValue.lastIndexOf(", "));
            hotelCategoryDBCondition = " and category_id in (" + hotelCategoriesValue + ")";
        }

        if(!getHotelNamesFSTravelList().isEmpty()){
            for(int i=0; i<getHotelNamesFSTravelList().size(); i++){

                Connection connection = null;
                try {
                    connection = DriverManager
                            .getConnection(DB_TUI_URL, DB_TUI_USER, DB_TUI_PASS);

                } catch (SQLException e) {
                    System.out.println("Connection Failed");
                    e.printStackTrace();
                    return;
                }

                String hotelNameValue = getHotelNamesFSTravelList().get(i);
                if(hotelNameValue.contains("'"))
                    hotelNameValue = getHotelNamesFSTravelList().get(i).substring(0, hotelNameValue.indexOf("'"));
                Statement statement = connection.createStatement();
                try {
                    String SQL = "select distinct id from hotels where name like '" + hotelNameValue + "%'" + hotelCategoryDBCondition;
                    if(!getCountryResortFSTravelList().get(i).equals(countryValue))
                        SQL += " and resort_id in (select id from resorts where name like '%" + getCountryResortFSTravelList().get(i) + "%')";
                    ResultSet resultSet = statement.executeQuery(SQL);

                    if (resultSet.next())
                        hotelIdFSTravelList.add(resultSet.getLong(1));
                    else
                        hotelIdFSTravelList.add(Long.valueOf(0));
                } finally {
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////

        for(int i=0; i<hotelIdFSTravelList.size(); i++){

            if(hotelIdFSTravelList.get(i) != Long.valueOf(0)){
                Connection connection = null;
                try {
                    connection = DriverManager
                            .getConnection(DB_INV_URL, DB_INV_USER, DB_INV_PASS);

                } catch (SQLException e) {
                    System.out.println("Connection Failed");
                    e.printStackTrace();
                    return;
                }
                Statement statement = connection.createStatement();
                try {
                    String SQL = "select gate_id from hotels_relations where out_system_id like '" + hotelIdFSTravelList.get(i) + "'  and source = 'samotui'";
                    ResultSet resultSet = statement.executeQuery(SQL);

                    if (resultSet.next()){
                        setHotelIdOfferList(String.valueOf(resultSet.getLong(1)));
                    }
                    else
                        setHotelIdOfferList("0");
                } finally {
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                }
            }
            else
                setHotelIdOfferList("0");
        }

        for(int i=0; i<getHotelIdOfferList().size(); i++){
            System.out.println("Hotel offer id: " + getHotelIdOfferList().get(i));
        }

    }

    public static void getPegastOfferHotelId(ArrayList<String> starsValue, String countryValue) throws SQLException{

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        String hotelCategoriesValue = "";
        for(int starsValueCounter = 0; starsValueCounter < starsValue.size(); starsValueCounter ++){
            hotelCategoriesValue += hotelCategoriesPegast.get(starsValue.get(starsValueCounter));
        }
        String hotelCategoryDBCondition = "";
        if(!hotelCategoriesValue.isEmpty()){
            hotelCategoriesValue = hotelCategoriesValue.substring(0, hotelCategoriesValue.lastIndexOf(", "));
            hotelCategoryDBCondition = " and category_id in (" + hotelCategoriesValue + ")";
        }

        if(!getHotelNamesPegastList().isEmpty()){
            for(int i=0; i<getHotelNamesPegastList().size(); i++){

                Connection connection = null;
                try {
                    connection = DriverManager
                            .getConnection(DB_PEGAST_URL, DB_PEGAST_USER, DB_PEGAST_PASS);

                } catch (SQLException e) {
                    System.out.println("Connection Failed");
                    e.printStackTrace();
                    return;
                }

                String hotelNameValue = getHotelNamesPegastList().get(i);
                if(hotelNameValue.contains("'"))
                    hotelNameValue = getHotelNamesPegastList().get(i).substring(0, hotelNameValue.indexOf("'"));
                Statement statement = connection.createStatement();
                try {
                    String SQL = "select distinct id from hotels where name like '" + hotelNameValue + "%'" + hotelCategoryDBCondition;
                    if(getCountryResortPegastList().get(i).equals(countryValue))
                        SQL += " and city_id in (select id from resorts where name like '%" + getCountryResortPegastList().get(i) + "%')";
                    ResultSet resultSet = statement.executeQuery(SQL);

                    if (resultSet.next())
                        hotelIdPegastList.add(resultSet.getLong(1));
                    else
                        hotelIdPegastList.add(Long.valueOf(0));
                } finally {
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                }
            }
        }

        ///////////////////////////////////////////////////////////////////////

        for(int i=0; i<hotelIdPegastList.size(); i++){

            if(hotelIdPegastList.get(i) != Long.valueOf(0)){
                Connection connection = null;
                try {
                    connection = DriverManager
                            .getConnection(DB_INV_URL, DB_INV_USER, DB_INV_PASS);

                } catch (SQLException e) {
                    System.out.println("Connection Failed");
                    e.printStackTrace();
                    return;
                }
                Statement statement = connection.createStatement();
                try {
                    String SQL = "select gate_id from hotels_relations where out_system_id like '" + hotelIdPegastList.get(i) + "'  and source = 'pegast'";
                    ResultSet resultSet = statement.executeQuery(SQL);

                    if (resultSet.next()){
                        setHotelIdOfferList(String.valueOf(resultSet.getLong(1)));
                    }
                    else
                        setHotelIdOfferList("0");
                } finally {
                    if (statement != null) {
                        statement.close();
                    }
                    if (connection != null) {
                        connection.close();
                    }
                }
            }
            else
                setHotelIdOfferList("0");
        }

        for(int i=0; i<getHotelIdOfferList().size(); i++){
            System.out.println("Hotel offer id: " + getHotelIdOfferList().get(i));
        }

    }

}
