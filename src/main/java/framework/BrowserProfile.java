package framework;

import com.codeborne.selenide.Condition;
import org.junit.Assert;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.Logs;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Set;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static framework.Constants.CONSTANT_20_SECONDS;
import static framework.WebDriverCommands.goToPage;

public class BrowserProfile {

    static final String LOGIN_URL = "https://online.sunmar.ru/login.aspx"; //
    static final String LOGIN_INPUT = "//input[contains(@id, 'UserName_I')]";
    static final String PWD_INPUT = "//input[contains(@id, 'UserPassword_I')]";
    static final String LOGIN_BUTTON = "//div[contains(@id, 'GirisBtn_CD')]";
    static final String LOGOUT_BUTTON = "//a[@href='/LogOff.aspx']";

    public static void main(String[] args) throws Exception {
        BrowserProfile bp = new BrowserProfile();
        //TODO replace with existing profile ID. Define the ID of the browser profile, where the code will be executed.
        String profileId = "a2e144d3-4b0c-4842-855f-05c7538086b3";

        //Define DesiredCapabilities
        DesiredCapabilities dc = new DesiredCapabilities();
        dc.setCapability("headless", true);

        //Instantiate the Remote Web Driver to connect to the browser profile launched by startProfile method
        RemoteWebDriver driver = new RemoteWebDriver(new URL(bp.startProfile(profileId)), dc);
        String s = driver.getCapabilities().getBrowserName();
        if(s.equals("chrome")){
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--headless");
            dc.setCapability(ChromeOptions.CAPABILITY, options);
        }

        //Perform automation
//        driver.get("https://multilogin.com/");
//        Assert.assertEquals("Multilogin - Replace Multiple Computers With Virtual Browser Profiles - Multilogin",driver.getTitle());

        //Sunmar
        driver.get("https://online.sunmar.ru/login.aspx?ReturnUrl=https%3a%2f%2fonline.sunmar.ru%3a443%2flogin.aspx");
        driver.findElementByXPath(LOGIN_INPUT).sendKeys("product@travelata.ru");
        driver.findElementByXPath(PWD_INPUT).sendKeys("RknNAA9q4");
        driver.findElementByXPath(LOGIN_BUTTON).click();

        driver.quit();
    }

    private String startProfile(String profileId) throws Exception {
        /*Send GET request to start the browser profile by profileId. Returns response in the following format:
        '{"status":"OK","value":"http://127.0.0.1:XXXXX"}', where XXXXX is the localhost port on which browser profile is
        launched. Please make sure that you have Multilogin listening port set to 35000. Otherwise please change the port
        value in the url string*/
        String url = "http://127.0.0.1:35000/api/v1/profile/start?automation=true&profileId=" + profileId;

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //Get JSON text from the response and return the value by key "value"
        JSONObject jsonResponse = new JSONObject(response.toString());
        return jsonResponse.getString("value");
    }
}