package framework;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.GeckoDriverService;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerOptions;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.*;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;


/**
 * Created by User on 26.08.2016.
 */
public class SeleniumTestCase extends WebDriverCommands {

    @BeforeClass
    public static void setUpClass() {

        WebDriverManager.chromedriver().setup(); //.version("97").setup();
//        WebDriverManager.firefoxdriver().setup();

    }



    @BeforeMethod
    public void setUp() throws Exception
    {
        ChromeOptions options = new ChromeOptions();
//        FirefoxOptions options = new FirefoxOptions();


//        for CI server
        options.addArguments("window-size=1920,1080");
        options.addArguments("--disable-notifications");
        options.addArguments("--no-sandbox");
        options.addArguments("--headless");
        options.addArguments("--ignore-certificate-errors");
        options.addArguments("--disable-gpu");

////        for local machine
//        options.addArguments("--start-maximized");
//        options.addArguments("--hide-scrollbars");
//        options.addArguments("--disable-popup-blocking");
//        options.addArguments("--disable-notifications");
//        options.addArguments("--disable-extensions");
//        options.setCapability("pageLoadStrategy", "normal");
//        options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);

        driver = new ChromeDriver(options);
//        driver = new FirefoxDriver(options);

        WebDriverRunner.setWebDriver(driver);
        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor)driver).executeScript("Object.defineProperty(navigator, 'webdriver', {get: () => undefined})");
        } else {
            throw new IllegalStateException("This driver does not support JavaScript!");
        }

    }

    @AfterMethod
    public void takeScreenShotOnFailure(ITestResult testResult) throws IOException
    {
        if (testResult.getStatus() == ITestResult.FAILURE)
        {
            Calendar c = Calendar.getInstance();
            Date dt = new Date();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd__HH-mm-ss");
            c.setTime(dt);

            File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File("screen.png"));
        }
    }

    @AfterClass
    public void tearDown()
    {
        System.out.println(errorMessage);
        System.out.println(driver.getCurrentUrl());
        getWebDriver().manage().deleteAllCookies();
        getWebDriver().quit();
    }

}