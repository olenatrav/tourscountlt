package framework;

import org.apache.poi.ss.usermodel.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;

import static pageobjects.Travelata.OfferEmailSearchPage.getHotelTourOfferAmountList;
import static pageobjects.operators.Alean.AleanMainPage.getHotelTourAleanAmountList;
//import static pageobjects.operators.Anex_new.AnexSearchPage.getHotelTourAnexAmountList;
//import static pageobjects.operators.BiblioGlobus.BGTourListPage.getHotelTourBGAmountList;
import static pageobjects.operators.TUI.TuiSearchPage.getHotelTourTuiAmountList;

public class PercentageCounter {

    private static PercentageCounter percentageCounter = null;
    public static PercentageCounter getPercentageCounter(){
        if(percentageCounter == null)
            percentageCounter = new PercentageCounter();

        return percentageCounter;
    }

    private static ArrayList<Double> percentageCounterList = new ArrayList<Double>();
    public static ArrayList <Double> getPercentageCounterList(){
        return percentageCounterList;
    }
    public static void addPercentageCounterList (Double percentageCounterValue){
        percentageCounterList.add(percentageCounterValue);
    }
    public static void setPercentageCounterList (int index, Double percentageCounterValue){
        percentageCounterList.set(index, percentageCounterValue);
    }

    private static ArrayList<Double> totalCounterList = new ArrayList<Double>();
    public static ArrayList <Double> getTotalCounterList(){
        return totalCounterList;
    }
    public static void addTotalCounterList (Double totalCounterValue){
        totalCounterList.add(totalCounterValue);
    }
    public static void setTotalCounterList (int index, Double totalCounterValue){
        totalCounterList.set(index, totalCounterValue);
    }

    public PercentageCounter countPercentage(Workbook book, String countryValue, String reportPathValue, String departureValue) throws Exception {

        FileInputStream fsIP = new FileInputStream(new File(reportPathValue));
//        Workbook book = new HSSFWorkbook(fsIP);

        CellStyle style = book.createCellStyle();
        Font font = book.createFont();
        font.setBold(true);
        style.setFont(font);

        Sheet sheet = null;

        if (book.getSheetIndex(departureValue + "-" + countryValue) == -1)
            sheet = book.createSheet(departureValue + "-" + countryValue);
        else
            sheet = book.getSheet(departureValue + "-" + countryValue);

        Row row = null;
        int rowCounter = 1;
        int cellNum = 0;

        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0){
            row = sheet.getRow(rowCounter);
            cellNum = sheet.getRow(rowCounter).getLastCellNum();
        }
        else
            row = sheet.createRow(rowCounter);

        Cell percentage = row.createCell(cellNum);
        percentage.setCellValue("% Trav/LT"); //OF
        percentage.setCellStyle(style);

        book.write(new FileOutputStream(reportPathValue));

        for (int i = 2; i <= sheet.getLastRowNum(); i++) {
                row = sheet.getRow(i);

                    if (row.getCell(cellNum - 2).getStringCellValue().equals("0")) {
                        if (row.getCell(cellNum - 1).getStringCellValue().equals("0") || row.getCell(cellNum - 1).getStringCellValue().equals(""))
                            addPercentageCounterList(-1.00);
                        else
                            addPercentageCounterList(100.00);
                    }
                    //If %Trav/LT > 200, add 100 to %Trav/LT
                    else{
                        Double d = Double.parseDouble(row.getCell(cellNum - 1).getStringCellValue()) / Double.parseDouble(row.getCell(cellNum - 2).getStringCellValue()) * 100;
                        if (d > 200.00)
                            addPercentageCounterList(100.00);
                        else
                            addPercentageCounterList(d);

                    }

                    if(row.getCell(cellNum - 2) != null){
                        percentage = row.createCell(cellNum);
                        if (getPercentageCounterList().get(getPercentageCounterList().size()-1) < 0)
                            percentage.setCellValue("-");
                        else
                            percentage.setCellValue(String.valueOf(Math.round(getPercentageCounterList().get(getPercentageCounterList().size()-1))));
                        book.write(new FileOutputStream(reportPathValue));
                }
        }

        //Overage percentage
//        row = sheet.getRow(0);
//        Cell averagePercentage = row.createCell(cellNum);
//        averagePercentage.setCellStyle(style);
//        averagePercentage.setCellValue(String.format("%.2f", getAveragePercentageValue()));
//        book.write(new FileOutputStream(reportPathValue));

//        row = sheet.getRow(1);
//        cellNum = row.getLastCellNum();
//        Cell total = row.createCell(cellNum);
//        total.setCellValue("Total");
//        total.setCellStyle(style);
//
//        for (int i = 2; i <= sheet.getLastRowNum(); i++){
//            row = sheet.getRow(i);
//            total = row.createCell(cellNum);
//
//            if(getPercentageCounterList().get(i-2) < 100.00 && getPercentageCounterList().get(i-2) >= 0){
//                double percentageValueSpot = getPercentageCounterList().get(i-2);
//                double averagePercentageValueSpot = getAveragePercentageValue();
//                setPercentageCounterList(i-2, 100.00);
//                total.setCellValue(String.format("%.2f", getAveragePercentageValue() - averagePercentageValueSpot));
//                setPercentageCounterList(i-2, percentageValueSpot);
//            }
//            else
//                total.setCellValue("-");
//
//            book.write(new FileOutputStream(reportPathValue));
//        }
//
//        row = sheet.getRow(0);
//        Cell idealAveragePercentage = row.createCell(cellNum);
//        idealAveragePercentage.setCellStyle(style);
//        idealAveragePercentage.setCellValue(String.format("%.2f", getIdealAveragePercentageValue()));
//
//        book.write(new FileOutputStream(reportPathValue));

//        book.close();

        return this;

    }

    public Double getAveragePercentageValue(){
        double averagePercentageValue = 0.0;
        int counter = 0;
        for (int i = 0; i<getPercentageCounterList().size(); i++){
            if (getPercentageCounterList().get(i) >= 0){
                averagePercentageValue += getPercentageCounterList().get(i);
                counter ++;
            }
        }
        return averagePercentageValue / counter;
    }

    public Double getIdealAveragePercentageValue(){
        double averagePercentageValue = 0.0;
        int counter = 0;
        for (int i = 0; i<getPercentageCounterList().size(); i++){
            if (getPercentageCounterList().get(i) >= 0){
                if (getPercentageCounterList().get(i) > 100)
                    averagePercentageValue += getPercentageCounterList().get(i);
                else
                    averagePercentageValue +=100.0;
                counter ++;
            }
        }
        return averagePercentageValue / counter;
    }

//    public static void getTotalBGValueList(){
//        for (int i=0; i<getHotelTourBGAmountList().size(); i++)
//            if(getHotelTourBGAmountList().get(i)!=0)
//            addTotalCounterList(Double.parseDouble(String.valueOf(getHotelTourOfferAmountList().get(i) / getHotelTourBGAmountList().get(i))));
//            else
//                addTotalCounterList(-1.00);
//    }

    public static void getTotalAleanValueList(){
        for (int i=0; i<getHotelTourAleanAmountList().size(); i++)
            if(getHotelTourAleanAmountList().get(i)!=0)
                addTotalCounterList(Double.parseDouble(String.valueOf(getHotelTourOfferAmountList().get(i) / getHotelTourAleanAmountList().get(i))));
            else
                addTotalCounterList(-1.00);
    }

//    public static void getTotalTuiValueList(){
//        for (int i=0; i<getHotelTourTuiAmountList().size(); i++)
//            if(getHotelTourTuiAmountList().get(i)!=0)
//                addTotalCounterList(Double.parseDouble(String.valueOf(getHotelTourOfferAmountList().get(i) / getHotelTourTuiAmountList().get(i))));
//            else
//                addTotalCounterList(-1.00);
//    }

//    public static void getTotalAnexValueList(){
//        for (int i=0; i<getHotelTourAnexAmountList().size(); i++)
//            if(getHotelTourAnexAmountList().get(i)!=0)
//                addTotalCounterList(Double.parseDouble(String.valueOf(getHotelTourOfferAmountList().get(i) / getHotelTourAnexAmountList().get(i))));
//            else
//                addTotalCounterList(-1.00);
//    }

    public static Double getAverageTotalValue(){
        double averageTotalSpot = 0.0;
        int counter = 0;
        for (int i=0; i < getTotalCounterList().size(); i++)
            if (getTotalCounterList().get(i) >= 0){
                averageTotalSpot += getTotalCounterList().get(i);
                counter ++;
            }
        return averageTotalSpot / counter;
    }

}
