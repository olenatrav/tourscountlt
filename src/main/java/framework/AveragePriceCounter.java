package framework;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import pageobjects.Travelata.TravelataSearchPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

import static pageobjects.LevelTravel.LevelTravelSearchPage.getHotelNamesSearchLTList;
import static pageobjects.LevelTravel.LevelTravelSearchPage.getHotelTourPricesSearchLTList;
import static pageobjects.LevelTravel.LevelTravelTourPage.getHotelTourPricesTourLTList;
import static framework.ReportGenerator.getFinalWithOilTaxSerpWithOilTaxLTRatioList;
import static framework.ReportGenerator.getFinalWithOilTaxSerpWithOilTaxTravRatioList;
import static pageobjects.Travelata.TravelataSearchPage.*;
import static pageobjects.Travelata.TravelataTourPage.getHotelTourPricesTourTravList;

public class AveragePriceCounter {

    private static AveragePriceCounter averagePriceCounter = null;
    public static AveragePriceCounter getAveragePriceCounter(){
        if(averagePriceCounter == null)
            averagePriceCounter = new AveragePriceCounter();

        return averagePriceCounter;
    }

    public AveragePriceCounter getAveragePrice (String countryValue, String reportPathValue) throws Exception {

        FileInputStream fsIP= new FileInputStream(new File(reportPathValue));
        Workbook book = new HSSFWorkbook(fsIP);
        Sheet sheet = book.getSheet(countryValue);
        CellStyle style = book.createCellStyle();
        Font font = book.createFont();
        font.setBold(true);
        style.setFont(font);

        int priceSumLT = 0;
        int priceSumTrav = 0;
        int priceSumWithoutOilTaxTrav = 0;
        for (int i=0; i<20; i++){
            priceSumLT += Integer.parseInt(getHotelTourPricesSearchLTList().get(i));
            priceSumTrav += Integer.parseInt(getHotelTourPricesSearchTravList().get(i));
            priceSumWithoutOilTaxTrav += Integer.parseInt(TravelataSearchPage.getHotelTourPriceWithoutOilTaxSearchTravList().get(i));
        }

        double averagePrice = (priceSumTrav/20.00) / (priceSumLT/20.00) * 100.00;
        double averagePriceWithoutOilTax = (priceSumWithoutOilTaxTrav/20.00) / (priceSumLT/20.00) * 100.00;

        int rowCounter = 42;
        int cellNum = sheet.getRow(rowCounter).getLastCellNum();
        if (cellNum > 0)
            cellNum -= 5;

        Row row = null;
        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
            row = sheet.getRow(rowCounter);
        else
            row = sheet.createRow(rowCounter);

        Cell total = row.createCell(cellNum);
        total.setCellStyle(style);
        total.setCellValue("Total");

        Cell emptyCell = row.createCell(cellNum + 1);
        emptyCell.setCellValue("");

        Cell avgPrice = row.createCell(cellNum + 2);
        avgPrice.setCellStyle(style);
        avgPrice.setCellValue(String.format("%.2f", averagePrice));

        emptyCell = row.createCell(cellNum + 3);
        emptyCell.setCellValue("");

        Cell avgPriceWithOilTax = row.createCell(cellNum + 4);
        avgPriceWithOilTax.setCellStyle(style);
        avgPriceWithOilTax.setCellValue(String.format("%.2f", averagePriceWithoutOilTax));

        rowCounter ++;

        while (rowCounter <= sheet.getLastRowNum()){
            sheet.removeRow(sheet.getRow(rowCounter));
            rowCounter ++;
        }


        book.write(new FileOutputStream(reportPathValue));
        book.close();

        return this;
    }

    public static double getTotalSerpPriceWithOilTax() throws Exception{
        int serpPriceWithOilTaxTrav = 0;
        int serpPriceWithOilTaxLT = 0;

        int travCounter = 0;
        while (travCounter < getHotelNamesSearchTravList().size()){
            serpPriceWithOilTaxTrav += Integer.parseInt(getHotelTourPricesSearchTravList().get(travCounter));
            travCounter ++;
        }

        int ltCounter = 0;
        while (ltCounter < getHotelNamesSearchLTList().size()){
            serpPriceWithOilTaxLT += Integer.parseInt(getHotelTourPricesSearchLTList().get(ltCounter));
            ltCounter ++;
        }

//        for (int i=0; i < hotelsCounter; i++){
//            serpPriceWithOilTaxTrav += Integer.parseInt(getHotelTourPricesSearchTravList().get(i));
//            serpPriceWithOilTaxLT += Integer.parseInt(LevelTravelSearchPage.getHotelTourPricesSearchLTList().get(i));
//
//        }

        double totalSerpPriceWithOilTax;
        if (serpPriceWithOilTaxLT==0)
            totalSerpPriceWithOilTax = 100.00;
         else
             totalSerpPriceWithOilTax = (serpPriceWithOilTaxTrav/(double)travCounter) / (serpPriceWithOilTaxLT/(double)ltCounter) * 100.00;

        return totalSerpPriceWithOilTax;
    }

    public static double getTotalSerpPriceWithoutOilTax() throws Exception{
        int serpPriceWithoutOilTaxTrav = 0;
        int serpPriceWithoutOilTaxLT = 0;

        int travCounter = 0;
        while (travCounter < getHotelNamesSearchTravList().size()){
            serpPriceWithoutOilTaxTrav += Integer.parseInt(getHotelTourPriceWithoutOilTaxSearchTravList().get(travCounter));
            travCounter ++;
        }

        int ltCounter = 0;
        while (ltCounter < getHotelNamesSearchLTList().size()){
            serpPriceWithoutOilTaxLT += Integer.parseInt(getHotelTourPricesSearchLTList().get(ltCounter));
            ltCounter ++;
        }

//        for(int i=0; i<hotelsCounter; i++){
//            serpPriceWithoutOilTaxTrav += Integer.parseInt(TravelataSearchPage.getHotelTourPriceWithoutOilTaxSearchTravList().get(i));
//            serpPriceWithoutOilTaxLT += Integer.parseInt(LevelTravelSearchPage.getHotelTourPricesSearchLTList().get(i));
//
//        }

        double totalSerpPriceWithoutOilTax;
        if (serpPriceWithoutOilTaxLT==0)
            totalSerpPriceWithoutOilTax = 100.00;
        else
            totalSerpPriceWithoutOilTax = (serpPriceWithoutOilTaxTrav/(double)travCounter) / (serpPriceWithoutOilTaxLT/(double)ltCounter) * 100.00;

        return totalSerpPriceWithoutOilTax;
    }

    public static double getTotalFinalPriceWithOilTax(int actualizedTourTravCounter, int actualizedTourLTCounter) throws Exception{
        int tourPriceWithOilTaxTrav = 0;
        int tourPriceWithOilTaxLT = 0;

        int travCounter = 0;
        while (travCounter < getHotelNamesSearchTravList().size()){
            if (!getHotelTourPricesTourTravList().get(travCounter).equals("-")){
                tourPriceWithOilTaxTrav += Integer.parseInt(getHotelTourPricesTourTravList().get(travCounter));
            }

            travCounter ++;
        }

        int ltCounter = 0;
        while (ltCounter < getHotelNamesSearchLTList().size()){
            if(!getHotelTourPricesTourLTList().get(ltCounter).equals("-")){
                tourPriceWithOilTaxLT += Integer.parseInt(getHotelTourPricesTourLTList().get(ltCounter));
            }

            ltCounter ++;
        }

//        for (int i=0; i<hotelsCounter; i++){
//            if (!getHotelTourPricesTourTravList().get(i).equals("-"))
//                tourPriceWithOilTaxTrav += Integer.parseInt(getHotelTourPricesTourTravList().get(i));
//            if(!getHotelTourPricesTourLTList().get(i).equals("-"))
//                tourPriceWithOilTaxLT += Integer.parseInt(getHotelTourPricesTourLTList().get(i));
//        }

        double totalFinalPriceWithOilTax;
        if (tourPriceWithOilTaxLT==0)
            totalFinalPriceWithOilTax = 100.00;
        else
            totalFinalPriceWithOilTax = (tourPriceWithOilTaxTrav/(double)actualizedTourTravCounter) / (tourPriceWithOilTaxLT/(double)actualizedTourLTCounter) * 100.00;

        return totalFinalPriceWithOilTax;
    }

    public static double getTotalFinalWithOilTaxSerpWithOilTaxLTRatio() throws Exception {
        double totalFinalWithOilTaxSerpWithOilTaxLTRatioValue = 0.00;
        int notNullCounter = 0;

        for (int counter = 0; counter < getFinalWithOilTaxSerpWithOilTaxLTRatioList().size(); counter ++){
            totalFinalWithOilTaxSerpWithOilTaxLTRatioValue += getFinalWithOilTaxSerpWithOilTaxLTRatioList().get(counter);
            if(!getFinalWithOilTaxSerpWithOilTaxLTRatioList().get(counter).equals(0.0))
                notNullCounter ++;
        }


        return totalFinalWithOilTaxSerpWithOilTaxLTRatioValue / notNullCounter;
    }

    public static double getTotalFinalWithOilTaxSerpWithOilTaxTravRatio() throws Exception {
        double totalFinalWithOilTaxSerpWithOilTaxTravRatioValue = 0;
        int notNullCounter = 0;

        for (int counter = 0; counter < getFinalWithOilTaxSerpWithOilTaxTravRatioList().size(); counter++){
            totalFinalWithOilTaxSerpWithOilTaxTravRatioValue += getFinalWithOilTaxSerpWithOilTaxTravRatioList().get(counter);
            if (!getFinalWithOilTaxSerpWithOilTaxTravRatioList().get(counter).equals(0.0))
                notNullCounter ++;
        }


        return totalFinalWithOilTaxSerpWithOilTaxTravRatioValue / notNullCounter;
    }

}
