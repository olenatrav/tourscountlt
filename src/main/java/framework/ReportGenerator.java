package framework;

import org.apache.poi.ss.usermodel.*;
import pageobjects.operators.Anex_new.AnexSearchPage;
import pageobjects.operators.BiblioGlobus.BGTourListPage;
import pageobjects.operators.FSTravel.FSTravelSearchPage;

import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static framework.AveragePriceCounter.*;
import static framework.AveragePriceCounter.getTotalFinalWithOilTaxSerpWithOilTaxTravRatio;
import static framework.JDBCPostgreSQLConnection.*;
import static framework.PercentageCounter.*;
import static framework.PercentageCounter.getTotalCounterList;
import static pageobjects.LevelTravel.LevelTravelSearchPage.*;
import static pageobjects.LevelTravel.LevelTravelTourPage.*;
import static pageobjects.Travelata.OfferEmailSearchPage.*;
import static pageobjects.Travelata.OfferEmailSearchPage.getCountryResortOfferList;
import static pageobjects.Travelata.TravelataSearchPage.*;
import static pageobjects.Travelata.TravelataTourPage.*;
import static pageobjects.operators.Alean.AleanMainPage.*;
import static pageobjects.operators.Alean.AleanTourPage.getHotelTourPriceActualizedAleanList;
import static pageobjects.operators.Anex_new.AnexSearchPage.*;
import static pageobjects.operators.Anex_new.AnexTourPage.getHotelTourPriceActualizedAnexList;
import static pageobjects.operators.BiblioGlobus.BGTourListPage.*;
import static pageobjects.operators.FSTravel.FSTravelHotelPage.getHotelTourPriceActualizedFSTravelList;
import static pageobjects.operators.FSTravel.FSTravelSearchPage.*;
import static pageobjects.operators.Pegast.PegastSearchPage.*;
import static pageobjects.operators.Pegast.PegastTourPage.getHotelTourPriceActualizedPegastList;
import static pageobjects.operators.TUI_new.TuiSearchPage.*;
import static pageobjects.operators.TUI_new.TuiTourPage.getHotelTourPriceActualizedTuiList;

public class ReportGenerator {

    private static ReportGenerator reportGenerator = null;
    public static ReportGenerator getReportGenerator(){
        if(reportGenerator == null)
            reportGenerator = new ReportGenerator();

        return reportGenerator;
    }

    private static ArrayList<Double> finalWithOilTaxSerpWithOilTaxLTRatioList = new ArrayList<Double>();
    public static ArrayList <Double> getFinalWithOilTaxSerpWithOilTaxLTRatioList(){
        return finalWithOilTaxSerpWithOilTaxLTRatioList;
    }
    public static void setFinalWithOilTaxSerpWithOilTaxLTRatioList (Double finalWithOilTaxSerpWithOilTaxLTRatioValue){
        finalWithOilTaxSerpWithOilTaxLTRatioList.add(finalWithOilTaxSerpWithOilTaxLTRatioValue);
    }

    private static ArrayList<Double> finalWithOilTaxSerpWithOilTaxTravRatioList = new ArrayList<Double>();
    public static ArrayList <Double> getFinalWithOilTaxSerpWithOilTaxTravRatioList(){
        return finalWithOilTaxSerpWithOilTaxTravRatioList;
    }
    public static void setFinalWithOilTaxSerpWithOilTaxTravRatioList (Double finalWithOilTaxSerpWithOilTaxTravRatioValue){
        finalWithOilTaxSerpWithOilTaxTravRatioList.add(finalWithOilTaxSerpWithOilTaxTravRatioValue);
    }

    private static ArrayList<Double> tourPriceActOfferRatioList = new ArrayList<Double>();
    public static ArrayList <Double> getTourPriceActOfferRatioList(){
        return tourPriceActOfferRatioList;
    }
    public static void setTourPriceActOfferRatioList (Double tourPriceActOfferRatioValue){
        tourPriceActOfferRatioList.add(tourPriceActOfferRatioValue);
    }

    private static String currentDateValue;
    public static String getCurrentDateValue() {
        currentDateValue = new SimpleDateFormat("dd.MM.yy").format(new Date());
        return currentDateValue;
    }

    private static int currentWeekNumberValue;
    public static int getCurrentWeekNumberValue() {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(new Date());
        currentWeekNumberValue = calendar.get(Calendar.WEEK_OF_YEAR);

        return currentWeekNumberValue;
    }

    private static String searchDate;
    public static String getSearchDate(int dateCounter){
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.DATE, dateCounter);
        searchDate = new SimpleDateFormat("dd.MM.yyyy").format(c.getTime());

        return searchDate;
    }

    public static String getSearchDateDashFormat(int dateCounter){
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.DATE, dateCounter);
        searchDate = new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());

        return searchDate;
    }

    public static String getSearchDateNoDots(int dateCounter){
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.DATE, dateCounter);
        searchDate = new SimpleDateFormat("ddMMyyyy").format(c.getTime());

        return searchDate;
    }

    public static String getSearchRussianDate(int dateCounter){
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.DATE, dateCounter);
        searchDate = DateFormat.getDateInstance(SimpleDateFormat.LONG, new Locale("ru")).format(c.getTime());


        return searchDate;
    }

    public ReportGenerator clearNotActualSheets(Workbook book, String reportName) throws Exception{
        for (int i=0; i < book.getNumberOfSheets(); i++){
            Sheet sheet = book.getSheetAt(i);
            String s= sheet.getSheetName();
            Row row = sheet.getRow(0);
            Cell cell = row.getCell(0);
            if(!cell.getStringCellValue().contains(getCurrentDateValue())){
                sheet.getRow(0).getCell(0).setCellValue("0");
                if(sheet.getRow(0).getLastCellNum()>0)
                    for(int cellCounter =1; cellCounter <= sheet.getRow(0).getLastCellNum(); cellCounter ++)
                        sheet.getRow(0).getCell(cellCounter).setCellValue("");
                for (int rowCounter=1; rowCounter <= sheet.getLastRowNum(); rowCounter++){
                    row = sheet.getRow(rowCounter);
                    sheet.removeRow(row);
                }
            }
        }

        book.write(new FileOutputStream(reportName));

        return this;
    }

    public ReportGenerator  generateReportLTTrav(Workbook book, String reportName, String countryValue, String departureValue, int dateCounterValue) throws Exception{

//        Workbook book;
//        File file = new File(reportName);
//        FileInputStream fileInputStream = new FileInputStream(file);
//        if (file.exists())
//            book = WorkbookFactory.create(fileInputStream);
//        else {
//            if (file.getName().endsWith(".xls"))
//                book = new HSSFWorkbook(fileInputStream);
//            else if (file.getName().endsWith(".xlsx"))
//                book = new XSSFWorkbook(fileInputStream);
//            else
//                throw new IllegalArgumentException("Unknown file extension");
//        }

//        while(book.getNumberOfSheets()>0)
//            book.removeSheetAt(0);

        Sheet sheet = null;

        if (book.getSheetIndex(departureValue + "-" + countryValue) == -1)
            sheet = book.createSheet(departureValue + "-" + countryValue);
        else
            sheet = book.getSheet(departureValue + "-" + countryValue);

        CellStyle style = book.createCellStyle();
        Font font = book.createFont();
        font.setBold(true);
        style.setFont(font);

        Row row = null;

        if(sheet.getLastRowNum() > 0){
            for (int i=0; i <= sheet.getLastRowNum(); i++){
                row = sheet.getRow(i);
                sheet.removeRow(row);
            }
        }

        book.write(new FileOutputStream(reportName));

        int rowCounter = 0;
        int cellNum = 0;

        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0){
            row = sheet.getRow(rowCounter);
            cellNum = sheet.getRow(1).getLastCellNum();
        }
        else
            row = sheet.createRow(rowCounter);

        // Delete old reports
//        int recordCounter = 5;
//        int columnCounter = 12;
//        if (cellNum == (recordCounter * columnCounter)){
//            for (int i=0; i<columnCounter; i++){
//                for (int j=0; j<=sheet.getLastRowNum(); j++){
//                    Cell c = sheet.getRow(j).getCell(0);
//                    if(!(c == null || c.getCellTypeEnum() == CellType.BLANK))
//                        sheet.getRow(j).removeCell(sheet.getRow(j).getCell(0));
//                }
//                deleteColumn(sheet, 0);
//                book.write(new FileOutputStream(reportName));
//            }
//        }

//        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0){
//            row = sheet.getRow(rowCounter);
//            cellNum = sheet.getRow(1).getLastCellNum();
//        }
//        else
//            row = sheet.createRow(rowCounter);

        Cell statisticDate = row.createCell(cellNum);
        statisticDate.setCellValue(getCurrentDateValue() +  " " + dateCounterValue + " " + getSearchDate(dateCounterValue));
        statisticDate.setCellStyle(style);

        rowCounter ++;

        if (rowCounter <= sheet.getLastRowNum()){
            row = sheet.getRow(rowCounter);
            cellNum = row.getLastCellNum();
        }
        else
            row = sheet.createRow(rowCounter);

        Cell weekNumber = row.createCell(cellNum);
        weekNumber.setCellValue("Неделя");
        weekNumber.setCellStyle(style);

        Cell dateValue = row.createCell(cellNum + 1);
        dateValue.setCellValue("Дата");
        dateValue.setCellStyle(style);

        Cell showcase = row.createCell(cellNum + 2);
        showcase.setCellValue("Витрина");
        showcase.setCellStyle(style);

        Cell hotelName = row.createCell(cellNum + 3);
        hotelName.setCellValue("Отель");
        hotelName.setCellStyle(style);

        Cell priceSerpWithOilTax = row.createCell(cellNum + 4);
        priceSerpWithOilTax.setCellValue("Серп с ТС");
        priceSerpWithOilTax.setCellStyle(style);

        Cell serpOilTax = row.createCell(cellNum + 5);
        serpOilTax.setCellValue("Серп ТС");
        serpOilTax.setCellStyle(style);

        Cell priceSerpWithoutOilTax = row.createCell(cellNum + 6);
        priceSerpWithoutOilTax.setCellValue("Серп без ТС");
        priceSerpWithoutOilTax.setCellStyle(style);

        Cell priceFinalWithOilTax = row.createCell(cellNum + 7);
        priceFinalWithOilTax.setCellValue("Финальная с ТС");
        priceFinalWithOilTax.setCellStyle(style);

        Cell finalOilTax = row.createCell(cellNum + 8);
        finalOilTax.setCellValue("Финальная ТС");
        finalOilTax.setCellStyle(style);

        Cell priceFinalWithoutOilTax = row.createCell(cellNum + 9);
        priceFinalWithoutOilTax.setCellValue("Финальная без ТС");
        priceFinalWithoutOilTax.setCellStyle(style);

        Cell finalWithOilTaxSerpWithOilTaxRatio = row.createCell(cellNum + 10);
        finalWithOilTaxSerpWithOilTaxRatio.setCellValue("Финальная с ТС / Серп с ТС");
        finalWithOilTaxSerpWithOilTaxRatio.setCellStyle(style);

        Cell operatorName = row.createCell(cellNum + 11);
        operatorName.setCellValue("ТО");
        operatorName.setCellStyle(style);

        rowCounter ++;

        //Level Travel

        int actualizedTourLTCounter = 0;

            for (int i = 0; i < getHotelNamesSearchLTList().size(); i++) {

                if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                    row = sheet.getRow(rowCounter);
                else
                    row = sheet.createRow(rowCounter);

                weekNumber = row.createCell(cellNum);
                weekNumber.setCellValue(getCurrentWeekNumberValue());

                dateValue = row.createCell(cellNum + 1);
                dateValue.setCellValue(getCurrentDateValue());

                showcase = row.createCell(cellNum + 2);
                showcase.setCellValue("LevelTravel");

                hotelName = row.createCell(cellNum + 3);
                hotelName.setCellValue(getHotelNamesSearchLTList().get(i));

                priceSerpWithOilTax = row.createCell(cellNum + 4);
                priceSerpWithOilTax.setCellValue(getHotelTourPricesSearchLTList().get(i));

                serpOilTax = row.createCell(cellNum + 5);
                serpOilTax.setCellValue("0");

                priceSerpWithoutOilTax = row.createCell(cellNum + 6);
                priceSerpWithoutOilTax.setCellValue(getHotelTourPricesSearchLTList().get(i));

                priceFinalWithOilTax = row.createCell(cellNum + 7);
                priceFinalWithOilTax.setCellValue(getHotelTourPricesTourLTList().get(i));
                if (!getHotelTourPricesTourLTList().get(i).equals("-"))
                    actualizedTourLTCounter++;

                finalOilTax = row.createCell(cellNum + 8);
                finalOilTax.setCellValue(getHotelTourPriceOilTaxTourLTList().get(i));

                priceFinalWithoutOilTax = row.createCell(cellNum + 9);
                priceFinalWithoutOilTax.setCellValue(getHotelTourPriceWithoutOilTaxTourLTList().get(i));

                finalWithOilTaxSerpWithOilTaxRatio = row.createCell(cellNum + 10);
                if(!getHotelTourPricesTourLTList().get(i).equals("-")){
                    setFinalWithOilTaxSerpWithOilTaxLTRatioList(Double.parseDouble(getHotelTourPricesTourLTList().get(i)) / Double.parseDouble(getHotelTourPricesSearchLTList().get(i)) * 100.00);
                    finalWithOilTaxSerpWithOilTaxRatio.setCellValue(String.format("%.2f", getFinalWithOilTaxSerpWithOilTaxLTRatioList().get(i)));
                }
                else{
                    setFinalWithOilTaxSerpWithOilTaxLTRatioList(0.0);
                    finalWithOilTaxSerpWithOilTaxRatio.setCellValue("-");
                }

                operatorName = row.createCell(cellNum + 11);
                operatorName.setCellValue(getToNameTourLTList().get(i));

                rowCounter++;

            }

        //Travelata

        int actualizedTourTravCounter = 0;
        for (int i=0; i < getHotelNamesSearchTravList().size() ;i++){

            if (rowCounter <= sheet.getLastRowNum()  && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            weekNumber = row.createCell(cellNum);
            weekNumber.setCellValue(getCurrentWeekNumberValue());

            dateValue = row.createCell(cellNum + 1);
            dateValue.setCellValue(getCurrentDateValue());

            showcase = row.createCell(cellNum + 2);
            showcase.setCellValue("Travelata");

            hotelName = row.createCell(cellNum + 3);
            hotelName.setCellValue(getHotelNamesSearchTravList().get(i));

            priceSerpWithOilTax = row.createCell(cellNum + 4);
            priceSerpWithOilTax.setCellValue(getHotelTourPricesSearchTravList().get(i));

            serpOilTax = row.createCell(cellNum + 5);
            serpOilTax.setCellValue(getHotelTourPriceOilTaxSearchTravList().get(i));

            priceSerpWithoutOilTax = row.createCell(cellNum + 6);
            priceSerpWithoutOilTax.setCellValue(getHotelTourPriceWithoutOilTaxSearchTravList().get(i));

            priceFinalWithOilTax = row.createCell(cellNum + 7);
            priceFinalWithOilTax.setCellValue(getHotelTourPricesTourTravList().get(i));
            if(!getHotelTourPricesTourTravList().get(i).equals("-"))
                actualizedTourTravCounter ++;

            finalOilTax = row.createCell(cellNum + 8);
            finalOilTax.setCellValue(getHotelTourPriceOilTaxTourTravList().get(i));

            priceFinalWithoutOilTax = row.createCell(cellNum + 9);
            priceFinalWithoutOilTax.setCellValue(getHotelTourPriceWithoutOilTaxTourTravList().get(i));

            finalWithOilTaxSerpWithOilTaxRatio = row.createCell(cellNum + 10);
            if(!getHotelTourPricesTourTravList().get(i).equals("-")){
                Double d = Double.parseDouble(getHotelTourPricesTourTravList().get(i)) / Double.parseDouble(getHotelTourPricesSearchTravList().get(i)) * 100.00;
                setFinalWithOilTaxSerpWithOilTaxTravRatioList(Double.parseDouble(getHotelTourPricesTourTravList().get(i)) / Double.parseDouble(getHotelTourPricesSearchTravList().get(i)) * 100.00);
                finalWithOilTaxSerpWithOilTaxRatio.setCellValue(String.format("%.2f", getFinalWithOilTaxSerpWithOilTaxTravRatioList().get(i)));
            }
            else{
                setFinalWithOilTaxSerpWithOilTaxTravRatioList(0.0);
                finalWithOilTaxSerpWithOilTaxRatio.setCellValue("-");
            }

            operatorName = row.createCell(cellNum + 11);
            operatorName.setCellValue(getToNameTourTravList().get(i));

            rowCounter ++;

        }

        //Statistic

        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
            row = sheet.getRow(rowCounter);
        else
            row = sheet.createRow(rowCounter);

        weekNumber = row.createCell(cellNum);
        weekNumber.setCellStyle(style);
        weekNumber.setCellValue(getCurrentWeekNumberValue());

        dateValue = row.createCell(cellNum + 1);
        dateValue.setCellStyle(style);
        dateValue.setCellValue(getCurrentDateValue());

        Cell total = row.createCell(cellNum + 2);
        total.setCellStyle(style);
        total.setCellValue("Total");

        Cell totalSerpPriceWithOilTax = row.createCell(cellNum + 4);
        totalSerpPriceWithOilTax.setCellStyle(style);
        totalSerpPriceWithOilTax.setCellValue(String.format("%.2f", getTotalSerpPriceWithOilTax()));

        Cell totalSerpPriceWithoutOilTax = row.createCell(cellNum + 6);
        totalSerpPriceWithoutOilTax.setCellStyle(style);
        totalSerpPriceWithoutOilTax.setCellValue(String.format("%.2f", getTotalSerpPriceWithoutOilTax()));

        Cell totalFinalPriceWithOilTax = row.createCell(cellNum + 7);
        totalFinalPriceWithOilTax.setCellStyle(style);
        totalFinalPriceWithOilTax.setCellValue(String.format("%.2f", getTotalFinalPriceWithOilTax(actualizedTourTravCounter, actualizedTourLTCounter)));

        Cell totalFinalWithOilTaxSerpWithOilTaxTravRatio = row.createCell(cellNum + 10);
        totalFinalWithOilTaxSerpWithOilTaxTravRatio.setCellStyle(style);
        totalFinalWithOilTaxSerpWithOilTaxTravRatio.setCellValue(String.format("%.2f", getTotalFinalWithOilTaxSerpWithOilTaxTravRatio()) +
                "; " + String.format("%.2f", getTotalFinalWithOilTaxSerpWithOilTaxLTRatio()));

        //Total
//        sheet = book.getSheet("Total");
//
//        for (int i = 1; i <= sheet.getLastRowNum(); i++){
//            if ((sheet.getRow(i).getCell(0).getNumericCellValue() == getCurrentWeekNumberValue()) &&
//                    sheet.getRow(i).getCell(2).toString().equals(departureValue) && sheet.getRow(i).getCell(3).toString().equals(countryValue))
//                rowCounter = i;
//        }
//
//        if (rowCounter < sheet.getLastRowNum() + 1)
//            row = sheet.getRow(rowCounter);
//        else
//            row = sheet.createRow(sheet.getLastRowNum() + 1);
//
//        cellNum = 0;
//        Cell statisticWeekNumber = row.createCell(cellNum);
//        statisticWeekNumber.setCellValue(getCurrentWeekNumberValue());
//
//        Cell statisticDateValue = row.createCell(cellNum + 1);
//        statisticDateValue.setCellValue(getCurrentDateValue());
//
//        Cell statisticFlightDateValue = row.createCell(cellNum + 2);
//        statisticFlightDateValue.setCellValue(getSearchDate(dateCounterValue));
//
//        Cell statisticDepartureCityValue = row.createCell(cellNum + 3);
//        statisticDepartureCityValue.setCellValue(departureValue);
//
//        Cell statisticCountryValue = row.createCell(cellNum + 4);
//        statisticCountryValue.setCellValue(countryValue);
//
//        Cell statisticTotalSerpPriceWithOilTax = row.createCell(cellNum + 5);
//        statisticTotalSerpPriceWithOilTax.setCellValue(String.format("%.2f", getTotalSerpPriceWithOilTax()));
//
//        Cell statisticTotalFinalPriceWithOilTax = row.createCell(cellNum + 6);
//        statisticTotalFinalPriceWithOilTax.setCellValue(String.format("%.2f", getTotalFinalPriceWithOilTax(actualizedTourTravCounter, actualizedTourLTCounter)));




//        book.write(new FileOutputStream(reportName));
//        book.close();
//        fileInputStream.close();

        return this;
    }

    public ReportGenerator  generateReportRegionPricesLTTrav(Workbook book, String reportName, String countryValue, String departureValue, int dateCounterValue) throws Exception{

        Sheet sheet = null;

        if (book.getSheetIndex(departureValue + "-" + countryValue) == -1)
            sheet = book.createSheet(departureValue + "-" + countryValue);
        else
            sheet = book.getSheet(departureValue + "-" + countryValue);

        CellStyle style = book.createCellStyle();
        Font font = book.createFont();
        font.setBold(true);
        style.setFont(font);

        Row row = null;

        if(sheet.getLastRowNum() > 0){
            for (int i=0; i <= sheet.getLastRowNum(); i++){
                row = sheet.getRow(i);
                sheet.removeRow(row);
            }
        }

        book.write(new FileOutputStream(reportName));

        int rowCounter = 0;
        int cellNum = 0;

        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0){
            row = sheet.getRow(rowCounter);
            cellNum = sheet.getRow(1).getLastCellNum();
        }
        else
            row = sheet.createRow(rowCounter);

        Cell statisticDate = row.createCell(cellNum);
        statisticDate.setCellValue(getCurrentDateValue() +  " " + dateCounterValue + " " + getSearchDate(dateCounterValue));
        statisticDate.setCellStyle(style);

        rowCounter ++;

        if (rowCounter <= sheet.getLastRowNum()){
            row = sheet.getRow(rowCounter);
            cellNum = row.getLastCellNum();
        }
        else
            row = sheet.createRow(rowCounter);

        Cell weekNumber = row.createCell(cellNum);
        weekNumber.setCellValue("Неделя");
        weekNumber.setCellStyle(style);

        Cell dateValue = row.createCell(cellNum + 1);
        dateValue.setCellValue("Дата");
        dateValue.setCellStyle(style);

        Cell showcase = row.createCell(cellNum + 2);
        showcase.setCellValue("Витрина");
        showcase.setCellStyle(style);

        Cell hotelName = row.createCell(cellNum + 3);
        hotelName.setCellValue("Отель");
        hotelName.setCellStyle(style);

        Cell priceSerpWithOilTax = row.createCell(cellNum + 4);
        priceSerpWithOilTax.setCellValue("Серп с ТС");
        priceSerpWithOilTax.setCellStyle(style);

//        Cell serpOilTax = row.createCell(cellNum + 5);
//        serpOilTax.setCellValue("Серп ТС");
//        serpOilTax.setCellStyle(style);
//
//        Cell priceSerpWithoutOilTax = row.createCell(cellNum + 6);
//        priceSerpWithoutOilTax.setCellValue("Серп без ТС");
//        priceSerpWithoutOilTax.setCellStyle(style);
//
//        Cell priceFinalWithOilTax = row.createCell(cellNum + 7);
//        priceFinalWithOilTax.setCellValue("Финальная с ТС");
//        priceFinalWithOilTax.setCellStyle(style);
//
//        Cell finalOilTax = row.createCell(cellNum + 8);
//        finalOilTax.setCellValue("Финальная ТС");
//        finalOilTax.setCellStyle(style);
//
//        Cell priceFinalWithoutOilTax = row.createCell(cellNum + 9);
//        priceFinalWithoutOilTax.setCellValue("Финальная без ТС");
//        priceFinalWithoutOilTax.setCellStyle(style);
//
//        Cell finalWithOilTaxSerpWithOilTaxRatio = row.createCell(cellNum + 10);
//        finalWithOilTaxSerpWithOilTaxRatio.setCellValue("Финальная с ТС / Серп с ТС");
//        finalWithOilTaxSerpWithOilTaxRatio.setCellStyle(style);
//
//        Cell operatorName = row.createCell(cellNum + 11);
//        operatorName.setCellValue("ТО");
//        operatorName.setCellStyle(style);

        rowCounter ++;

        //Level Travel

        int actualizedTourLTCounter = 0;

        for (int i = 0; i < getHotelNamesSearchLTList().size(); i++) {

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            weekNumber = row.createCell(cellNum);
            weekNumber.setCellValue(getCurrentWeekNumberValue());

            dateValue = row.createCell(cellNum + 1);
            dateValue.setCellValue(getCurrentDateValue());

            showcase = row.createCell(cellNum + 2);
            showcase.setCellValue("LevelTravel");

            hotelName = row.createCell(cellNum + 3);
            hotelName.setCellValue(getHotelNamesSearchLTList().get(i));

            priceSerpWithOilTax = row.createCell(cellNum + 4);
            priceSerpWithOilTax.setCellValue(getHotelTourPricesSearchLTList().get(i));

//            serpOilTax = row.createCell(cellNum + 5);
//            serpOilTax.setCellValue("0");
//
//            priceSerpWithoutOilTax = row.createCell(cellNum + 6);
//            priceSerpWithoutOilTax.setCellValue(getHotelTourPricesSearchLTList().get(i));
//
//            priceFinalWithOilTax = row.createCell(cellNum + 7);
//            priceFinalWithOilTax.setCellValue(getHotelTourPricesTourLTList().get(i));
//            if (!getHotelTourPricesTourLTList().get(i).equals("-"))
//                actualizedTourLTCounter++;
//
//            finalOilTax = row.createCell(cellNum + 8);
//            finalOilTax.setCellValue(getHotelTourPriceOilTaxTourLTList().get(i));
//
//            priceFinalWithoutOilTax = row.createCell(cellNum + 9);
//            priceFinalWithoutOilTax.setCellValue(getHotelTourPriceWithoutOilTaxTourLTList().get(i));
//
//            finalWithOilTaxSerpWithOilTaxRatio = row.createCell(cellNum + 10);
//            if(!getHotelTourPricesTourLTList().get(i).equals("-")){
//                setFinalWithOilTaxSerpWithOilTaxLTRatioList(Double.parseDouble(getHotelTourPricesTourLTList().get(i)) / Double.parseDouble(getHotelTourPricesSearchLTList().get(i)) * 100.00);
//                finalWithOilTaxSerpWithOilTaxRatio.setCellValue(String.format("%.2f", getFinalWithOilTaxSerpWithOilTaxLTRatioList().get(i)));
//            }
//            else{
//                setFinalWithOilTaxSerpWithOilTaxLTRatioList(0.0);
//                finalWithOilTaxSerpWithOilTaxRatio.setCellValue("-");
//            }
//
//            operatorName = row.createCell(cellNum + 11);
//            operatorName.setCellValue(getToNameTourLTList().get(i));

            rowCounter++;

        }

        //Travelata

//        int actualizedTourTravCounter = 0;
        for (int i=0; i < getHotelNamesSearchTravList().size() ;i++){

            if (rowCounter <= sheet.getLastRowNum()  && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            weekNumber = row.createCell(cellNum);
            weekNumber.setCellValue(getCurrentWeekNumberValue());

            dateValue = row.createCell(cellNum + 1);
            dateValue.setCellValue(getCurrentDateValue());

            showcase = row.createCell(cellNum + 2);
            showcase.setCellValue("Travelata");

            hotelName = row.createCell(cellNum + 3);
            hotelName.setCellValue(getHotelNamesSearchTravList().get(i));

            priceSerpWithOilTax = row.createCell(cellNum + 4);
            priceSerpWithOilTax.setCellValue(getHotelTourPricesSearchTravList().get(i));

//            serpOilTax = row.createCell(cellNum + 5);
//            serpOilTax.setCellValue(getHotelTourPriceOilTaxSearchTravList().get(i));
//
//            priceSerpWithoutOilTax = row.createCell(cellNum + 6);
//            priceSerpWithoutOilTax.setCellValue(getHotelTourPriceWithoutOilTaxSearchTravList().get(i));
//
//            priceFinalWithOilTax = row.createCell(cellNum + 7);
//            priceFinalWithOilTax.setCellValue(getHotelTourPricesTourTravList().get(i));
//            if(!getHotelTourPricesTourTravList().get(i).equals("-"))
//                actualizedTourTravCounter ++;
//
//            finalOilTax = row.createCell(cellNum + 8);
//            finalOilTax.setCellValue(getHotelTourPriceOilTaxTourTravList().get(i));
//
//            priceFinalWithoutOilTax = row.createCell(cellNum + 9);
//            priceFinalWithoutOilTax.setCellValue(getHotelTourPriceWithoutOilTaxTourTravList().get(i));
//
//            finalWithOilTaxSerpWithOilTaxRatio = row.createCell(cellNum + 10);
//            if(!getHotelTourPricesTourTravList().get(i).equals("-")){
//                Double d = Double.parseDouble(getHotelTourPricesTourTravList().get(i)) / Double.parseDouble(getHotelTourPricesSearchTravList().get(i)) * 100.00;
//                setFinalWithOilTaxSerpWithOilTaxTravRatioList(Double.parseDouble(getHotelTourPricesTourTravList().get(i)) / Double.parseDouble(getHotelTourPricesSearchTravList().get(i)) * 100.00);
//                finalWithOilTaxSerpWithOilTaxRatio.setCellValue(String.format("%.2f", getFinalWithOilTaxSerpWithOilTaxTravRatioList().get(i)));
//            }
//            else{
//                setFinalWithOilTaxSerpWithOilTaxTravRatioList(0.0);
//                finalWithOilTaxSerpWithOilTaxRatio.setCellValue("-");
//            }
//
//            operatorName = row.createCell(cellNum + 11);
//            operatorName.setCellValue(getToNameTourTravList().get(i));

            rowCounter ++;

        }

        //Statistic

//        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
//            row = sheet.getRow(rowCounter);
//        else
//            row = sheet.createRow(rowCounter);
//
//        weekNumber = row.createCell(cellNum);
//        weekNumber.setCellStyle(style);
//        weekNumber.setCellValue(getCurrentWeekNumberValue());
//
//        dateValue = row.createCell(cellNum + 1);
//        dateValue.setCellStyle(style);
//        dateValue.setCellValue(getCurrentDateValue());
//
//        Cell total = row.createCell(cellNum + 2);
//        total.setCellStyle(style);
//        total.setCellValue("Total");
//
//        Cell totalSerpPriceWithOilTax = row.createCell(cellNum + 4);
//        totalSerpPriceWithOilTax.setCellStyle(style);
//        totalSerpPriceWithOilTax.setCellValue(String.format("%.2f", getTotalSerpPriceWithOilTax()));
//
//        Cell totalSerpPriceWithoutOilTax = row.createCell(cellNum + 6);
//        totalSerpPriceWithoutOilTax.setCellStyle(style);
//        totalSerpPriceWithoutOilTax.setCellValue(String.format("%.2f", getTotalSerpPriceWithoutOilTax()));
//
//        Cell totalFinalPriceWithOilTax = row.createCell(cellNum + 7);
//        totalFinalPriceWithOilTax.setCellStyle(style);
//        totalFinalPriceWithOilTax.setCellValue(String.format("%.2f", getTotalFinalPriceWithOilTax(actualizedTourTravCounter, actualizedTourLTCounter)));
//
//        Cell totalFinalWithOilTaxSerpWithOilTaxTravRatio = row.createCell(cellNum + 10);
//        totalFinalWithOilTaxSerpWithOilTaxTravRatio.setCellStyle(style);
//        totalFinalWithOilTaxSerpWithOilTaxTravRatio.setCellValue(String.format("%.2f", getTotalFinalWithOilTaxSerpWithOilTaxTravRatio()) +
//                "; " + String.format("%.2f", getTotalFinalWithOilTaxSerpWithOilTaxLTRatio()));

        return this;
    }

    public ReportGenerator generateReportLTBayFilter(Workbook book, String reportName, String countryValue, String departureValue, int dateCounterValue) throws Exception{

        Sheet sheet = null;

        if (book.getSheetIndex(departureValue + "-" + countryValue + " " + dateCounterValue) == -1)
            sheet = book.createSheet(departureValue + "-" + countryValue + " " + dateCounterValue);
        else
            sheet = book.getSheet(departureValue + "-" + countryValue + " " + dateCounterValue);

        CellStyle style = book.createCellStyle();
        Font font = book.createFont();
        font.setBold(true);
        style.setFont(font);

        Row row = null;

        if(sheet.getLastRowNum() > 0){
            for (int i=0; i <= sheet.getLastRowNum(); i++){
                row = sheet.getRow(i);
                sheet.removeRow(row);
            }
        }

        book.write(new FileOutputStream(reportName));

        int rowCounter = 0;
        int cellNum = 0;

        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0){
            row = sheet.getRow(rowCounter);
            cellNum = sheet.getRow(1).getLastCellNum();
        }
        else
            row = sheet.createRow(rowCounter);

        Cell statisticDate = row.createCell(cellNum);
        statisticDate.setCellValue(getCurrentDateValue() +  " " + dateCounterValue + " " + getSearchDate(dateCounterValue));
        statisticDate.setCellStyle(style);

        rowCounter ++;

        if (rowCounter <= sheet.getLastRowNum()){
            row = sheet.getRow(rowCounter);
            cellNum = row.getLastCellNum();
        }
        else
            row = sheet.createRow(rowCounter);

        Cell weekNumber = row.createCell(cellNum);
        weekNumber.setCellValue("Неделя");
        weekNumber.setCellStyle(style);

        Cell dateValue = row.createCell(cellNum + 1);
        dateValue.setCellValue("Дата");
        dateValue.setCellStyle(style);

        Cell showcase = row.createCell(cellNum + 2);
        showcase.setCellValue("Витрина");
        showcase.setCellStyle(style);

        Cell hotelName = row.createCell(cellNum + 3);
        hotelName.setCellValue("Отель");
        hotelName.setCellStyle(style);

        Cell priceSerpWithOilTax = row.createCell(cellNum + 4);
        priceSerpWithOilTax.setCellValue("Серп с ТС");
        priceSerpWithOilTax.setCellStyle(style);

        Cell regionName = row.createCell(cellNum + 5);
        regionName.setCellValue("Курорт");
        regionName.setCellStyle(style);

        rowCounter ++;

        //Level Travel

        for (int i = 0; i < getHotelNamesSearchLTList().size(); i++) {

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            weekNumber = row.createCell(cellNum);
            weekNumber.setCellValue(getCurrentWeekNumberValue());

            dateValue = row.createCell(cellNum + 1);
            dateValue.setCellValue(getCurrentDateValue());

            showcase = row.createCell(cellNum + 2);
            showcase.setCellValue("LevelTravel");

            hotelName = row.createCell(cellNum + 3);
            hotelName.setCellValue(getHotelNamesSearchLTList().get(i));

            priceSerpWithOilTax = row.createCell(cellNum + 4);
            priceSerpWithOilTax.setCellValue(getHotelTourPricesSearchLTList().get(i));

            regionName = row.createCell(cellNum + 5);
            regionName.setCellValue(getRegionLTList().get(i));

            rowCounter++;

        }

        return this;
    }

    public ReportGenerator generateReportLTOffer(Workbook book, String countryValue, String departureValue, int dateCounter, String kidsAges) throws Exception{

        Sheet sheet = null;

        if (book.getSheetIndex(departureValue + "-" + countryValue) == -1)
            sheet = book.createSheet(departureValue + "-" + countryValue);
        else
            sheet = book.getSheet(departureValue + "-" + countryValue);

        Row row = null;
        int rowCounter = 0;
        int cellNum = 0;

        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0){
            row = sheet.getRow(rowCounter);
            cellNum = sheet.getRow(1).getLastCellNum();
        }
        else
            row = sheet.createRow(rowCounter);

        CellStyle style = book.createCellStyle();
        Font font = book.createFont();
        font.setBold(true);
        style.setFont(font);

        Cell weekNumber = row.createCell(cellNum);
        weekNumber.setCellValue("Неделя");
        weekNumber.setCellStyle(style);

        Cell dateValue = row.createCell(cellNum + 1);
        dateValue.setCellValue("Дата сборки");
        dateValue.setCellStyle(style);

        Cell flightDateValue = row.createCell(cellNum + 2);
        flightDateValue.setCellValue("Дата вылета");
        flightDateValue.setCellStyle(style);

        Cell showcase = row.createCell(cellNum + 3);
        showcase.setCellValue("Витрина");
        showcase.setCellStyle(style);

        Cell hotelName = row.createCell(cellNum + 4);
        hotelName.setCellValue("Отель");
        hotelName.setCellStyle(style);

        Cell regionName = row.createCell(cellNum + 5);
        regionName.setCellValue("Регион");
        regionName.setCellStyle(style);

        Cell kidsAge = row.createCell(cellNum + 6);
        kidsAge.setCellValue("Возраста детей");
        kidsAge.setCellStyle(style);

        Cell roomName = row.createCell(cellNum + 7);
        roomName.setCellValue("Тип номера");
        roomName.setCellStyle(style);

        Cell mealName = row.createCell(cellNum + 8);
        mealName.setCellValue("Питание");
        mealName.setCellStyle(style);

        Cell priceSerpWithOilTax = row.createCell(cellNum + 9);
        priceSerpWithOilTax.setCellValue("Серп с ТС");
        priceSerpWithOilTax.setCellStyle(style);

        Cell priceFinalWithOilTax = row.createCell(cellNum + 10);
        priceFinalWithOilTax.setCellValue("Финальная с ТС");
        priceFinalWithOilTax.setCellStyle(style);

        Cell finalWithOilTaxSerpWithOilTaxRatio = row.createCell(cellNum + 11);
        finalWithOilTaxSerpWithOilTaxRatio.setCellValue("Фин / Серп %");
        finalWithOilTaxSerpWithOilTaxRatio.setCellStyle(style);

        Cell operatorName = row.createCell(cellNum + 12);
        operatorName.setCellValue("ТО");
        operatorName.setCellStyle(style);

        rowCounter ++;

        //Level Travel

        int actualizedTourLTCounter = 0;

        for (int i = 0; i < getHotelNamesSearchLTList().size(); i++) {

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            weekNumber = row.createCell(cellNum);
            weekNumber.setCellValue(getCurrentWeekNumberValue());

            dateValue = row.createCell(cellNum + 1);
            dateValue.setCellValue(getCurrentDateValue());

            flightDateValue = row.createCell(cellNum + 2);
            flightDateValue.setCellValue(getSearchDate(dateCounter));

            showcase = row.createCell(cellNum + 3);
            showcase.setCellValue("LevelTravel");

            hotelName = row.createCell(cellNum + 4);
            hotelName.setCellValue(getHotelNamesSearchLTList().get(i));

            regionName = row.createCell(cellNum + 5);
            regionName.setCellValue(getRegionLTList().get(i));

            kidsAge = row.createCell(cellNum + 6);
            kidsAge.setCellValue(kidsAges);

            roomName = row.createCell(cellNum + 7);
            roomName.setCellValue(getRoomNameTourLTList().get(i));

            mealName = row.createCell(cellNum + 8);
            mealName.setCellValue(getMealNameTourLTList().get(i));

            priceSerpWithOilTax = row.createCell(cellNum + 9);
            priceSerpWithOilTax.setCellValue(getHotelTourPricesSearchLTList().get(i));

            priceFinalWithOilTax = row.createCell(cellNum + 10);
            priceFinalWithOilTax.setCellValue(getHotelTourPricesTourLTList().get(i));
            if (!getHotelTourPricesTourLTList().get(i).equals("-"))
                actualizedTourLTCounter++;

            finalWithOilTaxSerpWithOilTaxRatio = row.createCell(cellNum + 11);
            if(!getHotelTourPricesTourLTList().get(i).equals("-")){
                setFinalWithOilTaxSerpWithOilTaxLTRatioList(Double.parseDouble(getHotelTourPricesTourLTList().get(i)) / Double.parseDouble(getHotelTourPricesSearchLTList().get(i)) * 100.00);
                finalWithOilTaxSerpWithOilTaxRatio.setCellValue(String.format("%.2f", getFinalWithOilTaxSerpWithOilTaxLTRatioList().get(i)));
            }
            else{
                setFinalWithOilTaxSerpWithOilTaxLTRatioList(0.0);
                finalWithOilTaxSerpWithOilTaxRatio.setCellValue("-");
            }

            operatorName = row.createCell(cellNum + 12);
            operatorName.setCellValue(getToNameTourLTList().get(i));

            rowCounter++;

        }

        //Offer

        for (int i=0; i < getHotelNamesOfferList().size() ;i++){

            if (rowCounter <= sheet.getLastRowNum()  && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            weekNumber = row.createCell(cellNum);
            weekNumber.setCellValue(getCurrentWeekNumberValue());

            dateValue = row.createCell(cellNum + 1);
            dateValue.setCellValue(getCurrentDateValue());

            flightDateValue = row.createCell(cellNum + 2);
            flightDateValue.setCellValue(getSearchDate(dateCounter));

            showcase = row.createCell(cellNum + 3);
            showcase.setCellValue("Travelata");

            hotelName = row.createCell(cellNum + 4);
            hotelName.setCellValue(getHotelNamesOfferList().get(i));

            regionName = row.createCell(cellNum + 5);
            regionName.setCellValue(getRegionOfferList().get(i));

            kidsAge = row.createCell(cellNum + 6);
            kidsAge.setCellValue(kidsAges);

            roomName = row.createCell(cellNum + 7);
            roomName.setCellValue(getHotelNameRoomOfferList().get(i));

            mealName = row.createCell(cellNum + 8);
            mealName.setCellValue(getHotelNameMealOfferList().get(i));

            priceSerpWithOilTax = row.createCell(cellNum + 9);
            priceSerpWithOilTax.setCellValue(getHotelTourPriceOfferList().get(i));

            priceFinalWithOilTax = row.createCell(cellNum + 10);
            priceFinalWithOilTax.setCellValue(getHotelTourPriceActualizedOfferList().get(i));
            finalWithOilTaxSerpWithOilTaxRatio = row.createCell(cellNum + 11);
            if(!getHotelTourPriceActualizedOfferList().get(i).equals("-")){
                setFinalWithOilTaxSerpWithOilTaxTravRatioList(Double.parseDouble(getHotelTourPriceActualizedOfferList().get(i)) / Double.parseDouble(getHotelTourPriceOfferList().get(i)) * 100.00);
                finalWithOilTaxSerpWithOilTaxRatio.setCellValue(String.format("%.2f", getFinalWithOilTaxSerpWithOilTaxTravRatioList().get(i)));
            }
            else{
                setFinalWithOilTaxSerpWithOilTaxTravRatioList(0.0);
                finalWithOilTaxSerpWithOilTaxRatio.setCellValue("-");
            }

            operatorName = row.createCell(cellNum + 12);
            operatorName.setCellValue(getToNameOfferList().get(i));

            rowCounter ++;

        }

        return this;
    }

    public static void deleteColumn( Sheet sheet, int columnToDelete ){
        int maxColumn = 0;
        for ( int r=0; r < sheet.getLastRowNum()+1; r++ ){
            Row row = sheet.getRow( r );

            // if no row exists here; then nothing to do; next!
            if ( row == null )
                continue;

            // if the row doesn't have this many columns then we are good; next!
            int lastColumn = row.getLastCellNum();
            if ( lastColumn > maxColumn )
                maxColumn = lastColumn;

            if ( lastColumn < columnToDelete )
                continue;

            for ( int x=columnToDelete+1; x < lastColumn + 1; x++ ){
                Cell oldCell    = row.getCell(x-1);
                if ( oldCell != null )
                    row.removeCell( oldCell );

                Cell nextCell   = row.getCell( x );
                if ( nextCell != null ){
                    Cell newCell    = row.createCell( x-1, nextCell.getCellType() );
                    cloneCell(newCell, nextCell);
                }
            }
        }


        // Adjust the column widths
        for ( int c=0; c < maxColumn; c++ ){
            sheet.setColumnWidth( c, sheet.getColumnWidth(c+1) );
        }
    }

    private static void cloneCell( Cell cNew, Cell cOld ){
        cNew.setCellComment( cOld.getCellComment() );
        cNew.setCellStyle( cOld.getCellStyle() );
        cNew.setCellValue( cOld.getStringCellValue() );

    }

    public ReportGenerator generateReportBGOffer(Workbook book, String reportName, String countryValue, String departureValue, int dateCounterValue, int nightsCounter) throws Exception{

        Sheet sheet = null;

        if (book.getSheetIndex(departureValue + "-" + countryValue) == -1)
            sheet = book.createSheet(departureValue + "-" + countryValue);
        else
            sheet = book.getSheet(departureValue + "-" + countryValue);

        Row row = null;
        int rowCounter = 0;
        int cellNum = 0;

        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0){
            row = sheet.getRow(rowCounter);
            cellNum = sheet.getRow(1).getLastCellNum();
        }
        else
            row = sheet.createRow(rowCounter);

        CellStyle style = book.createCellStyle();
        Font font = book.createFont();
        font.setBold(true);
        style.setFont(font);

        Cell weekNumber = row.createCell(cellNum);
        weekNumber.setCellValue("Неделя");
        weekNumber.setCellStyle(style);

        Cell dateValue = row.createCell(cellNum + 1);
        dateValue.setCellValue("Дата");
        dateValue.setCellStyle(style);

        Cell flightDateValue = row.createCell(cellNum + 2);
        flightDateValue.setCellValue("Дата вылета");
        flightDateValue.setCellStyle(style);

        Cell nightsCounterValue = row.createCell(cellNum + 3);
        nightsCounterValue.setCellValue("Ночи");
        nightsCounterValue.setCellStyle(style);

        Cell countryResortBGValue = row.createCell(cellNum + 4);
        countryResortBGValue.setCellValue("Страна/Курорт BG");
        countryResortBGValue.setCellStyle(style);

        Cell hotelIdBGValue = row.createCell(cellNum + 5);
        hotelIdBGValue.setCellValue("Id BG");
        hotelIdBGValue.setCellStyle(style);

        Cell hotelNameBGValue = row.createCell(cellNum + 6);
        hotelNameBGValue.setCellValue("Отель BG");
        hotelNameBGValue.setCellStyle(style);

        Cell hotelStarsBGValue = row.createCell(cellNum + 7);
        hotelStarsBGValue.setCellValue("Категория отеля BG");
        hotelStarsBGValue.setCellStyle(style);

        Cell tourPriceBGValue = row.createCell(cellNum + 8);
        tourPriceBGValue.setCellValue("Цена BG");
        tourPriceBGValue.setCellStyle(style);

        Cell isBestPriceListBGValue = row.createCell(cellNum + 9);
        isBestPriceListBGValue.setCellValue("Лучшая цена BG");
        isBestPriceListBGValue.setCellStyle(style);

        Cell countryResortOfferValue = row.createCell(cellNum + 10);
        countryResortOfferValue.setCellValue("Страна/Курорт Offer");
        countryResortOfferValue.setCellStyle(style);

        Cell hotelIdOfferValue = row.createCell(cellNum + 11);
        hotelIdOfferValue.setCellValue("Id Offer");
        hotelIdOfferValue.setCellStyle(style);

        Cell hotelNameOfferValue = row.createCell(cellNum + 12);
        hotelNameOfferValue.setCellValue("Отель Offer");
        hotelNameOfferValue.setCellStyle(style);

        Cell hotelStarsOfferValue = row.createCell(cellNum + 13);
        hotelStarsOfferValue.setCellValue("Категория отеля Offer");
        hotelStarsOfferValue.setCellStyle(style);

        Cell tourPriceOfferValue = row.createCell(cellNum + 14);
        tourPriceOfferValue.setCellValue("Цена Offer");
        tourPriceOfferValue.setCellStyle(style);

        Cell tourPriceActOfferValue = row.createCell(cellNum + 15);
        tourPriceActOfferValue.setCellValue("Цена Offer Act");
        tourPriceActOfferValue.setCellStyle(style);

//        Cell tourPriceActOfferRatioValue = row.createCell(cellNum + 10);
//        tourPriceActOfferRatioValue.setCellValue("Offer Act/Offer");
//        tourPriceActOfferRatioValue.setCellStyle(style);
//
//        Cell resortValue = row.createCell(cellNum + 11);
//        resortValue.setCellValue("Resort");
//        resortValue.setCellStyle(style);
//
//        Cell totalValue = row.createCell(cellNum + 12);
//        totalValue.setCellValue("Total");
//        totalValue.setCellStyle(style);

        rowCounter ++;

        //BG Part
        for (int i = 0; i < getHotelNamesBGList().size(); i++) {

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            weekNumber = row.createCell(cellNum);
            weekNumber.setCellValue(getCurrentWeekNumberValue());

            dateValue = row.createCell(cellNum + 1);
            dateValue.setCellValue(getCurrentDateValue());

            flightDateValue = row.createCell(cellNum + 2);
            flightDateValue.setCellValue(getSearchDate(dateCounterValue));

            nightsCounterValue = row.createCell(cellNum + 3);
            nightsCounterValue.setCellValue(nightsCounter);

            countryResortBGValue = row.createCell(cellNum + 4);
            countryResortBGValue.setCellValue(getCountryResortBGList().get(i));

            hotelIdBGValue = row.createCell(cellNum + 5);
            hotelIdBGValue.setCellValue(getHotelIdBGList().get(i));

            hotelNameBGValue = row.createCell(cellNum + 6);
            hotelNameBGValue.setCellValue(getHotelNamesBGList().get(i));

            hotelStarsBGValue = row.createCell(cellNum + 7);
            hotelStarsBGValue.setCellValue(getHotelStarsBGList().get(i));

            tourPriceBGValue = row.createCell(cellNum + 8);
            tourPriceBGValue.setCellValue(getHotelTourPriceBGList().get(i));

            isBestPriceListBGValue = row.createCell(cellNum + 9);
            isBestPriceListBGValue.setCellValue(getIsBestPriceBGList().get(i));

            rowCounter++;

        }

        book.write(new FileOutputStream(reportName));

        //Offer Part
        rowCounter = 1;

        for (int i = 0; i < getHotelNamesOfferList().size(); i++) {

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            countryResortOfferValue = row.createCell(cellNum + 10);
            countryResortOfferValue.setCellValue(getRegionOfferList().get(i)); // getCountryResortOfferList()

            hotelIdOfferValue = row.createCell(cellNum + 11);
            hotelIdOfferValue.setCellValue(getHotelIdOfferList().get(i));

            hotelNameOfferValue = row.createCell(cellNum + 12);
            hotelNameOfferValue.setCellValue(getHotelNamesOfferList().get(i));

            hotelStarsOfferValue = row.createCell(cellNum + 13);
            hotelStarsOfferValue.setCellValue(getHotelStarsOfferList().get(i));

            tourPriceOfferValue = row.createCell(cellNum + 14);
            tourPriceOfferValue.setCellValue(getHotelTourPriceOfferList().get(i));

            tourPriceActOfferValue = row.createCell(cellNum + 15);
            tourPriceActOfferValue.setCellValue(getHotelTourPriceActualizedOfferList().get(i));

//            tourPriceActOfferRatioValue = row.createCell(cellNum + 10);
//            if(!getHotelTourPriceActualizedOfferList().get(i).equals("-") && !getHotelTourPriceOfferList().get(i).equals("-")){
//                setTourPriceActOfferRatioList(Double.parseDouble(getHotelTourPriceActualizedOfferList().get(i)) / Double.parseDouble(getHotelTourPriceOfferList().get(i)) * 100.00);
//                tourPriceActOfferRatioValue.setCellValue(String.format("%.2f", getTourPriceActOfferRatioList().get(i)));
//            }
//            else{
//                setTourPriceActOfferRatioList(0.0);
//                tourPriceActOfferRatioValue.setCellValue("-");
//            }
//
            rowCounter++;

        }

        book.write(new FileOutputStream(reportName));

        //Total part

//        getTotalBGValueList();
//
//        rowCounter = 1;
//
//        for (int i = 0; i < resortsBG.length; i++) {
//
//            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
//                row = sheet.getRow(rowCounter);
//            else
//                row = sheet.createRow(rowCounter);
//
//            resortValue = row.createCell(cellNum + 11);
//            resortValue.setCellValue(resortsBGOffer.get(resortsBG[i]));
//
//            totalValue = row.createCell(cellNum + 12);
//            if (getTotalCounterList().get(i) < 0)
//                totalValue.setCellValue("-");
//            else
//                totalValue.setCellValue(String.format("%.2f", getTotalCounterList().get(i)));
//
//            rowCounter ++;
//
//        }
//
//        book.write(new FileOutputStream(reportName));
//
//        rowCounter ++;
//
//        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
//            row = sheet.getRow(rowCounter);
//        else
//            row = sheet.createRow(rowCounter);
//
//        totalValue = row.createCell(cellNum + 12);
//        totalValue.setCellValue(String.format("%.2f", getAverageTotalValue()));
//
//        book.write(new FileOutputStream(reportName));

        return this;
    }

    public ReportGenerator generateReportAnexOffer(Workbook book, String reportName, String countryValue, String departureValue, int dateCounterValue, int nightsCounterValue) throws Exception{

        Sheet sheet = null;

        if (book.getSheetIndex(departureValue + "-" + countryValue) == -1)
            sheet = book.createSheet(departureValue + "-" + countryValue);
        else
            sheet = book.getSheet(departureValue + "-" + countryValue);

        Row row = null;
        int rowCounter = 0;
        int cellNum = 0;

        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0){
            row = sheet.getRow(rowCounter);
            cellNum = sheet.getRow(1).getLastCellNum();
        }
        else
            row = sheet.createRow(rowCounter);

        CellStyle style = book.createCellStyle();
        Font font = book.createFont();
        font.setBold(true);
        style.setFont(font);

        Cell weekNumber = row.createCell(cellNum);
        weekNumber.setCellValue("Неделя");
        weekNumber.setCellStyle(style);

        Cell dateValue = row.createCell(cellNum + 1);
        dateValue.setCellValue("Дата");
        dateValue.setCellStyle(style);

        Cell flightDateValue = row.createCell(cellNum + 2);
        flightDateValue.setCellValue("Дата вылета");
        flightDateValue.setCellStyle(style);

        Cell nightsValue = row.createCell(cellNum + 3);
        nightsValue.setCellValue("Ночи");
        nightsValue.setCellStyle(style);

        Cell countryResortAnexValue = row.createCell(cellNum + 4);
        countryResortAnexValue.setCellValue("Страна/Курорт Anex");
        countryResortAnexValue.setCellStyle(style);

        Cell hotelIdAnexValue = row.createCell(cellNum + 5);
        hotelIdAnexValue.setCellValue("Id Anex");
        hotelIdAnexValue.setCellStyle(style);

        Cell hotelNameAnexValue = row.createCell(cellNum + 6);
        hotelNameAnexValue.setCellValue("Отель Anex");
        hotelNameAnexValue.setCellStyle(style);

        Cell hotelStarsAnexValue = row.createCell(cellNum +7);
        hotelStarsAnexValue.setCellValue("Категория отеля Anex");
        hotelStarsAnexValue.setCellStyle(style);

        Cell tourPriceAnexValue = row.createCell(cellNum + 8);
        tourPriceAnexValue.setCellValue("Цена Anex");
        tourPriceAnexValue.setCellStyle(style);

        Cell tourPriceActAnexValue = row.createCell(cellNum + 9);
        tourPriceActAnexValue.setCellValue("Цена Anex Act");
        tourPriceActAnexValue.setCellStyle(style);

        Cell countryResortOfferValue = row.createCell(cellNum + 10);
        countryResortOfferValue.setCellValue("Страна/Курорт Offer");
        countryResortOfferValue.setCellStyle(style);

        Cell hotelIdOfferValue = row.createCell(cellNum + 11);
        hotelIdOfferValue.setCellValue("Id Offer");
        hotelIdOfferValue.setCellStyle(style);

        Cell hotelNameOfferValue = row.createCell(cellNum + 12);
        hotelNameOfferValue.setCellValue("Отель Offer");
        hotelNameOfferValue.setCellStyle(style);

        Cell hotelStarsOfferValue = row.createCell(cellNum + 13);
        hotelStarsOfferValue.setCellValue("Категория отеля Offer");
        hotelStarsOfferValue.setCellStyle(style);

        Cell tourPriceOfferValue = row.createCell(cellNum + 14);
        tourPriceOfferValue.setCellValue("Цена Offer");
        tourPriceOfferValue.setCellStyle(style);

        Cell tourPriceActOfferValue = row.createCell(cellNum + 15);
        tourPriceActOfferValue.setCellValue("Цена Offer Act");
        tourPriceActOfferValue.setCellStyle(style);

        rowCounter ++;

        //Anex Part
        for (int i = 0; i < getHotelNamesAnexList().size(); i++) {

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            weekNumber = row.createCell(cellNum);
            weekNumber.setCellValue(getCurrentWeekNumberValue());

            dateValue = row.createCell(cellNum + 1);
            dateValue.setCellValue(getCurrentDateValue());

            flightDateValue = row.createCell(cellNum + 2);
            flightDateValue.setCellValue(getSearchDate(dateCounterValue));

            nightsValue = row.createCell(cellNum + 3);
            nightsValue.setCellValue(nightsCounterValue);

            countryResortAnexValue = row.createCell(cellNum + 4);
            countryResortAnexValue.setCellValue(getCountryResortAnexList().get(i));

            hotelIdAnexValue = row.createCell(cellNum + 5);
            hotelIdAnexValue.setCellValue(getHotelIdAnexList().get(i));

            hotelNameAnexValue = row.createCell(cellNum + 6);
            hotelNameAnexValue.setCellValue(getHotelNamesAnexList().get(i));

            hotelStarsAnexValue = row.createCell(cellNum + 7);
            hotelStarsAnexValue.setCellValue(getHotelStarsAnexList().get(i));

            tourPriceAnexValue = row.createCell(cellNum + 8);
            tourPriceAnexValue.setCellValue(getHotelTourPriceAnexList().get(i));

            tourPriceActAnexValue = row.createCell(cellNum + 9);
            tourPriceActAnexValue.setCellValue(getHotelTourPriceActualizedAnexList().get(i));

            rowCounter++;

        }

        book.write(new FileOutputStream(reportName));

        //Offer Part
        rowCounter = 1;

        for (int i = 0; i < getHotelNamesOfferList().size(); i++) {

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            countryResortOfferValue = row.createCell(cellNum + 10);
            countryResortOfferValue.setCellValue(getRegionOfferList().get(i)); // getCountryResortOfferList()

            hotelIdOfferValue = row.createCell(cellNum + 11);
            hotelIdOfferValue.setCellValue(getHotelIdOfferList().get(i));

            hotelNameOfferValue = row.createCell(cellNum + 12);
            hotelNameOfferValue.setCellValue(getHotelNamesOfferList().get(i));

            hotelStarsOfferValue = row.createCell(cellNum + 13);
            hotelStarsOfferValue.setCellValue(getHotelStarsOfferList().get(i));

            tourPriceOfferValue = row.createCell(cellNum + 14);
            tourPriceOfferValue.setCellValue(getHotelTourPriceOfferList().get(i));

            tourPriceActOfferValue = row.createCell(cellNum + 15);
            tourPriceActOfferValue.setCellValue(getHotelTourPriceActualizedOfferList().get(i));

            rowCounter++;

        }

        book.write(new FileOutputStream(reportName));

        return this;
    }

    public ReportGenerator generateReportFSTravelOffer(Workbook book, String reportName, String countryValue, String departureValue, int dateCounterValue, int nightsCounterValue) throws Exception{

        Sheet sheet = null;

        if (book.getSheetIndex(departureValue + "-" + countryValue) == -1)
            sheet = book.createSheet(departureValue + "-" + countryValue);
        else
            sheet = book.getSheet(departureValue + "-" + countryValue);

        Row row = null;
        int rowCounter = 0;
        int cellNum = 0;

        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0){
            row = sheet.getRow(rowCounter);
            cellNum = sheet.getRow(1).getLastCellNum();
        }
        else
            row = sheet.createRow(rowCounter);

        CellStyle style = book.createCellStyle();
        Font font = book.createFont();
        font.setBold(true);
        style.setFont(font);

        Cell weekNumber = row.createCell(cellNum);
        weekNumber.setCellValue("Неделя");
        weekNumber.setCellStyle(style);

        Cell dateValue = row.createCell(cellNum + 1);
        dateValue.setCellValue("Дата");
        dateValue.setCellStyle(style);

        Cell flightDateValue = row.createCell(cellNum + 2);
        flightDateValue.setCellValue("Дата вылета");
        flightDateValue.setCellStyle(style);

        Cell nightsValue = row.createCell(cellNum + 3);
        nightsValue.setCellValue("Ночи");
        nightsValue.setCellStyle(style);

        Cell countryResortFSTravelValue = row.createCell(cellNum + 4);
        countryResortFSTravelValue.setCellValue("Страна/Курорт FSTravel");
        countryResortFSTravelValue.setCellStyle(style);

        Cell hotelIdFSTravelValue = row.createCell(cellNum + 5);
        hotelIdFSTravelValue.setCellValue("Id FSTravel");
        hotelIdFSTravelValue.setCellStyle(style);

        Cell hotelNameFSTravelValue = row.createCell(cellNum + 6);
        hotelNameFSTravelValue.setCellValue("Отель FSTravel");
        hotelNameFSTravelValue.setCellStyle(style);

        Cell hotelStarsFSTravelValue = row.createCell(cellNum +7);
        hotelStarsFSTravelValue.setCellValue("Категория отеля FSTravel");
        hotelStarsFSTravelValue.setCellStyle(style);

        Cell tourPriceFSTravelValue = row.createCell(cellNum + 8);
        tourPriceFSTravelValue.setCellValue("Цена FSTravel");
        tourPriceFSTravelValue.setCellStyle(style);

        Cell tourPriceActFSTravelValue = row.createCell(cellNum + 9);
        tourPriceActFSTravelValue.setCellValue("Цена FSTravel Act");
        tourPriceActFSTravelValue.setCellStyle(style);

        Cell countryResortOfferValue = row.createCell(cellNum + 10);
        countryResortOfferValue.setCellValue("Страна/Курорт Offer");
        countryResortOfferValue.setCellStyle(style);

        Cell hotelIdOfferValue = row.createCell(cellNum + 11);
        hotelIdOfferValue.setCellValue("Id Offer");
        hotelIdOfferValue.setCellStyle(style);

        Cell hotelNameOfferValue = row.createCell(cellNum + 12);
        hotelNameOfferValue.setCellValue("Отель Offer");
        hotelNameOfferValue.setCellStyle(style);

        Cell hotelStarsOfferValue = row.createCell(cellNum + 13);
        hotelStarsOfferValue.setCellValue("Категория отеля Offer");
        hotelStarsOfferValue.setCellStyle(style);

        Cell tourPriceOfferValue = row.createCell(cellNum + 14);
        tourPriceOfferValue.setCellValue("Цена Offer");
        tourPriceOfferValue.setCellStyle(style);

        Cell tourPriceActOfferValue = row.createCell(cellNum + 15);
        tourPriceActOfferValue.setCellValue("Цена Offer Act");
        tourPriceActOfferValue.setCellStyle(style);

        rowCounter ++;

        //FSTravel Part
        for (int i = 0; i < getHotelNamesFSTravelList().size(); i++) {

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            weekNumber = row.createCell(cellNum);
            weekNumber.setCellValue(getCurrentWeekNumberValue());

            dateValue = row.createCell(cellNum + 1);
            dateValue.setCellValue(getCurrentDateValue());

            flightDateValue = row.createCell(cellNum + 2);
            flightDateValue.setCellValue(getSearchDate(dateCounterValue));

            nightsValue = row.createCell(cellNum + 3);
            nightsValue.setCellValue(nightsCounterValue);

            countryResortFSTravelValue = row.createCell(cellNum + 4);
            countryResortFSTravelValue.setCellValue(getCountryResortFSTravelList().get(i));

            hotelIdFSTravelValue = row.createCell(cellNum + 5);
            hotelIdFSTravelValue.setCellValue(getHotelIdFSTravelList().get(i));

            hotelNameFSTravelValue = row.createCell(cellNum + 6);
            hotelNameFSTravelValue.setCellValue(getHotelNamesFSTravelList().get(i));

            hotelStarsFSTravelValue = row.createCell(cellNum + 7);
            hotelStarsFSTravelValue.setCellValue(getHotelStarsFSTravelList().get(i));

            tourPriceFSTravelValue = row.createCell(cellNum + 8);
            tourPriceFSTravelValue.setCellValue(getHotelTourPriceFSTravelList().get(i));

            tourPriceActFSTravelValue = row.createCell(cellNum + 9);
            tourPriceActFSTravelValue.setCellValue(getHotelTourPriceActualizedFSTravelList().get(i));

            rowCounter++;

        }

        book.write(new FileOutputStream(reportName));

        //Offer Part
        rowCounter = 1;

        for (int i = 0; i < getHotelNamesOfferList().size(); i++) {

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            countryResortOfferValue = row.createCell(cellNum + 10);
            countryResortOfferValue.setCellValue(getRegionOfferList().get(i)); // getCountryResortOfferList()

            hotelIdOfferValue = row.createCell(cellNum + 11);
            hotelIdOfferValue.setCellValue(getHotelIdOfferList().get(i));

            hotelNameOfferValue = row.createCell(cellNum + 12);
            hotelNameOfferValue.setCellValue(getHotelNamesOfferList().get(i));

            hotelStarsOfferValue = row.createCell(cellNum + 13);
            hotelStarsOfferValue.setCellValue(getHotelStarsOfferList().get(i));

            tourPriceOfferValue = row.createCell(cellNum + 14);
            tourPriceOfferValue.setCellValue(getHotelTourPriceOfferList().get(i));

            tourPriceActOfferValue = row.createCell(cellNum + 15);
            tourPriceActOfferValue.setCellValue(getHotelTourPriceActualizedOfferList().get(i));

            rowCounter++;

        }

        book.write(new FileOutputStream(reportName));

        return this;
    }

    public ReportGenerator generateRegionAnexOfferHotelAmount(Workbook book, String reportName, String countryValue) throws Exception{

        Sheet sheet = null;

        if (book.getSheetIndex(countryValue) == -1)
            sheet = book.createSheet(countryValue);
        else
            sheet = book.getSheet(countryValue);

        Row row = null;
        int rowCounter = 0;
        int cellNum = 0;

        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0){
            row = sheet.getRow(rowCounter);
            cellNum = sheet.getRow(1).getLastCellNum();
        }
        else
            row = sheet.createRow(rowCounter);

        CellStyle style = book.createCellStyle();
        Font font = book.createFont();
        font.setBold(true);
        style.setFont(font);

        Cell weekNumber = row.createCell(cellNum);
        weekNumber.setCellValue("Неделя");
        weekNumber.setCellStyle(style);

        Cell dateValue = row.createCell(cellNum + 1);
        dateValue.setCellValue("Дата");
        dateValue.setCellStyle(style);

        Cell flightDateValue = row.createCell(cellNum + 2);
        flightDateValue.setCellValue("Дата вылета");
        flightDateValue.setCellStyle(style);

        Cell nightsValue = row.createCell(cellNum + 3);
        nightsValue.setCellValue("Ночи");
        nightsValue.setCellStyle(style);

        Cell departureValue = row.createCell(cellNum + 4);
        departureValue.setCellValue("Город вылета");
        departureValue.setCellStyle(style);

        Cell hotelsAmountAnex = row.createCell(cellNum + 5);
        hotelsAmountAnex.setCellValue("Кол-во отелей Anex");
        hotelsAmountAnex.setCellStyle(style);

        Cell hotelsAmountOffer = row.createCell(cellNum + 6);
        hotelsAmountOffer.setCellValue("Кол-во отелей Offer");
        hotelsAmountOffer.setCellStyle(style);

        rowCounter ++;

        for (int i = 0; i < getHotelsAmountAnexList().size(); i++) {

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            weekNumber = row.createCell(cellNum);
            weekNumber.setCellValue(getCurrentWeekNumberValue());

            dateValue = row.createCell(cellNum + 1);
            dateValue.setCellValue(getCurrentDateValue());

            flightDateValue = row.createCell(cellNum + 2);
            flightDateValue.setCellValue(getSearchDate(AnexSearchPage.getActualDateCounterList().get(i)));

            nightsValue = row.createCell(cellNum + 3);
            nightsValue.setCellValue(AnexSearchPage.getActualNightsCounterList().get(i));

            departureValue = row.createCell(cellNum + 4);
            departureValue.setCellValue(getDepartureCityAnexList().get(i));

            hotelsAmountAnex = row.createCell(cellNum + 5);
            hotelsAmountAnex.setCellValue(getHotelsAmountAnexList().get(i));

            hotelsAmountOffer = row.createCell(cellNum + 6);
            hotelsAmountOffer.setCellValue(getHotelsAmountOfferList().get(i));

            rowCounter++;

        }

        book.write(new FileOutputStream(reportName));

        return this;
    }

    public ReportGenerator generateRegionBGOfferHotelAmount(Workbook book, String reportName, String countryToValue) throws Exception{

        Sheet sheet = null;

        if (book.getSheetIndex(countryToValue) == -1)
            sheet = book.createSheet(countryToValue);
        else
            sheet = book.getSheet(countryToValue);

        CellStyle style = book.createCellStyle();
        Font font = book.createFont();
        font.setBold(true);
        style.setFont(font);

        Row row = null;

        if(sheet.getLastRowNum() > 0){
            for (int i=0; i <= sheet.getLastRowNum(); i++){
                row = sheet.getRow(i);
                sheet.removeRow(row);
            }
        }

        book.write(new FileOutputStream(reportName));

        int rowCounter = 0;
        int cellNum = 0;

        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0){
            row = sheet.getRow(rowCounter);
            cellNum = sheet.getRow(1).getLastCellNum();
        }
        else
            row = sheet.createRow(rowCounter);

//        Sheet sheet = null;
//
//        if (book.getSheetIndex(countryValue) == -1)
//            sheet = book.createSheet(countryValue);
//        else
//            sheet = book.getSheet(countryValue);
//
//        Row row = null;
//        int rowCounter = 0;
//        int cellNum = 0;
//
//        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0){
//            row = sheet.getRow(rowCounter);
//            cellNum = sheet.getRow(1).getLastCellNum();
//        }
//        else
//            row = sheet.createRow(rowCounter);

//        CellStyle style = book.createCellStyle();
//        Font font = book.createFont();
//        font.setBold(true);
//        style.setFont(font);

        Cell weekNumber = row.createCell(cellNum);
        weekNumber.setCellValue("Неделя");
        weekNumber.setCellStyle(style);

        Cell dateValue = row.createCell(cellNum + 1);
        dateValue.setCellValue("Дата");
        dateValue.setCellStyle(style);

        Cell flightDateValue = row.createCell(cellNum + 2);
        flightDateValue.setCellValue("Дата вылета");
        flightDateValue.setCellStyle(style);

        Cell nightsValue = row.createCell(cellNum + 3);
        nightsValue.setCellValue("Ночи");
        nightsValue.setCellStyle(style);

        Cell departureValue = row.createCell(cellNum + 4);
        departureValue.setCellValue("Город вылета");
        departureValue.setCellStyle(style);

        Cell hotelsAmountBG = row.createCell(cellNum + 5);
        hotelsAmountBG.setCellValue("Кол-во отелей BG");
        hotelsAmountBG.setCellStyle(style);

        Cell hotelsAmountOffer = row.createCell(cellNum + 6);
        hotelsAmountOffer.setCellValue("Кол-во отелей Offer");
        hotelsAmountOffer.setCellStyle(style);

        rowCounter ++;

        for (int i = 0; i < getHotelsAmountBGList().size(); i++) {

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            weekNumber = row.createCell(cellNum);
            weekNumber.setCellValue(getCurrentWeekNumberValue());

            dateValue = row.createCell(cellNum + 1);
            dateValue.setCellValue(getCurrentDateValue());

            flightDateValue = row.createCell(cellNum + 2);
            flightDateValue.setCellValue(getSearchDate(BGTourListPage.getActualDateCounterList().get(i)));

            nightsValue = row.createCell(cellNum + 3);
            nightsValue.setCellValue(BGTourListPage.getActualNightsCounterList().get(i));

            departureValue = row.createCell(cellNum + 4);
            departureValue.setCellValue(getDepartureCityBGList().get(i));

            hotelsAmountBG = row.createCell(cellNum + 5);
            hotelsAmountBG.setCellValue(getHotelsAmountBGList().get(i));

            hotelsAmountOffer = row.createCell(cellNum + 6);
            hotelsAmountOffer.setCellValue(getHotelsAmountOfferList().get(i));

            rowCounter++;

        }

        book.write(new FileOutputStream(reportName));

        return this;
    }

    public ReportGenerator generateRegionFSTravelOfferHotelAmount(Workbook book, String reportName, String countryToValue) throws Exception{

        Sheet sheet = null;

        if (book.getSheetIndex(countryToValue) == -1)
            sheet = book.createSheet(countryToValue);
        else
            sheet = book.getSheet(countryToValue);

        CellStyle style = book.createCellStyle();
        Font font = book.createFont();
        font.setBold(true);
        style.setFont(font);

        Row row = null;

        if(sheet.getLastRowNum() > 0){
            for (int i=0; i <= sheet.getLastRowNum(); i++){
                row = sheet.getRow(i);
                sheet.removeRow(row);
            }
        }

        book.write(new FileOutputStream(reportName));

        int rowCounter = 0;
        int cellNum = 0;

        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0){
            row = sheet.getRow(rowCounter);
            cellNum = sheet.getRow(1).getLastCellNum();
        }
        else
            row = sheet.createRow(rowCounter);

//        Sheet sheet = null;
//
//        if (book.getSheetIndex(countryValue) == -1)
//            sheet = book.createSheet(countryValue);
//        else
//            sheet = book.getSheet(countryValue);
//
//        Row row = null;
//        int rowCounter = 0;
//        int cellNum = 0;
//
//        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0){
//            row = sheet.getRow(rowCounter);
//            cellNum = sheet.getRow(1).getLastCellNum();
//        }
//        else
//            row = sheet.createRow(rowCounter);

//        CellStyle style = book.createCellStyle();
//        Font font = book.createFont();
//        font.setBold(true);
//        style.setFont(font);

        Cell weekNumber = row.createCell(cellNum);
        weekNumber.setCellValue("Неделя");
        weekNumber.setCellStyle(style);

        Cell dateValue = row.createCell(cellNum + 1);
        dateValue.setCellValue("Дата");
        dateValue.setCellStyle(style);

        Cell flightDateValue = row.createCell(cellNum + 2);
        flightDateValue.setCellValue("Дата вылета");
        flightDateValue.setCellStyle(style);

        Cell nightsValue = row.createCell(cellNum + 3);
        nightsValue.setCellValue("Ночи");
        nightsValue.setCellStyle(style);

        Cell departureValue = row.createCell(cellNum + 4);
        departureValue.setCellValue("Город вылета");
        departureValue.setCellStyle(style);

        Cell hotelsAmountFSTravel = row.createCell(cellNum + 5);
        hotelsAmountFSTravel.setCellValue("Кол-во отелей FSTravel");
        hotelsAmountFSTravel.setCellStyle(style);

        Cell hotelsAmountOffer = row.createCell(cellNum + 6);
        hotelsAmountOffer.setCellValue("Кол-во отелей Offer");
        hotelsAmountOffer.setCellStyle(style);

        rowCounter ++;

        for (int i = 0; i < getHotelsAmountFSTravelList().size(); i++) {

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            weekNumber = row.createCell(cellNum);
            weekNumber.setCellValue(getCurrentWeekNumberValue());

            dateValue = row.createCell(cellNum + 1);
            dateValue.setCellValue(getCurrentDateValue());

            flightDateValue = row.createCell(cellNum + 2);
            flightDateValue.setCellValue(getSearchDate(FSTravelSearchPage.getActualDateCounterList().get(i)));

            nightsValue = row.createCell(cellNum + 3);
            nightsValue.setCellValue(FSTravelSearchPage.getActualNightsCounterList().get(i));

            departureValue = row.createCell(cellNum + 4);
            departureValue.setCellValue(getDepartureCityFSTravelList().get(i));

            hotelsAmountFSTravel = row.createCell(cellNum + 5);
            hotelsAmountFSTravel.setCellValue(getHotelsAmountFSTravelList().get(i));

            hotelsAmountOffer = row.createCell(cellNum + 6);
            hotelsAmountOffer.setCellValue(getHotelsAmountOfferList().get(i));

            rowCounter++;

        }

        book.write(new FileOutputStream(reportName));

        return this;
    }

    public ReportGenerator generateReportTuiOffer(Workbook book, String reportName, String countryValue, String departureValue, int dateCounterValue) throws Exception{

        Sheet sheet = null;

        if (book.getSheetIndex(departureValue + "-" + countryValue) == -1)
            sheet = book.createSheet(departureValue + "-" + countryValue);
        else
            sheet = book.getSheet(departureValue + "-" + countryValue);

        Row row = null;
        int rowCounter = 0;
        int cellNum = 0;

        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0){
            row = sheet.getRow(rowCounter);
            cellNum = sheet.getRow(1).getLastCellNum();
        }
        else
            row = sheet.createRow(rowCounter);

        CellStyle style = book.createCellStyle();
        Font font = book.createFont();
        font.setBold(true);
        style.setFont(font);

        Cell weekNumber = row.createCell(cellNum);
        weekNumber.setCellValue("Неделя");
        weekNumber.setCellStyle(style);

        Cell dateValue = row.createCell(cellNum + 1);
        dateValue.setCellValue("Дата");
        dateValue.setCellStyle(style);

        Cell flightDateValue = row.createCell(cellNum + 2);
        flightDateValue.setCellValue("Дата вылета");
        flightDateValue.setCellStyle(style);

        Cell countryResortTuiValue = row.createCell(cellNum + 3);
        countryResortTuiValue.setCellValue("Страна/Курорт Tui");
        countryResortTuiValue.setCellStyle(style);

        Cell hotelIdTuiValue = row.createCell(cellNum + 4);
        hotelIdTuiValue.setCellValue("Id Tui");
        hotelIdTuiValue.setCellStyle(style);

        Cell hotelNameTuiValue = row.createCell(cellNum + 5);
        hotelNameTuiValue.setCellValue("Отель Tui");
        hotelNameTuiValue.setCellStyle(style);

        Cell hotelStarsTuiValue = row.createCell(cellNum + 6);
        hotelStarsTuiValue.setCellValue("Категория отеля Tui");
        hotelStarsTuiValue.setCellStyle(style);

        Cell tourPriceTuiValue = row.createCell(cellNum + 7);
        tourPriceTuiValue.setCellValue("Цена Tui");
        tourPriceTuiValue.setCellStyle(style);

        Cell tourPriceActTuiValue = row.createCell(cellNum + 8);
        tourPriceActTuiValue.setCellValue("Цена Tui Act");
        tourPriceActTuiValue.setCellStyle(style);

        Cell countryResortOfferValue = row.createCell(cellNum + 9);
        countryResortOfferValue.setCellValue("Страна/Курорт Offer");
        countryResortOfferValue.setCellStyle(style);

        Cell hotelIdOfferValue = row.createCell(cellNum + 10);
        hotelIdOfferValue.setCellValue("Id Offer");
        hotelIdOfferValue.setCellStyle(style);

        Cell hotelNameOfferValue = row.createCell(cellNum + 11);
        hotelNameOfferValue.setCellValue("Отель Offer");
        hotelNameOfferValue.setCellStyle(style);

        Cell hotelStarsOfferValue = row.createCell(cellNum + 12);
        hotelStarsOfferValue.setCellValue("Категория отеля Offer");
        hotelStarsOfferValue.setCellStyle(style);

        Cell tourPriceOfferValue = row.createCell(cellNum + 13);
        tourPriceOfferValue.setCellValue("Цена Offer");
        tourPriceOfferValue.setCellStyle(style);

        Cell tourPriceActOfferValue = row.createCell(cellNum + 14);
        tourPriceActOfferValue.setCellValue("Цена Offer Act");
        tourPriceActOfferValue.setCellStyle(style);

        rowCounter ++;

        //Tui Part
        for (int i = 0; i < getHotelNamesTuiList().size(); i++) {

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            weekNumber = row.createCell(cellNum);
            weekNumber.setCellValue(getCurrentWeekNumberValue());

            dateValue = row.createCell(cellNum + 1);
            dateValue.setCellValue(getCurrentDateValue());

            flightDateValue = row.createCell(cellNum + 2);
            flightDateValue.setCellValue(getSearchDate(dateCounterValue));

            countryResortTuiValue = row.createCell(cellNum + 3);
            countryResortTuiValue.setCellValue(getCountryResortTuiList().get(i));

            hotelIdTuiValue = row.createCell(cellNum + 4);
            hotelIdTuiValue.setCellValue(getHotelIdTuiList().get(i));

            hotelNameTuiValue = row.createCell(cellNum + 5);
            hotelNameTuiValue.setCellValue(getHotelNamesTuiList().get(i));

            hotelStarsTuiValue = row.createCell(cellNum + 6);
            hotelStarsTuiValue.setCellValue(getHotelStarsTuiList().get(i));

            tourPriceTuiValue = row.createCell(cellNum + 7);
            tourPriceTuiValue.setCellValue(getHotelTourPriceTuiList().get(i));

            tourPriceActTuiValue = row.createCell(cellNum + 8);
            tourPriceActTuiValue.setCellValue(getHotelTourPriceActualizedTuiList().get(i));

            rowCounter++;

        }

        book.write(new FileOutputStream(reportName));

        //Offer Part
        rowCounter = 1;

        for (int i = 0; i < getHotelNamesOfferList().size(); i++) {

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            countryResortOfferValue = row.createCell(cellNum + 9);
            countryResortOfferValue.setCellValue(getRegionOfferList().get(i)); // getCountryResortOfferList()

            hotelIdOfferValue = row.createCell(cellNum + 10);
            hotelIdOfferValue.setCellValue(getHotelIdOfferList().get(i));

            hotelNameOfferValue = row.createCell(cellNum + 11);
            hotelNameOfferValue.setCellValue(getHotelNamesOfferList().get(i));

            hotelStarsOfferValue = row.createCell(cellNum + 12);
            hotelStarsOfferValue.setCellValue(getHotelStarsOfferList().get(i));

            tourPriceOfferValue = row.createCell(cellNum + 13);
            tourPriceOfferValue.setCellValue(getHotelTourPriceOfferList().get(i));

            tourPriceActOfferValue = row.createCell(cellNum + 14);
            tourPriceActOfferValue.setCellValue(getHotelTourPriceActualizedOfferList().get(i));

            rowCounter++;

        }

        book.write(new FileOutputStream(reportName));

        return this;
    }

    public ReportGenerator generateReportFSTravelOffer(Workbook book, String reportName, String countryValue, String departureValue, int dateCounterValue) throws Exception{

        Sheet sheet = null;

        if (book.getSheetIndex(departureValue + "-" + countryValue) == -1)
            sheet = book.createSheet(departureValue + "-" + countryValue);
        else
            sheet = book.getSheet(departureValue + "-" + countryValue);

        Row row = null;
        int rowCounter = 0;
        int cellNum = 0;

        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0){
            row = sheet.getRow(rowCounter);
            cellNum = sheet.getRow(1).getLastCellNum();
        }
        else
            row = sheet.createRow(rowCounter);

        CellStyle style = book.createCellStyle();
        Font font = book.createFont();
        font.setBold(true);
        style.setFont(font);

        Cell weekNumber = row.createCell(cellNum);
        weekNumber.setCellValue("Неделя");
        weekNumber.setCellStyle(style);

        Cell dateValue = row.createCell(cellNum + 1);
        dateValue.setCellValue("Дата");
        dateValue.setCellStyle(style);

        Cell flightDateValue = row.createCell(cellNum + 2);
        flightDateValue.setCellValue("Дата вылета");
        flightDateValue.setCellStyle(style);

        Cell countryResortFSTravelValue = row.createCell(cellNum + 3);
        countryResortFSTravelValue.setCellValue("Страна/Курорт FSTravel");
        countryResortFSTravelValue.setCellStyle(style);

        Cell hotelIdFSTravelValue = row.createCell(cellNum + 4);
        hotelIdFSTravelValue.setCellValue("Id FSTravel");
        hotelIdFSTravelValue.setCellStyle(style);

        Cell hotelNameFSTravelValue = row.createCell(cellNum + 5);
        hotelNameFSTravelValue.setCellValue("Отель FSTravel");
        hotelNameFSTravelValue.setCellStyle(style);

        Cell hotelStarsFSTravelValue = row.createCell(cellNum + 6);
        hotelStarsFSTravelValue.setCellValue("Категория отеля FSTravel");
        hotelStarsFSTravelValue.setCellStyle(style);

        Cell tourPriceFSTravelValue = row.createCell(cellNum + 7);
        tourPriceFSTravelValue.setCellValue("Цена FSTravel");
        tourPriceFSTravelValue.setCellStyle(style);

        Cell tourPriceActFSTravelValue = row.createCell(cellNum + 8);
        tourPriceActFSTravelValue.setCellValue("Цена FSTravel Act");
        tourPriceActFSTravelValue.setCellStyle(style);

        Cell countryResortOfferValue = row.createCell(cellNum + 9);
        countryResortOfferValue.setCellValue("Страна/Курорт Offer");
        countryResortOfferValue.setCellStyle(style);

        Cell hotelIdOfferValue = row.createCell(cellNum + 10);
        hotelIdOfferValue.setCellValue("Id Offer");
        hotelIdOfferValue.setCellStyle(style);

        Cell hotelNameOfferValue = row.createCell(cellNum + 11);
        hotelNameOfferValue.setCellValue("Отель Offer");
        hotelNameOfferValue.setCellStyle(style);

        Cell hotelStarsOfferValue = row.createCell(cellNum + 12);
        hotelStarsOfferValue.setCellValue("Категория отеля Offer");
        hotelStarsOfferValue.setCellStyle(style);

        Cell tourPriceOfferValue = row.createCell(cellNum + 13);
        tourPriceOfferValue.setCellValue("Цена Offer");
        tourPriceOfferValue.setCellStyle(style);

        Cell tourPriceActOfferValue = row.createCell(cellNum + 14);
        tourPriceActOfferValue.setCellValue("Цена Offer Act");
        tourPriceActOfferValue.setCellStyle(style);

        rowCounter ++;

        //FSTravel Part
        for (int i = 0; i < getHotelNamesFSTravelList().size(); i++) {

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            weekNumber = row.createCell(cellNum);
            weekNumber.setCellValue(getCurrentWeekNumberValue());

            dateValue = row.createCell(cellNum + 1);
            dateValue.setCellValue(getCurrentDateValue());

            flightDateValue = row.createCell(cellNum + 2);
            flightDateValue.setCellValue(getSearchDate(dateCounterValue));

            countryResortFSTravelValue = row.createCell(cellNum + 3);
            countryResortFSTravelValue.setCellValue(getCountryResortFSTravelList().get(i));

            hotelIdFSTravelValue = row.createCell(cellNum + 4);
            hotelIdFSTravelValue.setCellValue(getHotelIdFSTravelList().get(i));

            hotelNameFSTravelValue = row.createCell(cellNum + 5);
            hotelNameFSTravelValue.setCellValue(getHotelNamesFSTravelList().get(i));

            hotelStarsFSTravelValue = row.createCell(cellNum + 6);
            hotelStarsFSTravelValue.setCellValue(getHotelStarsFSTravelList().get(i));

            tourPriceFSTravelValue = row.createCell(cellNum + 7);
            tourPriceFSTravelValue.setCellValue(getHotelTourPriceFSTravelList().get(i));

            tourPriceActFSTravelValue = row.createCell(cellNum + 8);
            tourPriceActFSTravelValue.setCellValue(getHotelTourPriceActualizedFSTravelList().get(i));

            rowCounter++;

        }

        book.write(new FileOutputStream(reportName));

        //Offer Part
        rowCounter = 1;

        for (int i = 0; i < getHotelNamesOfferList().size(); i++) {

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            countryResortOfferValue = row.createCell(cellNum + 9);
            countryResortOfferValue.setCellValue(getRegionOfferList().get(i)); // getCountryResortOfferList()

            hotelIdOfferValue = row.createCell(cellNum + 10);
            hotelIdOfferValue.setCellValue(getHotelIdOfferList().get(i));

            hotelNameOfferValue = row.createCell(cellNum + 11);
            hotelNameOfferValue.setCellValue(getHotelNamesOfferList().get(i));

            hotelStarsOfferValue = row.createCell(cellNum + 12);
            hotelStarsOfferValue.setCellValue(getHotelStarsOfferList().get(i));

            tourPriceOfferValue = row.createCell(cellNum + 13);
            tourPriceOfferValue.setCellValue(getHotelTourPriceOfferList().get(i));

            tourPriceActOfferValue = row.createCell(cellNum + 14);
            tourPriceActOfferValue.setCellValue(getHotelTourPriceActualizedOfferList().get(i));

            rowCounter++;

        }

        book.write(new FileOutputStream(reportName));

        return this;
    }


    public ReportGenerator generateReportPegastOffer(Workbook book, String reportName, String countryValue, String departureValue, int dateCounterValue) throws Exception{

        Sheet sheet = null;

        if (book.getSheetIndex(departureValue + "-" + countryValue) == -1)
            sheet = book.createSheet(departureValue + "-" + countryValue);
        else
            sheet = book.getSheet(departureValue + "-" + countryValue);

        Row row = null;
        int rowCounter = 0;
        int cellNum = 0;

        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0){
            row = sheet.getRow(rowCounter);
            cellNum = sheet.getRow(1).getLastCellNum();
        }
        else
            row = sheet.createRow(rowCounter);

        CellStyle style = book.createCellStyle();
        Font font = book.createFont();
        font.setBold(true);
        style.setFont(font);

        Cell weekNumber = row.createCell(cellNum);
        weekNumber.setCellValue("Неделя");
        weekNumber.setCellStyle(style);

        Cell dateValue = row.createCell(cellNum + 1);
        dateValue.setCellValue("Дата");
        dateValue.setCellStyle(style);

        Cell flightDateValue = row.createCell(cellNum + 2);
        flightDateValue.setCellValue("Дата вылета");
        flightDateValue.setCellStyle(style);

        Cell countryResortPegastValue = row.createCell(cellNum + 3);
        countryResortPegastValue.setCellValue("Курорт Pegast");
        countryResortPegastValue.setCellStyle(style);

        Cell hotelIdPegastValue = row.createCell(cellNum + 4);
        hotelIdPegastValue.setCellValue("Id Pegast");
        hotelIdPegastValue.setCellStyle(style);

        Cell hotelNamePegastValue = row.createCell(cellNum + 5);
        hotelNamePegastValue.setCellValue("Отель Pegast");
        hotelNamePegastValue.setCellStyle(style);

        Cell hotelStarsPegastValue = row.createCell(cellNum + 6);
        hotelStarsPegastValue.setCellValue("Категория отеля Pegast");
        hotelStarsPegastValue.setCellStyle(style);

        Cell tourPricePegastValue = row.createCell(cellNum + 7);
        tourPricePegastValue.setCellValue("Цена Pegast");
        tourPricePegastValue.setCellStyle(style);

        Cell tourPriceActPegastValue = row.createCell(cellNum + 8);
        tourPriceActPegastValue.setCellValue("Цена Pegast Act");
        tourPriceActPegastValue.setCellStyle(style);

        Cell countryResortOfferValue = row.createCell(cellNum + 9);
        countryResortOfferValue.setCellValue("Курорт Offer");
        countryResortOfferValue.setCellStyle(style);

        Cell hotelIdOfferValue = row.createCell(cellNum + 10);
        hotelIdOfferValue.setCellValue("Id Offer");
        hotelIdOfferValue.setCellStyle(style);

        Cell hotelNameOfferValue = row.createCell(cellNum + 11);
        hotelNameOfferValue.setCellValue("Отель Offer");
        hotelNameOfferValue.setCellStyle(style);

        Cell hotelStarsOfferValue = row.createCell(cellNum + 12);
        hotelStarsOfferValue.setCellValue("Категория отеля Offer");
        hotelStarsOfferValue.setCellStyle(style);

        Cell tourPriceOfferValue = row.createCell(cellNum + 13);
        tourPriceOfferValue.setCellValue("Цена Offer");
        tourPriceOfferValue.setCellStyle(style);

        Cell tourPriceActOfferValue = row.createCell(cellNum + 14);
        tourPriceActOfferValue.setCellValue("Цена Offer Act");
        tourPriceActOfferValue.setCellStyle(style);

        rowCounter ++;

        //Pegast Part
        for (int i = 0; i < getHotelNamesPegastList().size(); i++) {

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            weekNumber = row.createCell(cellNum);
            weekNumber.setCellValue(getCurrentWeekNumberValue());

            dateValue = row.createCell(cellNum + 1);
            dateValue.setCellValue(getCurrentDateValue());

            flightDateValue = row.createCell(cellNum + 2);
            flightDateValue.setCellValue(getSearchDate(dateCounterValue));

            countryResortPegastValue = row.createCell(cellNum + 3);
            countryResortPegastValue.setCellValue(getCountryResortPegastList().get(i));

            hotelIdPegastValue = row.createCell(cellNum + 4);
            hotelIdPegastValue.setCellValue(getHotelIdPegastList().get(i));

            hotelNamePegastValue = row.createCell(cellNum + 5);
            hotelNamePegastValue.setCellValue(getHotelNamesPegastList().get(i));

            hotelStarsPegastValue = row.createCell(cellNum + 6);
            hotelStarsPegastValue.setCellValue(getHotelStarsPegastList().get(i));

            tourPricePegastValue = row.createCell(cellNum + 7);
            tourPricePegastValue.setCellValue(getHotelTourPricePegastList().get(i));

            tourPriceActPegastValue = row.createCell(cellNum + 8);
            tourPriceActPegastValue.setCellValue(getHotelTourPriceActualizedPegastList().get(i));

            rowCounter++;

        }

        book.write(new FileOutputStream(reportName));

        //Offer Part
        rowCounter = 1;

        for (int i = 0; i < getHotelNamesOfferList().size(); i++) {

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            countryResortOfferValue = row.createCell(cellNum + 9);
            countryResortOfferValue.setCellValue(getRegionOfferList().get(i)); // getCountryResortOfferList()

            hotelIdOfferValue = row.createCell(cellNum + 10);
            hotelIdOfferValue.setCellValue(getHotelIdOfferList().get(i));

            hotelNameOfferValue = row.createCell(cellNum + 11);
            hotelNameOfferValue.setCellValue(getHotelNamesOfferList().get(i));

            hotelStarsOfferValue = row.createCell(cellNum + 12);
            hotelStarsOfferValue.setCellValue(getHotelStarsOfferList().get(i));

            tourPriceOfferValue = row.createCell(cellNum + 13);
            tourPriceOfferValue.setCellValue(getHotelTourPriceOfferList().get(i));

            tourPriceActOfferValue = row.createCell(cellNum + 14);
            tourPriceActOfferValue.setCellValue(getHotelTourPriceActualizedOfferList().get(i));

            rowCounter++;

        }

        book.write(new FileOutputStream(reportName));

        return this;
    }

    public ReportGenerator generateReportAleanOffer(Workbook book, String reportName, String countryValue, String departureValue, int dateCounterValue, String[] resortsAlean) throws Exception{

        Sheet sheet = null;

        if (book.getSheetIndex(departureValue + "-" + countryValue) == -1)
            sheet = book.createSheet(departureValue + "-" + countryValue);
        else
            sheet = book.getSheet(departureValue + "-" + countryValue);
        CellStyle style = book.createCellStyle();
        Font font = book.createFont();
        font.setBold(true);
        style.setFont(font);

        // Delete old reports
//        int recordCounter = 5;
//        int columnCounter = 13;
//        if (cellNum == (recordCounter * columnCounter)){
//            for (int i=0; i<columnCounter; i++){
//                for (int j=0; j<=sheet.getLastRowNum(); j++){
//                    Cell c = sheet.getRow(j).getCell(0);
//                    if(!(c == null || c.getCellTypeEnum() == CellType.BLANK))
//                        sheet.getRow(j).removeCell(sheet.getRow(j).getCell(0));
//                }
//                deleteColumn(sheet, 0);
//                book.write(new FileOutputStream(reportName));
//            }
//        }
        //

        Row row = null;
        int rowCounter = 0;
        int cellNum = 0;

        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0){
            row = sheet.getRow(rowCounter);
            cellNum = sheet.getRow(1).getLastCellNum();
        }
        else
            row = sheet.createRow(rowCounter);

        Cell weekNumber = row.createCell(cellNum);
        weekNumber.setCellValue("Неделя");
        weekNumber.setCellStyle(style);

        Cell dateValue = row.createCell(cellNum + 1);
        dateValue.setCellValue("Дата");
        dateValue.setCellStyle(style);

        Cell flightDateValue = row.createCell(cellNum + 2);
        flightDateValue.setCellValue("Дата вылета");
        flightDateValue.setCellStyle(style);

        Cell countryResortAleanValue = row.createCell(cellNum + 3);
        countryResortAleanValue.setCellValue("Курорт Alean");
        countryResortAleanValue.setCellStyle(style);

        Cell hotelNameAleanValue = row.createCell(cellNum + 4);
        hotelNameAleanValue.setCellValue("Отель Alean");
        hotelNameAleanValue.setCellStyle(style);

        Cell tourPriceAleanValue = row.createCell(cellNum + 5);
        tourPriceAleanValue.setCellValue("Цена Alean");
        tourPriceAleanValue.setCellStyle(style);

        Cell tourPriceActAleanValue = row.createCell(cellNum + 6);
        tourPriceActAleanValue.setCellValue("Цена Alean Act");
        tourPriceActAleanValue.setCellStyle(style);

        Cell countryResortOfferValue = row.createCell(cellNum + 7);
        countryResortOfferValue.setCellValue("Курорт Offer");
        countryResortOfferValue.setCellStyle(style);

        Cell hotelNameOfferValue = row.createCell(cellNum + 8);
        hotelNameOfferValue.setCellValue("Отель Offer");
        hotelNameOfferValue.setCellStyle(style);

        Cell tourPriceOfferValue = row.createCell(cellNum + 9);
        tourPriceOfferValue.setCellValue("Цена Offer");
        tourPriceOfferValue.setCellStyle(style);

        Cell tourPriceActOfferValue = row.createCell(cellNum + 10);
        tourPriceActOfferValue.setCellValue("Цена Offer Act");
        tourPriceActOfferValue.setCellStyle(style);

        Cell tourPriceActOfferRatioValue = row.createCell(cellNum + 11);
        tourPriceActOfferRatioValue.setCellValue("Offer Act/Offer");
        tourPriceActOfferRatioValue.setCellStyle(style);

        Cell resortValue = row.createCell(cellNum + 12);
        resortValue.setCellValue("Resort");
        resortValue.setCellStyle(style);

        Cell totalValue = row.createCell(cellNum + 13);
        totalValue.setCellValue("Total");
        totalValue.setCellStyle(style);

//        Cell idealTotalValue = row.createCell(cellNum + 14);
//        idealTotalValue.setCellValue("Ideal total");
//        idealTotalValue.setCellStyle(style);

        rowCounter ++;

        //Alean Part
        for (int i = 0; i < getHotelNamesAleanList().size(); i++) {

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            weekNumber = row.createCell(cellNum);
            weekNumber.setCellValue(getCurrentWeekNumberValue());

            dateValue = row.createCell(cellNum + 1);
            dateValue.setCellValue(getCurrentDateValue());

            flightDateValue = row.createCell(cellNum + 2);
            flightDateValue.setCellValue(getSearchDate(dateCounterValue));

            countryResortAleanValue = row.createCell(cellNum + 3);
            countryResortAleanValue.setCellValue(getCountryResortAleanList().get(i));

            hotelNameAleanValue = row.createCell(cellNum + 4);
            hotelNameAleanValue.setCellValue(getHotelNamesAleanList().get(i));

            tourPriceAleanValue = row.createCell(cellNum + 5);
            tourPriceAleanValue.setCellValue(getHotelTourPriceAleanList().get(i));

            tourPriceActAleanValue = row.createCell(cellNum + 6);
            tourPriceActAleanValue.setCellValue(getHotelTourPriceActualizedAleanList().get(i));

            rowCounter++;

        }

        book.write(new FileOutputStream(reportName));

        //Offer part
        rowCounter = 1;

        for (int i = 0; i < getHotelNamesOfferList().size(); i++) {

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            countryResortOfferValue = row.createCell(cellNum + 7);
            countryResortOfferValue.setCellValue(getCountryResortOfferList().get(i));

            hotelNameOfferValue = row.createCell(cellNum + 8);
            hotelNameOfferValue.setCellValue(getHotelNamesOfferList().get(i));

            tourPriceOfferValue = row.createCell(cellNum + 9);
            tourPriceOfferValue.setCellValue(getHotelTourPriceOfferList().get(i));

            tourPriceActOfferValue = row.createCell(cellNum + 10);
            tourPriceActOfferValue.setCellValue(getHotelTourPriceActualizedOfferList().get(i));

            tourPriceActOfferRatioValue = row.createCell(cellNum + 11);
            if(!getHotelTourPriceActualizedOfferList().get(i).equals("-") && !getHotelTourPriceOfferList().get(i).equals("-")){
                setTourPriceActOfferRatioList(Double.parseDouble(getHotelTourPriceActualizedOfferList().get(i)) / Double.parseDouble(getHotelTourPriceOfferList().get(i)) * 100.00);
                tourPriceActOfferRatioValue.setCellValue(String.format("%.2f", getTourPriceActOfferRatioList().get(i)));
            }
            else{
                setTourPriceActOfferRatioList(0.0);
                tourPriceActOfferRatioValue.setCellValue("-");
            }

            rowCounter ++;
        }

        book.write(new FileOutputStream(reportName));

        //Total part

        getTotalAleanValueList();

        rowCounter = 1;

        for (int i = 0; i < resortsAlean.length; i++) {

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            resortValue = row.createCell(cellNum + 12);
            resortValue.setCellValue(resortsAlean[i]);

            totalValue = row.createCell(cellNum + 13);
            if (getTotalCounterList().get(i) < 0)
                totalValue.setCellValue("-");
            else
                totalValue.setCellValue(String.format("%.2f", getTotalCounterList().get(i)));

//            idealTotalValue = row.createCell(cellNum + 14);
//            if(getIdealTotalCounterList().get(i) < 0)
//                idealTotalValue.setCellValue("-");
//            else
//                idealTotalValue.setCellValue(String.format("%.2f", getIdealTotalCounterList().get(i)));

            rowCounter ++;

        }

        book.write(new FileOutputStream(reportName));

        rowCounter ++;

        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
            row = sheet.getRow(rowCounter);
        else
            row = sheet.createRow(rowCounter);

        totalValue = row.createCell(cellNum + 13);
        totalValue.setCellValue(String.format("%.2f", getAverageTotalValue()));

//        idealTotalValue = row.createCell(cellNum + 14);
//        idealTotalValue.setCellValue(String.format("%.2f", getIdealAverageTotalValue()));

        book.write(new FileOutputStream(reportName));

        return this;
    }

    public ReportGenerator generateReportTuiOfferAmount(Workbook book, String reportName, String countryValue, String departureValue, int dateCounterValue) throws Exception{
        Sheet sheet = null;

        if (book.getSheetIndex(departureValue + "-" + countryValue) == -1)
            sheet = book.createSheet(departureValue + "-" + countryValue);
        else
            sheet = book.getSheet(departureValue + "-" + countryValue);

        Row row = null;
        int rowCounter = 0;
        int cellNum = 0;

        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0){
            row = sheet.getRow(rowCounter);
            cellNum = sheet.getRow(1).getLastCellNum();
        }
        else
            row = sheet.createRow(rowCounter);

        CellStyle style = book.createCellStyle();
        Font font = book.createFont();
        font.setBold(true);
        style.setFont(font);

        Cell weekNumber = row.createCell(cellNum);
        weekNumber.setCellValue("Неделя");
        weekNumber.setCellStyle(style);

        Cell dateValue = row.createCell(cellNum + 1);
        dateValue.setCellValue("Дата");
        dateValue.setCellStyle(style);

        Cell flightDateValue = row.createCell(cellNum + 2);
        flightDateValue.setCellValue("Дата вылета");
        flightDateValue.setCellStyle(style);

        Cell countryResortBGValue = row.createCell(cellNum + 3);
        countryResortBGValue.setCellValue("Страна/Курорт Tui");
        countryResortBGValue.setCellStyle(style);

        Cell hotelNameBGValue = row.createCell(cellNum + 4);
        hotelNameBGValue.setCellValue("Отель Tui");
        hotelNameBGValue.setCellStyle(style);

        Cell tourPriceBGValue = row.createCell(cellNum + 5);
        tourPriceBGValue.setCellValue("Цена Tui");
        tourPriceBGValue.setCellStyle(style);

        Cell countryResortOfferValue = row.createCell(cellNum + 6);
        countryResortOfferValue.setCellValue("Страна/Курорт Offer");
        countryResortOfferValue.setCellStyle(style);

        Cell hotelNameOfferValue = row.createCell(cellNum + 7);
        hotelNameOfferValue.setCellValue("Отель Offer");
        hotelNameOfferValue.setCellStyle(style);

        Cell tourPriceOfferValue = row.createCell(cellNum + 8);
        tourPriceOfferValue.setCellValue("Цена Offer");
        tourPriceOfferValue.setCellStyle(style);

//        Cell tourPriceActOfferValue = row.createCell(cellNum + 9);
//        tourPriceActOfferValue.setCellValue("Цена Offer Act");
//        tourPriceActOfferValue.setCellStyle(style);
//
//        Cell tourPriceActOfferRatioValue = row.createCell(cellNum + 10);
//        tourPriceActOfferRatioValue.setCellValue("Offer Act/Offer");
//        tourPriceActOfferRatioValue.setCellStyle(style);
//
//        Cell resortValue = row.createCell(cellNum + 11);
//        resortValue.setCellValue("Resort");
//        resortValue.setCellStyle(style);
//
//        Cell totalValue = row.createCell(cellNum + 12);
//        totalValue.setCellValue("Total");
//        totalValue.setCellStyle(style);

        rowCounter ++;

        //Tui Part
        for (int i = 0; i < getHotelNamesTuiList().size(); i++) {

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            weekNumber = row.createCell(cellNum);
            weekNumber.setCellValue(getCurrentWeekNumberValue());

            dateValue = row.createCell(cellNum + 1);
            dateValue.setCellValue(getCurrentDateValue());

            flightDateValue = row.createCell(cellNum + 2);
            flightDateValue.setCellValue(getSearchDate(dateCounterValue));

            countryResortBGValue = row.createCell(cellNum + 3);
            countryResortBGValue.setCellValue(getCountryResortTuiList().get(i));

            hotelNameBGValue = row.createCell(cellNum + 4);
            hotelNameBGValue.setCellValue(getHotelNamesTuiList().get(i));

            tourPriceBGValue = row.createCell(cellNum + 5);
            tourPriceBGValue.setCellValue(getHotelTourPriceTuiList().get(i));

            rowCounter++;

        }

        book.write(new FileOutputStream(reportName));

        //Offer Part
        rowCounter = 1;

        for (int i = 0; i < getHotelNamesOfferList().size(); i++) {

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            countryResortOfferValue = row.createCell(cellNum + 6);
            countryResortOfferValue.setCellValue(getCountryResortOfferList().get(i));

            hotelNameOfferValue = row.createCell(cellNum + 7);
            hotelNameOfferValue.setCellValue(getHotelNamesOfferList().get(i));

            tourPriceOfferValue = row.createCell(cellNum + 8);
            tourPriceOfferValue.setCellValue(getHotelTourPriceOfferList().get(i));

//            tourPriceActOfferValue = row.createCell(cellNum + 9);
//            tourPriceActOfferValue.setCellValue(getHotelTourPriceActualizedOfferList().get(i));
//
//            tourPriceActOfferRatioValue = row.createCell(cellNum + 10);
//            if(!getHotelTourPriceActualizedOfferList().get(i).equals("-") && !getHotelTourPriceOfferList().get(i).equals("-")){
//                setTourPriceActOfferRatioList(Double.parseDouble(getHotelTourPriceActualizedOfferList().get(i)) / Double.parseDouble(getHotelTourPriceOfferList().get(i)) * 100.00);
//                tourPriceActOfferRatioValue.setCellValue(String.format("%.2f", getTourPriceActOfferRatioList().get(i)));
//            }
//            else{
//                setTourPriceActOfferRatioList(0.0);
//                tourPriceActOfferRatioValue.setCellValue("-");
//            }

            rowCounter++;

        }

        book.write(new FileOutputStream(reportName));

        //Total

        return this;
    }

    public ReportGenerator generateReportAnexOfferAmount(Workbook book, String reportName, String countryValue, String departureValue, int dateCounterValue) throws Exception {

        Sheet sheet = null;

        if (book.getSheetIndex(departureValue + "-" + countryValue) == -1)
            sheet = book.createSheet(departureValue + "-" + countryValue);
        else
            sheet = book.getSheet(departureValue + "-" + countryValue);

        Row row = null;
        int rowCounter = 0;
        int cellNum = 0;

        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0){
            row = sheet.getRow(rowCounter);
            cellNum = sheet.getRow(1).getLastCellNum();
        }
        else
            row = sheet.createRow(rowCounter);

        CellStyle style = book.createCellStyle();
        Font font = book.createFont();
        font.setBold(true);
        style.setFont(font);

        Cell weekNumber = row.createCell(cellNum);
        weekNumber.setCellValue("Неделя");
        weekNumber.setCellStyle(style);

        Cell dateValue = row.createCell(cellNum + 1);
        dateValue.setCellValue("Дата");
        dateValue.setCellStyle(style);

        Cell flightDateValue = row.createCell(cellNum + 2);
        flightDateValue.setCellValue("Дата вылета");
        flightDateValue.setCellStyle(style);

        Cell countryResortAnexValue = row.createCell(cellNum + 3);
        countryResortAnexValue.setCellValue("Курорт Anex");
        countryResortAnexValue.setCellStyle(style);

        Cell hotelNameAnexValue = row.createCell(cellNum + 4);
        hotelNameAnexValue.setCellValue("Отель Anex");
        hotelNameAnexValue.setCellStyle(style);

        Cell tourPriceAnexValue = row.createCell(cellNum + 5);
        tourPriceAnexValue.setCellValue("Цена Anex");
        tourPriceAnexValue.setCellStyle(style);

//        Cell tourPriceActAnexValue = row.createCell(cellNum + 6);
//        tourPriceActAnexValue.setCellValue("Цена Anex Act");
//        tourPriceActAnexValue.setCellStyle(style);

        Cell countryResortOfferValue = row.createCell(cellNum + 6);
        countryResortOfferValue.setCellValue("Курорт Offer");
        countryResortOfferValue.setCellStyle(style);

        Cell hotelNameOfferValue = row.createCell(cellNum + 7);
        hotelNameOfferValue.setCellValue("Отель Offer");
        hotelNameOfferValue.setCellStyle(style);

        Cell tourPriceOfferValue = row.createCell(cellNum + 8);
        tourPriceOfferValue.setCellValue("Цена Offer");
        tourPriceOfferValue.setCellStyle(style);

//        Cell tourPriceActOfferValue = row.createCell(cellNum + 10);
//        tourPriceActOfferValue.setCellValue("Цена Offer Act");
//        tourPriceActOfferValue.setCellStyle(style);
//
//        Cell tourPriceActOfferRatioValue = row.createCell(cellNum + 11);
//        tourPriceActOfferRatioValue.setCellValue("Offer Act/Offer");
//        tourPriceActOfferRatioValue.setCellStyle(style);
//
//        Cell resortValue = row.createCell(cellNum + 12);
//        resortValue.setCellValue("Resort");
//        resortValue.setCellStyle(style);
//
//        Cell totalValue = row.createCell(cellNum + 13);
//        totalValue.setCellValue("Total");
//        totalValue.setCellStyle(style);

        rowCounter ++;

        //Anex Part

        for (int i = 0; i < getHotelNamesAnexList().size(); i++) {

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            weekNumber = row.createCell(cellNum);
            weekNumber.setCellValue(getCurrentWeekNumberValue());

            dateValue = row.createCell(cellNum + 1);
            dateValue.setCellValue(getCurrentDateValue());

            flightDateValue = row.createCell(cellNum + 2);
            flightDateValue.setCellValue(getSearchDate(dateCounterValue));

            countryResortAnexValue = row.createCell(cellNum + 3);
            countryResortAnexValue.setCellValue(getCountryResortAnexList().get(i));

            hotelNameAnexValue = row.createCell(cellNum + 4);
            hotelNameAnexValue.setCellValue(getHotelNamesAnexList().get(i));

            tourPriceAnexValue = row.createCell(cellNum + 5);
            tourPriceAnexValue.setCellValue(getHotelTourPriceAnexList().get(i));

//            tourPriceActAnexValue = row.createCell(cellNum + 6);
//            tourPriceActAnexValue.setCellValue(getHotelTourPriceActualizedAnexList().get(i));

            rowCounter++;

        }

        book.write(new FileOutputStream(reportName));

        //Offer part
        rowCounter = 1;

        for (int i = 0; i < getHotelNamesOfferList().size(); i++) {

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            countryResortOfferValue = row.createCell(cellNum + 6);
            countryResortOfferValue.setCellValue(getCountryResortOfferList().get(i));

            hotelNameOfferValue = row.createCell(cellNum + 7);
            hotelNameOfferValue.setCellValue(getHotelNamesOfferList().get(i));

            tourPriceOfferValue = row.createCell(cellNum + 8);
            tourPriceOfferValue.setCellValue(getHotelTourPriceOfferList().get(i));

//            tourPriceActOfferValue = row.createCell(cellNum + 10);
//            tourPriceActOfferValue.setCellValue(getHotelTourPriceActualizedOfferList().get(i));

//            tourPriceActOfferRatioValue = row.createCell(cellNum + 11);
//            if(!getHotelTourPriceActualizedOfferList().get(i).equals("-") && !getHotelTourPriceOfferList().get(i).equals("-")){
//                setTourPriceActOfferRatioList(Double.parseDouble(getHotelTourPriceActualizedOfferList().get(i)) / Double.parseDouble(getHotelTourPriceOfferList().get(i)) * 100.00);
//                tourPriceActOfferRatioValue.setCellValue(String.format("%.2f", getTourPriceActOfferRatioList().get(i)));
//            }
//            else{
//                setTourPriceActOfferRatioList(0.0);
//                tourPriceActOfferRatioValue.setCellValue("-");
//            }

            rowCounter ++;
        }

        book.write(new FileOutputStream(reportName));

        //Total part

//        getTotalAnexValueList();
//
//        rowCounter = 1;
//
//        for (int i = 0; i < resortsAnex.length; i++) {
//
//            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
//                row = sheet.getRow(rowCounter);
//            else
//                row = sheet.createRow(rowCounter);
//
//            resortValue = row.createCell(cellNum + 12);
//            resortValue.setCellValue(resortsAnex[i]);
//
//            totalValue = row.createCell(cellNum + 13);
//            if(!getTotalCounterList().isEmpty()){
//                if (getTotalCounterList().get(i) < 0)
//                    totalValue.setCellValue("-");
//                else
//                    totalValue.setCellValue(String.format("%.2f", getTotalCounterList().get(i)));
//            }
//            else
//                totalValue.setCellValue("-");
//
//
//            rowCounter ++;
//
//        }
//
//        book.write(new FileOutputStream(reportName));
//
//        rowCounter ++;
//
//        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
//            row = sheet.getRow(rowCounter);
//        else
//            row = sheet.createRow(rowCounter);
//
//        totalValue = row.createCell(cellNum + 13);
//        totalValue.setCellValue(String.format("%.2f", getAverageTotalValue()));
//
//
//        book.write(new FileOutputStream(reportName));

        return this;

    }

    public ReportGenerator generateReportFSTravelOfferAmount(Workbook book, String reportName, String countryValue, String departureValue, int dateCounterValue) throws Exception {

        Sheet sheet = null;

        if (book.getSheetIndex(departureValue + "-" + countryValue) == -1)
            sheet = book.createSheet(departureValue + "-" + countryValue);
        else
            sheet = book.getSheet(departureValue + "-" + countryValue);

        Row row = null;
        int rowCounter = 0;
        int cellNum = 0;

        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0){
            row = sheet.getRow(rowCounter);
            cellNum = sheet.getRow(1).getLastCellNum();
        }
        else
            row = sheet.createRow(rowCounter);

        CellStyle style = book.createCellStyle();
        Font font = book.createFont();
        font.setBold(true);
        style.setFont(font);

        Cell weekNumber = row.createCell(cellNum);
        weekNumber.setCellValue("Неделя");
        weekNumber.setCellStyle(style);

        Cell dateValue = row.createCell(cellNum + 1);
        dateValue.setCellValue("Дата");
        dateValue.setCellStyle(style);

        Cell flightDateValue = row.createCell(cellNum + 2);
        flightDateValue.setCellValue("Дата вылета");
        flightDateValue.setCellStyle(style);

        Cell countryResortFSTravelValue = row.createCell(cellNum + 3);
        countryResortFSTravelValue.setCellValue("Курорт FSTravel");
        countryResortFSTravelValue.setCellStyle(style);

        Cell hotelNameFSTravelValue = row.createCell(cellNum + 4);
        hotelNameFSTravelValue.setCellValue("Отель FSTravel");
        hotelNameFSTravelValue.setCellStyle(style);

        Cell tourPriceFSTravelValue = row.createCell(cellNum + 5);
        tourPriceFSTravelValue.setCellValue("Цена FSTravel");
        tourPriceFSTravelValue.setCellStyle(style);

        Cell countryResortOfferValue = row.createCell(cellNum + 6);
        countryResortOfferValue.setCellValue("Курорт Offer");
        countryResortOfferValue.setCellStyle(style);

        Cell hotelNameOfferValue = row.createCell(cellNum + 7);
        hotelNameOfferValue.setCellValue("Отель Offer");
        hotelNameOfferValue.setCellStyle(style);

        Cell tourPriceOfferValue = row.createCell(cellNum + 8);
        tourPriceOfferValue.setCellValue("Цена Offer");
        tourPriceOfferValue.setCellStyle(style);

        rowCounter ++;

        //FSTravel Part

        for (int i = 0; i < getHotelNamesFSTravelList().size(); i++) {

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            weekNumber = row.createCell(cellNum);
            weekNumber.setCellValue(getCurrentWeekNumberValue());

            dateValue = row.createCell(cellNum + 1);
            dateValue.setCellValue(getCurrentDateValue());

            flightDateValue = row.createCell(cellNum + 2);
            flightDateValue.setCellValue(getSearchDate(dateCounterValue));

            countryResortFSTravelValue = row.createCell(cellNum + 3);
            countryResortFSTravelValue.setCellValue(getCountryResortFSTravelList().get(i));

            hotelNameFSTravelValue = row.createCell(cellNum + 4);
            hotelNameFSTravelValue.setCellValue(getHotelNamesFSTravelList().get(i));

            tourPriceFSTravelValue = row.createCell(cellNum + 5);
            tourPriceFSTravelValue.setCellValue(getHotelTourPriceFSTravelList().get(i));

            rowCounter++;

        }

        book.write(new FileOutputStream(reportName));

        //Offer part
        rowCounter = 1;

        for (int i = 0; i < getHotelNamesOfferList().size(); i++) {

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);

            countryResortOfferValue = row.createCell(cellNum + 6);
            countryResortOfferValue.setCellValue(getCountryResortOfferList().get(i));

            hotelNameOfferValue = row.createCell(cellNum + 7);
            hotelNameOfferValue.setCellValue(getHotelNamesOfferList().get(i));

            tourPriceOfferValue = row.createCell(cellNum + 8);
            tourPriceOfferValue.setCellValue(getHotelTourPriceOfferList().get(i));

            rowCounter ++;
        }

        book.write(new FileOutputStream(reportName));

        return this;

    }


}
