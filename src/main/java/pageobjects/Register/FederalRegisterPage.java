package pageobjects.Register;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.LineIterator;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.*;
import static framework.WebDriverCommands.driver;
import static framework.WebDriverCommands.goToPage;

public class FederalRegisterPage extends WebDriverCommands {

    private static FederalRegisterPage federalRegisterPage = null;
    public FederalRegisterPage(){

    }
    public static FederalRegisterPage getFederalRegisterPage(){
        if(federalRegisterPage == null)
            federalRegisterPage = new FederalRegisterPage();

        checkServerErrorAndRefresh();

        return federalRegisterPage;
    }

    public FederalRegisterPage gotoFederalRegisterPage(){
        goToPage("https://классификация-туризм.рф/displayAccommodation");
        return this;
    }

    private final String TITLE_FIELD ="//div[@class = 'header_image']";
    private final String HOTEL_TITLE_FIELD = "(//a[@class = 'field object-title'])[%s]";
    private final String HOTEL_NUMBER = "(//div[@class = 'main-info']/div[@class = 'field object-region'][1])[%s]";
    private final String HOTEL_DATE = "(//span[@class = 'classifiction-date'])[%s]";
    private final String NEXT_ARROW = "//li[contains(@class, 'next')]/a";
    private final String NEXT_ARROW_ACTIVE = "//li[@class = 'next']/a";
    private final String NUMBER_ON_PAGE = "//*[contains(text(), '%s')]";

    public FederalRegisterPage getHotelsList(String fileName) throws Exception{
        PrintWriter writer = new PrintWriter(fileName, "UTF-8");
        String currentHotelNumberValue = "";
            do {
                int hotelCounter = 1;
                while ($(byXpath(String.format(HOTEL_TITLE_FIELD, hotelCounter))).isDisplayed()){
                    writer.println($(byXpath(String.format(HOTEL_TITLE_FIELD, hotelCounter))).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).getText());
                    writer.println($(byXpath(String.format(HOTEL_NUMBER, hotelCounter))).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).getText());
                    String dateValue = $(byXpath(String.format(HOTEL_DATE, hotelCounter))).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).getText();
                    String dayMonthValue = dateValue.substring(0, dateValue.indexOf(" г")-4);
                    String yearValue = dateValue.substring(dateValue.indexOf(" г") - 4).replaceAll("\\D", "");
                    String yearFromValue = String.valueOf(Integer.parseInt(yearValue) - 3);
                    writer.println(dayMonthValue + yearFromValue);
                    writer.println("");

                    currentHotelNumberValue = $(byXpath(String.format(HOTEL_NUMBER, hotelCounter))).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).getText().replaceAll("\\D", "");
                    hotelCounter ++;
                }
                $(byXpath(NEXT_ARROW)).click();
            }
            while ($(byXpath(NEXT_ARROW_ACTIVE)).isDisplayed() || !$(byXpath(String.format(NUMBER_ON_PAGE, currentHotelNumberValue))).isDisplayed());

            writer.close();

        return this;
    }

    public FederalRegisterPage getNewHotelsList(String fileName) throws Exception{
        File file = new File(fileName);
        try {
            int hotelCounter = 1;
            while ($(byXpath(String.format(HOTEL_TITLE_FIELD, hotelCounter))).isDisplayed()){
                Scanner scanner = new Scanner(file);
                boolean isNewHotel = true;
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    if($(byXpath(String.format(HOTEL_TITLE_FIELD, hotelCounter))).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).getText().equals(line)) {
                        isNewHotel = false;
                    }
                }
                if(isNewHotel){
                    System.out.println($(byXpath(String.format(HOTEL_TITLE_FIELD, hotelCounter))).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).getText());
                    System.out.println($(byXpath(String.format(HOTEL_NUMBER, hotelCounter))).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).getText());
                    System.out.println();
                }
                hotelCounter ++;
            }
        }
        catch(FileNotFoundException e) {
            System.out.println("File not exists!");
        }

        return this;
    }

    public FederalRegisterPage getDeltaHotelsList(String fileName, String deltaFileName) throws Exception{
        File file = new File(fileName);
        Scanner scanner = new Scanner(file);
        String lastHotelValue = "";
        String lastHotelNumberValue = "";
        lastHotelValue = scanner.nextLine();
        lastHotelNumberValue = scanner.nextLine().replaceAll("\\D", "");

        PrintWriter writer = new PrintWriter(deltaFileName, "UTF-8");
        String currentHotelNumberValue = "";
        boolean ifDeltaActive = true;
        do {
            int hotelCounter = 1;
            while ($(byXpath(String.format(HOTEL_TITLE_FIELD, hotelCounter))).isDisplayed()){
                if(!$(byXpath(String.format(HOTEL_TITLE_FIELD, hotelCounter))).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).getText().equals(lastHotelValue) &&
                !$(byXpath(String.format(HOTEL_NUMBER, hotelCounter))).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).getText().contains(lastHotelNumberValue)){
//                    System.out.println($(byXpath(String.format(HOTEL_TITLE_FIELD, hotelCounter))).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).getText());
//                    System.out.println($(byXpath(String.format(HOTEL_NUMBER, hotelCounter))).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).getText());
//                    System.out.println();
                    writer.println($(byXpath(String.format(HOTEL_TITLE_FIELD, hotelCounter))).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).getText());
                    writer.println($(byXpath(String.format(HOTEL_NUMBER, hotelCounter))).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).getText());
                    String dateValue = $(byXpath(String.format(HOTEL_DATE, hotelCounter))).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).getText();
                    String dayMonthValue = dateValue.substring(0, dateValue.indexOf(" г")-4);
                    String yearValue = dateValue.substring(dateValue.indexOf(" г") - 4).replaceAll("\\D", "");
                    String yearFromValue = String.valueOf(Integer.parseInt(yearValue) - 3);
                    writer.println(dayMonthValue + yearFromValue);
                    writer.println("");
                }
                else{
                    writer.print("");
                    ifDeltaActive = false;
                    break;
                }

                currentHotelNumberValue = $(byXpath(String.format(HOTEL_NUMBER, hotelCounter))).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).getText().replaceAll("\\D", "");
                hotelCounter ++;
            }
            $(byXpath(NEXT_ARROW)).click();
        }
        while (($(byXpath(NEXT_ARROW_ACTIVE)).isDisplayed() || !$(byXpath(String.format(NUMBER_ON_PAGE, currentHotelNumberValue))).isDisplayed()) && ifDeltaActive);

        writer.close();
        scanner.close();

        return this;
    }

    public FederalRegisterPage addDeltaHotelsToFile(String fileName, String deltaFileName) throws IOException {
        String content = readFile(deltaFileName, Charset.forName("UTF-8"));
        prependPrefix(fileName, content);

        return this;
    }

    public static void prependPrefix(String fileName, String prefix) throws IOException {
        File file = new File(fileName);
        LineIterator li = FileUtils.lineIterator(file);
        File tempFile = File.createTempFile("prependPrefix", ".tmp");
        BufferedWriter w = new BufferedWriter(new FileWriter(tempFile));
        try {
            w.write(prefix);
            while (li.hasNext()) {
                w.write(li.next());
                w.write("\n");
            }
        } finally {
            w.close();
            li.close();
        }
        FileUtils.deleteQuietly(file);
        FileUtils.moveFile(tempFile, file);
    }

    static String readFile(String path, Charset encoding)
            throws IOException
    {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }

}
