package pageobjects.operators.BiblioGlobus;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;

import java.util.ArrayList;
import java.util.Arrays;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static framework.Constants.*;

public class BGTourPage extends WebDriverCommands {

    private static BGTourPage bgTourPage = null;

    public BGTourPage(){
    }
    public static BGTourPage getBGTourPage(){
        if(bgTourPage == null)
            bgTourPage = new BGTourPage();

        checkServerErrorAndRefresh();

        return bgTourPage;
    }

    private final String SELECT_CITY_FROM = "//input[contains(@class, 'city-input-wrap')]";
    private final String SELECT_CITY_FROM_VALUE = "//a[contains(@class, 'whiteCity') and text() = '%s']";
    private final String SELECT_COUNTRY_TO = "//input[contains(@class, 'country-input-wrap')]";
    private final String SELECT_COUNTRY_TO_VALUE = "//span[contains(@class, 'whiteCity') and text() = '%s ']";
    private final String SELECT_RESORT_TO = "//input[contains(@class, 'route-input-wrap')]";
    private final String SELECT_RESORT_TO_VALUE = "//span[contains(@class, 'whiteCity') and text() = '%s']";
    private final String SELECT_RESORT_TURKEY_TO_VALUE = "//span[contains(@class, 'whiteCity') and text() = 'Туры в %s']";
    private final String DATE_FROM_INPUT = "//div[@class = 'calendar-one']/.//input";
    private final String DATE_TO_INPUT = "//div[@class = 'calendar-two']/.//input";
    private final String DATE_ARROW_NEXT = "//a[contains(@class, 'react-datepicker__navigation--next')]";
    private final String DATE_CONTAINER = "//div[@class = 'react-datepicker__month-container']";
    private final String DATE_MONTH = "//div[@class = 'react-datepicker__current-month']";
    private final String DATE_DAY = "(//div[@aria-label = 'day-%s'])[%s]";
    private final String WITH_FLIGHTS_BUTTON = "//a[@class = 'buttons air ']";
    private final String TOUR_LISTS_LOADER = "//img[@class = 'load-gif']";
    private static final String LIST_LINK = "(//tr[@class = 'line-tour'])[%s]/.//a";
    private static final String BEST_PRICE_LIST_LINK = "(//tr[@class = 'line-tour'])[%s]/.//strong[contains(text(), 'ЛУЧШАЯ ЦЕНА')]";
    private static final String SEVERAL_PLACES_LIST_LINK = "(//tr[@class = 'line-tour'])[%s]/.//span[contains(text(), '+')]";
    private static final String EMPTY_LIST_LINK = "//td[text() = 'По заданным параметрам туры не найдены!']";

    public static String getListLink(){ return LIST_LINK; }
    public static String getEmptyListLink(){ return EMPTY_LIST_LINK; }

    public BGTourPage selectCityFrom(String cityFromValue) throws Exception{
        $(byXpath(SELECT_CITY_FROM)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        $(byXpath(String.format(SELECT_CITY_FROM_VALUE, cityFromValue))).waitUntil(Condition.visible, Constants.CONSTANT_2_SECONDS).click();

        return this;
    }

    public BGTourPage selectCountryTo(String countryToValue) throws Exception{
        $(byXpath(SELECT_COUNTRY_TO)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        $(byXpath(String.format(SELECT_COUNTRY_TO_VALUE, countryToValue))).waitUntil(Condition.visible, Constants.CONSTANT_2_SECONDS).click();

        return this;
    }

    public BGTourPage selectResortTo(String countryToValue, String resortToValue) throws Exception{
        $(byXpath(SELECT_RESORT_TO)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        if(countryToValue.equals("Турция"))
            $(byXpath(String.format(SELECT_RESORT_TURKEY_TO_VALUE, resortToValue))).waitUntil(Condition.visible, Constants.CONSTANT_2_SECONDS).click();
        else
            $(byXpath(String.format(SELECT_RESORT_TO_VALUE, resortToValue))).waitUntil(Condition.visible, Constants.CONSTANT_2_SECONDS).click();

        return this;
    }

    private void selectDateInCalendar(String dateValue){
        ArrayList<String> dateList = new ArrayList<String>(Arrays.asList(dateValue.split("\\s+")));
        $(byXpath(DATE_CONTAINER)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS);
        while (!$(byXpath(DATE_MONTH)).getText().equals(monthsRussian.get(dateList.get(1)) + " " + dateList.get(2)))
            $(byXpath(DATE_ARROW_NEXT)).click();
        if($(byXpath(String.format(DATE_DAY, dateList.get(0), 2))).isDisplayed())
            if(Integer.parseInt(dateList.get(0)) < 8)
                $(byXpath(String.format(DATE_DAY, dateList.get(0), 1))).click();
            else
                $(byXpath(String.format(DATE_DAY, dateList.get(0), 2))).click();
        else
            $(byXpath(String.format(DATE_DAY, dateList.get(0), 1))).click();
    }

    public BGTourPage selectDateFrom(String dateFromValue) throws Exception{
        $(byXpath(DATE_FROM_INPUT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        selectDateInCalendar(dateFromValue);

        return this;
    }

    public BGTourPage selectDateTo(String dateToValue) throws Exception{
        $(byXpath(DATE_TO_INPUT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        selectDateInCalendar(dateToValue);

        return this;
    }

    public BGTourPage clickWithFlightButton() throws Exception{
        sleep(CONSTANT_2_SECONDS);
        if($(byXpath(WITH_FLIGHTS_BUTTON)).isDisplayed())
            $(byXpath(WITH_FLIGHTS_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        return this;
    }

    public BGTourPage waitForTourListsLoad() throws Exception{
        $(byXpath(TOUR_LISTS_LOADER)).waitUntil(Condition.disappear, CONSTANT_120_SECONDS);

        return this;
    }

    public static boolean ifBestPriceTourListLink (int listValue) throws Exception{
        return $(byXpath(String.format(BEST_PRICE_LIST_LINK, listValue))).isDisplayed();
    }

    public static boolean ifSeveralPlacesListLink (int listValue) throws Exception{
        return $(byXpath(String.format(SEVERAL_PLACES_LIST_LINK, listValue))).isDisplayed();
    }

    public BGTourListPage clickBestPriceTourListLink (int listValue) throws Exception{
        $(byXpath(String.format(BEST_PRICE_LIST_LINK, listValue))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        return new BGTourListPage();
    }

    public BGTourListPage clickTourListLink (int listValue) throws Exception{
        $(byXpath(String.format(LIST_LINK, listValue))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        return new BGTourListPage();
    }

}
