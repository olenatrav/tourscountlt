package pageobjects.operators.BiblioGlobus;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;
import pageobjects.operators.Anex_new.AnexSearchPage;

import java.util.ArrayList;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static framework.Constants.*;

public class BGTourListPage extends WebDriverCommands {

    private static BGTourListPage bgTourListPage = null;

    public BGTourListPage(){
    }
    public static BGTourListPage getBGTourListPage(){
        if(bgTourListPage == null)
            bgTourListPage = new BGTourListPage();

        checkServerErrorAndRefresh();

        return bgTourListPage;
    }

    private static final String RESET_NIGHTS_BUTTON = "//button[@ng-click = 'resetDuration()']";
    private static final String NIGHTS_CHECKBOX = "//input[contains(@ng-checked, 'duration') and @value = '%s']";
    private static final String NIGHTS_FIRST_CHECKBOX = "(//div[@class = 'w__nights_content']/.//input)[1]";
    private final String STARS_CHECKBOX = "//b[contains(text(), '%s')]";
    private final String MEAL_CHECKBOX = "//b[contains(text(), '%s')]";
    private final String INSTANT_CONFIRM_CHECKBOX = "//input[@name = 'ok']";
    private final String SUBMIT_BUTTON = "//button[@type = 'submit']";
    private final String SEARCH_RESULTS = "//a[@id = 'results']";
    private final String HOTEL_NAME_LINE = "//tr[@class = 'b-pr_t_hl'][%s]";
    private final String HOTEL_NAME = "(//tr[@class = 'b-pr_t_hl'])[%s]/.//a[@target = '_blank']";
    private final String HOTEL_RESORT = "((//tr[@class = 'b-pr_t_hl'])[%s]/.//span)[1]";
    private final String HOTEL_TOUR_PRICE = "((//tr[@class = 'b-pr_t_hl'])[%s]/following-sibling::tr/.//b[@class = 'r'])[1]";
    private final String MORE_TOURS_LINK = "//u[text() = 'ЕЩЕ ПРЕДЛОЖЕНИЯ']";
    private static final String EMPTY_PRICE_LIST_LABEL = "//td[contains(text(), 'Вообще нет шаблона')]";

    private static ArrayList<String> hotelNamesBGList = new ArrayList<String>();
    public static ArrayList <String> getHotelNamesBGList(){
        return hotelNamesBGList;
    }
    public static void setHotelNamesBGList(String hotelNameBGListValue){
        hotelNamesBGList.add(hotelNameBGListValue);
    }

    private static ArrayList<String> hotelClearNamesBGList = new ArrayList<String>();
    public static ArrayList <String> getHotelClearNamesBGList(){
        return hotelClearNamesBGList;
    }
    public static void setHotelClearNamesBGList(String hotelClearNamesBGListValue){
        hotelClearNamesBGList.add(hotelClearNamesBGListValue);
    }

    private static ArrayList<String> hotelStarsBGList = new ArrayList<String>();
    public static ArrayList <String> getHotelStarsBGList(){
        return hotelStarsBGList;
    }
    public static void setHotelStarsBGList(String hotelStarsBGListValue){
        hotelStarsBGList.add(hotelStarsBGListValue);
    }

    private static ArrayList<String> countryResortBGList = new ArrayList<String>();
    public static ArrayList <String> getCountryResortBGList(){
        return countryResortBGList;
    }
    public static void setCountryResortBGList(String countryResortBGListValue){
        countryResortBGList.add(countryResortBGListValue);
    }

    private static ArrayList<String> hotelTourPriceBGList = new ArrayList<String>();
    public static ArrayList <String> getHotelTourPriceBGList(){
        return hotelTourPriceBGList;
    }
    public static void setHotelTourPriceBGList(String hotelTourPriceBGListValue){
        hotelTourPriceBGList.add(hotelTourPriceBGListValue);
    }

    private static ArrayList<String> isBestPriceBGList = new ArrayList<String>();
    public static ArrayList <String> getIsBestPriceBGList(){
        return isBestPriceBGList;
    }
    public static void setIsBestPriceBGList(String isBestPriceBGListValue){
        isBestPriceBGList.add(isBestPriceBGListValue);
    }

    private static boolean isNightCorrespond = true;
    public static boolean getIsNightCorrespond(){
        return isNightCorrespond;
    }
    public static void setIsNightCorrespond(boolean isNightCorrespondValue){
        isNightCorrespond = isNightCorrespondValue;
    }

    private static boolean isStarCorrespond = true;
    public static boolean getIsStarCorrespond(){
        return isStarCorrespond;
    }
    public static void setIsStarCorrespond(boolean isStarCorrespondValue){
        isStarCorrespond = isStarCorrespondValue;
    }

    private static ArrayList<String> hotelsAmountBGList = new ArrayList<String>();
    public static ArrayList <String> getHotelsAmountBGList(){
        return hotelsAmountBGList;
    }
    public static void setHotelsAmountBGList(String hotelsAmountBGListValue){
        hotelsAmountBGList.add(hotelsAmountBGListValue);
    }

    private static ArrayList<String> departureCityBGList = new ArrayList<String>();
    public static ArrayList <String> getDepartureCityBGList(){
        return departureCityBGList;
    }
    public static void setDepartureCityBGList(String departureCityBGListValue){
        departureCityBGList.add(departureCityBGListValue);
    }

    private static ArrayList<Integer> actualDateCounterList = new ArrayList<Integer>();
    public static ArrayList <Integer> getActualDateCounterList(){
        return actualDateCounterList;
    }
    public static void setActualDateCounterList(Integer actualDateCounterListValue){
        actualDateCounterList.add(actualDateCounterListValue);
    }

    private static ArrayList<Integer> actualNightsCounterList = new ArrayList<Integer>();
    public static ArrayList <Integer> getActualNightsCounterList(){
        return actualNightsCounterList;
    }
    public static void setActualNightsCounterList(Integer actualNightsCounterListValue){
        actualNightsCounterList.add(actualNightsCounterListValue);
    }

//    public static ArrayList<Double> hotelTourBGAmountList = new ArrayList<Double>();
//    public static ArrayList <Double> getHotelTourBGAmountList(){
//        return hotelTourBGAmountList;
//    }
//    public static void setHotelTourBGAmountList(Double hotelTourBGAmountListValue){
//        hotelTourBGAmountList.add(hotelTourBGAmountListValue);
//    }

    public static boolean isEmptyPriceList() throws Exception{
        sleep(CONSTANT_1_SECOND);
        return $(byXpath(EMPTY_PRICE_LIST_LABEL)).isDisplayed();
    }

    public BGTourListPage selectNights(int nightsValue) throws Exception{
        $(byXpath(RESET_NIGHTS_BUTTON)).waitUntil(Condition.visible, CONSTANT_60_SECONDS).click();
        if($(byXpath(String.format(NIGHTS_CHECKBOX, nightsValue))).isDisplayed())
            $(byXpath(String.format(NIGHTS_CHECKBOX, nightsValue))).waitUntil(Condition.visible, Constants.CONSTANT_2_SECONDS).click();
        else
            setIsNightCorrespond(false);

        return this;
    }

    public static int getFirstActiveNightsCounterValue(int nightsValue) throws Exception{
        $(byXpath(RESET_NIGHTS_BUTTON)).waitUntil(Condition.visible, CONSTANT_60_SECONDS).click();
        if($(byXpath(String.format(NIGHTS_CHECKBOX, nightsValue))).isDisplayed())
            return nightsValue;
        else
            return Integer.parseInt($(byXpath(NIGHTS_FIRST_CHECKBOX)).getAttribute("value"));

    }

    public BGTourListPage selectStars(ArrayList<String> starsValue) throws Exception{
        if(!starsValue.isEmpty()){
            for(int i = 0; i<starsValue.size(); i++){
                String hotelCategory = starsValue.get(i);
                if(starsValue.get(i).length() ==1)
                    hotelCategory += "*";
                if ($(byXpath(String.format(STARS_CHECKBOX, hotelCategory))).isDisplayed()){
                    $(byXpath(String.format(STARS_CHECKBOX, hotelCategory))).waitUntil(Condition.visible, Constants.CONSTANT_2_SECONDS).click();
                    setIsStarCorrespond(true);
                }
                else
                    setIsStarCorrespond(false);
            }

        }
        else{
            setIsStarCorrespond(true);
        }

        return this;
    }

    public BGTourListPage selectMeal(ArrayList<String> mealValue) throws Exception{
        if(!mealValue.isEmpty()){
            for(int i = 0; i<mealValue.size(); i++){
                $(byXpath(String.format(MEAL_CHECKBOX, mealValue.get(i)))).waitUntil(Condition.visible, Constants.CONSTANT_2_SECONDS).click();
            }
        }

        return this;
    }

    public BGTourListPage clickInstantConfirm(boolean isInstantConfirmation) throws Exception{
        if(isInstantConfirmation)
            $(byXpath(INSTANT_CONFIRM_CHECKBOX)).waitUntil(Condition.visible, CONSTANT_60_SECONDS).click();

        return this;
    }

    public BGTourListPage clickSubmitButton() throws Exception{
        $(byXpath(SUBMIT_BUTTON)).waitUntil(Condition.visible, CONSTANT_60_SECONDS).click();

        return this;
    }

    public int countHotelsInTourList() throws Exception{
        int hotelsCounter = 1;
        while ($(byXpath(String.format(HOTEL_NAME_LINE, hotelsCounter))).isDisplayed())
            hotelsCounter ++;

        return (hotelsCounter-1);
    }

    public int putHotelNamesBGList() throws Exception{ //String countryValue, String resortValues

        $(byXpath(SEARCH_RESULTS)).waitUntil(Condition.visible, CONSTANT_60_SECONDS);

        int hotelTourCounter = 0;
        boolean isLastPage = false;
        do {
            int hotelsCounter = 1;
            while ($(byXpath(String.format(HOTEL_NAME_LINE, hotelsCounter))).isDisplayed()){
                if(!getHotelNamesBGList().contains($(byXpath(String.format(HOTEL_NAME, hotelsCounter))).getText())){
                    setHotelNamesBGList($(byXpath(String.format(HOTEL_NAME, hotelsCounter))).getText());
                    setHotelTourPriceBGList($(byXpath(String.format(HOTEL_TOUR_PRICE, hotelsCounter))).getText());
                    String resortNameValue = $(byXpath(String.format(HOTEL_RESORT, hotelsCounter))).getText();
                    setCountryResortBGList(resortNameValue.substring(0, resortNameValue.indexOf("/")-1));
//                    if (resortValue.equals("-"))
//                        setCountryResortBGList(countryValue);
//                    else
//                        setCountryResortBGList(resortValue);
                }

                hotelsCounter ++;
                hotelTourCounter ++;
            }
            isLastPage = true;
            if($(byXpath(MORE_TOURS_LINK)).isDisplayed()){
                $(byXpath(MORE_TOURS_LINK)).waitUntil(Condition.visible, Constants.CONSTANT_2_SECONDS).click();
                sleep(Constants.CONSTANT_2_SECONDS);
                isLastPage = false;
            }
        }
        while(!isLastPage);

        return hotelTourCounter;
    }

    private static String returnBGHotelStars(String fullHotelNameValue){
        String clearHotelNameValue = fullHotelNameValue.replaceAll("\\d\\*$", "").replaceAll("-\\*$", "").replaceAll("APARTMENT$", "").replaceAll("Boutique$", "");
        return fullHotelNameValue.substring(clearHotelNameValue.length()).trim();
    }

    private static String returnClearBGHotelName(String fullHotelNameValue){
        return fullHotelNameValue.replaceAll("\\d\\*$", "").replaceAll("-\\*$", "").replaceAll("APARTMENT$", "").replaceAll("Boutique$", "").trim();
    }

    public BGTourListPage putHotelClearNamesBGList(String isBestPriceListValue) throws Exception{

        boolean isLastPage = false;
        do {
            int hotelsCounter = 1;
            while ($(byXpath(String.format(HOTEL_NAME_LINE, hotelsCounter))).isDisplayed()){
                if(!getHotelNamesBGList().contains(returnClearBGHotelName($(byXpath(String.format(HOTEL_NAME, hotelsCounter))).getText()))){
                    setHotelNamesBGList(returnClearBGHotelName($(byXpath(String.format(HOTEL_NAME, hotelsCounter))).getText()));
                    setHotelStarsBGList(returnBGHotelStars($(byXpath(String.format(HOTEL_NAME, hotelsCounter))).getText()));
                    setHotelTourPriceBGList($(byXpath(String.format(HOTEL_TOUR_PRICE, hotelsCounter))).getText());
                    String resortNameValue = $(byXpath(String.format(HOTEL_RESORT, hotelsCounter))).getText();
                    setCountryResortBGList(resortNameValue.substring(0, resortNameValue.indexOf("/")-1));
                    setIsBestPriceBGList(isBestPriceListValue);
                }

                hotelsCounter ++;
                sleep(CONSTANT_1_SECOND);
            }
            isLastPage = true;
            if($(byXpath(MORE_TOURS_LINK)).isDisplayed()){
                $(byXpath(MORE_TOURS_LINK)).waitUntil(Condition.visible, Constants.CONSTANT_2_SECONDS).click();
                sleep(Constants.CONSTANT_2_SECONDS);
                isLastPage = false;
            }
        }
        while(!isLastPage);

        return this;
    }

    public String getHotelName(int hotelsCounter) throws Exception{

        return $(byXpath(String.format(HOTEL_NAME, hotelsCounter))).waitUntil(Condition.visible, CONSTANT_60_SECONDS).getText();
    }

    public BGTourListPage putHotelsAmountBGList(){
        boolean isLastPage = false;
        do {
            int hotelsCounter = 1;
            while ($(byXpath(String.format(HOTEL_NAME_LINE, hotelsCounter))).isDisplayed()){
                if(!getHotelNamesBGList().contains(returnClearBGHotelName($(byXpath(String.format(HOTEL_NAME, hotelsCounter))).getText()))){
                    setHotelNamesBGList(returnClearBGHotelName($(byXpath(String.format(HOTEL_NAME, hotelsCounter))).getText()));
                }

                hotelsCounter ++;
            }
            isLastPage = true;
            if($(byXpath(MORE_TOURS_LINK)).isDisplayed()){
                $(byXpath(MORE_TOURS_LINK)).waitUntil(Condition.visible, Constants.CONSTANT_2_SECONDS).click();
                sleep(Constants.CONSTANT_2_SECONDS);
                isLastPage = false;
            }
        }
        while(!isLastPage);

        return this;
    }

}
