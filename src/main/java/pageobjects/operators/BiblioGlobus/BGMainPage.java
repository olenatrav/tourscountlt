package pageobjects.operators.BiblioGlobus;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class BGMainPage extends WebDriverCommands {

    private static BGMainPage bgMainPage = null;

    public BGMainPage(){
    }
    public static BGMainPage getBGMainPage(){
        if(bgMainPage == null)
            bgMainPage = new BGMainPage();

        checkServerErrorAndRefresh();

        return bgMainPage;
    }

    private final String BG_START_URL = "https://www.bgoperator.ru/";
    private final String TOURS_TAB = "//a[text() = 'Туры']";

    public BGMainPage gotoBGMainPage() throws Exception{
        goToPage(BG_START_URL);

        return this;
    }

    public BGTourPage gotoBGTourPage() throws Exception{
        $(byXpath(TOURS_TAB)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        return new BGTourPage();
    }

}
