package pageobjects.operators.Anex_new;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.ArrayList;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.*;

public class AnexTourPage extends WebDriverCommands {

    private static AnexTourPage anexTourPage = null;
    public AnexTourPage(){
    }
    public static AnexTourPage getAnexTourPage(){
        if(anexTourPage == null)
            anexTourPage = new AnexTourPage();

        checkServerErrorAndRefresh();

        return anexTourPage;
    }

    private final String ACTUALIZATION_PROGRESS = "(//*[name()='svg']/*[name() = 'rect'][@role = 'presentation'])[1]"; ////div[contains(@class, 'top-headerFull')]
    private final String SHOW_ALL_BUTTON = "(//span[text() = 'Показать все'])[%s]";
    private final String SERVICES_PANEL = "(//li[contains(@class, 'hidden block relative')]/.//label[contains(@class, 'items-start')])[%s]";
    private final String CHECKED_SERVICES_PANEL = "(//li[contains(@class, 'hidden block relative')]/.//label[contains(@class, 'text-white')])[%s]";
    private final String CHECKED_SERVICES_BUTTON = "(//ul[@id = 'BOOKING_SERVICES_ID']/.//li/label[contains(@class, 'text-white')])[%s]/button";
    private final String CHECK_SERVICE_ALERT = "//div[contains(@class, 'Toastify') and @role = 'alert']";
    private final String BOOKING_BUTTON = "(//button[@gtm-label = 'booking-button-step1'])[2]";
    private final String ACTUALIZED_PRICE_VALUE = "(//h3[text() = 'Итого']/following-sibling::span)[2]"; //text
    private final String FINAL_ACTUALIZED_PRICE_VALUE = "(//p[text() = 'Итоговая стоимость']/following-sibling::span/span[contains(@class, 'bold')])[2]";
    private final String ALERT_ERROR = "//div[contains(@class, 'error')]";
    private final String EMPTY_FLIGHTS_ERROR = "//span[@gtm-label = 'search-error']";

    private static ArrayList<String> hotelTourPriceActualizedAnexList = new ArrayList<String>();
    public static ArrayList <String> getHotelTourPriceActualizedAnexList(){
        return hotelTourPriceActualizedAnexList;
    }
    public static void setHotelTourPriceActualizedAnexList(String hotelTourPriceActualizedAnexListValue){
        hotelTourPriceActualizedAnexList.add(hotelTourPriceActualizedAnexListValue);
    }

    public AnexTourPage waitForActualizationFinished() throws Exception{
        $(byXpath(ACTUALIZATION_PROGRESS)).waitUntil(Condition.disappear, Constants.CONSTANT_300_SECONDS);
        sleep(Constants.CONSTANT_1_SECOND);

        return this;
    }

    public String getActualizedPriceValue() throws Exception{
        int showAllButtonCounter = 1;
        waitForActualizationFinished();
        if(!$(byXpath(EMPTY_FLIGHTS_ERROR)).isDisplayed() && !($(byXpath(ALERT_ERROR)).isDisplayed())){
            while($(byXpath(String.format(SHOW_ALL_BUTTON, showAllButtonCounter))).exists()){
                if($(byXpath(String.format(SHOW_ALL_BUTTON, showAllButtonCounter))).isDisplayed()){
                    scrollToElement(String.format(SHOW_ALL_BUTTON, showAllButtonCounter));
                    scrollDownOnSomePxl(200);
                    $(byXpath(String.format(SHOW_ALL_BUTTON, showAllButtonCounter))).click();
                    sleep(Constants.CONSTANT_1_SECOND);
                }
                else
                    showAllButtonCounter ++;
            }
            scrollUpOnPage();
            sleep(Constants.CONSTANT_1_SECOND);
            int checkedServiceCounter = 1;
            while ($(byXpath(String.format(SERVICES_PANEL, checkedServiceCounter))).isDisplayed()){
                scrollToElement(String.format(SERVICES_PANEL, checkedServiceCounter));
                scrollDownOnSomePxl(120);
                if($(byXpath(String.format(SERVICES_PANEL, checkedServiceCounter))).getAttribute("class").contains("text-white")){
                    $(byXpath(String.format(SERVICES_PANEL, checkedServiceCounter))).click();
                    sleep(Constants.CONSTANT_2_SECONDS);
                }
                checkedServiceCounter ++;
            }
            waitForActualizationFinished();
            return $(byXpath(FINAL_ACTUALIZED_PRICE_VALUE)).getText().replaceAll("[^\\d\\.]", "");
        }
        else
            return "-";

    }

    public boolean ifErrorActualization() throws Exception{
        return $(byXpath(ALERT_ERROR)).isDisplayed();
    }

}
