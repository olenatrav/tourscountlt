package pageobjects.operators.Anex_new;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class AnexAuthPage extends WebDriverCommands {

    private static AnexAuthPage anexAuthPage = null;
    public AnexAuthPage(){
    }
    public static AnexAuthPage getAnexAuthPage(){
        if(anexAuthPage == null)
            anexAuthPage = new AnexAuthPage();

        checkServerErrorAndRefresh();

        return anexAuthPage;
    }

    private final String ANEX_AUTH_URL = "https://agent.anextour.com/";
    private final String ANEX_LOGIN_INPUT = "//input[@name = 'login']";
    private final String ANEX_PASSWORD_INPUT = "//input[@name = 'password']";
    private final String ANEX_SUBMIT_BUTTON = "//button[@type = 'submit']";

    private final String SEARCH_BUTTON = "//a[text() = 'Поиск']";

    public AnexAuthPage gotoAnexAuthPage() throws Exception{
        goToPage(ANEX_AUTH_URL);

        return this;
    }

    public AnexLKPage loginAnexLKPage(String loginValue, String pwdValue) throws Exception{
        sleep(Constants.CONSTANT_2_SECONDS);
        $(byXpath(ANEX_LOGIN_INPUT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        $(byXpath(ANEX_LOGIN_INPUT)).sendKeys(loginValue);
        $(byXpath(ANEX_PASSWORD_INPUT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        $(byXpath(ANEX_PASSWORD_INPUT)).sendKeys(pwdValue);
        $(byXpath(ANEX_SUBMIT_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        sleep(Constants.CONSTANT_1_SECOND);

        return new AnexLKPage();
    }

    public AnexSearchPage gotoAnexSearchPage(){
        $(byXpath(SEARCH_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        return new AnexSearchPage();
    }

}
