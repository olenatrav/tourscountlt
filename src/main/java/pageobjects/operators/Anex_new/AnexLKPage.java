package pageobjects.operators.Anex_new;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class AnexLKPage extends WebDriverCommands {

    private static AnexLKPage anexLKPage = null;
    public AnexLKPage(){
    }
    public static AnexLKPage getAnexLKPage(){
        if(anexLKPage == null)
            anexLKPage = new AnexLKPage();

        checkServerErrorAndRefresh();

        return anexLKPage;
    }

    private final String SEARCH_BUTTON = "//a[text() = 'Поиск']";

    public AnexSearchPage gotoAnexSearchPage(){
        $(byXpath(SEARCH_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        return new AnexSearchPage();
    }

}
