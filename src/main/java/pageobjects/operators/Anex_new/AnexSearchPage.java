package pageobjects.operators.Anex_new;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import pageobjects.operators.BiblioGlobus.BGTourListPage;

import java.util.ArrayList;
import java.util.Arrays;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static framework.Constants.*;
import static framework.ReportGenerator.getSearchRussianDate;
import static pageobjects.operators.Anex_new.AnexTourPage.setHotelTourPriceActualizedAnexList;

public class AnexSearchPage extends WebDriverCommands {

    private static AnexSearchPage anexSearchPage = null;
    public AnexSearchPage(){
    }
    public static AnexSearchPage getAnexSearchPage(){
        if(anexSearchPage == null)
            anexSearchPage = new AnexSearchPage();

        checkServerErrorAndRefresh();

        return anexSearchPage;
    }

    private final String CITY_FROM_SELECT = "//input[@data-inputname = 'TOWNFROMINC']";
    private final String CITY_FROM_VALUE = "//button[@title = '%s']";
    private final String COUNTRY_TO_SELECT = "//input[@data-inputname = 'STATEINC']";
    private static final String COUNTRY_TO_VALUE = "//button[@title = '%s']";
    private static final String DATE_SELECT = "//input[@data-inputname = 'CHECKIN_BEG']";
    private static final String DATE_MONTH_VALUE = "//button[contains(text(), '%s')]";
    private static final String DATE_DAY_VALUE = "//span[text() = '%s']/ancestor::button";
    private static final String NIGHTS_FROM_SELECT = "//input[@data-inputname = 'NIGHTS_FROM']";
    private final String NIGHTS_FROM_VALUE = "//button[@title = '%s']"; // "//span[text() = '%s']";
    private static String NIGHTS_TO_SELECT = "//input[@data-inputname = 'NIGHTS_TILL']";
    private final String NIGHTS_TO_VALUE = "//button[@title = '%s']";// "//span[text() = '%s']";
    private static final String ACTUAL_NIGHTS_VALUE = "(//span[contains(@class, 'bg-green text-white')])[%s]";
    private static final String EXIST_NIGHTS_VALUE = "(//span[contains(@class, 'justify-center')])[%s]";
    private final String RESORT_INPUT = "(//input[@placeholder = 'Поиск по городам'])[2]";
    private final String RESORT_VALUE = "((//input[@placeholder = 'Поиск по городам'])[2]/ancestor::div[contains(@class, 'border-grayLight')]/.//div[@class = 'py-16']/.//label[@title = '%s'])[1]";
    private final String TOURISTS_SELECT = "//input[@name = 'ADULT']";
    private final String STARS_CHECKBOX = "(//span[text() = '%s']/img[contains(@src, 'star')])[2]";
    private final String MEAL_CHECKBOX = "(//span[text() = '%s'])[2]";
    private final String INSTANT_CONFIRM_BUTTON = "//span[contains(text(), 'Мгновенное подтверждение')]";
    private final String SUBMIT_SEARCH_BUTTON = "//button[@type = 'submit']";

    private final String SEARCH_RESULT = "//div[@status = 'success']";
    private final String SEARCH_PROGRESS_LABEL = "//div[contains(text(), 'Идет поиск')]";
    private final String EMPTY_SEARCH_RESULT = "//div[@status = 'success' and text() = 'Ничего не найдено']";
    private static final String NO_TOURS_RESULT = "//div[text() = '0 туров']";
    private final String GROUP_BY_HOTEL_CHECKBOX = "//span[text() = 'Группировка по отелю']";
    private final String GROUP_BY_HOTEL_CHECKED = "//span[text() = 'Группировка по отелю']/preceding-sibling::span[contains(@class, 'border-purple-light')]";
    private final String HOTELS_AMOUNT_LABEL = "//div[@class = 'text-32 font-bold  sm:px-outer-mobile']";

    private final String HOTEL_PRICE_EXPAND_BUTTON = "(//button[contains(@class, 'button-text-white')])[%s]";
    private final String HOTEL_NAME_VALUE = "(//div/a[contains(@href, '&HOTEL_INC=') and contains(@class, 'text-purple') and contains(@class, 'font-bold')])[%s]"; //text
    private final String HOTEL_CATEGORY_VALUE = "(//div/a[contains(@href, '&HOTEL_INC=') and contains(@class, 'text-purple') and contains(@class, 'font-bold')]/span)[%s]";
    private final String HOTEL_RESORT_VALUE = "(//div/a[contains(@href, '&HOTEL_INC=') and contains(@class, 'text-purple') and contains(@class, 'font-bold')]/following-sibling::span)[%s]"; //text
    private final String TOURS_LOAD_PROGRESS = "//*[name()='svg']/*[name() = 'rect'][@role = 'presentation']";
    private final String TOUR_PRICE_VALUE = "(//span[contains(text(), ' ₽')])[%s]";
    private final String TOUR_PRICE_BUTTON = "(//a[contains(@href, '/booking/tour?')])[%s]"; //text

    private static ArrayList<String> hotelNamesAnexList = new ArrayList<String>();
    public static ArrayList <String> getHotelNamesAnexList(){
        return hotelNamesAnexList;
    }
    public static void setHotelNamesAnexList(String hotelNameAnexListValue){
        hotelNamesAnexList.add(hotelNameAnexListValue);
    }

    private static ArrayList<String> hotelStarsAnexList = new ArrayList<String>();
    public static ArrayList <String> getHotelStarsAnexList(){
        return hotelStarsAnexList;
    }
    public static void setHotelStarsAnexList(String hotelStarsAnexListValue){
        hotelStarsAnexList.add(hotelStarsAnexListValue);
    }

    private static ArrayList<String> countryResortAnexList = new ArrayList<String>();
    public static ArrayList <String> getCountryResortAnexList(){
        return countryResortAnexList;
    }
    public static void setCountryResortAnexList(String countryResortAnexListValue){
        countryResortAnexList.add(countryResortAnexListValue);
    }

    private static ArrayList<String> hotelTourPriceAnexList = new ArrayList<String>();
    public static ArrayList <String> getHotelTourPriceAnexList(){
        return hotelTourPriceAnexList;
    }
    public static void setHotelTourPriceAnexList(String hotelTourPriceAnexListValue){
        hotelTourPriceAnexList.add(hotelTourPriceAnexListValue);
    }

    private static ArrayList<String> hotelsAmountAnexList = new ArrayList<String>();
    public static ArrayList <String> getHotelsAmountAnexList(){
        return hotelsAmountAnexList;
    }
    public static void setHotelsAmountAnexList(String hotelsAmountAnexListValue){
        hotelsAmountAnexList.add(hotelsAmountAnexListValue);
    }

    private static ArrayList<String> departureCityAnexList = new ArrayList<String>();
    public static ArrayList <String> getDepartureCityAnexList(){
        return departureCityAnexList;
    }
    public static void setDepartureCityAnexList(String departureCityAnexListValue){
        departureCityAnexList.add(departureCityAnexListValue);
    }

    private static ArrayList<Integer> actualDateCounterList = new ArrayList<Integer>();
    public static ArrayList <Integer> getActualDateCounterList(){
        return actualDateCounterList;
    }
    public static void setActualDateCounterList(Integer actualDateCounterListValue){
        actualDateCounterList.add(actualDateCounterListValue);
    }

    private static ArrayList<Integer> actualNightsCounterList = new ArrayList<Integer>();
    public static ArrayList <Integer> getActualNightsCounterList(){
        return actualNightsCounterList;
    }
    public static void setActualNightsCounterList(Integer actualNightsCounterListValue){
        actualNightsCounterList.add(actualNightsCounterListValue);
    }

//    private static ArrayList<String> hotelTourPriceActualizedAnexList = new ArrayList<String>();
//    public static ArrayList <String> getHotelTourPriceActualizedAnexList(){
//        return hotelTourPriceActualizedAnexList;
//    }
//    public static void setHotelTourPriceActualizedAnexList(String hotelTourPriceActualizedAnexListValue){
//        hotelTourPriceActualizedAnexList.add(hotelTourPriceActualizedAnexListValue);
//    }

//    private static ArrayList<Double> hotelTourAnexAmountList = new ArrayList<Double>();
//    public static ArrayList <Double> getHotelTourAnexAmountList(){
//        return hotelTourAnexAmountList;
//    }
//    public static void setHotelTourOfferAmountList(Double hotelTourAnexAmountListValue){
//        hotelTourAnexAmountList.add(hotelTourAnexAmountListValue);
//    }

    public AnexSearchPage selectCityFrom(String cityFromValue) throws Exception{
        $(byXpath(CITY_FROM_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        sleep(CONSTANT_2_SECONDS);
        $(byXpath(CITY_FROM_SELECT)).clear();
        sleep(CONSTANT_2_SECONDS);
        $(byXpath(CITY_FROM_SELECT)).sendKeys(cityFromValue);
        sleep(CONSTANT_2_SECONDS);
        $(byXpath(String.format(CITY_FROM_VALUE, cityFromValue))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        sleep(CONSTANT_1_SECOND);

        return this;
    }

    public AnexSearchPage openCountryToSelect(String countryToValue) throws Exception{
        $(byXpath(COUNTRY_TO_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        $(byXpath(COUNTRY_TO_SELECT)).clear();
        $(byXpath(COUNTRY_TO_SELECT)).sendKeys(countryToValue);
        sleep(CONSTANT_1_SECOND);

        return this;
    }

    public AnexSearchPage selectCountryTo(String countryToValue) throws Exception{
        $(byXpath(String.format(COUNTRY_TO_VALUE, countryToValue))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        sleep(CONSTANT_1_SECOND);

        return this;
    }

    public static boolean isCountryActive(String countryToValue) throws Exception{
        return $(byXpath(String.format(COUNTRY_TO_VALUE, countryToValue))).isDisplayed();
    }

    public AnexSearchPage openCalendar() throws Exception{
        $(byXpath(DATE_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        sleep(CONSTANT_1_SECOND);

        return this;
    }

    public AnexSearchPage selectFlightDate(String flightDateValue) throws Exception{
        ArrayList<String> dateList = new ArrayList<String>(Arrays.asList(flightDateValue.split("\\s+")));
        $(byXpath(String.format(DATE_MONTH_VALUE, monthsRussian.get(dateList.get(1))))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        $(byXpath(String.format(DATE_DAY_VALUE, dateList.get(0)))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        $(byXpath(String.format(DATE_DAY_VALUE, dateList.get(0)))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();//if start date = finish date
        sleep(CONSTANT_2_SECONDS);

        return this;
    }

    public static boolean isDateActive(String flightDateValue) throws Exception{
        ArrayList<String> dateList = new ArrayList<String>(Arrays.asList(flightDateValue.split("\\s+")));
        $(byXpath(String.format(DATE_MONTH_VALUE, monthsRussian.get(dateList.get(1))))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        if($(byXpath(String.format(DATE_DAY_VALUE, dateList.get(0)))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).getAttribute("class").contains("pointer-events-none"))
            return false;
        else
            return true;

    }

    public static int getFirstActiveDateCounterValue(int dateCounter) throws Exception{
        int currentDateCounter = 1;
        if(dateCounter > 7)
            currentDateCounter = dateCounter - 7;
        int finishDateCounter = dateCounter + 7;
        while(currentDateCounter <= finishDateCounter){
            if(isDateActive(getSearchRussianDate(currentDateCounter)))
                return currentDateCounter;
            currentDateCounter ++;
        }
        return 0;
    }

    public AnexSearchPage selectNightsFrom(int nightsFromValue) throws Exception{
        $(byXpath(NIGHTS_FROM_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        $(byXpath(String.format(NIGHTS_FROM_VALUE, nightsFromValue))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        sleep(CONSTANT_1_SECOND);

        return this;
    }

    public AnexSearchPage selectNightsTo(int nightsToValue) throws Exception{
        $(byXpath(NIGHTS_TO_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        $(byXpath(String.format(NIGHTS_TO_VALUE, nightsToValue))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        sleep(CONSTANT_1_SECOND);

        return this;
    }

    public static int getFirstActiveNightsCounterValue(int nightsValue) throws Exception{
        $(byXpath(NIGHTS_TO_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        boolean isNightsActual = false;
        int nightsValueCounter = 1;
        while($(byXpath(String.format(ACTUAL_NIGHTS_VALUE, nightsValueCounter))).isDisplayed()){
            if ($(byXpath(String.format(ACTUAL_NIGHTS_VALUE, nightsValueCounter))).getText().equals(String.valueOf(nightsValue))){
                isNightsActual = true;
                break;
            }
            nightsValueCounter ++;
        }
        int firstActualNightsCounterValue = nightsValue;
        if($(byXpath(String.format(ACTUAL_NIGHTS_VALUE, 1))).isDisplayed())
            firstActualNightsCounterValue = Integer.parseInt($(byXpath(String.format(ACTUAL_NIGHTS_VALUE, 1))).getText());
        else{
            int nightCounter = 1;
            while(!$(byXpath(String.format(EXIST_NIGHTS_VALUE, nightCounter))).isDisplayed())
                nightCounter ++;
            firstActualNightsCounterValue = Integer.parseInt($(byXpath(String.format(EXIST_NIGHTS_VALUE, nightCounter))).getText());
        }
        $(byXpath(NIGHTS_FROM_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        sleep(CONSTANT_1_SECOND);

        if (isNightsActual)
            return nightsValue;
        else
            return firstActualNightsCounterValue;

    }

    public AnexSearchPage selectResortTo(String resortToValue) throws Exception{
        $(byXpath(RESORT_INPUT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        $(byXpath(RESORT_INPUT)).clear();
        $(byXpath(RESORT_INPUT)).sendKeys(resortToValue);
        $(byXpath(String.format(RESORT_VALUE, resortToValue))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        sleep(CONSTANT_1_SECOND);

        return this;
    }

    public AnexSearchPage selectMeal(ArrayList<String> mealValue) throws Exception{
        if(!mealValue.isEmpty()){
            for(int i = 0; i<mealValue.size(); i++){
                if ($(byXpath(String.format(MEAL_CHECKBOX, hotelMealsAnex.get(mealValue.get(i))))).isDisplayed())
                    $(byXpath(String.format(MEAL_CHECKBOX, hotelMealsAnex.get(mealValue.get(i))))).waitUntil(Condition.visible, Constants.CONSTANT_2_SECONDS).click();
            }
        }

        return this;
    }

    public AnexSearchPage selectStars(ArrayList<String> starsValue) throws Exception{
        if(!starsValue.isEmpty()){
            for(int i = 0; i<starsValue.size(); i++){
                if ($(byXpath(String.format(STARS_CHECKBOX, starsValue.get(i)))).isDisplayed())
                    $(byXpath(String.format(STARS_CHECKBOX, starsValue.get(i)))).waitUntil(Condition.visible, Constants.CONSTANT_2_SECONDS).click();
            }
        }

        return this;
    }

    public AnexSearchPage clickInstantConfirmButton(boolean isInstantConfirmation) throws Exception{
        if(isInstantConfirmation)
            $(byXpath(INSTANT_CONFIRM_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        return this;
    }

    public AnexSearchPage clickSubmitSearchButton() throws Exception{
        $(byXpath(SUBMIT_SEARCH_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        return this;
    }

    public AnexSearchPage waitForSearchResult() throws Exception{
        sleep(CONSTANT_2_SECONDS);
        $(byXpath(SEARCH_PROGRESS_LABEL)).waitUntil(Condition.disappear, Constants.CONSTANT_180_SECONDS);

        return this;
    }

    private boolean ifEmptySearchResult() throws Exception{
        waitForSearchResult();
        return $(byXpath(EMPTY_SEARCH_RESULT)).isDisplayed();
    }

    public static boolean ifNoToursResult() throws Exception{
        return $(byXpath(NO_TOURS_RESULT)).isDisplayed();
    }

    public AnexSearchPage clickGroupByHotelCheckbox() throws Exception{
        waitForSearchResult();
        if(!$(byXpath(GROUP_BY_HOTEL_CHECKED)).isDisplayed())
            $(byXpath(GROUP_BY_HOTEL_CHECKBOX)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        sleep(CONSTANT_1_SECOND);
        waitForSearchResult();

        return this;
    }

    public AnexSearchPage putHotelsAmountAnexList(String departureValue, int dateCounterValue, int nightsCounterValue) throws Exception {
        String fullHotelsAmountValue = $(byXpath(HOTELS_AMOUNT_LABEL)).waitUntil(Condition.visible, CONSTANT_300_SECONDS).getText();
        if(!fullHotelsAmountValue.contains("Более"))
            setHotelsAmountAnexList(fullHotelsAmountValue.replaceAll("\\D", ""));
        else{
            int hotelCounter = 1;
            while($(byXpath(String.format(HOTEL_NAME_VALUE, hotelCounter))).isDisplayed()) {

                if ($(byXpath(String.format(HOTEL_NAME_VALUE, hotelCounter + 1))).isDisplayed()) {
                    WebElement currentHotelBlock = driver.findElement(By.xpath(String.format(HOTEL_NAME_VALUE, hotelCounter + 1)));
                    Actions actions = new Actions(driver);
                    actions.moveToElement(currentHotelBlock);
                    actions.perform();
//                    sleep(CONSTANT_1_SECOND);
                } else {
                    scrollDownOnPage();
                    sleep(CONSTANT_2_SECONDS);
                    if ($(byXpath(TOURS_LOAD_PROGRESS)).isDisplayed())
                        $(byXpath(String.format(HOTEL_NAME_VALUE, hotelCounter + 1))).waitUntil(Condition.visible, CONSTANT_30_SECONDS);
                }
                hotelCounter ++;
            }
            setHotelsAmountAnexList(String.valueOf(hotelCounter -1));
        }
        setDepartureCityAnexList(departureValue);
        setActualDateCounterList(dateCounterValue);
        setActualNightsCounterList(nightsCounterValue);

        return this;
    }

    public String getFullHotelNameValue(int hotelCounter){
        return $(byXpath(String.format(HOTEL_NAME_VALUE, hotelCounter))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).getText();
    }

    public String getHotelCategoryValue(int hotelCounter){
        return $(byXpath(String.format(HOTEL_CATEGORY_VALUE, hotelCounter))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).getText();
    }

    public String getClearHotelNameValue(int hotelCounter){
        String clearHotelNameValue = getFullHotelNameValue(hotelCounter);
        clearHotelNameValue = clearHotelNameValue.substring(0, clearHotelNameValue.indexOf(getHotelCategoryValue(hotelCounter)));
        return clearHotelNameValue;
    }

    public String getHotelResortValue(int hotelCounter){
        return $(byXpath(String.format(HOTEL_RESORT_VALUE, hotelCounter))).getText();
    }

    public String getTourPriceValue(int hotelCounter){
        return $(byXpath(String.format(TOUR_PRICE_VALUE, hotelCounter))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).getText().replaceAll("\\D", "");
    }

    public AnexTourPage gotoAnexTourPage(int hotelCounter){
        $(byXpath(String.format(TOUR_PRICE_BUTTON, hotelCounter))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        return new AnexTourPage();
    }

    public void putHotelNamesAnexList(boolean isActualizationNeeded) throws Exception {

        ArrayList<String> hotelNamesAnexTempList = new ArrayList<String>();

//        int hotelAnexCounter = 0;
//        int anexHotelCounter = 10;

        if(!ifEmptySearchResult()){
            int hotelCounter = 1;
            //int hotelExpandCounter = 1;
            while($(byXpath(String.format(HOTEL_NAME_VALUE, hotelCounter))).isDisplayed() && hotelCounter <= 500){ // && hotelCounter <= 20
                String oldTab = driver.getWindowHandle();

                if($(byXpath(String.format(HOTEL_NAME_VALUE, hotelCounter + 1))).isDisplayed()){
                    WebElement currentHotelBlock = driver.findElement(By.xpath(String.format(HOTEL_NAME_VALUE, hotelCounter + 1)));
                    Actions actions = new Actions(driver);
                    actions.moveToElement(currentHotelBlock);
                    actions.perform();
                    sleep(CONSTANT_1_SECOND);
                }
                else
                {
                    scrollDownOnPage();
                    sleep(CONSTANT_2_SECONDS);
                    if($(byXpath(TOURS_LOAD_PROGRESS)).isDisplayed())
                        $(byXpath(String.format(HOTEL_NAME_VALUE, hotelCounter + 1))).waitUntil(Condition.visible, CONSTANT_30_SECONDS);
                }

                if(!hotelNamesAnexTempList.contains($(byXpath(String.format(HOTEL_NAME_VALUE, hotelCounter))).getText())){
                    hotelNamesAnexTempList.add(getFullHotelNameValue(hotelCounter));
                    setHotelNamesAnexList(getClearHotelNameValue(hotelCounter));
                    System.out.println(getClearHotelNameValue(hotelCounter));
                    setHotelStarsAnexList(getHotelCategoryValue(hotelCounter));
                    setCountryResortAnexList(getHotelResortValue(hotelCounter));

//                    if($(byXpath(String.format(HOTEL_PRICE_EXPAND_BUTTON, hotelExpandCounter))).isDisplayed()){
//                        $(byXpath(String.format(HOTEL_PRICE_EXPAND_BUTTON, hotelExpandCounter))).click();
//                        $(byXpath(ACTUALIZATION_PROGRESS)).waitUntil(Condition.disappear, CONSTANT_60_SECONDS);
//                        setHotelTourPriceAnexList($(byXpath(String.format(TOUR_PRICE_VALUE, hotelCounter))).getText().replaceAll("\\D", ""));
//                        hotelExpandCounter ++;
//                    }

                    setHotelTourPriceAnexList($(byXpath(String.format(TOUR_PRICE_VALUE, hotelCounter))).getText().replaceAll("\\D", ""));
//                    setHotelTourPriceActualizedAnexList($(byXpath(String.format(TOUR_PRICE_VALUE, hotelCounter))).getText().replaceAll("\\D", ""));

                    //Actualization
                    if(isActualizationNeeded){
                        $(byXpath(String.format(TOUR_PRICE_BUTTON, hotelCounter))).waitUntil(Condition.visible, CONSTANT_60_SECONDS).click();

                        String hotelTab = driver.getWindowHandle();
                        ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
                        newTab.remove(hotelTab);
                        driver.switchTo().window(newTab.get(0));

                        AnexTourPage anexTourPage = new AnexTourPage();
                        anexTourPage.waitForActualizationFinished();
                        if(anexTourPage.ifErrorActualization())
                            setHotelTourPriceActualizedAnexList("-");
                        else
                            setHotelTourPriceActualizedAnexList(anexTourPage.getActualizedPriceValue());

                        sleep(Constants.CONSTANT_1_SECOND);
                        driver.close();
                        driver.switchTo().window(oldTab);
                    }
                    else{
                        setHotelTourPriceActualizedAnexList("-");
                    }

                    //////////////////////////////////////////////////////
                }

                hotelCounter ++;

            }
        }

        else
            putEmptyHotelNamesAnexList();

//        setHotelTourOfferAmountList((double) hotelAnexCounter);

        hotelNamesAnexTempList.clear();

    }

    public void putEmptyHotelNamesAnexList() throws Exception{
        setHotelNamesAnexList("-");
        setCountryResortAnexList("-");
        setHotelTourPriceAnexList("-");
//        setHotelTourPriceActualizedAnexList("-");
    }

}
