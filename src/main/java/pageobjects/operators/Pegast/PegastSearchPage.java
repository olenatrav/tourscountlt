package pageobjects.operators.Pegast;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;

import java.util.ArrayList;
import java.util.Random;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static framework.Constants.*;
import static pageobjects.operators.Pegast.PegastTourPage.setHotelTourPriceActualizedPegastList;

public class PegastSearchPage extends WebDriverCommands {

    private static PegastSearchPage pegastSearchPage = null;
    public PegastSearchPage(){
    }
    public static PegastSearchPage getPegastSearchPage(){
        if(pegastSearchPage == null)
            pegastSearchPage = new PegastSearchPage();

        checkServerErrorAndRefresh();

        return pegastSearchPage;
    }

    private final String CLOSE_JIVO_SITE_BUTTON = "//jdiv[@id = 'jivo_close_button']";
    private final String STARS_CHECKBOX = "//div[@class = 'pgs-side-filter-item__text' and text() = '%s*']";
    private final String MEAL_CHECKBOX = "//div[@class = 'pgs-side-filter-item__text' and contains(text(), '%s')]";
    private final String SUBMIT_BUTTON = "//button[contains(text(), 'Применить')]";
    private final String SEARCH_LOADER = "//div[@class = 'pgs-loader-main-circle']";
    private final String HOTEL_NAME_VALUE = "(//a[@class = 'hotel-search-item__hotel-name'])[%s]";
    private final String HOTEL_RESORT_VALUE = "(//a[@class = 'pgs-hotel-location__location'])[%s]";
    private final String HOTEL_STARS_VALUE = "(//div[@class = 'vue-star-rating'])[%s]/span[@class = 'vue-star-rating-star'][%s]";
    private final String HOTEL_PRICE_BUTTON_VALUE = "(//a[contains(@class, 'hotel-search-tour-price__price')])[%s]";
    private final String EMPTY_SEARCH_RESULTS = "//div[text() = 'Ничего не найдено']";
    private final String NEXT_PAGE_BUTTON = "//a[contains(@class, 'next-page')]";
    private final String SEARCH_RESULTS_LABEL = "//div[@class = 'hotel-search-summary-message']";

    private static ArrayList<String> hotelNamesPegastList = new ArrayList<String>();
    public static ArrayList <String> getHotelNamesPegastList(){
        return hotelNamesPegastList;
    }
    public static void setHotelNamesPegastList(String hotelNamePegastListValue){
        hotelNamesPegastList.add(hotelNamePegastListValue);
    }

    private static ArrayList<String> hotelStarsPegastList = new ArrayList<String>();
    public static ArrayList <String> getHotelStarsPegastList(){
        return hotelStarsPegastList;
    }
    public static void setHotelStarsPegastList(String hotelStarsPegastListValue){
        hotelStarsPegastList.add(hotelStarsPegastListValue);
    }

    private static ArrayList<String> countryResortPegastList = new ArrayList<String>();
    public static ArrayList <String> getCountryResortPegastList(){
        return countryResortPegastList;
    }
    public static void setCountryResortPegastList(String countryResortPegastListValue){
        countryResortPegastList.add(countryResortPegastListValue);
    }

    private static ArrayList<String> hotelTourPricePegastList = new ArrayList<String>();
    public static ArrayList <String> getHotelTourPricePegastList(){

        return hotelTourPricePegastList;
    }
    public static void setHotelTourPricePegastList(String hotelTourPricePegastListValue){
        hotelTourPricePegastList.add(hotelTourPricePegastListValue);
    }

    public PegastSearchPage closeJivoBanner() throws Exception{
        sleep(CONSTANT_1_SECOND);
        if($(byXpath(CLOSE_JIVO_SITE_BUTTON)).isDisplayed())
            $(byXpath(CLOSE_JIVO_SITE_BUTTON)).click();

        return this;
    }

    public PegastSearchPage selectMeal(ArrayList<String> mealValue) throws Exception{
        for(int i = 0; i<mealValue.size(); i++){
            scrollToElement(String.format(MEAL_CHECKBOX, hotelMealsPegast.get(mealValue.get(i))));
            $(byXpath(String.format(MEAL_CHECKBOX, hotelMealsPegast.get(mealValue.get(i))))).waitUntil(Condition.visible, Constants.CONSTANT_2_SECONDS).click();
        }

        return this;
    }

    public PegastSearchPage selectStars(ArrayList<String> starsValue) throws Exception{
        for(int i = 0; i<starsValue.size(); i++){
            scrollToElement(String.format(STARS_CHECKBOX, starsValue.get(i)));
            $(byXpath(String.format(STARS_CHECKBOX, starsValue.get(i)))).waitUntil(Condition.visible, Constants.CONSTANT_2_SECONDS).click();
        }

        return this;
    }

    public PegastSearchPage clickSearchButton() throws Exception{
        scrollToElement(SUBMIT_BUTTON);
        scrollDownOnPage();
        $(byXpath(SUBMIT_BUTTON)).waitUntil(Condition.visible, CONSTANT_60_SECONDS).click();

        return this;
    }

    public PegastSearchPage waitForSearchResults() throws Exception{
        sleep(CONSTANT_1_SECOND);
        $(byXpath(SEARCH_LOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);

        return this;
    }

    private boolean ifEmptySearchResult() throws Exception{
        waitForSearchResults();
        return $(byXpath(EMPTY_SEARCH_RESULTS)).isDisplayed();
    }

    public String getHotelNameValue(int hotelCounter) throws Exception {
        return $(byXpath(String.format(HOTEL_NAME_VALUE, hotelCounter))).waitUntil(Condition.visible, CONSTANT_60_SECONDS).getText();
    }

    public String getHotelResortValue(int hotelCounter) throws Exception {
        return $(byXpath(String.format(HOTEL_RESORT_VALUE, hotelCounter))).waitUntil(Condition.visible, CONSTANT_60_SECONDS).getText();
    }

    public String getHotelPriceValue(int hotelCounter) throws Exception {
        return $(byXpath(String.format(HOTEL_PRICE_BUTTON_VALUE, hotelCounter))).waitUntil(Condition.visible, CONSTANT_60_SECONDS).getText().replaceAll("\\D", "");
    }

    public String getSearchResultsValue() throws Exception {
        return $(byXpath(SEARCH_RESULTS_LABEL)).waitUntil(Condition.visible, CONSTANT_60_SECONDS).getText().replaceAll("\\D", "");
    }

    public void putHotelNamesPegastList(String adultsValue) throws Exception {

        Random random = new Random();

        scrollUpOnPage();
        if(!ifEmptySearchResult()){
            do{
                int hotelCounter = 1;
                while($(byXpath(String.format(HOTEL_NAME_VALUE, hotelCounter))).isDisplayed() && hotelCounter <= 20){
                    String oldTab = driver.getWindowHandle();

                    setHotelNamesPegastList(getHotelNameValue(hotelCounter));
                    setCountryResortPegastList(getHotelResortValue(hotelCounter));
                    setHotelTourPricePegastList(getHotelPriceValue(hotelCounter));

                    /////////////////////////////////////////////////Actualization
                    scrollToElement(String.format(HOTEL_PRICE_BUTTON_VALUE, hotelCounter));
                    scrollDownOnSomePxl(100);
                    $(byXpath(String.format(HOTEL_PRICE_BUTTON_VALUE, hotelCounter))).waitUntil(Condition.visible, CONSTANT_60_SECONDS).click();

                    String hotelTab = driver.getWindowHandle();
                    ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
                    newTab.remove(hotelTab);
                    driver.switchTo().window(newTab.get(0));

                    PegastHotelPage pegastHotelPage = PegastHotelPage.getPegastHotelPage();
                    sleep(random.nextInt(5000));

                    pegastHotelPage
                            .reloadPage()
                            .closeJivoBanner()
                            .putHotelRatingValue()
                            .gotoPegastTourPage();

                    driver.close();
                    newTab = new ArrayList<String>(driver.getWindowHandles());
                    sleep(Constants.CONSTANT_1_SECOND);
                    newTab.remove(oldTab);
                    driver.switchTo().window(newTab.get(0));

                    PegastTourPage pegastTourPage = PegastTourPage.getPegastTourPage();
                    sleep(random.nextInt(5000));

                    pegastTourPage
                            .reloadPage()
                            .closeJivoBanner()
//                        .selectCheapestInsurance(Integer.parseInt(adultsValue))
                            .putActualizedPriceValue();

                    sleep(Constants.CONSTANT_1_SECOND);
                    driver.close();
                    driver.switchTo().window(oldTab);

                    //////////////////////////////////////////////////////

                    hotelCounter ++;
                }

                if($(byXpath(NEXT_PAGE_BUTTON)).isDisplayed() && hotelCounter <= Integer.parseInt(getSearchResultsValue())){
                    $(byXpath(NEXT_PAGE_BUTTON)).click();
                    waitForSearchResults();
                }
                else
                    break;

            }
            while ($(byXpath(String.format(HOTEL_NAME_VALUE, 1))).isDisplayed());

        }
        else
            putEmptyHotelNamesPegastList();

    }

    public void putEmptyHotelNamesPegastList() throws Exception{
        setHotelNamesPegastList("-");
        setCountryResortPegastList("-");
        setHotelTourPricePegastList("-");
        setHotelTourPriceActualizedPegastList("-");
    }

}
