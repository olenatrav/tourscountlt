package pageobjects.operators.Pegast;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static framework.Constants.*;
import static pageobjects.operators.Pegast.PegastSearchPage.setHotelStarsPegastList;

public class PegastHotelPage extends WebDriverCommands {

    private static PegastHotelPage pegastHotelPage = null;
    public PegastHotelPage(){
    }
    public static PegastHotelPage getPegastHotelPage(){
        if(pegastHotelPage == null)
            pegastHotelPage = new PegastHotelPage();

        checkServerErrorAndRefresh();

        return pegastHotelPage;
    }

    private final String CLOSE_JIVO_SITE_BUTTON = "//jdiv[@id = 'jivo_close_button']";
    private final String HOTEL_RATING_VALUE = "(//div[contains(@class, 'vue-star-rating-inline')])[1]";
    private final String BUY_TOUR_BUTTON = "(//div[@class = 'hotel-tour__actions']/a)[1]";
    private final String HOTEL_PAGE_HEADER = "//header[@class = 'pgs-header']";

    public boolean isHotelPageLoad() throws Exception{
        return $(byXpath(HOTEL_PAGE_HEADER)).isDisplayed();
    }

    public PegastHotelPage reloadPage() throws Exception{
        while(!$(byXpath(HOTEL_PAGE_HEADER)).isDisplayed()){
            System.out.println("Too many requests on hotel page!");
            sleep(CONSTANT_2_SECONDS);
            refreshPage();
        }
        return this;
    }

    public PegastHotelPage closeJivoBanner() throws Exception{
        sleep(CONSTANT_1_SECOND);
        if($(byXpath(CLOSE_JIVO_SITE_BUTTON)).isDisplayed())
            $(byXpath(CLOSE_JIVO_SITE_BUTTON)).click();

        return this;
    }

    public PegastHotelPage putHotelRatingValue() throws Exception{
        setHotelStarsPegastList($(byXpath(HOTEL_RATING_VALUE)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).getAttribute("rating"));

        return this;
    }

    public PegastTourPage gotoPegastTourPage() throws Exception{
        sleep(CONSTANT_2_SECONDS);
        scrollToElement(BUY_TOUR_BUTTON);
        scrollDownOnSomePxl(130);
        $(byXpath(BUY_TOUR_BUTTON)).waitUntil(Condition.visible, CONSTANT_60_SECONDS).click();

        return new PegastTourPage();
    }

}
