package pageobjects.operators.Pegast;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;

import java.util.ArrayList;
import java.util.Arrays;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static framework.Constants.*;

public class PegastMainPage extends WebDriverCommands {

    private static PegastMainPage pegastMainPage = null;
    public PegastMainPage(){
    }
    public static PegastMainPage getPegastMainPage(){
        if(pegastMainPage == null)
            pegastMainPage = new PegastMainPage();

        checkServerErrorAndRefresh();

        return pegastMainPage;
    }

    private final String PEGAST_START_URL = "https://pegast.ru/";
    private final String FROM_DROPDOWN_LABEL = "//label[text() = 'Откуда']";
    private final String FROM_DROPDOWN_VALUE = "//li[contains(text(), '%s')]";
    private final String TO_DROPDOWN_LABEL = "//label[text() = 'Куда']";
    private final String TO_DROPDOWN_VALUE = "//span[@class = 'suggestion-name' and text() = '%s']";
    private final String DATE_DROPDOWN_LABEL = "//label[text() = 'Дата вылета']";
    private final String DATE_DROPDOWN_MONTH_TAB = "//div[contains(@class, 'pgs-month-selector-panel__item') and contains(text(), '%s')]";
    private final String DATE_DROPDOWN_VALUE = "//span[contains(@class, 'label-month') and contains(text(), '%s')]/ancestor::div[@class = 'pgs-datepicker-pane']/.//span[contains(@class, 'item_selectable') and text() = '%s']";
    private final String NIGHTS_DROPDOWN_VALUE = "(//li[contains(@class, 'duration_selectable') and contains(text(), '%s')])[1]";
    private final String DATE_NIGHTS_APPLY_BUTTON = "//button[contains(@class, 'action_apply')]";
    private final String SUBMIT_BUTTON = "//button[contains(text(), 'Найти туры')]";

    public PegastMainPage gotoPegastMainPage() throws Exception{
        goToPage(PEGAST_START_URL);

        return this;
    }

    public PegastMainPage selectCityFrom(String cityFromValue) throws Exception{
        $(byXpath(FROM_DROPDOWN_LABEL)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        $(byXpath(String.format(FROM_DROPDOWN_VALUE, cityFromValue))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        return this;
    }

    public PegastMainPage selectCountryTo(String countryToValue) throws Exception{
        $(byXpath(TO_DROPDOWN_LABEL)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        $(byXpath(String.format(TO_DROPDOWN_VALUE, countryToValue))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        return this;
    }

    public PegastMainPage selectDateNights(String flightDateValue, int nightsValue) throws Exception{
        //date
        $(byXpath(DATE_DROPDOWN_LABEL)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        ArrayList<String> dateList = new ArrayList<String>(Arrays.asList(flightDateValue.split("\\s+")));
        $(byXpath(String.format(DATE_DROPDOWN_MONTH_TAB, monthsRussianCut.get(dateList.get(1))))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        $(byXpath(String.format(DATE_DROPDOWN_VALUE, monthsRussianCapital.get(dateList.get(1)), dateList.get(0)))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        //nights
        $(byXpath(String.format(NIGHTS_DROPDOWN_VALUE, nightsValue))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        //apply
        scrollToElement(DATE_NIGHTS_APPLY_BUTTON);
        scrollDownOnSomePxl(100);
        $(byXpath(DATE_NIGHTS_APPLY_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        return this;
    }

    public PegastSearchPage gotoPegastSearchPage() throws Exception{
        $(byXpath(SUBMIT_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        return new PegastSearchPage();
    }

}
