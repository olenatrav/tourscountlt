package pageobjects.operators.Pegast;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;

import java.util.ArrayList;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static framework.Constants.CONSTANT_1_SECOND;
import static framework.Constants.CONSTANT_2_SECONDS;

public class PegastTourPage extends WebDriverCommands {

    private static PegastTourPage pegastTourPage = null;
    public PegastTourPage(){
    }
    public static PegastTourPage getPegastTourPage(){
        if(pegastTourPage == null)
            pegastTourPage = new PegastTourPage();

        checkServerErrorAndRefresh();

        return pegastTourPage;
    }

    private final String CLOSE_JIVO_SITE_BUTTON = "//jdiv[@id = 'jivo_close_button']";
    private final String ACTUALIZED_PRICE_VALUE = "//span[@class = 'totalLocalePriceString']";
    private final String EXTRA_MEAL_PRICE_VALUE = "//span[@class = 'extra-meals-item__order-price extra-meals-item__result-price']/span/span[@class = 'price__payment-currency']";
    private final String INSURANCE_EDIT_ICON = "(//div[contains(@class, 'insurance-edit')])[1]";
    private final String ALERT_WINDOW_BUTTON = "//div[contains(@class, 'close_pegasys_alert_window')]";
    private final String FIRST_INSURANCE_LABEL = "(//div[@class = 'ins-inner-wrap'])[1]";
    private final String SAVE_INSURANCE_BUTTON = "//div[@class = 'main-button save_insurances']";
    private final String LOADING_POPUP = "//div[@class= 'notification']";
    private final String TOUR_PAGE_HEADER = "//header[@class = 'pgs-header']";

    private static ArrayList<String> hotelTourPriceActualizedPegastList = new ArrayList<String>();
    public static ArrayList <String> getHotelTourPriceActualizedPegastList(){
        return hotelTourPriceActualizedPegastList;
    }
    public static void setHotelTourPriceActualizedPegastList(String hotelTourPriceActualizedPegastListValue){
        hotelTourPriceActualizedPegastList.add(hotelTourPriceActualizedPegastListValue);
    }

    public boolean isTourPageLoad() throws Exception{
        return $(byXpath(TOUR_PAGE_HEADER)).isDisplayed();
    }

    public PegastTourPage reloadPage() throws Exception{
        while(!$(byXpath(TOUR_PAGE_HEADER)).isDisplayed()){
            System.out.println("Too many requests on tour page!");
            sleep(CONSTANT_2_SECONDS);
            refreshPage();
        }
        return this;
    }

    public PegastTourPage closeJivoBanner() throws Exception{
        sleep(CONSTANT_1_SECOND);
        if($(byXpath(CLOSE_JIVO_SITE_BUTTON)).isDisplayed())
            $(byXpath(CLOSE_JIVO_SITE_BUTTON)).click();

        return this;
    }

    public String getFullActualizedPriceValue() throws Exception{
        String fullPriceValue = $(byXpath(ACTUALIZED_PRICE_VALUE)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).getText();
        String priceValue = fullPriceValue.substring(0, fullPriceValue.indexOf(".")).replaceAll("\\D", "");
        return priceValue;
    }

    public String getExtraMealPriceValue() throws Exception{
        return $(byXpath(EXTRA_MEAL_PRICE_VALUE)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).getText().replaceAll("\\D", "");
    }

    public PegastTourPage closeErrorPopup() throws Exception{
        while($(byXpath(ALERT_WINDOW_BUTTON)).isDisplayed()){
            $(byXpath(ALERT_WINDOW_BUTTON)).click();
            sleep(Constants.CONSTANT_1_SECOND);
        }

        return this;
    }

    public PegastTourPage selectCheapestInsurance(int touristCounter) throws Exception{
        int insuranceCounter = 1;
        while(insuranceCounter <= touristCounter){
            $(byXpath(INSURANCE_EDIT_ICON)).click();
            $(byXpath(LOADING_POPUP)).waitUntil(Condition.disappear, Constants.CONSTANT_20_SECONDS);
            closeErrorPopup();
            $(byXpath(FIRST_INSURANCE_LABEL)).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).click();
            $(byXpath(LOADING_POPUP)).waitUntil(Condition.disappear, Constants.CONSTANT_20_SECONDS);
            closeErrorPopup();
            while ($(SAVE_INSURANCE_BUTTON).isDisplayed()){
                $(SAVE_INSURANCE_BUTTON).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).click();
                $(byXpath(LOADING_POPUP)).waitUntil(Condition.disappear, Constants.CONSTANT_20_SECONDS);
                closeErrorPopup();
                sleep(Constants.CONSTANT_1_SECOND);
            }

            insuranceCounter ++;
        }

        return this;
    }

    public PegastTourPage putActualizedPriceValue() throws Exception{
        setHotelTourPriceActualizedPegastList(String.valueOf(Integer.parseInt(getFullActualizedPriceValue()) - Integer.parseInt(getExtraMealPriceValue())));

        return this;
    }

    public PegastTourPage putEmptyActualizedPriceValue() throws Exception{
        setHotelTourPriceActualizedPegastList("-");

        return this;
    }

}
