package pageobjects.operators.Alean;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;

import java.util.ArrayList;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class AleanTourPage extends WebDriverCommands {

    private static AleanTourPage aleanTourPage = null;

    public AleanTourPage(){
    }
    public static AleanTourPage getAleanTourPage(){
        if(aleanTourPage == null)
            aleanTourPage = new AleanTourPage();

        checkServerErrorAndRefresh();

        return aleanTourPage;
    }

    private final String ACTUALIZATION_PROGRESS_BAR = "//div[@class = 'progress-bar']";
    private final String ERROR_POPUP = "//div[@role = 'dialog']";
    private final String FINAL_PRICE_VALUE = "//span[contains(@class, 'bookingTotalCost-sumValue')]";

    private static ArrayList<String> hotelTourPriceActualizedAleanList = new ArrayList<String>();
    public static ArrayList <String> getHotelTourPriceActualizedAleanList(){
        return hotelTourPriceActualizedAleanList;
    }
    public static void setHotelTourPriceActualizedAleanList(String hotelTourPriceActualizedAleanListValue){
        hotelTourPriceActualizedAleanList.add(hotelTourPriceActualizedAleanListValue);
    }

    public AleanTourPage waitForActualizationFinish() throws Exception{
        $(byXpath(ACTUALIZATION_PROGRESS_BAR)).waitUntil(Condition.disappear, Constants.CONSTANT_300_SECONDS);
        sleep(Constants.CONSTANT_3_SECONDS);

        return this;
    }

    public AleanTourPage putFinalAleanPrice() throws Exception{
        if ($(byXpath(ERROR_POPUP)).isDisplayed() || driver.getTitle().isEmpty())
            setHotelTourPriceActualizedAleanList("-");
        else
            setHotelTourPriceActualizedAleanList($(byXpath(FINAL_PRICE_VALUE)).waitUntil(Condition.visible, Constants.CONSTANT_120_SECONDS).getText().replaceAll("\\s", ""));

        return this;
    }

}
