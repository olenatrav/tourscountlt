package pageobjects.operators.Alean;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;
import java.util.ArrayList;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static pageobjects.operators.Alean.AleanTourPage.setHotelTourPriceActualizedAleanList;

public class AleanMainPage extends WebDriverCommands {

    private static AleanMainPage aleanMainPage = null;

    public AleanMainPage(){
    }
    public static AleanMainPage getAleanMainPage(){
        if(aleanMainPage == null)
            aleanMainPage = new AleanMainPage();

        checkServerErrorAndRefresh();

        return aleanMainPage;
    }

    private final String ALEAN_START_URL = "https://www.alean.ru/agency/";
//    private final String TOURS_TAB = "(//div[@class = 'finderHome-menuItem'])[1]";
    private final String TOURS_TAB = "//div[text() = 'Туры с авиабилетами']";
    private final String CITY_FROM_SELECT = "//select[@id = 'finder-tspTownSelect']";
    private final String CITY_FROM_SELECTED_VALUE = "//select[@id = 'finder-tspTownSelect']/option[contains(@label, '%s')]";
    private final String COUNTRY_RESORT_TO_SELECT = "//input[@id = 'finderLook-input']";
    private final String COUNTRY_RESORT_TO_SELECTED_VALUE = "(//strong[contains(text(), '%s')])[1]";
    private final String DATE_SELECT = "//input[@id = 'iDrPicker-input']";
    private final String AGENCY_BUTTON = "//button[contains(@class, 'btn-agency')]";
    private final String SUBMIT_BUTTON = "//button[contains(@class, '-submit')]";
    private final String MAILING_POPUP_LINK = "//div[@class = 'popmechanic-conten']";
    private final String MAILING_POPUP = "//div[@id = 'popmechanic-main']";
    private final String CLOSE_MAILING_POPUP_BUTTON = "//div[@class = 'popmechanic-close']";
    private final String FIND_TOURS_PROGRESS = "//div[contains(@class, 'progress-striped active')]";
    private static final String INSTANT_CONFIRMATION_CHECKBOX = "//span[text() = 'Только гарантированные места']";
    private static final String TOUR_SORT_SELECT = "//select[contains(@class, 'resultGrid-sort')]";
    private static final String TOUR_SORT_SELECT_BY_ASC = "//option[@value = 'string:byCostAsc']";
    private static final String EMPTY_RESULT = "//div[contains(text(), 'по вашему запросу ничего не найдено')]";
    private static final String TOUR_BLOCK = "(//div[contains(@ng-repeat, 'productsOrder')])[%s]";
    private static final String TOUR_HOTEL_NAME = "(//span[contains(@ng-bind, 'name')])[%s]";
    private static final String TOUR_HOTEL_PRICE = "(//span[contains(@ng-bind, 'finalCost')])[%s]";

    private static final String NEXT_PAGE_BUTTON = "//a[@ng-click= 'selectPage(page + 1, $event)']";
    private static final String NEXT_PAGE_BUTTON_DISABLED = "//a[@ng-click= 'selectPage(page + 1, $event)' and @disabled]";
    private static final String ALEAN_TOURS_COUNTER = "//b[contains(@ng-bind, 'productsCount')]";
    private static final String PAGINATION_PANEL = "//ul[@uib-pagination]";

    private static final String DISABLED_HOTEL_BLOCK = "//div[@class = 'result ng-scope ng-isolate-scope blockPending']";
    private final String TOGO_HOTEL_PAGE_BUTTON = "(//div[contains(@class, 'checkoutBtn')])[%s]";

    private static ArrayList<String> hotelNamesAleanList = new ArrayList<String>();
    public static ArrayList <String> getHotelNamesAleanList(){
        return hotelNamesAleanList;
    }
    public static void setHotelNamesAleanList(String hotelNameAleanListValue){
        hotelNamesAleanList.add(hotelNameAleanListValue);
    }

    private static ArrayList<String> countryResortAleanList = new ArrayList<String>();
    public static ArrayList <String> getCountryResortAleanList(){
        return countryResortAleanList;
    }
    public static void setCountryResortAleanList(String countryResortAleanListValue){
        countryResortAleanList.add(countryResortAleanListValue);
    }

    private static ArrayList<String> hotelTourPriceAleanList = new ArrayList<String>();
    public static ArrayList <String> getHotelTourPriceAleanList(){
        return hotelTourPriceAleanList;
    }
    public static void setHotelTourPriceAleanList(String hotelTourPriceAleanListValue){
        hotelTourPriceAleanList.add(hotelTourPriceAleanListValue);
    }

    public static ArrayList<Double> hotelTourAleanAmountList = new ArrayList<Double>();
    public static ArrayList <Double> getHotelTourAleanAmountList(){
        return hotelTourAleanAmountList;
    }
    public static void setHotelTourAleanAmountList(Double hotelTourAleanAmountListValue){
        hotelTourAleanAmountList.add(hotelTourAleanAmountListValue);
    }

    public static String getTourBlock() {
        return TOUR_BLOCK;
    }

    public static String getNextPageButton() {
        return NEXT_PAGE_BUTTON;
    }

    public static String getNextPageButtonDisabled() {
        return NEXT_PAGE_BUTTON_DISABLED;
    }

    public static String getPaginationPanel() {
        return PAGINATION_PANEL;
    }

    public AleanMainPage gotoAleanMainPage() throws Exception{
        goToPage(ALEAN_START_URL);
        refreshPage();

        return new AleanMainPage();
    }

    public AleanMainPage clickTourTab() throws Exception{
        sleep(Constants.CONSTANT_1_SECOND);
        $(byXpath(TOURS_TAB)).waitUntil(Condition.exist, Constants.CONSTANT_60_SECONDS).click();

        return this;
    }

    public boolean checkAndCloseMailingPopup() throws Exception{
        if ($(byXpath(MAILING_POPUP)).isDisplayed()){
            $(byXpath(CLOSE_MAILING_POPUP_BUTTON)).click();
            return true;
        }
        else
            return false;
    }

    public AleanMainPage closeMailingPopup() throws Exception{
        sleep(Constants.CONSTANT_3_SECONDS);
        if(!checkAndCloseMailingPopup()){
            $(byXpath(MAILING_POPUP_LINK)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
            $(byXpath(MAILING_POPUP)).waitUntil(Condition.visible, Constants.CONSTANT_10_SECONDS);
            $(byXpath(CLOSE_MAILING_POPUP_BUTTON)).click();
            sleep(Constants.CONSTANT_1_SECOND);
        }


        return this;
    }

    private AleanMainPage selectCityFrom(String cityFromValue) throws Exception{
        sleep(Constants.CONSTANT_1_SECOND);
        $(byXpath(CITY_FROM_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        $(byXpath(String.format(CITY_FROM_SELECTED_VALUE, cityFromValue))).waitUntil(Condition.visible, Constants.CONSTANT_10_SECONDS).click();

        return this;
    }

    private AleanMainPage selectCountryResortTo(String countryResortToValue) throws Exception{
        $(byXpath(COUNTRY_RESORT_TO_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).clear();
        $(byXpath(COUNTRY_RESORT_TO_SELECT)).sendKeys(countryResortToValue);
        $(byXpath(String.format(COUNTRY_RESORT_TO_SELECTED_VALUE, countryResortToValue))).waitUntil(Condition.visible, Constants.CONSTANT_10_SECONDS).click();

        return this;
    }

    private AleanMainPage selectDate(String dateFromValue, String dateToValue) throws Exception{
        $(byXpath(DATE_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).clear();
        $(byXpath(DATE_SELECT)).sendKeys(dateFromValue + dateToValue);

        return this;
    }

    private AleanMainPage clickStartSearchButton() throws Exception{
        $(byXpath(SUBMIT_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        return this;
    }

    private AleanMainPage clickAgencyButton() throws Exception{
        sleep(Constants.CONSTANT_1_SECOND);
        if ($(byXpath(AGENCY_BUTTON)).isDisplayed())
            $(byXpath(AGENCY_BUTTON)).click();

        return this;
    }

    public void clickInstantConfirmationCheckbox(boolean isInstantConfirmation) throws Exception{
        if(isInstantConfirmation){
            $(byXpath(INSTANT_CONFIRMATION_CHECKBOX)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
            sleep(Constants.CONSTANT_1_SECOND);
            waitForHotelBlockActive();
        }
    }

    public static void clickSortByAsc() throws Exception{
        $(byXpath(TOUR_SORT_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        $(byXpath(TOUR_SORT_SELECT_BY_ASC)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        sleep(Constants.CONSTANT_1_SECOND);
        waitForHotelBlockActive();
    }

    public AleanMainPage getToursAmountByTourCriteria(String cityFromValue, String countryResortToValue, String dateFromValue, String dateToValue) throws Exception{
        selectCityFrom(cityFromValue);
        selectCountryResortTo(countryResortToValue);
        selectDate(dateFromValue, dateToValue);
        clickStartSearchButton();
        clickAgencyButton();
        sleep(Constants.CONSTANT_1_SECOND);
        $(byXpath(FIND_TOURS_PROGRESS)).waitUntil(Condition.disappear, Constants.CONSTANT_300_SECONDS);

        return this;
    }

    public static String getAleanToursCounterValue() throws Exception{

        return $(byXpath(ALEAN_TOURS_COUNTER)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).getText();
    }

    public static boolean ifEmptyAleanSearch() throws Exception{
        if ($(byXpath(EMPTY_RESULT)).isDisplayed())
            return true;
        else
            return false;
    }

    public static void putHotelNamePriceAleanList(String countryResortToValue, int hotelCounter) throws Exception{
        setHotelNamesAleanList($(byXpath(String.format(TOUR_HOTEL_NAME, hotelCounter))).waitUntil(Condition.visible, Constants.CONSTANT_10_SECONDS).getText());
        setCountryResortAleanList(countryResortToValue);
        setHotelTourPriceAleanList($(byXpath(String.format(TOUR_HOTEL_PRICE, hotelCounter))).waitUntil(Condition.visible, Constants.CONSTANT_10_SECONDS).getText());
        setHotelTourPriceActualizedAleanList($(byXpath(String.format(TOUR_HOTEL_PRICE, hotelCounter))).waitUntil(Condition.visible, Constants.CONSTANT_10_SECONDS).getText());

    }

//    public AleanMainPage putHotelNamesAleanList(String countryResortToValue) throws Exception{
//        if ($(byXpath(EMPTY_RESULT)).isDisplayed()){
//            setHotelNamesAleanList("-");
//            setCountryResortAleanList(countryResortToValue);
//            setHotelTourPriceAleanList("-");
//        }
//        else{
//            clickSortByAsc();
//            int hotelCounterSpot = 0;
//            do{
//                int hotelCounter = 1;
//                while ($(byXpath(String.format(TOUR_BLOCK, hotelCounter))).isDisplayed()){
//                    putHotelNamePriceAleanList(countryResortToValue, hotelCounter);
//                    hotelCounterSpot ++;
//
//                    hotelCounter ++;
//                }
//                if($(byXpath(NEXT_PAGE_BUTTON)).isDisplayed())
//                    $(byXpath(NEXT_PAGE_BUTTON)).click();
//            }
//            while (!(getAleanToursCounterValue().equals(String.valueOf(hotelCounterSpot)) && $(byXpath(NEXT_PAGE_BUTTON_DISABLED)).isDisplayed())); //!$(byXpath(NEXT_PAGE_BUTTON_DISABLED)).isDisplayed() &&
//        }
//
//        return this;
//    }

    public static void waitForHotelBlockActive() throws Exception{
        $(byXpath(DISABLED_HOTEL_BLOCK)).waitUntil(Condition.disappear, Constants.CONSTANT_120_SECONDS);

    }

    public AleanHotelPage gotoAleanHotelPage(int hotelCounterValue) throws Exception{
        waitForHotelBlockActive();
        $(byXpath(String.format(TOGO_HOTEL_PAGE_BUTTON, hotelCounterValue))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        return new AleanHotelPage();
    }

}
