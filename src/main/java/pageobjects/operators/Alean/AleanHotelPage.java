package pageobjects.operators.Alean;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;

import static com.codeborne.selenide.Selectors.by;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class AleanHotelPage extends WebDriverCommands {

    private static AleanHotelPage aleanHotelPage = null;

    public AleanHotelPage(){
    }
    public static AleanHotelPage getAleanHotelPage(){
        if(aleanHotelPage == null)
            aleanHotelPage = new AleanHotelPage();

        checkServerErrorAndRefresh();

        return aleanHotelPage;
    }

    private static final String PROGRESS_BAR = "//div[contains(@class, 'progress-striped active')]";
    private static final String PROGRESS_BAR_LOADER = "//div[@class = 'progressBarLoader']";
    private static final String CALCULATE_BUTTON = "//button[contains(@class, 'finderProductObj-submit')]";
    private static final String EXPAND_BUTTON = "(//div[@class = 'resultProductRoomsCollapsed-expand'])[1]";
    private static final String COLLAPSE_BUTTON = "(//div[@class = 'resultProductRoomsExpanded-collapse'])[1]";
    private static final String CHEAPEST_ROOM_LINK = "(//span[text() = 'В наличии'])[1]";
    private static final String CHEAPEST_SUBMIT_BUTTON = "(//button[text() = 'Забронировать'])[1]";
    private static final String NO_TOURS_LABEL = "//div[contains(text(), 'К сожалению, по вашему запросу ничего не найдено')]";
    private static final String EMPTY_RESULT_LABEL = "//div[contains(text(), 'Empty session')]";

    public AleanHotelPage waitForProgressBarDisappear() throws Exception{
        $(byXpath(PROGRESS_BAR)).waitUntil(Condition.disappear, Constants.CONSTANT_120_SECONDS);
        sleep(Constants.CONSTANT_1_SECOND);

        return this;
    }

    public static boolean ifNoToursFound() throws Exception{
        return $(byXpath(NO_TOURS_LABEL)).isDisplayed();
    }



    public static boolean ifNoPossibleToOrder() throws Exception{
        $(byXpath(PROGRESS_BAR_LOADER)).waitUntil(Condition.disappear, Constants.CONSTANT_120_SECONDS);
        $(byXpath(CALCULATE_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        sleep(Constants.CONSTANT_1_SECOND);
        $(byXpath(PROGRESS_BAR_LOADER)).waitUntil(Condition.disappear, Constants.CONSTANT_120_SECONDS);

        return ($(byXpath(NO_TOURS_LABEL)).isDisplayed() || $(byXpath(EMPTY_RESULT_LABEL)).isDisplayed());
    }

    public static AleanTourPage gotoAleanTourPage() throws Exception{
        $(byXpath(EXPAND_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click(); //CHEAPEST_ROOM_LINK
        if(!$(byXpath(COLLAPSE_BUTTON)).isDisplayed())
            $(byXpath(EXPAND_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        $(byXpath(CHEAPEST_SUBMIT_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        return new AleanTourPage();
    }

}
