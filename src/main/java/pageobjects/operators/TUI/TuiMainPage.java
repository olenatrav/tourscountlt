package pageobjects.operators.TUI;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.Arrays;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static framework.Constants.monthsRussian;
import static framework.Constants.monthsRussianNumber;

public class TuiMainPage extends WebDriverCommands {

    private  static TuiMainPage tuiMainPage = null;
    public TuiMainPage(){
    }
    public static TuiMainPage getTuiMainPage(){
        if(tuiMainPage == null)
            tuiMainPage = new TuiMainPage();

        return tuiMainPage;
    }

    private final String TUI_START_URL = "https://www.tui.ru/";
    private final String SUBMIT_COOKIE_POLICY_BUTTON = "//button[contains(@class, 'CookiePolicy')]";
    private final String CITY_FROM_BAR = "//div[@id = 'departureCity']";
    private final String CITY_FROM_CHECKBOX = "//label[contains(@for, 'direction_from')]/.//span[text() = '%s']";
    private final String COUNTRY_TO_BAR = "//div[@id = 'arrival']";
    private final String COUNTRY_TO_CHECKBOX = "//label[contains(@for, 'ArrivalCountry')]/.//span[text() = '%s']";
    private final String RESORT_TO_CHECKBOX = "//label[contains(@for, 'ArrivalRegion')]/.//span[text() = '%s']";
    private final String DESTINATION_CHECKBOX_CHECKED = "//div[text() = 'Город']/following::div[contains(@class, 'item-checked')]/.//span[@class = 'item-label']";
    private final String DATE_BAR = "//div[@id = 'departure_date']";
    private final String DATE_CONTAINER = "//div[contains(@class, 'date-from')]/.//div[@class = 'suggestions-body']";
    private final String DATE_MONTH_YEAR_LABEL = "//div[@class = 'react-datepicker__current-month']";
    private final String DATE_DAY_VALUE = "//div[@aria-label = 'month-2020-%s']/.//div[contains(@class, 'react-datepicker__day') and text()='%s']";
    private final String NEXT_ARROW = "//button[contains(@class, 'react-datepicker__navigation--next')]";
    private final String NIGHTS_BAR = "//div[@id = 'nights']";
    private final String NIGHTS_FROM_VALUE = "(//div[@class = 'counter-value-wrapper'])[1]/div[@class = 'value']";
    private final String NIGHTS_FROM_DECREMENT = "(//div[@class = 'counter-button decrement '])[1]";
    private final String NIGHTS_FROM_INCREMENT = "(//div[@class = 'counter-button increment '])[1]";
    private final String NIGHTS_TO_VALUE = "(//div[@class = 'counter-value-wrapper'])[2]/div[@class = 'value']";
    private final String NIGHTS_TO_DECREMENT = "(//div[@class = 'counter-button decrement '])[2]";
    private final String NIGHTS_TO_INCREMENT = "(//div[@class = 'counter-button increment '])[2]";
    private final String TOURISTS_BAR = "//div[@id = 'tourists']";
    private final String SUBMIT_BUTTON = "//button[@id = 'search-page-button-id']";
    private final String WIDGET_FRAME_1 = "//iframe[@id = 'fl-402437']";
    private final String WIDGET_FRAME_2 = "//iframe[@id = 'fl-402438']";
    private final String WIDGET_FORM_CLOSE_BUTTON = "//button[@class = 'widget__close']";

    public TuiMainPage gotoTuiMainPage() throws Exception{
        if (!getCurrentUrl().equals(TUI_START_URL))
            goToPage(TUI_START_URL);

        return this;
    }

    public TuiMainPage clickSubmitCookiePolicyButton() throws Exception{
        $(byXpath(SUBMIT_COOKIE_POLICY_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_5_SECONDS).click();

        return this;
    }

    private void closeWidgetForm() throws Exception{
        sleep(Constants.CONSTANT_3_SECONDS);

        if ($(byXpath(WIDGET_FRAME_1)).isDisplayed()){
            driver.switchTo().frame($(byXpath(WIDGET_FRAME_1)));
            $(byXpath(WIDGET_FORM_CLOSE_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_10_SECONDS).click();
            driver.switchTo().defaultContent();
        }
        if ($(byXpath(WIDGET_FRAME_2)).isDisplayed()){
            driver.switchTo().frame($(byXpath(WIDGET_FRAME_2)));
            $(byXpath(WIDGET_FORM_CLOSE_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_10_SECONDS).click();
            driver.switchTo().defaultContent();
        }

    }

    public TuiMainPage selectCityFrom(String cityFromValue) throws Exception{
        closeWidgetForm();
        $(byXpath(CITY_FROM_BAR)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        $(byXpath(String.format(CITY_FROM_CHECKBOX, cityFromValue))).waitUntil(Condition.visible, Constants.CONSTANT_2_SECONDS).click();

        scrollUpOnPage();

        return this;
    }

    public TuiMainPage selectDestination(String countryValue, String resortValue) throws Exception{
        closeWidgetForm();
        $(byXpath(COUNTRY_TO_BAR)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
//        $(byXpath(String.format(COUNTRY_TO_CHECKBOX, countryValue))).waitUntil(Condition.visible, Constants.CONSTANT_10_SECONDS).scrollTo();
        $(byXpath(String.format(COUNTRY_TO_CHECKBOX, countryValue))).click();
        sleep(Constants.CONSTANT_1_SECOND);
        while($(byXpath(DESTINATION_CHECKBOX_CHECKED)).isDisplayed())
            $(byXpath(DESTINATION_CHECKBOX_CHECKED)).click();
        if(!resortValue.equals("-"))
            $(byXpath(String.format(RESORT_TO_CHECKBOX, resortValue))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        scrollUpOnPage();

        return this;
    }

    public TuiMainPage selectDateFrom(String dateFromValue) throws Exception{
        closeWidgetForm();
        $(byXpath(DATE_BAR)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        ArrayList<String> dateList = new ArrayList<String>(Arrays.asList(dateFromValue.split("\\s+")));
        $(byXpath(DATE_CONTAINER)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS);
        while (!$(byXpath(DATE_MONTH_YEAR_LABEL)).getText().toLowerCase().equals(monthsRussian.get(dateList.get(1)) + " " + dateList.get(2)))
            $(byXpath(NEXT_ARROW)).click();
        $(byXpath(String.format(DATE_DAY_VALUE, monthsRussianNumber.get(dateList.get(1)), dateList.get(0)))).click();

        scrollUpOnPage();

        return this;
    }

    public TuiMainPage selectNights(String nightsValue) throws Exception{
        closeWidgetForm();
        $(byXpath(NIGHTS_BAR)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        while (!$(byXpath(NIGHTS_FROM_VALUE)).getText().equals(nightsValue)){
            if(Integer.parseInt($(byXpath(NIGHTS_FROM_VALUE)).getText()) < Integer.parseInt(nightsValue))
                $(byXpath(NIGHTS_FROM_INCREMENT)).waitUntil(Condition.visible, Constants.CONSTANT_2_SECONDS).click();
            else
                $(byXpath(NIGHTS_FROM_DECREMENT)).waitUntil(Condition.visible, Constants.CONSTANT_2_SECONDS).click();
        }

        while (!$(byXpath(NIGHTS_TO_VALUE)).getText().equals(nightsValue)){
            if(Integer.parseInt($(byXpath(NIGHTS_TO_VALUE)).getText()) < Integer.parseInt(nightsValue))
                $(byXpath(NIGHTS_TO_INCREMENT)).waitUntil(Condition.visible, Constants.CONSTANT_2_SECONDS).click();
            else
                $(byXpath(NIGHTS_TO_DECREMENT)).waitUntil(Condition.visible, Constants.CONSTANT_2_SECONDS).click();
        }

        scrollUpOnPage();

        return this;
    }

    public TuiSearchPage clickSubmitButton() throws Exception{
        closeWidgetForm();
        $(byXpath(SUBMIT_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        return new TuiSearchPage();
    }

}
