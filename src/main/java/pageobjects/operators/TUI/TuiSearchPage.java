package pageobjects.operators.TUI;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;
import org.openqa.selenium.Keys;
import pageobjects.operators.TUI_new.TuiHotelPage;
import pageobjects.operators.TUI_new.TuiTourPage;

import java.util.ArrayList;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class TuiSearchPage extends WebDriverCommands {

    private  static TuiSearchPage tuiSearchPage = null;
    public TuiSearchPage(){
    }
    public static TuiSearchPage getTuiSearchPage(){
        if(tuiSearchPage == null)
            tuiSearchPage = new TuiSearchPage();

        return tuiSearchPage;
    }

    private final String SORT_BY_PRICE_LABEL = "//span[contains(@class, 'sort-item-btn') and text() = 'по цене']";
    private static final String SEARCH_RESULTS = "//div[@class = 'content-results']";
    private final String TOURS_COUNT_TITLE = "//h2[@class = 'search-result-count__title']";
    private final String BREADCRUMBS = "//div[@class = 'breadcrumbs']";
    private static final String EMPTY_RESULT = "//div[contains(@class, 'EmptySearch-module')]";
    private final String HOTEL_BLOCK = "(//div[@class = 'search-result'])[%s]";
    private final String HOTEL_BLOCK_NAME = "(//div[@class = 'search-result'])[%s]/.//a[contains(@class, 'hotel-name-class')]";
    private final String HOTEL_BLOCK_PRICE = "(//div[@class = 'search-result'])[%s]/.//span[@class = 'price']/span";
    private final String HOTEL_BLOCK_GOTO_TOUR_BUTTON = "(//div[@class = 'search-result'])[%s]/.//a[contains(@class, 'choose-tour-class')]";
    private final String SHOW_MORE_TOURS_BUTTON = "//button[@class = 'search-results__show-more']";

    private static ArrayList<String> hotelNamesTuiList = new ArrayList<String>();
    public static ArrayList <String> getHotelNamesTuiList(){
        return hotelNamesTuiList;
    }
    public static void setHotelNamesTuiList(String hotelNameTuiListValue){
        hotelNamesTuiList.add(hotelNameTuiListValue);
    }

    private static ArrayList<String> countryResortTuiList = new ArrayList<String>();
    public static ArrayList <String> getCountryResortTuiList(){
        return countryResortTuiList;
    }
    public static void setCountryResortTuiList(String countryResortTuiValue){
        countryResortTuiList.add(countryResortTuiValue);
    }

    private static ArrayList<String> hotelTourPriceTuiList = new ArrayList<String>();
    public static ArrayList <String> getHotelTourPriceTuiList(){
        return hotelTourPriceTuiList;
    }
    public static void setHotelTourPriceTuiList(String hotelTourPriceTuiListValue){
        hotelTourPriceTuiList.add(hotelTourPriceTuiListValue);
    }

    public static ArrayList<Double> hotelTourTuiAmountList = new ArrayList<Double>();
    public static ArrayList <Double> getHotelTourTuiAmountList(){
        return hotelTourTuiAmountList;
    }
    public static void setHotelTourTuiAmountList(Double hotelTourTuiAmountListValue){
        hotelTourTuiAmountList.add(hotelTourTuiAmountListValue);
    }

    public TuiSearchPage clickSortByPriceButton() throws Exception{
        moveToElement($(byXpath(BREADCRUMBS)));
        $(byXpath(SORT_BY_PRICE_LABEL)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        return this;
    }

    public static boolean ifEmptyTuiSearch() throws Exception{
        sleep(Constants.CONSTANT_1_SECOND);
        $(byXpath(SEARCH_RESULTS)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS);
        if ($(byXpath(EMPTY_RESULT)).isDisplayed())
            return true;
        else
            return false;
    }

    public TuiSearchPage getHotelNamePrice(String countryValue, String resortValue) throws Exception{
        int hotelCounter = 1;

        while($(byXpath(SHOW_MORE_TOURS_BUTTON)).isDisplayed()){
            $(byXpath(SHOW_MORE_TOURS_BUTTON)).click();
            sleep(Constants.CONSTANT_2_SECONDS);
        }

        while($(byXpath(String.format(HOTEL_BLOCK, hotelCounter))).isDisplayed()){
            if (resortValue.equals("-"))
                setCountryResortTuiList(countryValue);
            else
                setCountryResortTuiList(resortValue);
            setHotelNamesTuiList($(byXpath(String.format(HOTEL_BLOCK_NAME, hotelCounter))).getText());
            setHotelTourPriceTuiList($(byXpath(String.format(HOTEL_BLOCK_PRICE, hotelCounter))).getText().replaceAll("\\D", ""));

            hotelCounter ++;
        }
        setHotelTourTuiAmountList((double) hotelCounter);

        return this;
    }

    public TuiHotelPage gotoHotelPage(int hotelCounter) throws Exception{
        String keysLinkOpenInNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN);
        $(byXpath(String.format(HOTEL_BLOCK_GOTO_TOUR_BUTTON, hotelCounter))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).sendKeys(keysLinkOpenInNewTab);

        return new TuiHotelPage();
    }

    public TuiSearchPage getHotelActualizedPrice() throws Exception{
        int hotelCounter = 1;

        while($(byXpath(SHOW_MORE_TOURS_BUTTON)).isDisplayed()){
            $(byXpath(SHOW_MORE_TOURS_BUTTON)).click();
            sleep(Constants.CONSTANT_2_SECONDS);
        }

        String oldTab = driver.getWindowHandle();

        while($(byXpath(String.format(HOTEL_BLOCK, hotelCounter))).isDisplayed()){
            gotoHotelPage(hotelCounter);

            String hotelTab = driver.getWindowHandle();
            ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
            newTab.remove(hotelTab);
            driver.switchTo().window(newTab.get(0));

            TuiHotelPage.getTuiHotelPage()
                        .waitForLoaderDissapear()
                        .waitForCalendarLoaded()
                        .gotoTuiTourPage();

            TuiTourPage.getTuiTourPage()
                        .getFinalPrice();

            driver.close();
            driver.switchTo().window(oldTab);

            hotelCounter ++;
        }

        return this;
    }

}
