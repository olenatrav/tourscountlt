package pageobjects.operators.FSTravel;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class FSTravelMainPage extends WebDriverCommands {

    private static FSTravelMainPage fsTravelMainPage = null;
    public FSTravelMainPage(){
    }
    public static FSTravelMainPage getFSTravelMainPage(){
        if(fsTravelMainPage == null)
            fsTravelMainPage = new FSTravelMainPage();

        checkServerErrorAndRefresh();

        return fsTravelMainPage;
    }

    private final String FS_TRAVEL_MAIN_URL = "https://fstravel.com/searchtour";
    private final String CITY_FROM_POPUP = "//span[text() = 'Где вы находитесь?']";
    private final String CITY_FROM_VALUE = "//label[contains(text(), '%s')]";
    private final String CITY_FROM_SELECT = "//input[@data-searchname = 'departureCityId']";
    private final String COUNTRY_TO_LOADING = "//div[contains(@class, 'v-elastic-loading')]";
    private static final String COUNTRY_TO_SELECT = "//input[@placeholder = 'Страна, город или отель']";
    private static final String COUNTRY_TO_VALUE = "//label[contains(text(), '%s')]"; //scrollTo
    private final String CALENDAR_SELECT = "//input[@id = 'calendar_range']";
    private final String CALENDAR_MONTH_VALUE = "(//button[contains(@class, 'mx-btn-current-month')])[1]";
    private final String CALENDAR_YEAR_VALUE = "(//button[contains(@class, 'mx-btn-current-year')])[1]";
    private final String CALENDAR_NEXT_MONTH_ARROW = "//i[@class = 'mx-icon-right calendar-left-btn']";
    private final String CALENDAR_DATE_VALUE = "//td[@title = '%s']"; //yyyy-MM-dd format
    private final String NIGHTS_SELECT = "//div[contains(text(), 'Количество ночей')]/following-sibling::div[1]";
    private final String NIGHTS_FROM_MINUS = "(//span[contains(@class, 'v-minus-circle')])[1]";
    private final String NIGHTS_FROM_VALUE = "(//span[contains(@class, 'v-minus-circle')])[1]/following-sibling::p";
    private final String NIGHTS_FROM_PLUS = "(//span[contains(@class, 'v-plus-circle')])[1]";
    private final String NIGHTS_TO_MINUS = "(//span[contains(@class, 'v-minus-circle')])[2]";
    private final String NIGHTS_TO_VALUE = "(//span[contains(@class, 'v-minus-circle')])[2]/following-sibling::p";
    private final String NIGHTS_TO_PLUS = "(//span[contains(@class, 'v-plus-circle')])[2]";
    private final String TOURISTS_SELECT = "(//div[contains(text(), 'Туристы')]/following-sibling::div)[1]";
    private final String ADULTS_MINUS = "(//span[@class = 'v-minus-circle'])[1]";
    private final String ADULTS_VALUE = "(//span[@class = 'v-minus-circle'])[1]/following-sibling::p";
    private final String ADULTS_PLUS = "(//span[@class = 'v-plus-circle active'])[1]";
    private final String KIDS_MINUS = "(//span[@class = 'v-minus-circle'])[2]";
    private final String KIDS_VALUE = "(//span[@class = 'v-minus-circle'])[2]/following-sibling::p";
    private final String KIDS_PLUS = "(//span[@class = 'v-plus-circle active'])[2]";
    private final String SEARCH_BUTTON = "//button[contains(@class, 'v-search-button')]";


    public FSTravelMainPage gotoFSTravelMainPage() throws Exception{
        goToPage(FS_TRAVEL_MAIN_URL);
        return this;
    }

    public FSTravelMainPage selectCityFrom(String cityFromValue) throws Exception{
        sleep(Constants.CONSTANT_1_SECOND);
        $(byXpath(COUNTRY_TO_LOADING)).waitUntil(Condition.disappear, Constants.CONSTANT_60_SECONDS);
        if($(byXpath(CITY_FROM_POPUP)).isDisplayed()){
            $(byXpath(String.format(CITY_FROM_VALUE, cityFromValue))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
            sleep(Constants.CONSTANT_1_SECOND);
        }
        else{
            $(byXpath(CITY_FROM_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
            sleep(Constants.CONSTANT_2_SECONDS);
            $(byXpath(CITY_FROM_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).clear();
            sleep(Constants.CONSTANT_1_SECOND);
            $(byXpath(CITY_FROM_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).sendKeys(cityFromValue);
            $(byXpath(String.format(CITY_FROM_VALUE, cityFromValue))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        }
        $(byXpath(COUNTRY_TO_LOADING)).waitUntil(Condition.disappear, Constants.CONSTANT_60_SECONDS);

        return this;
    }

    public static boolean isCountryDisplayed(String countryToValue) throws Exception{
        sleep(Constants.CONSTANT_1_SECOND);
        $(byXpath(COUNTRY_TO_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
//        $(byXpath(COUNTRY_TO_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).clear();
        sleep(Constants.CONSTANT_1_SECOND);
//        $(byXpath(COUNTRY_TO_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).sendKeys(countryToValue);
        if($(byXpath(String.format(COUNTRY_TO_VALUE, countryToValue))).isDisplayed())
            return true;
        else
            return false;
    }

    public FSTravelMainPage selectCountryTo(String countryToValue) throws Exception{
//        $(byXpath(COUNTRY_TO_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
//        $(byXpath(COUNTRY_TO_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).clear();
//        sleep(Constants.CONSTANT_1_SECOND);
//        $(byXpath(COUNTRY_TO_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).sendKeys(countryToValue);
        scrollToElement(String.format(COUNTRY_TO_VALUE, countryToValue));
        scrollDownOnSomePxl(150);
        $(byXpath(String.format(COUNTRY_TO_VALUE, countryToValue))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        scrollUpOnPage();
        $(byXpath(COUNTRY_TO_LOADING)).waitUntil(Condition.disappear, Constants.CONSTANT_60_SECONDS);

        return this;
    }

    public FSTravelMainPage selectFlightDate(String flightDateValue) throws Exception{
        $(byXpath(CALENDAR_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        sleep(Constants.CONSTANT_1_SECOND);
        while(!$(byXpath(String.format(CALENDAR_DATE_VALUE, flightDateValue))).isDisplayed()){
            $(byXpath(CALENDAR_NEXT_MONTH_ARROW)).click();
            sleep(Constants.CONSTANT_1_SECOND);
        }
        $(byXpath(String.format(CALENDAR_DATE_VALUE, flightDateValue))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).doubleClick();

        return this;
    }

    public FSTravelMainPage selectNightsValue(int nightsValue) throws Exception{
        $(byXpath(NIGHTS_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        while (nightsValue != Integer.parseInt($(byXpath(NIGHTS_FROM_VALUE)).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).getText())){
            if (nightsValue < Integer.parseInt($(byXpath(NIGHTS_FROM_VALUE)).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).getText()))
                $(byXpath(NIGHTS_FROM_MINUS)).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).click();
            else
                $(byXpath(NIGHTS_FROM_PLUS)).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).click();
        }
        while (nightsValue != Integer.parseInt($(byXpath(NIGHTS_TO_VALUE)).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).getText())){
            if (nightsValue < Integer.parseInt($(byXpath(NIGHTS_TO_VALUE)).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).getText()))
                $(byXpath(NIGHTS_TO_MINUS)).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).click();
            else
                $(byXpath(NIGHTS_TO_PLUS)).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).click();
        }

        return this;
    }

    public FSTravelSearchPage clickSearchButton() throws Exception{
        $(byXpath(SEARCH_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        return new FSTravelSearchPage();
    }

}
