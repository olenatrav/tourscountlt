package pageobjects.operators.FSTravel;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;

import java.util.ArrayList;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class FSTravelHotelPage extends WebDriverCommands {

    private static FSTravelHotelPage fsTravelHotelPage = null;
    public FSTravelHotelPage(){
    }
    public static FSTravelHotelPage getFSTravelHotelPage(){
        if(fsTravelHotelPage == null)
            fsTravelHotelPage = new FSTravelHotelPage();

        checkServerErrorAndRefresh();

        return fsTravelHotelPage;
    }

    private final String SHOW_PRICES_BUTTON = "//div[contains(@class, 'v-price-show-btn')]";
    private final String SHOW_FINAL_PRICE_LINK = "(//span[contains(@class, 'v-more-link')])[1]";
    private final String ACTUALIZATION_PRELOADER = "//div[@class = 'v-more-information v-mt-4 preloader']";
    private final String FINAL_PRICE_VALUE = "//p[contains(@class, 'v-more-button-block__price')]";
    private final String SEARCH_RESULTS_BLOCK = "//div[@id = 'searchResult']";
    private final String EMPTY_SEARCH_LABEL = "//div[contains(text(), 'Предложений не найдено')]";
    private final String ACTUALIZATION_FAILED_LABEL = "//h4[text() = 'Упс...']";

    private static ArrayList<String> hotelTourPriceActualizedFSTravelList = new ArrayList<String>();
    public static ArrayList <String> getHotelTourPriceActualizedFSTravelList(){
        return hotelTourPriceActualizedFSTravelList;
    }
    public static void setHotelTourPriceActualizedFSTravelList(String hotelTourPriceActualizedFSTravelValue){
        hotelTourPriceActualizedFSTravelList.add(hotelTourPriceActualizedFSTravelValue);
    }

    public FSTravelHotelPage clickShowPricesButton() throws Exception{
        if ($(byXpath(SHOW_PRICES_BUTTON)).isDisplayed())
            $(byXpath(SHOW_PRICES_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_300_SECONDS).click();
        sleep(Constants.CONSTANT_1_SECOND);

        return this;
    }

    public boolean isHotelSearchResultsShown() throws Exception{
        return $(byXpath(SEARCH_RESULTS_BLOCK)).isDisplayed();
    }

    public boolean isEmptyHotelSearch() throws Exception{
        return $(byXpath(EMPTY_SEARCH_LABEL)).isDisplayed();
    }

    public FSTravelHotelPage clickShowFinalTourPriceButton() throws Exception{
        sleep(Constants.CONSTANT_1_SECOND);
        $(byXpath(SHOW_FINAL_PRICE_LINK)).waitUntil(Condition.visible, Constants.CONSTANT_300_SECONDS).click();

        return this;
    }

    public FSTravelHotelPage waitForActualizationComplete() throws Exception{
        $(byXpath(ACTUALIZATION_PRELOADER)).waitUntil(Condition.disappear, Constants.CONSTANT_360_SECONDS);

        return this;
    }

    public boolean isActualizationFailed() throws Exception{
        return $(byXpath(ACTUALIZATION_FAILED_LABEL)).isDisplayed();
    }

    public void getFinalPrice() throws Exception{
        if(isActualizationFailed())
            setHotelTourPriceActualizedFSTravelList("-");
        else{
            scrollToElement(FINAL_PRICE_VALUE);
            setHotelTourPriceActualizedFSTravelList($(byXpath(FINAL_PRICE_VALUE)).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).getText().replaceAll("\\D", ""));
        }
    }

}
