package pageobjects.operators.FSTravel;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;

import java.util.ArrayList;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static framework.Constants.*;
import static pageobjects.Travelata.OfferEmailSearchPage.setHotelTourOfferAmountList;
import static pageobjects.operators.FSTravel.FSTravelHotelPage.setHotelTourPriceActualizedFSTravelList;

public class FSTravelSearchPage extends WebDriverCommands {

    private static FSTravelSearchPage fsTravelSearchPage = null;
    public FSTravelSearchPage(){
    }
    public static FSTravelSearchPage getFSTravelSearchPage(){
        if(fsTravelSearchPage == null)
            fsTravelSearchPage = new FSTravelSearchPage();

        checkServerErrorAndRefresh();

        return fsTravelSearchPage;
    }

    private static final String SEARCH_PRELOADER = "//div[@class = 'v-search-main-block container preloader']";
    private final String HOTELS_AMOUNT_LABEL = "//h3[@class = 'v-block-padding-title']";
    private final String HOTEL_CLASS_BLOCK = "//p[contains(@trslt, 'hotelCategory')]";
    private final String ALL_STARS_CHECKBOX = "//p[contains(@trslt, 'hotelCategory')]/ancestor::div[contains(@class, 'searchFilter')]/.//*[text()[contains(.,'Все')]]"; //label[@for = 'allStars']
    private final String STARS_CHECKBOX = "//p[contains(@trslt, 'hotelCategory')]/ancestor::div[contains(@class, 'searchFilter')]/.//*[text()[contains(.,'%s Звезд')]]"; //label[@for = 'star%s']
    private final String MEAL_BLOCK = "//p[contains(@trslt, 'meal')]";
    private final String ALL_MEAL_CHECKBOX = "(//p[contains(@trslt, 'meal')]/ancestor::div[contains(@class, 'searchFilter')]/.//*[text()[contains(.,'Все')]])[1]"; //label[@for = 'allMealTypes']
    private final String MEAL_CHECKBOX = "//p[contains(@trslt, 'meal')]/ancestor::div[contains(@class, 'searchFilter')]/.//*[text()[contains(.,'%s')]]"; //label[@for = 'meal%s']
    private final String HOTEL_NAME_BLOCK = "(//div[contains(@class, 'v-hotel-title')])[%s]";
    private final String HOTEL_NAME_VALUE = "(//a[contains(@href, '/hotel/')])[%s]";
    private final String HOTEL_STARS_VALUE = "(//a[contains(@href, '/hotel/')]/span)[%s]";
    private final String HOTEL_RESORT_VALUE = "(//div[contains(@class, 'v-space-between')]/p)[%s]";
    private final String TOUR_PRICE_VALUE = "(//div[@class = 'v-price-block-right']/.//span)[%s]";
    private final String SHOW_MORE_BUTTON = "//div[contains(@class, 'show-more')]";
    private static final String EMPTY_SEARCH_LABEL = "//div[@class = 'v-no-tours']";
    private static final String NO_TOURS_LABEL = "//h4[contains(text(), 'не найдено')]";

    private static ArrayList<String> hotelNamesFSTravelList = new ArrayList<String>();
    public static ArrayList <String> getHotelNamesFSTravelList(){
        return hotelNamesFSTravelList;
    }
    public static void setHotelNamesFSTravelList(String hotelNamesFSTravelListValue){
        hotelNamesFSTravelList.add(hotelNamesFSTravelListValue);
    }

    private static ArrayList<String> hotelStarsFSTravelList = new ArrayList<String>();
    public static ArrayList <String> getHotelStarsFSTravelList(){
        return hotelStarsFSTravelList;
    }
    public static void setHotelStarsFSTravelList(String hotelStarsFSTravelListValue){
        hotelStarsFSTravelList.add(hotelStarsFSTravelListValue);
    }

    private static ArrayList<String> countryResortFSTravelList = new ArrayList<String>();
    public static ArrayList <String> getCountryResortFSTravelList(){
        return countryResortFSTravelList;
    }
    public static void setCountryResortFSTravelList(String countryResortFSTravelListValue){
        countryResortFSTravelList.add(countryResortFSTravelListValue);
    }

    private static ArrayList<String> hotelTourPriceFSTravelList = new ArrayList<String>();
    public static ArrayList <String> getHotelTourPriceFSTravelList(){
        return hotelTourPriceFSTravelList;
    }
    public static void setHotelTourPriceFSTravelList(String hotelTourPriceFSTravelListValue){
        hotelTourPriceFSTravelList.add(hotelTourPriceFSTravelListValue);
    }

    private static ArrayList<String> hotelsAmountFSTravelList = new ArrayList<String>();
    public static ArrayList <String> getHotelsAmountFSTravelList(){
        return hotelsAmountFSTravelList;
    }
    public static void setHotelsAmountFSTravelList(String hotelsAmountFSTravelListValue){
        hotelsAmountFSTravelList.add(hotelsAmountFSTravelListValue);
    }

    private static ArrayList<String> departureCityFSTravelList = new ArrayList<String>();
    public static ArrayList <String> getDepartureCityFSTravelList(){
        return departureCityFSTravelList;
    }
    public static void setDepartureCityFSTravelList(String departureCityFSTravelListValue){
        departureCityFSTravelList.add(departureCityFSTravelListValue);
    }

    private static ArrayList<Integer> actualDateCounterList = new ArrayList<Integer>();
    public static ArrayList <Integer> getActualDateCounterList(){
        return actualDateCounterList;
    }
    public static void setActualDateCounterList(Integer actualDateCounterListValue){
        actualDateCounterList.add(actualDateCounterListValue);
    }

    private static ArrayList<Integer> actualNightsCounterList = new ArrayList<Integer>();
    public static ArrayList <Integer> getActualNightsCounterList(){
        return actualNightsCounterList;
    }
    public static void setActualNightsCounterList(Integer actualNightsCounterListValue){
        actualNightsCounterList.add(actualNightsCounterListValue);
    }

    public static FSTravelSearchPage waitForSearchResults() throws Exception{
        scrollUpOnPage();
        $(byXpath(SEARCH_PRELOADER)).waitUntil(Condition.disappear, Constants.CONSTANT_360_SECONDS);

        return new FSTravelSearchPage();
    }

    public FSTravelSearchPage selectMeal(ArrayList<String> mealValue) throws Exception{
        if(!mealValue.isEmpty()){
            scrollToElement(MEAL_BLOCK);
            scrollDownOnSomePxl(350);
            $(byXpath(ALL_MEAL_CHECKBOX)).waitUntil(Condition.visible, CONSTANT_20_SECONDS).click();
            sleep(CONSTANT_1_SECOND);
            for(int i = 0; i<mealValue.size(); i++){
                $(byXpath(String.format(MEAL_CHECKBOX, hotelMealsFSTravel.get(mealValue.get(i))))).waitUntil(Condition.visible, Constants.CONSTANT_2_SECONDS).click();
            }
            sleep(CONSTANT_1_SECOND);
        }

        return this;
    }

    public FSTravelSearchPage selectStars(ArrayList<String> starsValue) throws Exception{
        if(!starsValue.isEmpty()){
            scrollToElement(HOTEL_CLASS_BLOCK);
            scrollDownOnSomePxl(350);
            $(byXpath(ALL_STARS_CHECKBOX)).waitUntil(Condition.visible, CONSTANT_20_SECONDS).click();
            sleep(CONSTANT_1_SECOND);
            for(int i = 0; i<starsValue.size(); i++){
                $(byXpath(String.format(STARS_CHECKBOX, starsValue.get(i)))).waitUntil(Condition.visible, Constants.CONSTANT_2_SECONDS).click();
            }
            sleep(CONSTANT_1_SECOND);
        }

        return this;
    }

    public static boolean isEmptySearchResult() throws Exception{
        waitForSearchResults();
        return $(byXpath(EMPTY_SEARCH_LABEL)).isDisplayed();
    }

    public static boolean isNoToursResult() throws Exception{
        waitForSearchResults();
        return $(byXpath(NO_TOURS_LABEL)).isDisplayed();
    }

    public String getResortFSTravelHotelValue(int hotelCounter) throws Exception{
        String resortValue = $(byXpath(String.format(HOTEL_RESORT_VALUE, hotelCounter))).getText();
        return resortValue.substring(resortValue.lastIndexOf("|") + 1).trim();
    }

    public void putHotelNamesFSTravelList(String countryToValue, boolean isActualizationNeeded) throws Exception {

        if(!isEmptySearchResult()){

            while ($(byXpath(SHOW_MORE_BUTTON)).isDisplayed()){
                scrollToElement(SHOW_MORE_BUTTON);
                scrollDownOnSomePxl(100);
                $(byXpath(SHOW_MORE_BUTTON)).click();
                sleep(Constants.CONSTANT_1_SECOND);
            }

            scrollUpOnPage();

            int hotelCounter = 1;
            while($(byXpath(String.format(HOTEL_NAME_BLOCK, hotelCounter))).isDisplayed()){
                scrollToElement(String.format(HOTEL_NAME_VALUE, hotelCounter));
                scrollDownOnSomePxl(100);
                setHotelNamesFSTravelList($(byXpath(String.format(HOTEL_NAME_BLOCK, hotelCounter))).getAttribute("data-info"));
                if($(byXpath(String.format(HOTEL_STARS_VALUE, hotelCounter))).isDisplayed())
                    setHotelStarsFSTravelList($(byXpath(String.format(HOTEL_STARS_VALUE, hotelCounter))).getText() + "*");
                else
                    setHotelStarsFSTravelList("0*");
                setCountryResortFSTravelList(getResortFSTravelHotelValue(hotelCounter));
                setHotelTourPriceFSTravelList($(byXpath(String.format(TOUR_PRICE_VALUE, hotelCounter))).getText().replaceAll("\\D", ""));

                //Actualization
                if(isActualizationNeeded) {
                    String oldTab = driver.getWindowHandle();

//                scrollToElement(String.format(HOTEL_NAME_VALUE, hotelCounter));
                    String tourUrlValue = $(byXpath(String.format(HOTEL_NAME_VALUE, hotelCounter))).getAttribute("href");
                    openLinkInNewTab(tourUrlValue);

//                String hotelTab = driver.getWindowHandle();
//                ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
//                newTab.remove(hotelTab);
//                driver.switchTo().window(newTab.get(0));

                    FSTravelHotelPage fsTravelHotelPage = new FSTravelHotelPage();

                    fsTravelHotelPage.clickShowPricesButton();
                    if(fsTravelHotelPage.isEmptyHotelSearch() || !(fsTravelHotelPage.isHotelSearchResultsShown()))
                        setHotelTourPriceActualizedFSTravelList("-");
                    else{
                        fsTravelHotelPage
                                .clickShowFinalTourPriceButton()
                                .waitForActualizationComplete()
                                .getFinalPrice();
                    }
                    sleep(Constants.CONSTANT_1_SECOND);
                    driver.close();
                    driver.switchTo().window(oldTab);
                }
                else{
                    setHotelTourPriceActualizedFSTravelList("-");
                }
                ///////////////////////////////////////

                hotelCounter ++;
            }
        }
        else
            putEmptyHotelNamesFSTravelList(countryToValue);


        setHotelTourOfferAmountList((double) getHotelNamesFSTravelList().size());

    }

    public FSTravelSearchPage putHotelsAmountFSTravelList(String departureValue, int dateCounterValue, int nightsCounterValue){
        setHotelsAmountFSTravelList($(byXpath(HOTELS_AMOUNT_LABEL)).waitUntil(Condition.visible, CONSTANT_300_SECONDS).getText().replaceAll("\\D", ""));
        setDepartureCityFSTravelList(departureValue);
        setActualDateCounterList(dateCounterValue);
        setActualNightsCounterList(nightsCounterValue);

        return this;
    }

    public static void putEmptyHotelNamesFSTravelList(String countryToValue) throws Exception{
        setHotelNamesFSTravelList("-");
        setHotelStarsFSTravelList("-");
        setCountryResortFSTravelList(countryToValue);
        setHotelTourPriceFSTravelList("-");
        setHotelTourPriceActualizedFSTravelList("-");

    }

}
