package pageobjects.operators.Anex;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;

import java.util.ArrayList;
import java.util.Arrays;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static framework.Constants.*;

public class AnexMainPage extends WebDriverCommands {

    private  static AnexMainPage anexMainPage = null;
    public AnexMainPage(){
    }
    public static AnexMainPage getAnexMainPage(){
        if(anexMainPage == null)
            anexMainPage = new AnexMainPage();

        return anexMainPage;
    }

    private final String ANEX_START_URL = "https://www.anextour.com/tours/";
    private final String CITY_FROM_SELECT = "//span[text() = 'Откуда']/.././/input";
    private final String CITY_FROM_VALUE = "//button[@title = '%s']";
    private final String COUNTRY_TO_SELECT = "//span[text() = 'Куда']/.././/input";
    private final String COUNTRY_TO_VALUE = "//div[text() = '%s']";
    private final String FLIGHT_DATE_SELECT = "//span[text() = 'Вылет']/.././/input";
    private final String FLIGHT_DATE_MONTH_VALUE = "(//button[contains(text(), '%s')])[1]";
    private final String FLIGHT_DATE_DAY_VALUE = "(//span[text() = '%s'])";
    private final String NIGHTS_AMOUNT_SELECT = "//span[text() = 'Ночей']/.././/input";
    private final String NIGHTS_AMOUNT_VALUE = "(//span[text() = '%s'])[1]";
    private final String TOURISTS_AMOUNT_SELECT = "//span[text() = 'Туристы']/.././/input";
    private final String SEARCH_BUTTON = "//button[@gtm-label = 'search-button']";
    public static boolean ifEmptySearch = false;

    public AnexMainPage gotoAnexCountryMainPage(String countryValue) throws Exception{
        if (!getCurrentUrl().equals(ANEX_START_URL + countryValue))
            goToPage(ANEX_START_URL + countryValue);

        return this;
    }

    public AnexMainPage selectCityFrom(String cityFromValue) throws Exception{
        $(byXpath(CITY_FROM_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        sleep(CONSTANT_2_SECONDS);
        $(byXpath(CITY_FROM_SELECT)).clear();
        sleep(CONSTANT_2_SECONDS);
        $(byXpath(CITY_FROM_SELECT)).sendKeys(cityFromValue);
        sleep(CONSTANT_2_SECONDS);
        $(byXpath(String.format(CITY_FROM_VALUE, cityFromValue))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        sleep(CONSTANT_1_SECOND);

        return this;
    }

    public AnexMainPage selectCountryTo(String countryToValue) throws Exception{
        $(byXpath(COUNTRY_TO_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        sleep(CONSTANT_1_SECOND);
        $(byXpath(COUNTRY_TO_SELECT)).clear();
        sleep(CONSTANT_1_SECOND);
        $(byXpath(COUNTRY_TO_SELECT)).sendKeys(countryToValue);
        sleep(CONSTANT_1_SECOND);
        $(byXpath(String.format(COUNTRY_TO_VALUE, countryToValue))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        sleep(CONSTANT_1_SECOND);

        return this;
    }

    public AnexMainPage selectFlightDate(String flightDateValue) throws Exception{
        $(byXpath(FLIGHT_DATE_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        ArrayList<String> dateList = new ArrayList<String>(Arrays.asList(flightDateValue.split("\\s+")));
        $(byXpath(String.format(FLIGHT_DATE_MONTH_VALUE, monthsRussian.get(dateList.get(1))))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        if(!$(byXpath(String.format(FLIGHT_DATE_DAY_VALUE, dateList.get(0)))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).getAttribute("class").contains("line-through")){
            $(byXpath(String.format(FLIGHT_DATE_DAY_VALUE, dateList.get(0)))).click();
            sleep(CONSTANT_2_SECONDS);
        }
        else
            ifEmptySearch = true;
//        if (dateList.get(0).length()<2)
//            $(byXpath(String.format(FLIGHT_DATE_DAY_VALUE, monthsRussian.get(dateList.get(1)), "0" + dateList.get(0)))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
//        else
//            $(byXpath(String.format(FLIGHT_DATE_DAY_VALUE, monthsRussian.get(dateList.get(1)),dateList.get(0)))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        return this;
    }

    public AnexMainPage selectNights(String nightsValue) throws Exception{
        $(byXpath(NIGHTS_AMOUNT_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        sleep(CONSTANT_1_SECOND);
        String s = $(byXpath(String.format(NIGHTS_AMOUNT_VALUE, nightsValue))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).getAttribute("class");
        if(!$(byXpath(String.format(NIGHTS_AMOUNT_VALUE, nightsValue))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).getAttribute("class").contains("line-through")) {
            $(byXpath(String.format(NIGHTS_AMOUNT_VALUE, nightsValue))).click();
            sleep(CONSTANT_1_SECOND);
        }
        else
            ifEmptySearch = true;

        return this;
    }

    public AnexSearchPage clickSearchButton() throws Exception{
        $(byXpath(SEARCH_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        return new AnexSearchPage();
    }

}
