package pageobjects.operators.Anex;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.codeborne.selenide.Selectors.by;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static pageobjects.operators.Anex.AnexTourPage.setHotelTourPriceActualizedAnexList;

public class AnexHotelPage extends WebDriverCommands {

    private  static AnexHotelPage anexHotelPage = null;
    public AnexHotelPage(){
    }
    public static AnexHotelPage getAnexHotelPage(){
        if(anexHotelPage == null)
            anexHotelPage = new AnexHotelPage();

        return anexHotelPage;
    }

    private final String FIRST_ROOM_BLOCK = "//li[1]/.//div[@gtm-label = 'room-card-item']";
    private final String FIRST_ROOM_PRICE_BUTTON = "//li[1]/.//button[@gtm-label = 'room-card-item-button']/span";
    private final String CHEAPEST_ROOM_PRICE_BUTTON = "//li[1]/.//a[@gtm-label = 'room-card-item-offer'][1]/.//button[@gtm-label = 'room-card-item-offer-button']";
    private final String ERROR_PAGE = "//h1[text() = 'Ошибка 404']";
    private final String EMPTY_SEARCH = "//div[contains(text(), 'Ничего не найдено')]";

    public boolean ifEmptySearch() throws Exception{
        if($(byXpath(ERROR_PAGE)).isDisplayed())
            refreshPage();

        try {
            (new WebDriverWait(driver, Constants.CONSTANT_30_SECONDS)).until(ExpectedConditions.visibilityOfElementLocated(byXpath(FIRST_ROOM_PRICE_BUTTON)));
        }
        catch (org.openqa.selenium.TimeoutException timeout){

        }
        if($(byXpath(EMPTY_SEARCH)).isDisplayed())
            return true;

        if($(byXpath(FIRST_ROOM_PRICE_BUTTON)).isDisplayed())
            return false;
        else
            return true;

    }

    public AnexTourPage gotoAnexTourPage() throws Exception{
        String priceValue = $(byXpath(FIRST_ROOM_PRICE_BUTTON)).getText();
        $(byXpath(FIRST_ROOM_PRICE_BUTTON)).click();
        sleep(Constants.CONSTANT_1_SECOND);
        if(priceValue.contains("от"))
            $(byXpath(CHEAPEST_ROOM_PRICE_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        return new AnexTourPage();
    }

}
