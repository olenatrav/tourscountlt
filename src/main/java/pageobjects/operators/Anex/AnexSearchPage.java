package pageobjects.operators.Anex;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import framework.Constants;
import framework.WebDriverCommands;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.util.ArrayList;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static framework.Constants.*;
import static pageobjects.operators.Anex.AnexMainPage.ifEmptySearch;
import static pageobjects.operators.Anex.AnexTourPage.setHotelTourPriceActualizedAnexList;
import static pageobjects.operators.Anex_new.AnexSearchPage.*;

public class AnexSearchPage extends WebDriverCommands {

    private  static AnexSearchPage anexSearchPage = null;
    public AnexSearchPage(){
    }
    public static AnexSearchPage getAnexSearchPage(){
        if(anexSearchPage == null)
            anexSearchPage = new AnexSearchPage();

        return anexSearchPage;
    }

    private final String RESORT_SELECT = "//button[text() = 'Курорт']";
    private final String RESORT_SEARCH_INPUT = "(//input[@placeholder = 'Поиск по городам'])[2]";
    private final String SHOW_ALL_RESORTS_BUTTON = "//button[contains(@class, 'buttonMore')]";
    private final String RESORT_CHECKBOX = "(//span[text() = '%s'])[2]";
    private final String SELECT_RESORT_SUBMIT_BUTTON = "//button[text() = 'Показать']";
    private static final String HOTELS_COUNTER_TITLE = "(//div[contains(text(), ' вариант')])[1]";
    private static final String HOTELS_EMPTY_SEARCH_TITLE = "//h4[contains(text(), 'туров не найдено')]";
    private final String PAGINATION_BUTTON = "//span[text() = 'Показать еще']";
    private final String HOTEL_BLOCK = "//li[contains(@class, 'listCardsHotel-module__item')][%s]";
    private final String HOTEL_NAME = "//li[contains(@class, 'listCardsHotel-module__item')][%s]/.//h4[contains(@class, 'cardHotelDesktop-module__hotelName')]";
    private final String HOTEL_NAME_LOCATION ="//li[contains(@class, 'listCardsHotel-module__item')][%s]/.//div[contains(@class, 'cardHotelDesktop-module__firstLine')]";
    private final String HOTEL_PRICE = "//li[contains(@class, 'listCardsHotel-module__item')][%s]/.//button[contains(@class, 'cardHotelDesktop-module__price')]/span";
    private static final String EMPTY_RESULT = "//p[contains(text(), 'Ничего не найдено')]";
    private static final String NO_TOURS_RESULT = "//h4[contains(text(), 'не найдено')]";
    private final String COOKIE_DIALOG_CLOSE_BUTTON = "//button[contains(@class, 'bg-no-repeat')]";
    private final String SHOW_MORE_TOURS_BUTTON = "//button[contains(@class, 'module__button_pagination')]";
    private final String PRICE_CONDITION_LABEL = "//span[contains(text(), 'Цены указаны')]";
    private final String TOURS_LOADER = "(//span[contains(@class, 'overflow-hidden')]/.//*[@role = 'presentation'])[1]";

    private static ArrayList<String> hotelNamesAnexList = new ArrayList<String>();
    public static ArrayList <String> getHotelNamesAnexList(){
        return hotelNamesAnexList;
    }
    public static void setHotelNamesAnexList(String hotelNameAnexListValue){
        hotelNamesAnexList.add(hotelNameAnexListValue);
    }

    private static ArrayList<String> countryResortAnexList = new ArrayList<String>();
    public static ArrayList <String> getCountryResortAnexList(){
        return countryResortAnexList;
    }
    public static void setCountryResortAnexList(String countryResortAnexValue){
        countryResortAnexList.add(countryResortAnexValue);
    }

    private static ArrayList<String> hotelTourPriceAnexList = new ArrayList<String>();
    public static ArrayList <String> getHotelTourPriceAnexList(){
        return hotelTourPriceAnexList;
    }
    public static void setHotelTourPriceAnexList(String hotelTourPriceAnexListValue){
        hotelTourPriceAnexList.add(hotelTourPriceAnexListValue);
    }

    public static ArrayList<Double> hotelTourAnexAmountList = new ArrayList<Double>();
    public static ArrayList <Double> getHotelTourAnexAmountList(){
        return hotelTourAnexAmountList;
    }
    public static void setHotelTourAnexAmountList(Double hotelTourAnexAmountListValue){
        hotelTourAnexAmountList.add(hotelTourAnexAmountListValue);
    }

    public static SelenideElement waitForToursFound() throws Exception{
        return $(byXpath(HOTELS_COUNTER_TITLE)).waitUntil(Condition.visible, Constants.CONSTANT_480_SECONDS);

    }



    public static SelenideElement waitForEmptyToursFound() throws Exception{
        return $(byXpath(HOTELS_EMPTY_SEARCH_TITLE)).waitUntil(Condition.visible, Constants.CONSTANT_120_SECONDS);

    }

    public AnexSearchPage closeCookieDialog() throws Exception{
        if (waitForToursFound().isDisplayed() || waitForEmptyToursFound().isDisplayed())
            if($(byXpath(COOKIE_DIALOG_CLOSE_BUTTON)).isDisplayed())
            $(byXpath(COOKIE_DIALOG_CLOSE_BUTTON)).click();

        return this;
    }

    public AnexSearchPage selectResort(String resortValue) throws Exception{
        if(waitForToursFound().isDisplayed()){
            $(byXpath(RESORT_SEARCH_INPUT)).waitUntil(Condition.visible, Constants.CONSTANT_120_SECONDS).click();
            $(byXpath(RESORT_SEARCH_INPUT)).sendKeys(resortValue);
            $(byXpath(String.format(RESORT_CHECKBOX, resortValue))).waitUntil(Condition.visible, Constants.CONSTANT_10_SECONDS).click();
            sleep(Constants.CONSTANT_1_SECOND);
            $(byXpath(SELECT_RESORT_SUBMIT_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_10_SECONDS).click();
        }

        return this;
    }

    public AnexSearchPage unselectResort() throws Exception{
        if(waitForToursFound().isDisplayed()){
            $(byXpath(RESORT_SEARCH_INPUT)).waitUntil(Condition.visible, Constants.CONSTANT_120_SECONDS).click();
            $(byXpath(RESORT_SEARCH_INPUT)).clear();
            $(byXpath(SELECT_RESORT_SUBMIT_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_10_SECONDS).click();
        }

        return this;
    }

    public static boolean ifEmptyAnexSearch() throws Exception{
        if ($(byXpath(EMPTY_RESULT)).isDisplayed() || $(byXpath(NO_TOURS_RESULT)).isDisplayed() || (ifEmptySearch))
            return true;
        else
            return false;
    }

    public AnexSearchPage getHotelNamePrice(String resortValue) throws Exception{
        while($(byXpath(PAGINATION_BUTTON)).isDisplayed())
            $(byXpath(PAGINATION_BUTTON)).click();

        int hotelCounter = 1;
        while($(byXpath(String.format(HOTEL_BLOCK, hotelCounter))).isDisplayed()){
            hotelNamesAnexList.add($(byXpath(String.format(HOTEL_NAME, hotelCounter))).getText());
            setHotelTourPriceAnexList($(byXpath(String.format(HOTEL_PRICE, hotelCounter))).getText().replaceAll("\\D", ""));
            setCountryResortAnexList(resortValue);

            hotelCounter ++;
        }
        setHotelTourAnexAmountList((double) hotelCounter);

        scrollUpOnPage();

        return this;
    }

    public AnexHotelPage gotoHotelPage(int hotelCounter) throws Exception{
        $(byXpath(String.format(HOTEL_NAME_LOCATION, hotelCounter))).waitUntil(Condition.visible, Constants.CONSTANT_10_SECONDS).scrollTo();
        $(byXpath(String.format(HOTEL_PRICE, hotelCounter))).waitUntil(Condition.visible, Constants.CONSTANT_10_SECONDS).click();

        return new AnexHotelPage();
    }

    public AnexSearchPage getHotelActualizedPrice() throws Exception{

        int hotelCounter = 1;

        while($(byXpath(SHOW_MORE_TOURS_BUTTON)).isDisplayed()){
            $(byXpath(SHOW_MORE_TOURS_BUTTON)).click();
            sleep(Constants.CONSTANT_2_SECONDS);
        }

        while($(byXpath(String.format(HOTEL_BLOCK, hotelCounter))).isDisplayed()){
            gotoHotelPage(hotelCounter);
            if(!AnexHotelPage.getAnexHotelPage().ifEmptySearch()){
                AnexHotelPage.getAnexHotelPage()
                        .gotoAnexTourPage();
                AnexTourPage.getAnexTourPage()
                        .getFinalPrice();
                driver.navigate().back();
                driver.navigate().back();
            }
            else
                setHotelTourPriceActualizedAnexList("-");

            while($(byXpath(PAGINATION_BUTTON)).isDisplayed())
                $(byXpath(PAGINATION_BUTTON)).click();
            scrollUpOnPage();

            hotelCounter ++;
        }

        return this;
    }

    public AnexSearchPage putHotelsAmountAnexList(String departureValue, int dateCounterValue, int nightsCounterValue) throws Exception {
        sleep(CONSTANT_1_SECOND);
        $(byXpath(TOURS_LOADER)).waitUntil(Condition.disappear, CONSTANT_420_SECONDS);
        sleep(CONSTANT_1_SECOND);
        if(ifEmptyAnexSearch()){
            setHotelsAmountAnexList("0");
            setDepartureCityAnexList(departureValue);
            setActualDateCounterList(dateCounterValue);
            setActualNightsCounterList(nightsCounterValue);
        }
        else{
            setHotelsAmountAnexList($(byXpath(HOTELS_COUNTER_TITLE)).getText().replaceAll("\\D", ""));
            setDepartureCityAnexList(departureValue);
            setActualDateCounterList(dateCounterValue);
            setActualNightsCounterList(nightsCounterValue);
        }
        ifEmptySearch = false;

        return this;
    }

}
