package pageobjects.operators.Anex;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;

import java.util.ArrayList;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class AnexTourPage extends WebDriverCommands {

    private  static AnexTourPage anexTourPage = null;
    public AnexTourPage(){
    }
    public static AnexTourPage getAnexTourPage(){
        if(anexTourPage == null)
            anexTourPage = new AnexTourPage();

        return anexTourPage;
    }

    private final String FUEL_TAX_VALUE_FROM = "//h4[contains(text(), 'Топливная надбавка туда')]/following::span[contains(@class, 'font-bold')][1]";
    private final String FUEL_TAX_VALUE_TO = "//h4[contains(text(), 'Топливная надбавка обратно')]/following::span[contains(@class, 'font-bold')][1]";
    private final String FINAL_PRICE_VALUE = "//div[text() = 'Стоимость тура']/following-sibling::span[contains(@class, 'font-bold')]";
    private final String FINAL_PRICE_BLOCK = "//h4[text() = 'Детали бронирования']";
    private final String TOUR_IS_ABSENT_ERROR = "//p[text() = 'Ошибка 500']";

    private static ArrayList<String> hotelTourPriceActualizedAnexList = new ArrayList<String>();
    public static ArrayList <String> getHotelTourPriceActualizedAnexList(){
        return hotelTourPriceActualizedAnexList;
    }
    public static void setHotelTourPriceActualizedAnexList(String hotelTourPriceActualizedAnexListValue){
        hotelTourPriceActualizedAnexList.add(hotelTourPriceActualizedAnexListValue);
    }

    public void getFinalPrice() throws Exception{
        sleep(Constants.CONSTANT_1_SECOND);
        if ($(byXpath(TOUR_IS_ABSENT_ERROR)).isDisplayed())
            setHotelTourPriceActualizedAnexList("-");
        else{
            $(byXpath(FINAL_PRICE_BLOCK)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS);
            int finalValue = Integer.parseInt($(byXpath(FINAL_PRICE_VALUE)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).getText().replaceAll("\\D", ""));
            if($(byXpath(FUEL_TAX_VALUE_FROM)).isDisplayed())
                finalValue += Integer.parseInt($(byXpath(FUEL_TAX_VALUE_FROM)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).getText().replaceAll("\\D", ""));
            if($(byXpath(FUEL_TAX_VALUE_TO)).isDisplayed())
                finalValue += Integer.parseInt($(byXpath(FUEL_TAX_VALUE_TO)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).getText().replaceAll("\\D", ""));
            hotelTourPriceActualizedAnexList.add(String.valueOf(finalValue));
//        setHotelTourPriceActualizedAnexList(String.valueOf(Integer.parseInt(fuelTaxFromValue) + Integer.parseInt(fuelTaxToValue) + Integer.parseInt(finalTourPriceValue)));
        }

    }

}
