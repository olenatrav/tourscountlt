package pageobjects.operators.TUI_new;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;

import java.util.ArrayList;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class TuiTourPage extends WebDriverCommands {

    private  static TuiTourPage tuiTourPage = null;
    public TuiTourPage(){
    }
    public static TuiTourPage getTuiTourPage(){
        if(tuiTourPage == null)
            tuiTourPage = new TuiTourPage();

        checkServerErrorAndRefresh();

        return tuiTourPage;
    }

    private final String TOURS_LOADER_START = "//div[@class = 'plane-loader start']";
    private final String TOURS_LOADER_HIDE = "//div[@class = 'plane-loader hide']";
    private final String TOUR_LOADER_PROGRESS_CONTAINER = "//div[@class = 'flight-loader__container']";
    private final String DISABLED_TOUR_POPUP = "//h2[text() = 'Тур недоступен']";
    private final String DISABLED_TOUR_LABEL = "//p[text() = 'Тур недоступен']";
    private final String FINAL_PRICE_VALUE = "//div[@class = 'price-line total']/.//div[@class = 'value']/span";

    private static ArrayList<String> hotelTourPriceActualizedTuiList = new ArrayList<String>();
    public static ArrayList <String> getHotelTourPriceActualizedTuiList(){
        return hotelTourPriceActualizedTuiList;
    }
    public static void setHotelTourPriceActualizedTuiList(String hotelTourPriceActualizedTuiListValue){
        hotelTourPriceActualizedTuiList.add(hotelTourPriceActualizedTuiListValue);
    }

    private boolean isActualizationComplete() throws Exception {
        boolean ifComplete = true;
        $(byXpath(TOURS_LOADER_START)).waitUntil(Condition.disappear, Constants.CONSTANT_300_SECONDS);
        $(byXpath(TOURS_LOADER_HIDE)).waitUntil(Condition.exist, Constants.CONSTANT_300_SECONDS);
        while ($(byXpath(TOUR_LOADER_PROGRESS_CONTAINER)).isDisplayed())
            if ($(byXpath(DISABLED_TOUR_POPUP)).isDisplayed()){
                ifComplete =  false;
                break;
            }

        return ifComplete;
    }

    public boolean isTourAbsent() throws Exception{
        return $(byXpath(DISABLED_TOUR_LABEL)).isDisplayed();
    }

    public void getFinalPrice() throws Exception{
        sleep(Constants.CONSTANT_1_SECOND);
        if(!isTourAbsent()){
            if (!isActualizationComplete() || !$(byXpath(FINAL_PRICE_VALUE)).exists())
                setHotelTourPriceActualizedTuiList("-");
            else
                setHotelTourPriceActualizedTuiList($(byXpath(FINAL_PRICE_VALUE)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).getText().replaceAll("\\D", ""));
        }
        else
            setHotelTourPriceActualizedTuiList("-");
    }

}
