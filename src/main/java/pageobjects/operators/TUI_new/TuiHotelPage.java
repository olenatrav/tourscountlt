package pageobjects.operators.TUI_new;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class TuiHotelPage extends WebDriverCommands {

    private  static TuiHotelPage tuiHotelPage = null;
    public TuiHotelPage(){
    }
    public static TuiHotelPage getTuiHotelPage(){
        if(tuiHotelPage == null)
            tuiHotelPage = new TuiHotelPage();

        checkServerErrorAndRefresh();

        return tuiHotelPage;
    }

    private final String TOURS_LOADER_START = "//div[@class = 'plane-loader start']";
    private final String TOURS_LOADER_HIDE = "//div[@class = 'plane-loader hide']";
    private final String PICT_GALLERY = "//div[@class = 'slick-list']";
    private final String CALENDAR_BLOCK = "//div[@class = 'minPricesChart-mobile']";
    private final String TOURS_COUNT = "//div[@class = 'totalCount']";
    private final String SELECT_TOUR_SCROLL_BUTTON = "//button[@id = 'choose-tour-button-id']";
    private final String CHEAPEST_TOUR_BLOCK_BUTTON = "//div[@class = 'search-result-hotel'][1]/.//button[contains(@class, 'hotel-book-tour-class')]";

    public TuiHotelPage waitForLoaderDissapear() throws Exception{
        $(byXpath(TOURS_LOADER_START)).waitUntil(Condition.disappear, Constants.CONSTANT_300_SECONDS);
        $(byXpath(TOURS_LOADER_HIDE)).waitUntil(Condition.exist, Constants.CONSTANT_300_SECONDS);

        return this;
    }

    public TuiHotelPage waitForPictGalleryLoaded() throws Exception{
        $(byXpath(PICT_GALLERY)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS);

        return this;
    }

    public TuiHotelPage waitForCalendarLoaded() throws Exception{
        $(byXpath(CALENDAR_BLOCK)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS);

        return this;
    }

    public TuiTourPage gotoTuiTourPage() throws Exception{
        if(!$(byXpath(TOURS_COUNT)).isDisplayed())
            refreshPage();
        $(byXpath(SELECT_TOUR_SCROLL_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        sleep(Constants.CONSTANT_1_SECOND);
        $(byXpath(CHEAPEST_TOUR_BLOCK_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        return new TuiTourPage();
    }

}
