package pageobjects.operators.TUI_new;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;

import java.util.ArrayList;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static framework.Constants.*;
import static pageobjects.Travelata.OfferEmailSearchPage.setHotelTourOfferAmountList;
import static pageobjects.operators.TUI_new.TuiTourPage.setHotelTourPriceActualizedTuiList;

public class TuiSearchPage extends WebDriverCommands {

    private static TuiSearchPage tuiSearchPage = null;
    public TuiSearchPage(){
    }
    public static TuiSearchPage getTuiSearchPage(){
        if(tuiSearchPage == null)
            tuiSearchPage = new TuiSearchPage();

        checkServerErrorAndRefresh();

        return tuiSearchPage;
    }

    private final String SORT_PANEL = "//div[@class= 'sort-panel']";
    private final String FILTER_PANEL = "//ul[@class = 'search-filters__list']";
    private final String MEAL_CHECKBOX = "//span[text() = '%s']";
    private final String STARS_CHECKBOX = "//label[contains(@for, 'MultipleFilter_%s*')]";
    private final String SEARCH_RESULTS = "//div[@class= 'search-results']";
    private final String HOTEL_NAME_VALUE = "(//a[contains(@class, 'hotel-name-class')])[%s]";
    private final String HOTEL_STARS_VALUE = "(//div[@class= 'search-results']/.//span[@class = 'Stars'])[%s]/span[@class = 'star'][%s]";
    private final String HOTEL_COUNTRY_RESORT_VALUE = "(//div[@class = 'result-hotel-location'])[%s]";
    private final String HOTEL_RESORT_VALUE = "(//div[@class = 'result-hotel-location'])[%s]/a[@class = 'result-hotel-location'][%s]";
    private final String HOTEL_PRICE_VALUE = "(//span[@class = 'price'])[%s]";
    private final String SELECT_TOUR_BUTTON = "(//div[@class='arrow']/..)[%s]";
    private final String SHOW_MORE_BUTTON = "//button[@class = 'search-results__show-more']";
    private final String EMPTY_SEARCH_LABEL = "//div[contains(@class, 'EmptySearch')]";

    private static ArrayList<String> hotelNamesTuiList = new ArrayList<String>();
    public static ArrayList <String> getHotelNamesTuiList(){
        return hotelNamesTuiList;
    }
    public static void setHotelNamesTuiList(String hotelNamesTuiListValue){
        hotelNamesTuiList.add(hotelNamesTuiListValue);
    }

    private static ArrayList<String> hotelStarsTuiList = new ArrayList<String>();
    public static ArrayList <String> getHotelStarsTuiList(){
        return hotelStarsTuiList;
    }
    public static void setHotelStarsTuiList(String hotelStarsTuiListValue){
        hotelStarsTuiList.add(hotelStarsTuiListValue);
    }

    private static ArrayList<String> countryResortTuiList = new ArrayList<String>();
    public static ArrayList <String> getCountryResortTuiList(){
        return countryResortTuiList;
    }
    public static void setCountryResortTuiList(String countryResortTuiListValue){
        countryResortTuiList.add(countryResortTuiListValue);
    }

    private static ArrayList<String> hotelTourPriceTuiList = new ArrayList<String>();
    public static ArrayList <String> getHotelTourPriceTuiList(){
        return hotelTourPriceTuiList;
    }
    public static void setHotelTourPriceTuiList(String hotelTourPriceTuiListValue){
        hotelTourPriceTuiList.add(hotelTourPriceTuiListValue);
    }

    public TuiSearchPage waitForFilterPanel() throws Exception{
        $(byXpath(SORT_PANEL)).waitUntil(Condition.visible, Constants.CONSTANT_180_SECONDS);
        $(byXpath(FILTER_PANEL)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS);

        return this;
    }


    public TuiSearchPage selectMeal(ArrayList<String> mealValue) throws Exception{
        for(int i = 0; i<mealValue.size(); i++){
            $(byXpath(String.format(MEAL_CHECKBOX, hotelMealsTui.get(mealValue.get(i))))).waitUntil(Condition.visible, Constants.CONSTANT_2_SECONDS).click();
        }
        sleep(CONSTANT_1_SECOND);

        return this;
    }

    public TuiSearchPage selectStars(ArrayList<String> starsValue) throws Exception{
        for(int i = 0; i<starsValue.size(); i++){
            $(byXpath(String.format(STARS_CHECKBOX, starsValue.get(i)))).waitUntil(Condition.visible, Constants.CONSTANT_2_SECONDS).click();
        }
        sleep(CONSTANT_1_SECOND);

        return this;
    }

    public TuiSearchPage waitForSearchResult() throws Exception{
        $(byXpath(SORT_PANEL)).waitUntil(Condition.visible, Constants.CONSTANT_180_SECONDS);
        $(byXpath(SEARCH_RESULTS)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS);

        return this;
    }

    private boolean ifEmptySearchResult() throws Exception{
        waitForSearchResult();
        return $(byXpath(EMPTY_SEARCH_LABEL)).isDisplayed();
    }

    public String getHotelStarsValue(int hotelCounter){
        int starsCounter = 1;
        while($(byXpath(String.format(HOTEL_STARS_VALUE, hotelCounter, starsCounter))).isDisplayed())
            starsCounter ++;
        return String.valueOf(starsCounter-1);
    }

    public String getHotelResortValue(int hotelCounter){
        int locationCounter = 1;
        while($(byXpath(String.format(HOTEL_RESORT_VALUE, hotelCounter, locationCounter))).isDisplayed())
            locationCounter ++;
        locationCounter --;
        return $(byXpath(String.format(HOTEL_RESORT_VALUE, hotelCounter, locationCounter))).getText();
    }

    public void putHotelNamesTuiList(String countryToValue) throws Exception {

        if(!ifEmptySearchResult()){

            while ($(byXpath(SHOW_MORE_BUTTON)).isDisplayed()){
                $(byXpath(SHOW_MORE_BUTTON)).click();
                sleep(Constants.CONSTANT_1_SECOND);
            }

            scrollUpOnPage();

            int hotelCounter = 1;
            while($(byXpath(String.format(HOTEL_NAME_VALUE, hotelCounter))).isDisplayed()){
                setHotelNamesTuiList($(byXpath(String.format(HOTEL_NAME_VALUE, hotelCounter))).getText());
                setHotelStarsTuiList(getHotelStarsValue(hotelCounter) + "*");
                setCountryResortTuiList(getHotelResortValue(hotelCounter));
                setHotelTourPriceTuiList($(byXpath(String.format(HOTEL_PRICE_VALUE, hotelCounter))).getText().replaceAll("\\D", ""));

                //Actualization
                String oldTab = driver.getWindowHandle();

                scrollToElement(String.format(SELECT_TOUR_BUTTON, hotelCounter));
                String tourUrlValue = $(byXpath(String.format(SELECT_TOUR_BUTTON, hotelCounter))).getAttribute("href");
                openLinkInNewTab(tourUrlValue);

//                String hotelTab = driver.getWindowHandle();
//                ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
//                newTab.remove(hotelTab);
//                driver.switchTo().window(newTab.get(0));

                TuiHotelPage tuiHotelPage = new TuiHotelPage();
                tuiHotelPage
                        .waitForLoaderDissapear()
                        .waitForPictGalleryLoaded()
                        .waitForCalendarLoaded()
                        .gotoTuiTourPage();
                TuiTourPage tuiTourPage = new TuiTourPage();
                tuiTourPage.getFinalPrice();

                sleep(Constants.CONSTANT_1_SECOND);
                driver.close();
                driver.switchTo().window(oldTab);
                ///////////////////////////////////////

                hotelCounter ++;
            }
        }
        else
            putEmptyHotelNamesTuiList(countryToValue);


        setHotelTourOfferAmountList((double) getHotelNamesTuiList().size());

    }

    public void putEmptyHotelNamesTuiList(String countryToValue) throws Exception{
        setHotelNamesTuiList("-");
        setHotelStarsTuiList("-");
        setCountryResortTuiList(countryToValue);
        setHotelTourPriceTuiList("-");
        setHotelTourPriceActualizedTuiList("-");

    }

}
