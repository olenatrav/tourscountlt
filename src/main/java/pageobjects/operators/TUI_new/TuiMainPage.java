package pageobjects.operators.TUI_new;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;
import org.openqa.selenium.By;

import java.util.ArrayList;
import java.util.Arrays;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static framework.Constants.*;

public class TuiMainPage extends WebDriverCommands {

    private static TuiMainPage tuiMainPage = null;
    public TuiMainPage(){
    }
    public static TuiMainPage getTuiMainPage(){
        if(tuiMainPage == null)
            tuiMainPage = new TuiMainPage();

        checkServerErrorAndRefresh();

        return tuiMainPage;
    }

    private final String TUI_MAIN_URL = "https://www.tui.ru/";
    private final String SEARCH_PANEL = "//div[@class= 'SearchBox']";
    private final String CITY_FROM_SELECT = "//div[@id = 'departureCity']";
    private final String INPUT_VALUE = "//span[text() = '%s']";
    private final String COUNTRY_TO_SELECT = "//div[@id = 'arrival']";
    private final String DATE_SELECT = "//div[@id = 'departure_date']";
    private final String DATE_MONTH_VALUE = "(//div[@class = 'react-datepicker__current-month'])[1]"; //text
    private final String DATE_DAY_VALUE = "(//div[contains(@class, 'react-datepicker__day') and text() = '%s'])[1]";
    private final String DATE_NEXT_BUTTON = "//button[contains(@class, 'navigation--next')]";
    private final String NIGHTS_SELECT = "//div[@id = 'nights']";
    private final String NIGHTS_FROM_VALUE = "(//div[@class = 'counter-value-wrapper']/div[@class = 'value'])[1]";
    private final String NIGHTS_FROM_DECREMENT = "(//div[contains(@class, 'decrement')])[1]";
    private final String NIGHTS_FROM_INCREMENT = "(//div[contains(@class, 'increment')])[1]";
    private final String NIGHTS_TO_VALUE = "(//div[@class = 'counter-value-wrapper']/div[@class = 'value'])[2]";
    private final String NIGHTS_TO_DECREMENT = "(//div[contains(@class, 'decrement')])[2]";
    private final String NIGHTS_TO_INCREMENT = "(//div[contains(@class, 'increment')])[2]";
    private final String TOURISTS_SELECT = "//div[@id = 'tourists']";

    private final String SEARCH_BUTTON = "//button[@id = 'search-page-button-id']";
    private static final String FSTRAVEL_WIDGET = "(//iframe[@class = 'flocktory-widget'])[%s]";
    private static final String FSTRAVEL_WIDGET_CLOSE_BUTTON = "//div[@class = 'widget']/div[@class = 'close']";
    private static final String COOKIE_POLICY_PANEL = "//div[contains(@class, 'CookiePolicy')]";
    private static final String COOKIE_POLICY_BUTTON = "//div[contains(@class, 'CookiePolicy')]/.//button";


    public TuiMainPage gotoTuiMainPage() throws Exception{
        goToPage(TUI_MAIN_URL);

        return this;
    }

    public TuiMainPage waitForSearchPanelLoaded() throws Exception{
        $(byXpath(SEARCH_PANEL)).waitUntil(Condition.visible, CONSTANT_60_SECONDS);

        return this;
    }

    public TuiMainPage closeFstravelBanner() throws Exception{
        scrollUpOnPage();
        sleep(CONSTANT_2_SECONDS);
        int widgetCounter = 1;
        while($(byXpath(String.format(FSTRAVEL_WIDGET, widgetCounter))).exists()){
            driver.switchTo().frame(driver.findElement(By.xpath(String.format(FSTRAVEL_WIDGET, widgetCounter))));
            if($(byXpath(FSTRAVEL_WIDGET_CLOSE_BUTTON)).isDisplayed())
                $(byXpath(FSTRAVEL_WIDGET_CLOSE_BUTTON)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();
            driver.switchTo().defaultContent();

            widgetCounter ++;
        }
//        $(byXpath(CITY_FROM_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        return this;
    }

    public TuiMainPage acceptCookiePolicy() throws Exception{
        if($(byXpath(COOKIE_POLICY_PANEL)).isDisplayed())
            $(byXpath(COOKIE_POLICY_BUTTON)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public TuiMainPage selectCityFrom(String cityFromValue) throws Exception{
        $(byXpath(CITY_FROM_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        scrollToElement(String.format(INPUT_VALUE, cityFromValue));
        $(byXpath(String.format(INPUT_VALUE, cityFromValue))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        scrollUpOnPage();

        return this;
    }

    public TuiMainPage selectCountryTo(String countryToValue) throws Exception{
        $(byXpath(COUNTRY_TO_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        scrollToElement(String.format(INPUT_VALUE, countryToValue));
        $(byXpath(String.format(INPUT_VALUE, countryToValue))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        scrollUpOnPage();

        return this;
    }

    public TuiMainPage selectFlightDate(String flightDateValue) throws Exception{
        $(byXpath(DATE_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        ArrayList<String> dateList = new ArrayList<String>(Arrays.asList(flightDateValue.split("\\s+")));
        while(!$(byXpath(String.format(DATE_MONTH_VALUE, monthsRussian.get(dateList.get(1))))).isDisplayed())
            $(byXpath(DATE_NEXT_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        $(byXpath(String.format(DATE_DAY_VALUE, dateList.get(0)))).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        return this;
    }

    public TuiMainPage selectNightsFromValue(int nightsFromValue) throws Exception{
        $(byXpath(NIGHTS_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        String s = $(byXpath(NIGHTS_FROM_VALUE)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).getText();
        while(!$(byXpath(NIGHTS_FROM_VALUE)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).getText().equals(String.valueOf(nightsFromValue)))
            if(Integer.parseInt($(byXpath(NIGHTS_FROM_VALUE)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).getText()) < nightsFromValue)
                $(byXpath(NIGHTS_FROM_INCREMENT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
            else
                $(byXpath(NIGHTS_FROM_DECREMENT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        $(byXpath(NIGHTS_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        return this;
    }

    public TuiMainPage selectNightsToValue(int nightsToValue) throws Exception{
        $(byXpath(NIGHTS_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        while(!$(byXpath(NIGHTS_TO_VALUE)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).getText().equals(String.valueOf(nightsToValue)))
            if(Integer.parseInt($(byXpath(NIGHTS_TO_VALUE)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).getText()) < nightsToValue)
                $(byXpath(NIGHTS_TO_INCREMENT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
            else
                $(byXpath(NIGHTS_TO_DECREMENT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        $(byXpath(NIGHTS_SELECT)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        return this;
    }

    public TuiSearchPage clickSearchButton() throws Exception{
        $(byXpath(SEARCH_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();

        return new TuiSearchPage();
    }

}
