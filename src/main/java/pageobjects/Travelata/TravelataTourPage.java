package pageobjects.Travelata;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;
import pageobjects.LevelTravel.LevelTravelTourPage;

import java.util.ArrayList;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class TravelataTourPage extends WebDriverCommands {

    private static TravelataTourPage travelataTourPage = null;
    public TravelataTourPage(){

    }
    public static TravelataTourPage getTravelataTourPage(){
        if(travelataTourPage == null)
            travelataTourPage = new TravelataTourPage();

        checkServerErrorAndRefresh();

        return travelataTourPage;
    }

    private final String TOUR_LOADER = "//div[@class = 'progressValue' and @style = 'width: 100%;']";
    private final String PAYMENT_BLOCK = "//div[@class = 'paymentInfoLabel']";
    private final String TOTAL_PRICE_LABEL = "//span[@class = 'totalPriceValue']";
    private final String TOUR_PRICE_LABEL = "//span[@class = 'tourPriceValue']";
    private final String FUEL_PRICE_LABEL = "//span[@class = 'additionalTourPriceValue']";
    private final String TO_LABEL = "//img[@class = 'js-operator-logo']";
    private final String EXPIRED_TOUR_POPUP = "//div[contains(@class, 'expiredActualize')]";
    private final String EXPIRED_TOUR_LABEL ="//div[@class = 'search-empty__text']";
    private final String TOUR_IS_ABSENT_LABEL = "//div[@class = 'tourDataLoading-header' and text() = 'Выбранный тур только что раскупили']";
    private final String TOUR_IS_ABSENT_TITLE = "//div[@class = 'isAbsent__title']";
    private final String ERROR_TITLE = "//div[@class = 'error-code']";

    private static final String SEARCH_TOURS_LABEL = "//div[@class = 'brawn-progress-bar__searching-text']";
    private static final String TOUR_COMPLETE_LOADER = "//div[@class = 'progressValue' and @style = 'width: 100%;']";

    //////////////////////////////////////////////////////////////////////////////////////
    private static final String FLIGHTS_LOADER = "//div[@class = 'routes-list__loader js-routes-list__loader']";
    private static final String PRICE_ROUTE_CHANGED_CONTINUE_BUTTON = "//div[contains(@class, 'priceOrRouteChanged__button') and text() = 'Продолжить']";
    private static final String ROUTE_SOLD_CONTINUE_BUTTON = "//div[contains(@class, 'routesSold__button ') and text() = 'Продолжить']";
    private static final String TOUR_EXPIRED_CONTINUE_BUTTON = "//div[contains(@class, 'js-return-to-hotel-page') and text() = 'Выбрать другое']";
    private static final String MIN_TOUR_PRICE_LABEL = "(//span[@class = 'js-route-total-price'])[1]";
    private static final String MIN_OIL_TAX_LABEL = "(//span[@class = 'js-cheapest-price-difference'])[1]";
    private static final String MIN_TOUR_TO_LABEL = "(//ul[@class = 'routeItem__tour-description']/li[1])[1]";


    private static ArrayList<String> toNameTourTravList = new ArrayList<String>();
    public static ArrayList <String> getToNameTourTravList(){
        return toNameTourTravList;
    }
    public static void setToNameTourTravList (String toNameTourTravListValue){
        toNameTourTravList.add(toNameTourTravListValue);
    }

    private static ArrayList <String> hotelTourPricesTourTravList = new ArrayList<String>();
    public static ArrayList <String> getHotelTourPricesTourTravList(){
        return hotelTourPricesTourTravList;
    }
    public static void setHotelTourPricesTourTravList (String hotelTourPriceTourTravValue){
        hotelTourPricesTourTravList.add(hotelTourPriceTourTravValue);
    }

    private static   ArrayList <String> hotelTourPriceOilTaxTourTravList = new ArrayList<String>();
    public static ArrayList <String> getHotelTourPriceOilTaxTourTravList(){
        return hotelTourPriceOilTaxTourTravList;
    }
    public static void setHotelTourPriceOilTaxTourTravList (String hotelTourPriceOilTaxTourTravValue){
        hotelTourPriceOilTaxTourTravList.add(hotelTourPriceOilTaxTourTravValue);
    }

    private static ArrayList <String> hotelTourPriceWithoutOilTaxTourTravList = new ArrayList<String>();
    public static ArrayList <String> getHotelTourPriceWithoutOilTaxTourTravList() {
        return hotelTourPriceWithoutOilTaxTourTravList;
    }
    public static void setHotelTourPriceWithoutOilTaxTourTravList (String hotelTourPriceWithoutOilTaxTourTravValue){
        hotelTourPriceWithoutOilTaxTourTravList.add(hotelTourPriceWithoutOilTaxTourTravValue);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    public TravelataTourPage waitForLoaderDisappear() throws Exception{
        $(byXpath(TOUR_LOADER)).waitUntil(Condition.exist, Constants.CONSTANT_180_SECONDS);

        return this;
    }

    public boolean checkIfTourTravOutdated() throws Exception{
        sleep(Constants.CONSTANT_3_SECONDS);
        if ($(byXpath(SEARCH_TOURS_LABEL)).isDisplayed())
            $(byXpath(TOUR_COMPLETE_LOADER)).waitUntil(Condition.exist, Constants.CONSTANT_120_SECONDS);
        if ($(byXpath(EXPIRED_TOUR_POPUP)).isDisplayed() || $(byXpath(TOUR_IS_ABSENT_LABEL)).isDisplayed() || $(byXpath(EXPIRED_TOUR_LABEL)).isDisplayed() || $(byXpath(TOUR_IS_ABSENT_TITLE)).isDisplayed())
            return true;
        else
            return false;
    }

    public TravelataTourPage addEmptyTourTOPrice() throws Exception{
        setHotelTourPricesTourTravList("-");
        setHotelTourPriceOilTaxTourTravList("-");
        setHotelTourPriceWithoutOilTaxTourTravList("-");
        setToNameTourTravList("-");
        return this;
    }

    public TravelataTourPage addTourTOPrice() throws Exception{

        setHotelTourPricesTourTravList($(byXpath(TOTAL_PRICE_LABEL)).waitUntil(Condition.visible, Constants.CONSTANT_300_SECONDS).getText().replaceAll("\\D", ""));
        setHotelTourPriceOilTaxTourTravList($(byXpath(FUEL_PRICE_LABEL)).waitUntil(Condition.visible, Constants.CONSTANT_10_SECONDS).getText().replaceAll("\\D", ""));
        setHotelTourPriceWithoutOilTaxTourTravList($(byXpath(TOUR_PRICE_LABEL)).waitUntil(Condition.visible, Constants.CONSTANT_10_SECONDS).getText().replaceAll("\\D", ""));
        setToNameTourTravList($(byXpath(TO_LABEL)).waitUntil(Condition.visible, Constants.CONSTANT_10_SECONDS).getAttribute("title"));

        return this;
    }

    public TravelataTourPage reloadIfErrorDisplayed() throws Exception{
        if ($(byXpath(ERROR_TITLE)).isDisplayed())
            refreshPage();

        return this;

    }

    ///////////////////////////////////////////////////////Constructor
    public TravelataTourPage waitForFlightsLoaded() throws Exception{
        $(byXpath(FLIGHTS_LOADER)).waitUntil(Condition.disappear, Constants.CONSTANT_300_SECONDS);
        sleep(Constants.CONSTANT_1_SECOND);
        if($(byXpath(PRICE_ROUTE_CHANGED_CONTINUE_BUTTON)).isDisplayed()){
            $(byXpath(PRICE_ROUTE_CHANGED_CONTINUE_BUTTON)).click();
            sleep(Constants.CONSTANT_1_SECOND);
        }
        if($(byXpath(ROUTE_SOLD_CONTINUE_BUTTON)).isDisplayed()){
            $(byXpath(ROUTE_SOLD_CONTINUE_BUTTON)).click();
            sleep(Constants.CONSTANT_1_SECOND);
        }

        return this;
    }

    public TravelataTourPage addCheapestTourPrice() throws Exception{
        setHotelTourPricesTourTravList($(byXpath(MIN_TOUR_PRICE_LABEL)).waitUntil(Condition.visible, Constants.CONSTANT_300_SECONDS).getText().replaceAll("\\D", ""));
        setHotelTourPriceOilTaxTourTravList($(byXpath(MIN_OIL_TAX_LABEL)).waitUntil(Condition.visible, Constants.CONSTANT_300_SECONDS).getText().replaceAll("\\D", ""));
        setHotelTourPriceWithoutOilTaxTourTravList($(byXpath(MIN_TOUR_PRICE_LABEL)).waitUntil(Condition.visible, Constants.CONSTANT_300_SECONDS).getText().replaceAll("\\D", ""));
        setToNameTourTravList($(byXpath(MIN_TOUR_TO_LABEL)).waitUntil(Condition.visible, Constants.CONSTANT_300_SECONDS).getText());

        return this;
    }

    public boolean checkIfTourOutdated() throws Exception{
        sleep(Constants.CONSTANT_3_SECONDS);
        if ($(byXpath(SEARCH_TOURS_LABEL)).isDisplayed())
            $(byXpath(TOUR_COMPLETE_LOADER)).waitUntil(Condition.exist, Constants.CONSTANT_120_SECONDS);
        if ($(byXpath(EXPIRED_TOUR_POPUP)).isDisplayed() || $(byXpath(TOUR_IS_ABSENT_LABEL)).isDisplayed() || $(byXpath(EXPIRED_TOUR_LABEL)).isDisplayed() || $(byXpath(TOUR_IS_ABSENT_TITLE)).isDisplayed()
        || $(byXpath(TOUR_EXPIRED_CONTINUE_BUTTON)).isDisplayed()) //constructor
            return true;
        else
            return false;
    }

}
