package pageobjects.Travelata;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;
import org.apache.poi.ss.formula.functions.T;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static framework.Constants.CONSTANT_1_SECOND;
import static framework.Constants.CONSTANT_5_SECONDS;

public class TravelataHotelPage extends WebDriverCommands {

    private static TravelataHotelPage travelataHotelPage = null;
    public TravelataHotelPage(){

    }
    public static TravelataHotelPage getTravelataHotelPage(){
        if(travelataHotelPage == null)
            travelataHotelPage = new TravelataHotelPage();

        checkServerErrorAndRefresh();

        return travelataHotelPage;
    }

    private static final String HOTEL_H1 = "//h1";
    private static final String HOTEL_DETAILS_BLOCK = "//div[@class = 'hotelDetails']";
    private static final String SEARCH_TOURS_LABEL = "//div[@class = 'brawn-progress-bar__searching-text']";
    private static final String TOUR_COMPLETE_LOADER = "//div[@class = 'progressValue' and @style = 'width: 100%;']";
    private static final String CASHBACK_POPUP = "//div[@class = 'cashbackDg__header']";
    private static final String CASHBACK_POPUP_CLOSE_BUTTON = "//div[@class = 'cashbackDg__header']/div[@class = 'popupClose']/i";
    private static final String LOTTERY_POPUP = "//div[contains(@class, 'js-lottery-dg')]";
    private static final String LOTTERY_POPUP_CLOSE_BUTTON = "//div[contains(@class, 'js-lottery-dg')]/div[@class = 'popupClose']";
    private final String TOUR_BLOCK = "(//div[@class = 'hotelTour'])[1]";
    private final String HOTEL_CONTENT_BLOCK = "//div[@class = 'hotelContent__title-text']";
    private final String HOTEL_TOUR_BLOCK = "(//div[@class = 'hotelTour__container'])[1]";
    private final String DATE_TAB_BLOCK = "datesTabsComponentPlace"; //id
    private final String FAQ_WIDGET_ICON = "//div[@class = 'faqWidget-slim__icon']";
    private final String FAQ_WIDGET_CLOSE_ICON = "//div[contains(@class, 'faqWidget-to-slim')]";
    private static final String PAGE_HEADER = "//div[@class = 'headerContainer__container']";
    private static final String DEPARTURE_CITY_BLOCK = "//div[@class = 'hotelContent__departureCity']";
    private final String ACTUALIZE_TOUR_LINK ="(//span[@class = 'actualize'])[1]";
    private final String GOTO_TOUR_PAGE_BUTTON = "(//div[@class = 'hotelTour__air-info__night'])[1]"; // "(//div[@class = 'hotelTour__tour'])[1]/.//div[@class = 'hotelTour__air-info__night']/span";
    private static final String TOUR_HOTEL_CONTAINER = "//div[@class = 'hotelTour__container']";
    private final String FIRST_TOUR_PRICE_BUTTON = "(//div[@class = 'hotelTour__price-block__btn'])[1]";
    private static final String EMPTY_SEARCH_LABEL = "//div[@class = 'search-empty']";
    private static final String EMPTY_PAGE_LABEL = "//div[@class = 'content-404']";
    private static final String TOUR_IS_ABSENT_LABEL = "//div[@class = 'isAbsent']";
    private static final String TOUR_NOT_FOUND_LABEL = "//p[text() = 'К сожалению, по вашим критериям ничего не найдено ']";
    private final String ERROR_TITLE = "//div[@class = 'error-code']";

    public static String getHotelDetailsBlock(){ return HOTEL_DETAILS_BLOCK; }

    public static boolean checkIfEmptyPage() throws Exception{
        if ($(byXpath(EMPTY_PAGE_LABEL)).isDisplayed())
            return true;
        else
            return false;
    }

    public static TravelataHotelPage closeCashBackPopup() throws Exception{
        if ($(byXpath(CASHBACK_POPUP)).isDisplayed())
            $(byXpath(CASHBACK_POPUP_CLOSE_BUTTON)).waitUntil(Condition.visible, CONSTANT_5_SECONDS).click();

        return new TravelataHotelPage();
    }

    public static TravelataHotelPage closeLotteryPopup() throws Exception{
        if ($(byXpath(LOTTERY_POPUP)).isDisplayed())
            $(byXpath(LOTTERY_POPUP_CLOSE_BUTTON)).waitUntil(Condition.visible, CONSTANT_5_SECONDS).click();

        return new TravelataHotelPage();
    }

    public static TravelataHotelPage waitForLoaderDisappear() throws Exception{
        int loadCounter = 1;
        while(!$(byXpath(PAGE_HEADER)).isDisplayed()){
            refreshPage();
            System.out.println("Error 500, page refreshed! " + loadCounter);
            sleep(Constants.CONSTANT_1_SECOND);
            closeCashBackPopup();
            sleep(CONSTANT_1_SECOND);
            closeLotteryPopup();
            loadCounter ++;
        }
        $(byXpath(DEPARTURE_CITY_BLOCK)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS);
        WebElement currentHotelBlock = driver.findElement(By.xpath(DEPARTURE_CITY_BLOCK));
        Actions actions = new Actions(driver);
        actions.moveToElement(currentHotelBlock);
        actions.perform();
        if ($(byXpath(SEARCH_TOURS_LABEL)).isDisplayed())
            $(byXpath(TOUR_COMPLETE_LOADER)).waitUntil(Condition.exist, Constants.CONSTANT_300_SECONDS);

        return new TravelataHotelPage();
    }

    public static boolean checkIfEmptySearch() throws Exception{
        if ($(byXpath(EMPTY_SEARCH_LABEL)).isDisplayed() || $(byXpath(TOUR_NOT_FOUND_LABEL)).isDisplayed() || !($(byXpath(TOUR_HOTEL_CONTAINER)).isDisplayed()))
            return true;
        else
            return false;
    }

    public TravelataHotelPage closeFaqBanner() throws Exception{
        $(byXpath(FAQ_WIDGET_ICON)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        $(byXpath(FAQ_WIDGET_CLOSE_ICON)).waitUntil(Condition.visible, Constants.CONSTANT_5_SECONDS).click();

        return this;
    }

    public TravelataTourPage gotoTravelataTourPage() throws Exception{
//        $(byXpath(HOTEL_TOUR_BLOCK)).waitUntil(Condition.visible, Constants.CONSTANT_180_SECONDS).scrollTo();
//        $(byXpath(ACTUALIZE_TOUR_LINK)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).click();
        $(byXpath(HOTEL_H1)).waitUntil(Condition.visible, Constants.CONSTANT_180_SECONDS).scrollTo();
        $(byXpath(FIRST_TOUR_PRICE_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_300_SECONDS).click(); //GOTO_TOUR_PAGE_BUTTON

        return new TravelataTourPage();
    }

    public static boolean checkIfTourAbsent() throws Exception{
        if ($(byXpath(TOUR_IS_ABSENT_LABEL)).isDisplayed())
            return true;
        else
            return false;
    }

    public TravelataHotelPage reloadIfErrorDisplayed() throws Exception{
        if ($(byXpath(ERROR_TITLE)).isDisplayed())
            refreshPage();

        return this;

    }

}
