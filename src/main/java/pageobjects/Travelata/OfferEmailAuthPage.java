package pageobjects.Travelata;

import com.codeborne.selenide.Condition;
import framework.WebDriverCommands;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static framework.Constants.CONSTANT_10_SECONDS;
import static framework.Constants.CONSTANT_1_SECOND;

public class OfferEmailAuthPage extends WebDriverCommands {

    private static OfferEmailAuthPage offerEmailAuthPage = null;
    public OfferEmailAuthPage(){
    }
    public static OfferEmailAuthPage getOfferEmailAuthPage(){
        if(offerEmailAuthPage == null)
            offerEmailAuthPage = new OfferEmailAuthPage();

        checkServerErrorAndRefresh();

        return offerEmailAuthPage;
    }

    private final String OFFER_EMAIL_AUTH_URL = "http://offer.travadm.org/index/auth";
    private final String OFFER_EMAIL_START_URL = "http://offer.travadm.org/index";
    private final String OFFER_EMAIL_AUTH_LABEL = "//div[@class = 'signin-info']";
    private final String EMAIL_INPUT = "//input[@name = 'email']";
    private final String PASSWORD_INPUT = "//input[@name = 'password']";
    private final String ENTER_BUTTON = "//input[@name = 'submit']";

    public OfferEmailAuthPage gotoOfferEmailAuthPage() throws Exception{
        goToPage(OFFER_EMAIL_AUTH_URL);

        return this;
    }

    public OfferEmailAuthPage gotoOfferEmailPage() throws Exception{
        goToPage(OFFER_EMAIL_START_URL);

        return this;
    }

    public OfferEmailSearchPage enterOfferEmail(String nameValue, String passwordValue) throws Exception{
        if ($(byXpath(OFFER_EMAIL_AUTH_LABEL)).isDisplayed()){
            $(byXpath(EMAIL_INPUT)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).setValue(nameValue);
            $(byXpath(PASSWORD_INPUT)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).setValue(passwordValue);
            $(byXpath(ENTER_BUTTON)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();
            sleep(CONSTANT_1_SECOND);
        }

        return new OfferEmailSearchPage();
    }

}
