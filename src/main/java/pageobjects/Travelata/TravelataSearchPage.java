package pageobjects.Travelata;

import com.codeborne.selenide.Condition;
import framework.WebDriverCommands;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import pageobjects.LevelTravel.LevelTravelSearchPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static framework.Constants.*;
import static pageobjects.LevelTravel.LevelTravelSearchPage.getTOList;

public class TravelataSearchPage extends WebDriverCommands {

    private static TravelataSearchPage travelataSearchPage = null;
    public TravelataSearchPage(){

    }
    public static TravelataSearchPage getTravelataSearchPage(){
        if(travelataSearchPage == null)
            travelataSearchPage = new TravelataSearchPage();

        checkServerErrorAndRefresh();

        return travelataSearchPage;
    }

    private final String HEADER_MENU_PANEL = "siteMenu"; //id
    private final String SEARCH_PROGRESS_BAR = "//div[@id = 'searchProgressBar' and @style = 'display: none;']";
    private final String SORT_BY_PRICE_BUTTON = "//span[@class = 'serpFilterName' and contains(text(), 'от дешевых к дорогим')]";
    private final String SORT_BY_PRICE_BUTTON_ACTIVE = "//span[@class = 'serpFilterName checked' and contains(text(), 'от дешевых к дорогим')]";
    private final static String HOTEL_BLOCK = "(//div[@class = 'serpHotelCard__container'])[%s]";
    private final static String HOTEL_STARS = "(//div[@class = 'serpHotelCard__container'])[%s]/.//div[@class = 'serpHotelCard__stars']";
    private final static String HOTEL_NAME_LINK = "(//div[@class = 'serpHotelCard__container'])[%s]/.//a[contains(@class, 'serpHotelCard__title')]";
    private final String HOTEL_TOUR_PRICE = "(//div[@class = 'serpHotelCard__container'])[%s]/.//span[@class = 'serpHotelCard__btn-price']/span";
    private final String HOTEL_TOUR_OIL_TAX_PRICE = "(//div[@class = 'serpHotelCard__container'])[%s]/.//span[@class = 'serpHotelCard__btn-oilTax']/span";
    private final static String EMPTY_SEARCH_BUNNER = "//div[@class = 'no-tours-banner']";
    private final String TO_FILTER = "(//input[contains(@data-layer-name, 'туроператор_')])[%s]"; //data-count, data-layer-name attributes

    private final String LOTTERY_POPUP = "//div[contains(@class, 'js-lottery-dg')]";
    private final String LOTTERY_POPUP_CLOSE_BUTTON = "//div[contains(@class, 'js-lottery-dg')]/div[@class = 'popupClose']";

    private final String CASHBACK_POPUP = "//div[@class = 'cashbackDg__header']";
    private final String CASHBACK_POPUP_CLOSE_BUTTON = "//div[@class = 'cashbackDg__header']/div[@class = 'popupClose']/i";

    private final String HR_POPUP = "//div[@class = 'hrdPromo__container']";
    private final String HR_POPUP_CLOSE_BUTTON = "//div[@class = 'hrdPromo__container']/div[@class = 'popupClose']";

    private final String PAYLATE_POPUP = "//div[@class = 'paylateInformer__container']";
    private final String PAYLATE_POPUP_CLOSE_BUTTON = "//div[contains(@class, 'paylateInformer')]/i[contains(@class, 'popoverStackItem__closeButton')]";

    private final String EXPIRED_POPUP = "//div[@id = 'expired']";
    private final String EXPIRED_POPUP_CLOSE_BUTTON = "//div[@id = 'expired']/div[@class = 'popupClose']";

    private final static String COVID_BANNER = "//div[@class = 'covid-serp-banner']";

    public static String getTravHotelBlock(){ return HOTEL_BLOCK; }

    public static String getTravHotelNameLink(){ return HOTEL_NAME_LINK; }

    public static String getTravHotelStars(){ return HOTEL_STARS; }

    //////////////////////////////////////////////////////////////////////////////////////
    private static ArrayList<String> hotelNamesSearchTravList = new ArrayList<String>();
    public static ArrayList <String> getHotelNamesSearchTravList(){
        return hotelNamesSearchTravList;
    }
    public static void setHotelNamesSearchTravList (String hotelNamesSearchTravListValue){
        hotelNamesSearchTravList.add(hotelNamesSearchTravListValue);
    }

    private static ArrayList <String> hotelTourPricesSearchTravList = new ArrayList<String>();
    public static ArrayList <String> getHotelTourPricesSearchTravList(){
        return hotelTourPricesSearchTravList;
    }
    public static void setHotelTourPricesSearchTravList (String hotelTourPriceSearchTravValue){
        hotelTourPricesSearchTravList.add(hotelTourPriceSearchTravValue);
    }
    public static void resetHotelTourPricesSearchTravList (int i, String hotelTourPriceSearchTravValue){
        hotelTourPricesSearchTravList.set(i, hotelTourPriceSearchTravValue);
    }


    private static ArrayList <String> hotelTourPriceOilTaxSearchTravList = new ArrayList<String>();
    public static ArrayList <String> getHotelTourPriceOilTaxSearchTravList(){
        return hotelTourPriceOilTaxSearchTravList;
    }
    public static void setHotelTourPriceOilTaxSearchTravList (String hotelTourPriceOilTaxSearchTravValue){
        hotelTourPriceOilTaxSearchTravList.add(hotelTourPriceOilTaxSearchTravValue);
    }

    private static ArrayList <String> hotelTourPriceWithoutOilTaxSearchTravList = new ArrayList<String>();
    public static ArrayList <String> getHotelTourPriceWithoutOilTaxSearchTravList(){
        return hotelTourPriceWithoutOilTaxSearchTravList;
    }
    public static void setHotelTourPriceWithoutOilTaxSearchTravList (String hotelTourPriceWithoutOilTaxSearchTravValue){
        hotelTourPriceWithoutOilTaxSearchTravList.add(hotelTourPriceWithoutOilTaxSearchTravValue);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    public TravelataSearchPage gotoTravelataMain() throws Exception{
        goToPage(HTTPS + TRAVELATA_URL + AB);

        return this;
    }

    public TravelataSearchPage gotoSearchPageWithCriteria (String countryValue, String resortValue, int dateCounter, boolean isFlexibleDate, int nightsValue, boolean isFlexibleNights,
                                                             String adultsValue, ArrayList<String> kidsAges, String departureValue, ArrayList<String> starsValues) throws Exception{

        String searchUrlByCountryResort = HTTPS + TRAVELATA_URL + "search#?fromCity=" + departureOffer.get(departureValue) + "&toCountry=" + destinationCountryOffer.get(countryValue); // + "&toCity=" 1624, 1615, 1660, 1798, 1691
        if(!resortValue.isEmpty())
            searchUrlByCountryResort += "&toCity=" + resortValue;

        Date dt = new Date();
        Calendar c1 = Calendar.getInstance();
        Calendar c2 = Calendar.getInstance();
        c1.setTime(dt);
        c2.setTime(dt);
        int flexibleDateValue;
        String searchDateFrom;
        String searchDateTo;
//        if (departureValue.equals("Москва"))
//            flexibleDateValue = 3;
//        else flexibleDateValue = 5;
        flexibleDateValue = 6;
        if (isFlexibleDate){
            c1.add(Calendar.DATE, dateCounter - flexibleDateValue);
            c2.add(Calendar.DATE, dateCounter + flexibleDateValue);
        }
        else{
            c1.add(Calendar.DATE, dateCounter);
            c2.add(Calendar.DATE, dateCounter);
        }
//        c.add(Calendar.DATE, dateCounter);
//        String searchDate = new SimpleDateFormat("dd.MM.yyyy").format(c.getTime());
        searchDateFrom = new SimpleDateFormat("dd.MM.yyyy").format(c1.getTime());
        searchDateTo = new SimpleDateFormat("dd.MM.yyyy").format(c2.getTime());

        searchUrlByCountryResort += "&dateFrom=" + searchDateFrom + "&dateTo=" + searchDateTo; //searchDate

        if (isFlexibleNights)
            searchUrlByCountryResort += "&nightFrom=" + (nightsValue - 2) + "&nightTo=" + (nightsValue + 2);
        else
            searchUrlByCountryResort += "&nightFrom=" + (nightsValue) + "&nightTo=" + (nightsValue);

        searchUrlByCountryResort += "&adults=" + adultsValue;

        if (kidsAges.get(0).equals("0")){
            searchUrlByCountryResort += "&kids=0&infants=0";
        }
        else{
            int kidsCountValue = 0;
            int infantsCountValue = 0;
            for (int i=0; i< kidsAges.size(); i++) {
                if (Integer.parseInt(kidsAges.get(i)) > 1)
                    kidsCountValue ++;
                else
                    infantsCountValue ++;
            }
            searchUrlByCountryResort += "&kids=" + kidsCountValue + "&infants=" + infantsCountValue;
        }

        if(starsValues.size() > 0){
            searchUrlByCountryResort += "&hotelClass=";
            for(int i=0; i<starsValues.size(); i++)
                searchUrlByCountryResort += hotelStarsOffer.get(starsValues.get(i)) + ".";
        }

        searchUrlByCountryResort += "&meal=all&priceFrom=6000&priceTo=1000000";

        if ($(byXpath(LOTTERY_POPUP)).isDisplayed())
            $(byXpath(LOTTERY_POPUP_CLOSE_BUTTON)).waitUntil(Condition.visible, CONSTANT_20_SECONDS).click();

        if(ifEmptyTravSearch())
            refreshPage();
        goToPage(searchUrlByCountryResort);
        System.out.println(searchUrlByCountryResort);

        if (!$(byId(HEADER_MENU_PANEL)).isDisplayed())
            refreshPage();
        sleep(CONSTANT_1_SECOND);
        closeLotteryPopup();
        $(byXpath(SEARCH_PROGRESS_BAR)).waitUntil(Condition.exist, CONSTANT_300_SECONDS);

        return this;
    }

    public TravelataSearchPage closeLotteryPopup() throws Exception{
        if ($(byXpath(LOTTERY_POPUP)).isDisplayed())
            $(byXpath(LOTTERY_POPUP_CLOSE_BUTTON)).waitUntil(Condition.visible, CONSTANT_5_SECONDS).click();

        return this;
    }

    public TravelataSearchPage closeCashBackPopup() throws Exception{
        if ($(byXpath(CASHBACK_POPUP)).isDisplayed())
            $(byXpath(CASHBACK_POPUP_CLOSE_BUTTON)).waitUntil(Condition.visible, CONSTANT_5_SECONDS).click();

        return this;
    }

    public TravelataSearchPage closeHRPopup() throws Exception{
        if ($(byXpath(HR_POPUP)).isDisplayed())
            $(byXpath(HR_POPUP_CLOSE_BUTTON)).waitUntil(Condition.visible, CONSTANT_5_SECONDS).click();

        return this;
    }

    public TravelataSearchPage closePaylatePopup() throws Exception{
        if ($(byXpath(PAYLATE_POPUP)).isDisplayed())
            $(byXpath(PAYLATE_POPUP_CLOSE_BUTTON)).waitUntil(Condition.visible, CONSTANT_5_SECONDS).click();

        return this;
    }

    public TravelataSearchPage closeExpiredPopup() throws Exception{
        if ($(byXpath(EXPIRED_POPUP)).isDisplayed())
            $(byXpath(EXPIRED_POPUP_CLOSE_BUTTON)).waitUntil(Condition.visible, CONSTANT_5_SECONDS).click();

        return this;
    }

    public TravelataSearchPage clickSortByPriceLink() throws Exception{

        if (!$(byXpath(SORT_BY_PRICE_BUTTON_ACTIVE)).isDisplayed())
            $(byXpath(SORT_BY_PRICE_BUTTON)).waitUntil(Condition.visible, CONSTANT_60_SECONDS).click();

        $(byXpath(SEARCH_PROGRESS_BAR)).waitUntil(Condition.exist, CONSTANT_120_SECONDS);

        return this;
    }

    public TravelataSearchPage getHotelNameTourPriceOilTax(String countryValue, String departureValue, String reportPathValue) throws Exception{

        FileInputStream fsIP= new FileInputStream(new File(reportPathValue));
        Workbook book = new HSSFWorkbook(fsIP);
        Sheet sheet = book.getSheet(departureValue + " " + countryValue);
        CellStyle style = book.createCellStyle();
        Font font = book.createFont();
        font.setBold(true);
        style.setFont(font);

        int rowCounter = 22;
        int cellNum = sheet.getRow(rowCounter).getLastCellNum();
        if (cellNum > 0)
            cellNum -= 5;

        Row row = sheet.getRow(rowCounter);
        Cell hotel = row.createCell(cellNum);
        hotel.setCellStyle(style);

        Cell showcase = row.createCell(cellNum + 1);
        showcase.setCellStyle(style);

        Cell priceWithOilTax = row.createCell(cellNum + 2);
        priceWithOilTax.setCellStyle(style);

        Cell oilTax = row.createCell(cellNum + 3);
        oilTax.setCellStyle(style);

        Cell priceWithoutOilTax = row.createCell(cellNum + 4);
        priceWithoutOilTax.setCellStyle(style);

        int i=0;
        while (i<20){
            int hotelBlockCounter = 1;
            while ($(byXpath(String.format(HOTEL_BLOCK, hotelBlockCounter))).isDisplayed()){
                boolean isUniqueHotel = true;
                if (i>0){
                    for (int hotelCounter = 0; hotelCounter < i; hotelCounter ++){
                        if (getHotelNamesSearchTravList().get(hotelCounter).equals($(byXpath(String.format(HOTEL_NAME_LINK, hotelBlockCounter))).getText()))
                            isUniqueHotel = false;
                    }
                }
                if (isUniqueHotel){
                    setHotelNamesSearchTravList($(byXpath(String.format(HOTEL_NAME_LINK, hotelBlockCounter))).getText());
                    setHotelTourPricesSearchTravList($(byXpath(String.format(HOTEL_TOUR_PRICE, hotelBlockCounter))).getText().replaceAll("\\D", ""));
                    try{
                        if ($(byXpath(String.format(HOTEL_TOUR_OIL_TAX_PRICE, hotelBlockCounter))).getText().replaceAll("\\D", "").isEmpty()){
                            setHotelTourPriceOilTaxSearchTravList("-");
                            if(getHotelTourPricesSearchTravList().get(i).equals(""))
                                resetHotelTourPricesSearchTravList(i, "0");
                            setHotelTourPriceWithoutOilTaxSearchTravList(String.valueOf(Integer.parseInt(getHotelTourPricesSearchTravList().get(i))));
                        }
                        else{
                            setHotelTourPriceOilTaxSearchTravList($(byXpath(String.format(HOTEL_TOUR_OIL_TAX_PRICE, hotelBlockCounter))).getText().replaceAll("\\D", ""));
                            setHotelTourPriceWithoutOilTaxSearchTravList(String.valueOf(Integer.parseInt(getHotelTourPricesSearchTravList().get(i)) -
                                    Integer.parseInt(getHotelTourPriceOilTaxSearchTravList().get(i))));
                        }
                    }
                    catch (NumberFormatException ex){

                    }

                    if (rowCounter <= sheet.getLastRowNum()  && sheet.getLastRowNum() > 0)
                        row = sheet.getRow(rowCounter);
                    else
                        row = sheet.createRow(rowCounter);

                    hotel = row.createCell(cellNum);
                    hotel.setCellValue(getHotelNamesSearchTravList().get(i));

                    showcase = row.createCell(cellNum + 1);
                    showcase.setCellValue("Travelata");

                    priceWithOilTax = row.createCell(cellNum + 2);
                    priceWithOilTax.setCellValue(getHotelTourPricesSearchTravList().get(i));

                    oilTax = row.createCell(cellNum + 3);
                    oilTax.setCellValue(getHotelTourPriceOilTaxSearchTravList().get(i));

                    priceWithoutOilTax = row.createCell(cellNum + 4);
                    priceWithoutOilTax.setCellValue(getHotelTourPriceWithoutOilTaxSearchTravList().get(i));

                    rowCounter ++;
                    i++;
                }
                hotelBlockCounter ++;
            }

            WebElement currentHotelBlock = driver.findElement(By.xpath(String.format(HOTEL_STARS, hotelBlockCounter - 1)));
            Actions actions = new Actions(driver);
            actions.moveToElement(currentHotelBlock);
            actions.perform();
        }

        book.write(new FileOutputStream(reportPathValue));
        book.close();

        return this;
    }

    public TravelataSearchPage addHotelNameTourPriceOilTax(int hotelBlockCounter) throws Exception{

        setHotelNamesSearchTravList($(byXpath(String.format(HOTEL_NAME_LINK, hotelBlockCounter))).getText());
        setHotelTourPricesSearchTravList($(byXpath(String.format(HOTEL_TOUR_PRICE, hotelBlockCounter))).getText().replaceAll("\\D", ""));
        try{
            if ($(byXpath(String.format(HOTEL_TOUR_OIL_TAX_PRICE, hotelBlockCounter))).getText().replaceAll("\\D", "").isEmpty()){
                setHotelTourPriceOilTaxSearchTravList("-");
                if(getHotelTourPricesSearchTravList().get(getHotelTourPricesSearchTravList().size()-1).equals(""))
                    resetHotelTourPricesSearchTravList(getHotelTourPricesSearchTravList().size()-1, "0");

                setHotelTourPriceWithoutOilTaxSearchTravList(String.valueOf(Integer.parseInt(getHotelTourPricesSearchTravList().get(getHotelTourPricesSearchTravList().size()-1))));
            }

            else{
                setHotelTourPriceOilTaxSearchTravList($(byXpath(String.format(HOTEL_TOUR_OIL_TAX_PRICE, hotelBlockCounter))).getText().replaceAll("\\D", ""));
                setHotelTourPriceWithoutOilTaxSearchTravList(String.valueOf(Integer.parseInt(getHotelTourPricesSearchTravList().get(getHotelTourPricesSearchTravList().size()-1)) -
                        Integer.parseInt(getHotelTourPriceOilTaxSearchTravList().get(getHotelTourPriceOilTaxSearchTravList().size()-1))));
            }
        }
        catch (NumberFormatException ex){

        }

        return this;
    }

    public TravelataHotelPage gotoTravelataHotelPage(int hotelBlockCounter) throws Exception{
        closeLotteryPopup();

        $(byXpath(String.format(HOTEL_NAME_LINK, hotelBlockCounter))).waitUntil(Condition.visible, CONSTANT_60_SECONDS).click();

        return new TravelataHotelPage();
    }

    public TravelataSearchPage clearLastHotelNameTourPrice() throws Exception{
        if (getHotelNamesSearchTravList().size()>0)
            getHotelNamesSearchTravList().remove(getHotelNamesSearchTravList().size()-1);
        if (getHotelTourPricesSearchTravList().size()>0)
            getHotelTourPricesSearchTravList().remove(getHotelTourPricesSearchTravList().size()-1);

        return this;
    }

    public static boolean ifEmptyTravSearch() throws Exception{
        if ($(byXpath(EMPTY_SEARCH_BUNNER)).isDisplayed() || $(byXpath(COVID_BANNER)).isDisplayed())
            return true;
        else
            return false;
    }

    public TravelataSearchPage getTravStatisticByTO(Workbook book, String countryValue, int dateCounter, boolean isFlexibleDate, int nightsValue, boolean isFlexibleNights,
                                                      String adultsValue, ArrayList<String> kidsAges, String departureValue, ArrayList<String> starsValues, String reportPathValue) throws Exception{

//        FileInputStream fsIP= new FileInputStream(new File(reportPathValue));
//        Workbook book = new HSSFWorkbook(fsIP);

        Sheet sheet = book.getSheet(departureValue + "-" + countryValue);
        CellStyle style = book.createCellStyle();
        Font font = book.createFont();
        font.setBold(true);
        style.setFont(font);

        Row row = sheet.getRow(1);
        int cellNum = row.getLastCellNum();

        Cell tourCountOE = row.createCell(cellNum);
        tourCountOE.setCellValue("Travelata"); //OfferEmail
        tourCountOE.setCellStyle(style);

        String currentToursCountValue = "";
        String resortCellValue = "";

        gotoSearchPageWithCriteria (countryValue, "", dateCounter, isFlexibleDate, nightsValue, isFlexibleNights, adultsValue, kidsAges, departureValue, starsValues);
        closeLotteryPopup();
        closeCashBackPopup();

        if(!ifEmptyTravSearch()){

            for (int i=0; i< getTOList().size(); i++) {

                row = sheet.getRow(i + 2);

                //Country
                if (row.getCell(cellNum - 3).getStringCellValue().equals(countryValue)) {

                    //By TO
                    if (!row.getCell(cellNum - 2).getStringCellValue().equals("Всего")) {
                        int j = 2;
                        while ($(byXpath(String.format(TO_FILTER, j))).exists()) {
                            if ($(byXpath(String.format(TO_FILTER, j))).waitUntil(Condition.exist, CONSTANT_10_SECONDS).getAttribute("data-layer-name").replaceAll("туроператор_", "")
                                    .contains(TOOffer.get(getTOList().get(i)))) {
                                currentToursCountValue = $(byXpath(String.format(TO_FILTER, j))).waitUntil(Condition.exist, CONSTANT_60_SECONDS).getAttribute("data-count");
                                break;
                            } else
                                currentToursCountValue = "";
                            j++;
                        }
                    } else
                        currentToursCountValue = $(byXpath(String.format(TO_FILTER, 1))).waitUntil(Condition.exist, CONSTANT_60_SECONDS).getAttribute("data-count");

                }

                //Resorts
                else {

                    if (!resortOffer.get(row.getCell(cellNum - 3).getStringCellValue()).equals(resortCellValue)){
                        gotoSearchPageWithCriteria(countryValue, resortOffer.get(row.getCell(cellNum - 3).getStringCellValue()), dateCounter, isFlexibleDate, nightsValue, isFlexibleNights, adultsValue, kidsAges, departureValue, starsValues);
                        closeLotteryPopup();
                        closeCashBackPopup();
                    }

                    if (!ifEmptyTravSearch()){

                        //By TO
                        if (!(new LevelTravelSearchPage().getTOList().get(i).equals("Всего"))) {
                            int j = 2;
                            while ($(byXpath(String.format(TO_FILTER, j))).exists()) {
                                if ($(byXpath(String.format(TO_FILTER, j))).waitUntil(Condition.exist, CONSTANT_10_SECONDS).getAttribute("data-layer-name").replaceAll("туроператор_", "")
                                        .contains(TOOffer.get(getTOList().get(i)))) {
                                    currentToursCountValue = $(byXpath(String.format(TO_FILTER, j))).waitUntil(Condition.exist, CONSTANT_60_SECONDS).getAttribute("data-count");
                                    break;
                                } else
                                    currentToursCountValue = "";
                                j++;
                            }
                        } else
                            currentToursCountValue = $(byXpath(String.format(TO_FILTER, 1))).waitUntil(Condition.exist, CONSTANT_60_SECONDS).getAttribute("data-count");

                    }

                    else {
                        $(byXpath(SEARCH_PROGRESS_BAR)).waitUntil(Condition.exist, CONSTANT_300_SECONDS);
                        currentToursCountValue = "0";
                        tourCountOE = row.createCell(cellNum);
                        tourCountOE.setCellValue(currentToursCountValue);
                    }

                    resortCellValue = resortOffer.get(row.getCell(cellNum - 3).getStringCellValue());
                }

                if (currentToursCountValue.equals(""))
                    currentToursCountValue = "0";
                tourCountOE = row.createCell(cellNum);
                tourCountOE.setCellValue(currentToursCountValue);

            }

        }

        else {
            $(byXpath(SEARCH_PROGRESS_BAR)).waitUntil(Condition.exist, CONSTANT_300_SECONDS);
            for (int i=0; i< getTOList().size(); i++) {
                row = sheet.getRow(i + 2);
                currentToursCountValue = "0";
                tourCountOE = row.createCell(cellNum);
                tourCountOE.setCellValue(currentToursCountValue);
            }
        }

        new LevelTravelSearchPage().getTOList().clear();

        book.write(new FileOutputStream(reportPathValue));
//        book.close();

        return this;
    }

}
