package pageobjects.Travelata;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import framework.WebDriverCommands;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.openqa.selenium.ElementNotInteractableException;
import pageobjects.LevelTravel.LevelTravelSearchPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.codeborne.selenide.Selectors.byId;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.*;
import static framework.Constants.*;
import static pageobjects.LevelTravel.LevelTravelSearchPage.getTOList;


public class OfferEmailSearchPage extends WebDriverCommands {

    private  static OfferEmailSearchPage offerEmailSearchPage = null;
    public OfferEmailSearchPage(){
    }
    public static OfferEmailSearchPage getOfferEmailSearchPage(){
        if(offerEmailSearchPage == null)
            offerEmailSearchPage = new OfferEmailSearchPage();

        checkServerErrorAndRefresh();

        return offerEmailSearchPage;
    }

    private final String QUIT_URL = "//a[contains(@href, '/index/logout')]";
    private final String OFFER_EMAIL_URL = "http://offer.travadm.org";
    private final String TOURS_SEARCH_LOADER = "loadingCanvas"; //id
    private final String TOURS_SEARCH_LOADER_SMALL = "(//p[@class = 'loading']/i)[2]";
    private final String TOURS_SEARCH_HEADER = "flyingResultTextSearch";    //id
    private final String SHOW_ALL_TOURS_BUTTON = "(//div[contains(@class, 'showAll')]/span)[2]";
    private final String OFFER_TOURS_VALUE = "(//strong[contains(@data-bind, 'hotelsForHtml')])[2]";
    private final String BAD_GATEWAY_LABEL = "//h1[text() = '502 Bad Gateway']";
    private final String EMPTY_SEARCH_LABEL = "//p[text() = 'Ничего не найдено с данными базовыми критериями.']";
    private final String SORT_BY_PRICE_BUTTON = "(//div[text() = 'По Цене'])[2]";
    private final String SORT_BY_PRICE_FROM_LESS_LINK = "(//label[@for = 'priceUp'])[1]";
    private final String BULLET_CELLS_ICON = "//i[contains(@class, 'bullet_cells')]";

    private final String TOUR_OPERATORS_SELECT = "(//select[@class = 'operators']/following-sibling::button)[2]";
    private final String ENABLED_TOUR_OPERATOR_CHECKBOX = "(//ul[@style = 'height: 400px;']/.//input[contains(@id, 'ui-multiselect') and not(@disabled = 'disabled')])[%s]";
    private final String ENABLED_TOUR_OPERATOR_NAME ="(//ul[@style = 'height: 400px;']/.//input[contains(@id, 'ui-multiselect') and not(@disabled = 'disabled')])[%s]/following-sibling::span";

    private final String HOTEL_LINE = "(//ul[contains(@class, 'smallTourItem')])[%s]";
    private final String HOTEL_BLOCK = "(//div[@class = 'tourItem_hotel'])[%s]";
    private final String HOTEL_NAME = "(//div[contains(@data-bind, 'tour.hotel.name')])[%s]";
    private final String HOTEL_NAME_IN_BLOCK = "(//a[contains(@data-bind, 'tour.hotel.name')])[%s]";
    private final String HOTEL_ROOM_NAME_IN_BLOCK = "(//div[contains(@data-bind, 'tour.room.name')])[%s]";
    private final String HOTEL_MEAL_NAME_IN_BLOCK = "(//div[contains(@data-bind, 'tour.meal.get')])[%s]";
    private final String HOTEL_PRICE_BEFORE_ACT = "(//span[contains(@data-bind, 'tour.price()')])[%s]";
    private final String HOTEL_OIL_TAX_BEFORE_ACT = "(//div[@class = 'tourItem_price'])[%s]/following-sibling::span[contains(@data-bind, 'oilTaxes')]";
    private final String PAGINATION_NEXT_LINK  = "//div[@class = 'paginationNext']";
    private final String PAGINATION_NEXT_LINK_DISABLED  = "//div[@class = 'paginationNext disable']";

    private final String ACTUALIZE_TOUR_BUTTON = "(//div[@data-bind = 'click:actualize'])[%s]";
    private final String ACTUALIZE_IN_PROGRESS_TOUR_LABEL = "//ul[@class = 'smallTourItem actualizeInProgress']";
    private final String ACTUALIZE_IN_PROGRESS_TOUR_LABEL_IN_BLOCk = "//div[@class = 'tourItem actualizeInProgress']";
    private final String ACTUALIZED_TOUR_LABEL = "(//ul[@class = 'smallTourItem isActualized'])[%s]";
    private final String ACTUALIZED_TOUR_LABEL_IN_BLOCK = "(//div[@class = 'tourItem isActualized'])[%s]";
    private final String HOTEL_PRICE_AFTER_ACT = "(//ul[@class = 'smallTourItem isActualized']/.//span[contains(@data-bind, 'tour.price')])[%s]";
    private final String HOTEL_PRICE_AFTER_ACT_IN_BLOCK = "(//div[@class = 'tourItem isActualized']/.//span[contains(@data-bind, 'tour.price')])[%s]";

    private final String POPULAR_FILTER_LABEL = "(//label[contains(@data-bind, 'recommend')])[2]";

    private final String GROUP_BY_HOTEL_BUTTON = "(//i[contains(@class, 'cells_groups')])[1]";
    private final String GROUP_BY_HOTEL_HOTEL_BLOCK = "(//div[@class = 'hotelToursItem'])[%s]";
    private final String GROUP_BY_HOTEL_HOTEL_NAME = "(//div[@class = 'hotelToursItem'])[%s]/.//a[contains(@data-bind, 'text:name')]";
    private final String GROUP_BY_HOTEL_CHEAPEST_TOUR = "(//div[@class = 'hotelToursItem'])[%s]/.//div[@class = 'tourItem'][1]";
    private final String GROUP_BY_HOTEL_CHEAPEST_TOUR_PRICE = "(//div[@class = 'hotelToursItem'])[%s]/.//div[@class = 'tourItem'][1]/.//span[contains(@data-bind, 'tour.price()')]";
    private final String GROUP_BY_HOTEL_ACTUALIZE_BUTTON = "(//div[@class = 'hotelToursItem'])[%s]/.//div[@class = 'tourItem'][1]/.//div[@data-bind = 'click:actualize']";
    private final String GROUP_BY_HOTEL_ACTUALIZE_IN_PROGRESS = "(//div[@class = 'hotelToursItem'])[%s]/.//div[@class = 'tourItem actualizeInProgress'][1]";
    private final String GROUP_BY_HOTEL_ACTUALIZED = "(//div[@class = 'hotelToursItem'])[%s]/.//div[@class = 'tourItem isActualized'][1]";
    private final String GROUP_BY_HOTEL_CHEAPEST_TOUR_PRICE_ACT = "(//div[@class = 'hotelToursItem'])[%s]/.//div[@class = 'tourItem isActualized'][1]/.//span[contains(@data-bind, 'tour.price()')]";

    private static ArrayList<String> hotelIdOfferList = new ArrayList<String>();
    public static ArrayList <String> getHotelIdOfferList(){
        return hotelIdOfferList; }
    public static void setHotelIdOfferList(String hotelIdOfferListValue){
        hotelIdOfferList.add(hotelIdOfferListValue);
    }

    private static ArrayList<String> hotelNamesOfferList = new ArrayList<String>();
    public static ArrayList <String> getHotelNamesOfferList(){
        return hotelNamesOfferList; }
    public static void setHotelNamesOfferList(String hotelNameOfferListValue){
        hotelNamesOfferList.add(hotelNameOfferListValue);
    }

    private static ArrayList<String> hotelStarsOfferList = new ArrayList<String>();
    public static ArrayList <String> getHotelStarsOfferList(){
        return hotelStarsOfferList; }
    public static void setHotelStarsOfferList(String hotelStarsOfferListValue){
        hotelStarsOfferList.add(hotelStarsOfferListValue);
    }

    private static ArrayList<String> hotelNameRoomOfferList = new ArrayList<String>();
    public static ArrayList <String> getHotelNameRoomOfferList(){
        return hotelNameRoomOfferList;
    }
    public static void setHotelNameRoomOfferList(String hotelNameRoomOfferListValue){
        hotelNameRoomOfferList.add(hotelNameRoomOfferListValue);
    }

    private static ArrayList<String> hotelNameMealOfferList = new ArrayList<String>();
    public static ArrayList <String> getHotelNameMealOfferList(){
        return hotelNameMealOfferList;
    }
    public static void setHotelNameMealOfferList(String hotelNameMealOfferListValue){
        hotelNameMealOfferList.add(hotelNameMealOfferListValue);
    }

    private static ArrayList<String> countryResortOfferList = new ArrayList<String>();
    public static ArrayList <String> getCountryResortOfferList(){
        return countryResortOfferList;
    }
    public static void setCountryResortOfferList(String countryResortOfferListValue){
        countryResortOfferList.add(countryResortOfferListValue);
    }

    private static ArrayList<String> regionOfferList = new ArrayList<String>();
    public static ArrayList <String> getRegionOfferList(){
        return regionOfferList;
    }
    public static void setRegionOfferList(String regionOfferListValue){
        regionOfferList.add(regionOfferListValue);
    }

    private static ArrayList<String> toNameOfferList = new ArrayList<String>();
    public static ArrayList <String> getToNameOfferList(){
        return toNameOfferList;
    }
    public static void setToNameOfferList(String toNameOfferListValue){
        toNameOfferList.add(toNameOfferListValue);
    }

    private static ArrayList<String> hotelTourPriceOfferList = new ArrayList<String>();
    public static ArrayList <String> getHotelTourPriceOfferList(){
        return hotelTourPriceOfferList;
    }
    public static void setHotelTourPriceOfferList(String hotelTourPriceOfferListValue){
        hotelTourPriceOfferList.add(hotelTourPriceOfferListValue);
    }

    private static ArrayList<String> hotelTourPriceActualizedOfferList = new ArrayList<String>();
    public static ArrayList <String> getHotelTourPriceActualizedOfferList(){
        return hotelTourPriceActualizedOfferList;
    }
    public static void setHotelTourPriceActualizedOfferList(String hotelTourPriceActualizedOfferListValue){
        hotelTourPriceActualizedOfferList.add(hotelTourPriceActualizedOfferListValue);
    }

    private static ArrayList<Double> hotelTourOfferAmountList = new ArrayList<Double>();
    public static ArrayList <Double> getHotelTourOfferAmountList(){
        return hotelTourOfferAmountList;
    }
    public static void setHotelTourOfferAmountList(Double hotelTourOfferAmountListValue){
        hotelTourOfferAmountList.add(hotelTourOfferAmountListValue);
    }

    private static ArrayList<String> hotelsAmountOfferList = new ArrayList<String>();
    public static ArrayList <String> getHotelsAmountOfferList(){
        return hotelsAmountOfferList;
    }
    public static void setHotelsAmountOfferList(String hotelsAmountOfferListValue){
        hotelsAmountOfferList.add(hotelsAmountOfferListValue);
    }

    public OfferEmailSearchPage getOfferStatisticByTO(String countryValue, int dateCounter, boolean isFlexibleDate, int nightsValue, boolean isFlexibleNights,
                                                      String adultsValue, ArrayList<String> kidsAges, String departureValue, ArrayList<String> starsValues, String reportPathValue) throws Exception{

        FileInputStream fsIP= new FileInputStream(new File(reportPathValue));
        Workbook book = new HSSFWorkbook(fsIP);
        Sheet sheet = book.getSheet(departureValue + "-" + countryValue);
        CellStyle style = book.createCellStyle();
        Font font = book.createFont();
        font.setBold(true);
        style.setFont(font);

        Row row = sheet.getRow(1);
        int cellNum = row.getLastCellNum();

        Cell tourCountOE = row.createCell(cellNum);
        tourCountOE.setCellValue("OfferEmail");
        tourCountOE.setCellStyle(style);

        String offerSearchUrl = OFFER_EMAIL_URL + "/search" + "#?" + "fromCity=" + departureOffer.get(departureValue) +
                "&toCountry=" + destinationCountryOffer.get(countryValue);

//            Variable dates
//            if (isFlexibleDate){
//                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
//                Calendar c = Calendar.getInstance();
//                c.setTime(sdf.parse(dateValue));
//                c.add(Calendar.DATE, -2);
//                offerSearchUrl += "&dateFrom=" + sdf.format(c.getTime());
//                c.add(Calendar.DATE, 4);
//                offerSearchUrl += "&dateTo=" + sdf.format(c.getTime());
//            }
//            else offerSearchUrl += "&dateFrom=" + dateValue + "&dateTo=" + dateValue;


//          Date +n from current
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        if (isFlexibleDate){
            c.setTime(dt);
            c.add(Calendar.DATE, dateCounter - 2);
            offerSearchUrl += "&dateFrom=" + new SimpleDateFormat("dd.MM.yyyy").format(c.getTime());
            c.setTime(dt);
            c.add(Calendar.DATE, dateCounter + 2);
            offerSearchUrl += "&dateTo=" + new SimpleDateFormat("dd.MM.yyyy").format(c.getTime());
        }
        else{
            c.setTime(dt);
            c.add(Calendar.DATE, dateCounter);
            offerSearchUrl += "&dateFrom=" + new SimpleDateFormat("dd.MM.yyyy").format(c.getTime())
                    + "&dateTo=" + new SimpleDateFormat("dd.MM.yyyy").format(c.getTime());
        }

        if(isFlexibleNights)
            offerSearchUrl += "&nightFrom=" + (nightsValue - 2) + "&nightTo=" + (nightsValue + 2);
        else
            offerSearchUrl += "&nightFrom=" + nightsValue + "&nightTo=" + nightsValue;

        offerSearchUrl += "&adults=" + adultsValue;

        if (kidsAges.size() > 0){
            int kidsCountValue = 0;
            int infantsCountValue = 0;
            for (int i=0; i< kidsAges.size(); i++) {
                if (Integer.parseInt(kidsAges.get(i)) > 1)
                    kidsCountValue ++;
                else
                    infantsCountValue ++;
            }
            offerSearchUrl += "&kids=" + kidsCountValue + "&infants=" + infantsCountValue;
        }
        else
            offerSearchUrl += "&kids=0&infants=0";

        offerSearchUrl += "&hotelClass=";

        if(starsValues.size()>0){
            for(int i=0; i<starsValues.size(); i++){
                offerSearchUrl += hotelStarsOffer.get(starsValues.get(i)) + ".";
            }
        }
        else
            offerSearchUrl += "all";

        offerSearchUrl += "&meal=all&priceFrom=6000&priceTo=1000000";
        String offerSearchUrlCountry = offerSearchUrl;

        String currentToursCountValue = "";
        String resortCellValue = "";

        driver.navigate().refresh();
        goToPage(offerSearchUrlCountry);
        $(byId(TOURS_SEARCH_LOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
        $(byXpath(TOURS_SEARCH_LOADER_SMALL)).waitUntil(Condition.disappear, CONSTANT_240_SECONDS);
        sleep(CONSTANT_1_SECOND);
        if (!$(byXpath(EMPTY_SEARCH_LABEL)).isDisplayed()){
            if ($(byXpath(SHOW_ALL_TOURS_BUTTON)).isDisplayed())
                $(byXpath(SHOW_ALL_TOURS_BUTTON)).click();
            if($(byXpath(BAD_GATEWAY_LABEL)).isDisplayed())
                refreshPage();

            for (int i=0; i< getTOList().size(); i++) {

                row = sheet.getRow(i + 2);

                //Country
                if (row.getCell(cellNum - 3).getStringCellValue().equals(countryValue)) {

                    $(byXpath(TOUR_OPERATORS_SELECT)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();

                    //By TO
                    if (!row.getCell(cellNum - 2).getStringCellValue().equals("Всего")) {
                        int j = 1;
                        while ($(byXpath(String.format(ENABLED_TOUR_OPERATOR_CHECKBOX, j))).exists()) {
                            if ($(byXpath(String.format(ENABLED_TOUR_OPERATOR_NAME, j))).waitUntil(Condition.visible, CONSTANT_10_SECONDS).getText()
                                    .contains(TOOffer.get(getTOList().get(i)))) {
                                $(byXpath(String.format(ENABLED_TOUR_OPERATOR_CHECKBOX, j))).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();
                                sleep(CONSTANT_1_SECOND);
                                currentToursCountValue = $(byXpath(OFFER_TOURS_VALUE)).waitUntil(Condition.exist, CONSTANT_60_SECONDS).getText();
                                $(byXpath(String.format(ENABLED_TOUR_OPERATOR_CHECKBOX, j))).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();
                                break;
                            } else
                                currentToursCountValue = "";
                            j++;
                        }
                    } else
                        currentToursCountValue = $(byXpath(OFFER_TOURS_VALUE)).waitUntil(Condition.exist, CONSTANT_60_SECONDS).getText();

                    $(byXpath(TOUR_OPERATORS_SELECT)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();

                }

                //Resorts
                else {

//                    $(byXpath(TOUR_OPERATORS_SELECT)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();

                    if (!resortOffer.get(row.getCell(cellNum - 3).getStringCellValue()).equals(resortCellValue)) {
                        goToPage(offerSearchUrl + "&toCity=" + resortOffer.get(row.getCell(cellNum - 3).getStringCellValue()));
                        $(byId(TOURS_SEARCH_LOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
                        $(byXpath(TOURS_SEARCH_LOADER_SMALL)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
                        sleep(CONSTANT_1_SECOND);
                        if ($(byXpath(SHOW_ALL_TOURS_BUTTON)).isDisplayed())
                            $(byXpath(SHOW_ALL_TOURS_BUTTON)).click();
                        if ($(byXpath(BAD_GATEWAY_LABEL)).isDisplayed())
                            refreshPage();
                    }

//                    $(byXpath(TOUR_OPERATORS_SELECT)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();

                    if (!$(byXpath(EMPTY_SEARCH_LABEL)).isDisplayed()){

                        $(byXpath(TOUR_OPERATORS_SELECT)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();

                        //By TO
                        if (!(new LevelTravelSearchPage().getTOList().get(i).equals("Всего"))) {
                            int j = 1;
                            while ($(byXpath(String.format(ENABLED_TOUR_OPERATOR_CHECKBOX, j))).exists()) {
                                if ($(byXpath(String.format(ENABLED_TOUR_OPERATOR_NAME, j))).waitUntil(Condition.visible, CONSTANT_10_SECONDS).getText()
                                        .contains(TOOffer.get(getTOList().get(i)))) {
                                    $(byXpath(String.format(ENABLED_TOUR_OPERATOR_CHECKBOX, j))).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();
                                    sleep(CONSTANT_1_SECOND);
                                    currentToursCountValue = $(byXpath(OFFER_TOURS_VALUE)).waitUntil(Condition.exist, CONSTANT_60_SECONDS).getText();
                                    $(byXpath(String.format(ENABLED_TOUR_OPERATOR_CHECKBOX, j))).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();
                                    break;
                                } else
                                    currentToursCountValue = "";
                                j++;
                            }
                        } else
                            currentToursCountValue = $(byXpath(OFFER_TOURS_VALUE)).waitUntil(Condition.exist, CONSTANT_60_SECONDS).getText();

                        $(byXpath(TOUR_OPERATORS_SELECT)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();

                    }

                    else {
                        currentToursCountValue = "0";
                        tourCountOE = row.createCell(cellNum);
                        tourCountOE.setCellValue(currentToursCountValue);
                    }

                    resortCellValue = resortOffer.get(row.getCell(cellNum - 3).getStringCellValue());
                }

                if (currentToursCountValue.equals(""))
                    currentToursCountValue = "0";
                tourCountOE = row.createCell(cellNum);
                tourCountOE.setCellValue(currentToursCountValue);

//                $(byXpath(TOUR_OPERATORS_SELECT)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();

            }

        }

        else {
            for (int i=0; i< getTOList().size(); i++) {
                row = sheet.getRow(i + 2);
                currentToursCountValue = "0";
                tourCountOE = row.createCell(cellNum);
                tourCountOE.setCellValue(currentToursCountValue);
            }
        }

        book.write(new FileOutputStream(reportPathValue));
        new LevelTravelSearchPage().getTOList().clear();

        book.close();

        return this;
    }

    public OfferEmailSearchPage gotoSearchCriteria(String countryValue, int dateCounter, boolean isFlexibleDate, int nightsValue, boolean isFlexibleNights,
                                                   String adultsValue, ArrayList<String> kidsAges, String departureValue, ArrayList<String> starsValues, String resortValue, String operatorValue) throws Exception{

        String offerSearchUrl = OFFER_EMAIL_URL + "/search" + "#?" + "fromCity=" + departureOffer.get(departureValue) +
                "&toCountry=" + destinationCountryOffer.get(countryValue);

//            Variable dates
//            if (isFlexibleDate){
//                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
//                Calendar c = Calendar.getInstance();
//                c.setTime(sdf.parse(dateValue));
//                c.add(Calendar.DATE, -2);
//                offerSearchUrl += "&dateFrom=" + sdf.format(c.getTime());
//                c.add(Calendar.DATE, 4);
//                offerSearchUrl += "&dateTo=" + sdf.format(c.getTime());
//            }
//            else offerSearchUrl += "&dateFrom=" + dateValue + "&dateTo=" + dateValue;


//          Date +n from current
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        if (isFlexibleDate){
            c.setTime(dt);
            c.add(Calendar.DATE, dateCounter - 2);
            offerSearchUrl += "&dateFrom=" + new SimpleDateFormat("dd.MM.yyyy").format(c.getTime());
            c.setTime(dt);
            c.add(Calendar.DATE, dateCounter + 2);
            offerSearchUrl += "&dateTo=" + new SimpleDateFormat("dd.MM.yyyy").format(c.getTime());
        }
        else{
            c.setTime(dt);
            c.add(Calendar.DATE, dateCounter);
            offerSearchUrl += "&dateFrom=" + new SimpleDateFormat("dd.MM.yyyy").format(c.getTime())
                    + "&dateTo=" + new SimpleDateFormat("dd.MM.yyyy").format(c.getTime());
        }

        if(isFlexibleNights)
            offerSearchUrl += "&nightFrom=" + (nightsValue - 2) + "&nightTo=" + (nightsValue + 2);
        else
            offerSearchUrl += "&nightFrom=" + nightsValue + "&nightTo=" + nightsValue;

        offerSearchUrl += "&adults=" + adultsValue;

        if (kidsAges.size() > 0){
            int kidsCountValue = 0;
            int infantsCountValue = 0;
            for (int i=0; i< kidsAges.size(); i++) {
                if (Integer.parseInt(kidsAges.get(i)) > 1)
                    kidsCountValue ++;
                else
                    infantsCountValue ++;
            }
            offerSearchUrl += "&kids=" + kidsCountValue + "&infants=" + infantsCountValue;
        }
        else
            offerSearchUrl += "&kids=0&infants=0";

        offerSearchUrl += "&hotelClass=";

        if(starsValues.size() > 0){
            for(int i=0; i<starsValues.size(); i++){
                    offerSearchUrl += hotelStarsOffer.get(starsValues.get(i)) + ".";
            }
        }
        else
            offerSearchUrl += "all";

        if(!resortValue.equals("-"))
            offerSearchUrl += "&toCity=" + resortValue;

        offerSearchUrl += "&meal=all&priceFrom=6000&priceTo=1000000&operators=" + operatorValue;

        driver.navigate().refresh();
        goToPage(offerSearchUrl);
        $(byId(TOURS_SEARCH_LOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
        $(byXpath(TOURS_SEARCH_LOADER_SMALL)).waitUntil(Condition.disappear, CONSTANT_240_SECONDS);
        sleep(CONSTANT_1_SECOND);

        return new OfferEmailSearchPage();
    }

    public OfferEmailSearchPage gotoSearchCriteriaByTO(String countryValue, int dateCounter, boolean isFlexibleDate, int nightsValue, boolean isFlexibleNights,
                                                   String adultsValue, ArrayList <String> kidsAges, String departureValue, ArrayList<String> starsValues, ArrayList<String> mealValues, String resortValue, String operatorValue) throws Exception{

        String offerSearchUrl = OFFER_EMAIL_URL + "/search" + "#?" + "fromCity=" + departureOffer.get(departureValue) +
                "&toCountry=" + destinationCountryOffer.get(countryValue);

//            Variable dates
//            if (isFlexibleDate){
//                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
//                Calendar c = Calendar.getInstance();
//                c.setTime(sdf.parse(dateValue));
//                c.add(Calendar.DATE, -2);
//                offerSearchUrl += "&dateFrom=" + sdf.format(c.getTime());
//                c.add(Calendar.DATE, 4);
//                offerSearchUrl += "&dateTo=" + sdf.format(c.getTime());
//            }
//            else offerSearchUrl += "&dateFrom=" + dateValue + "&dateTo=" + dateValue;


//          Date +n from current
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        if (isFlexibleDate){
            c.setTime(dt);
            c.add(Calendar.DATE, dateCounter - 2);
            offerSearchUrl += "&dateFrom=" + new SimpleDateFormat("dd.MM.yyyy").format(c.getTime());
            c.setTime(dt);
            c.add(Calendar.DATE, dateCounter + 2);
            offerSearchUrl += "&dateTo=" + new SimpleDateFormat("dd.MM.yyyy").format(c.getTime());
        }
        else{
            c.setTime(dt);
            c.add(Calendar.DATE, dateCounter);
            offerSearchUrl += "&dateFrom=" + new SimpleDateFormat("dd.MM.yyyy").format(c.getTime())
                    + "&dateTo=" + new SimpleDateFormat("dd.MM.yyyy").format(c.getTime());
        }

        if(isFlexibleNights)
            offerSearchUrl += "&nightFrom=" + (nightsValue - 2) + "&nightTo=" + (nightsValue + 2);
        else
            offerSearchUrl += "&nightFrom=" + nightsValue + "&nightTo=" + nightsValue;

        offerSearchUrl += "&adults=" + adultsValue;

        if (kidsAges.get(0).equals("0")){
            offerSearchUrl += "&kids=0&infants=0";
        }
        else{
            int kidsCountValue = 0;
            int infantsCountValue = 0;
            for (int i=0; i< kidsAges.size(); i++) {
                if (Integer.parseInt(kidsAges.get(i)) > 1)
                    kidsCountValue ++;
                else
                    infantsCountValue ++;
            }
            offerSearchUrl += "&kids=" + kidsCountValue + "&infants=" + infantsCountValue;
        }

        offerSearchUrl += "&hotelClass=";

        if(starsValues.size() > 0){
            for(int i=0; i<starsValues.size(); i++){
                    offerSearchUrl += hotelStarsOffer.get(starsValues.get(i)) + ".";
            }
        }
        else
            offerSearchUrl += "all";

        if(!resortValue.equals("-"))
            offerSearchUrl += "&toCity=" + resortValue;

        offerSearchUrl += "&meal=";

        if(mealValues.size() > 0){
            for(int i=0; i<mealValues.size(); i++){
                offerSearchUrl += hotelMealsOffer.get(mealValues.get(i)) + ".";
            }
        }
        else
            offerSearchUrl += "all";

        offerSearchUrl += "&priceFrom=6000&priceTo=1000000&operators=" + operatorValue;

        driver.navigate().refresh();
        System.out.println(offerSearchUrl);
        goToPage(offerSearchUrl);
        $(byId(TOURS_SEARCH_LOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
        $(byXpath(TOURS_SEARCH_LOADER_SMALL)).waitUntil(Condition.disappear, CONSTANT_240_SECONDS);
        sleep(CONSTANT_1_SECOND);

        return new OfferEmailSearchPage();
    }

    public OfferEmailSearchPage gotoSearchCriteriaByTOWithHotel(String countryValue, int dateCounter, boolean isFlexibleDate, int nightsValue, boolean isFlexibleNights,
                                                       String adultsValue, ArrayList<String> kidsAges, String departureValue, ArrayList<String> starsValues, ArrayList<String> mealValues,
                                                                String resortValue, String operatorValue, String hotelIdValue, int hotelCounter) throws Exception{

        String offerSearchUrl = OFFER_EMAIL_URL + "/search" + "#?" + "fromCity=" + departureOffer.get(departureValue) +
                "&toCountry=" + destinationCountryOffer.get(countryValue);

//            Variable dates
//            if (isFlexibleDate){
//                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
//                Calendar c = Calendar.getInstance();
//                c.setTime(sdf.parse(dateValue));
//                c.add(Calendar.DATE, -2);
//                offerSearchUrl += "&dateFrom=" + sdf.format(c.getTime());
//                c.add(Calendar.DATE, 4);
//                offerSearchUrl += "&dateTo=" + sdf.format(c.getTime());
//            }
//            else offerSearchUrl += "&dateFrom=" + dateValue + "&dateTo=" + dateValue;


//          Date +n from current
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        if (isFlexibleDate){
            c.setTime(dt);
            c.add(Calendar.DATE, dateCounter - 2);
            offerSearchUrl += "&dateFrom=" + new SimpleDateFormat("dd.MM.yyyy").format(c.getTime());
            c.setTime(dt);
            c.add(Calendar.DATE, dateCounter + 2);
            offerSearchUrl += "&dateTo=" + new SimpleDateFormat("dd.MM.yyyy").format(c.getTime());
        }
        else{
            c.setTime(dt);
            c.add(Calendar.DATE, dateCounter);
            offerSearchUrl += "&dateFrom=" + new SimpleDateFormat("dd.MM.yyyy").format(c.getTime())
                    + "&dateTo=" + new SimpleDateFormat("dd.MM.yyyy").format(c.getTime());
        }

        if(isFlexibleNights)
            offerSearchUrl += "&nightFrom=" + (nightsValue - 2) + "&nightTo=" + (nightsValue + 2);
        else
            offerSearchUrl += "&nightFrom=" + nightsValue + "&nightTo=" + nightsValue;

        offerSearchUrl += "&adults=" + adultsValue;

        if (kidsAges.get(0).equals("0")){
            offerSearchUrl += "&kids=0&infants=0";
        }
        else{
            int kidsCountValue = 0;
            int infantsCountValue = 0;
            for (int i=0; i< kidsAges.size(); i++) {
                if (Integer.parseInt(kidsAges.get(i)) > 1)
                    kidsCountValue ++;
                else
                    infantsCountValue ++;
            }
            offerSearchUrl += "&kids=" + kidsCountValue + "&infants=" + infantsCountValue;
        }

        offerSearchUrl += "&hotelClass=";

//        if(starsValues.size()>0){
//               for(int i=0; i<starsValues.size(); i++){
//                   offerSearchUrl += hotelStarsOffer.get(starsValues.get(i)) + ".";
//               }
//        }
//        else
            offerSearchUrl += "all";

        if(!resortValue.equals("-"))
            offerSearchUrl += "&toCity=" + resortValue;

        offerSearchUrl += "&meal=";

        if(mealValues.size()>0){
            for(int i=0; i<mealValues.size(); i++){
                offerSearchUrl += hotelMealsOffer.get(mealValues.get(i)) + ".";
            }
        }
        else
            offerSearchUrl += "all";

        offerSearchUrl += "&hotels=" + hotelIdValue;
        offerSearchUrl += "&operators=" + operatorValue;

        System.out.println(offerSearchUrl);
        goToPage(offerSearchUrl);
        $(byId(TOURS_SEARCH_LOADER)).waitUntil(Condition.disappear, CONSTANT_420_SECONDS);

        return this;
    }

    public OfferEmailSearchPage gotoSearchCriteriaForDepartureCity(String countryValue, int dateCounter, boolean isFlexibleDate, int nightsValue, boolean isFlexibleNights,
                                                                   String adultsValue, ArrayList<String> kidsAges, String departureValue, ArrayList<String> starsValues, ArrayList<String> mealValues,
                                                                   String resortValue, String operatorValue) throws Exception{

        String offerSearchUrl = OFFER_EMAIL_URL + "/search" + "#?" + "fromCity=" + departureOffer.get(departureValue) +
                "&toCountry=" + destinationCountryOffer.get(countryValue);

        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        if (isFlexibleDate){
            c.setTime(dt);
            c.add(Calendar.DATE, dateCounter - 2);
            offerSearchUrl += "&dateFrom=" + new SimpleDateFormat("dd.MM.yyyy").format(c.getTime());
            c.setTime(dt);
            c.add(Calendar.DATE, dateCounter + 2);
            offerSearchUrl += "&dateTo=" + new SimpleDateFormat("dd.MM.yyyy").format(c.getTime());
        }
        else{
            c.setTime(dt);
            c.add(Calendar.DATE, dateCounter);
            offerSearchUrl += "&dateFrom=" + new SimpleDateFormat("dd.MM.yyyy").format(c.getTime())
                    + "&dateTo=" + new SimpleDateFormat("dd.MM.yyyy").format(c.getTime());
        }

        if(isFlexibleNights)
            offerSearchUrl += "&nightFrom=" + (nightsValue - 2) + "&nightTo=" + (nightsValue + 2);
        else
            offerSearchUrl += "&nightFrom=" + nightsValue + "&nightTo=" + nightsValue;

        offerSearchUrl += "&adults=" + adultsValue;

        if (kidsAges.get(0).equals("0")){
            offerSearchUrl += "&kids=0&infants=0";
        }
        else{
            int kidsCountValue = 0;
            int infantsCountValue = 0;
            for (int i=0; i< kidsAges.size(); i++) {
                if (Integer.parseInt(kidsAges.get(i)) > 1)
                    kidsCountValue ++;
                else
                    infantsCountValue ++;
            }
            offerSearchUrl += "&kids=" + kidsCountValue + "&infants=" + infantsCountValue;
        }

        offerSearchUrl += "&hotelClass=";
        if(starsValues.size()>0){
            for(int i=0; i<starsValues.size(); i++){
                offerSearchUrl += hotelStarsOffer.get(starsValues.get(i)) + ".";
            }
        }
        else
        offerSearchUrl += "all";

        if(!resortValue.equals("-"))
            offerSearchUrl += "&toCity=" + resortValue;

        offerSearchUrl += "&meal=";

        if(mealValues.size()>0){
            for(int i=0; i<mealValues.size(); i++){
                offerSearchUrl += hotelMealsOffer.get(mealValues.get(i)) + ".";
            }
        }
        else
            offerSearchUrl += "all";

        offerSearchUrl += "&operators=" + operatorValue;

        sleep(CONSTANT_1_SECOND);
        goToPage(offerSearchUrl);
        $(byId(TOURS_SEARCH_LOADER)).waitUntil(Condition.disappear, CONSTANT_420_SECONDS);

        return this;
    }

    public OfferEmailSearchPage waitMoreTours() throws Exception{
        $(byXpath(TOURS_SEARCH_LOADER_SMALL)).waitUntil(Condition.disappear, CONSTANT_420_SECONDS);
        sleep(CONSTANT_1_SECOND);

        return this;
    }

    public OfferEmailSearchPage sortByPriceFromLess() throws Exception{
        $(byXpath(SORT_BY_PRICE_BUTTON)).waitUntil(Condition.visible, CONSTANT_60_SECONDS).click();
        $(byXpath(SORT_BY_PRICE_FROM_LESS_LINK)).waitUntil(Condition.visible, CONSTANT_2_SECONDS).click();

        return this;
    }

    public OfferEmailSearchPage clickBulletCellsIcon() throws Exception{
        $(byXpath(BULLET_CELLS_ICON)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public OfferEmailSearchPage clickBulletCellsGroupsIcon() throws Exception{
        $(byXpath(GROUP_BY_HOTEL_BUTTON)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    private static String returnHotelCategoryValue(String fullHotelNameValue){
        String hotelCategoryValue = null;
        for(int i=0; i<hotelCategoriesOffer.length; i++){
            if (fullHotelNameValue.contains(hotelCategoriesOffer[i] + ", ")){
                if (hotelCategoriesOffer[i].contains("-")){
                    StringBuilder strToRegexp = new StringBuilder(hotelCategoriesOffer[i]);
                    strToRegexp.insert(hotelCategoriesOffer[i].indexOf("-")-1, "\\");
                    hotelCategoryValue = fullHotelNameValue.replaceAll("[^" + strToRegexp + "]", "");
                }
                else
                    hotelCategoryValue = fullHotelNameValue.replaceAll("[^" + hotelCategoriesOffer[i] + "]", "");
                break;
            }
        }
        hotelCategoryValue = fullHotelNameValue.replaceAll(".+(?=\\d{1}\\*{1})", "");

        return hotelCategoryValue.trim();
    }

    private static String returnClearHotelNameValue(String fullHotelNameValue){
        String clearHotelNameValue = "";
        for(int i=0; i<hotelCategoriesOffer.length; i++){
            if (hotelCategoriesOffer[i].contains("-")){
                StringBuilder strToRegexp = new StringBuilder(hotelCategoriesOffer[i]);
                strToRegexp.insert(hotelCategoriesOffer[i].indexOf("-")-1, "\\");
                clearHotelNameValue = fullHotelNameValue.replaceAll(String.valueOf(strToRegexp), "");
            }
            else
                clearHotelNameValue = fullHotelNameValue.replaceAll(hotelCategoriesOffer[i], "");
        }
        clearHotelNameValue = fullHotelNameValue.replaceAll("\\d\\*", "");

        return clearHotelNameValue.trim();
    }

    //Fixed
    public void putHotelNamesOfferList(String countryToValue, String resortValue) throws Exception {

        ArrayList<String> hotelNamesOfferTempList = new ArrayList<String>();

        int hotelOfferCounter = 0;

        if(!ifEmptySearchResults()){
            if ($(byXpath(SHOW_ALL_TOURS_BUTTON)).isDisplayed())
                $(byXpath(SHOW_ALL_TOURS_BUTTON)).click();

            sortByPriceFromLess();
            clickBulletCellsIcon();

            do{
                int hotelCounter = 1;
                int actualizeButtonCounter = 1;
                while($(byXpath(String.format(HOTEL_BLOCK, hotelCounter))).isDisplayed()){ // HOTEL_BLOCK
                    if(!hotelNamesOfferTempList.contains($(byXpath(String.format(HOTEL_NAME_IN_BLOCK, hotelCounter))).getText())){ //HOTEL_NAME
                        hotelNamesOfferTempList.add($(byXpath(String.format(HOTEL_NAME_IN_BLOCK, hotelCounter))).getText());
                        setHotelNamesOfferList($(byXpath(String.format(HOTEL_NAME_IN_BLOCK, hotelCounter))).getText()); //HOTEL_NAME
                        if (resortValue.equals("-")){
                            setCountryResortOfferList(countryToValue);
                        }

                        else
                            setCountryResortOfferList(resortValue);
                        setHotelTourPriceOfferList($(byXpath(String.format(HOTEL_PRICE_BEFORE_ACT, hotelCounter))).getText().replaceAll("\\s", ""));

                        //Actualization
                        if ($(byXpath(String.format(ACTUALIZE_TOUR_BUTTON, actualizeButtonCounter))).isDisplayed()){
                            try {
                                $(byXpath(String.format(ACTUALIZE_TOUR_BUTTON, actualizeButtonCounter))).click();
                                $(byXpath(ACTUALIZE_IN_PROGRESS_TOUR_LABEL_IN_BLOCk)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS); //ACTUALIZE_IN_PROGRESS_TOUR_LABEL
                                if($(byXpath(String.format(HOTEL_PRICE_BEFORE_ACT, hotelCounter))).isDisplayed()){ //HOTEL_PRICE_AFTER_ACT //$(byXpath(String.format(HOTEL_PRICE_AFTER_ACT_IN_BLOCK, actualizedHotelCounter))).isDisplayed()
                                    setHotelTourPriceActualizedOfferList($(byXpath(String.format(HOTEL_PRICE_BEFORE_ACT, hotelCounter))).getText().replaceAll("\\s", "")); //HOTEL_PRICE_AFTER_ACT
                                }
                                else
                                    setHotelTourPriceActualizedOfferList("-");
                            }
                            catch (ElementNotInteractableException e){
                            };
                        }
                        else
                            setHotelTourPriceActualizedOfferList("-");

                        hotelOfferCounter ++;
                        //////////////////////////////////////////////////////
                    }
                    else
                        actualizeButtonCounter ++;

                    hotelCounter ++;
                }

                if($(byXpath(PAGINATION_NEXT_LINK)).isDisplayed())
                    $(byXpath(PAGINATION_NEXT_LINK)).waitUntil(Condition.visible, CONSTANT_2_SECONDS).click();
                else
                    break;
//                sleep(CONSTANT_1_SECOND);
//                moveToElement($(byXpath(POPULAR_FILTER_LABEL)));

            }
            while ($(byXpath(String.format(HOTEL_BLOCK, 1))).isDisplayed());

        }

        else
            putEmptyHotelNamesOfferList(countryToValue, resortValue);

        setHotelTourOfferAmountList((double) hotelOfferCounter);

        hotelNamesOfferTempList.clear();

    }

    public void putHotelNamesOfferListWithResort(String countryToValue) throws Exception { //, String toValue

        ArrayList<String> hotelNamesOfferTempList = new ArrayList<String>();

        int hotelOfferCounter = 0;

        if(!ifEmptySearchResults()){
            if ($(byXpath(SHOW_ALL_TOURS_BUTTON)).isDisplayed())
                $(byXpath(SHOW_ALL_TOURS_BUTTON)).click();

            sortByPriceFromLess();
            clickBulletCellsIcon();

            do{
                int hotelCounter = 1;
                int actualizeButtonCounter = 1;
                while($(byXpath(String.format(HOTEL_BLOCK, hotelCounter))).isDisplayed()){ // HOTEL_BLOCK
                    if(!hotelNamesOfferTempList.contains($(byXpath(String.format(HOTEL_NAME_IN_BLOCK, hotelCounter))).getText())){ //HOTEL_NAME
                        hotelNamesOfferTempList.add($(byXpath(String.format(HOTEL_NAME_IN_BLOCK, hotelCounter))).getText());
                        setHotelNamesOfferList($(byXpath(String.format(HOTEL_NAME_IN_BLOCK, hotelCounter))).getText()); //HOTEL_NAME
                        String fullHotelNameValue = $(byXpath(String.format(HOTEL_NAME_IN_BLOCK, hotelCounter))).getText();
                        setCountryResortOfferList(fullHotelNameValue.substring(fullHotelNameValue.indexOf(",") + 2));
//                        if (resortValue.equals("-")){
//                            setCountryResortOfferList(countryToValue);
//                        }
//
//                        else
//                            setCountryResortOfferList(resortValue);
                        String priceBeforeAct = $(byXpath(String.format(HOTEL_PRICE_BEFORE_ACT, hotelCounter))).getText().replaceAll("\\s", "");
                        setHotelTourPriceOfferList(priceBeforeAct);

                        //Actualization
//                        if(toValue.equals("BG") || toValue.equals("Tui")){
//                            setHotelTourPriceActualizedOfferList(priceBeforeAct);
//                        }
//                        else {
//                            if ($(byXpath(String.format(ACTUALIZE_TOUR_BUTTON, actualizeButtonCounter))).isDisplayed()){
//                                try {
//                                    $(byXpath(String.format(ACTUALIZE_TOUR_BUTTON, actualizeButtonCounter))).click();
//                                    $(byXpath(ACTUALIZE_IN_PROGRESS_TOUR_LABEL_IN_BLOCk)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS); //ACTUALIZE_IN_PROGRESS_TOUR_LABEL
//                                    if($(byXpath(String.format(HOTEL_PRICE_BEFORE_ACT, hotelCounter))).isDisplayed()){ //HOTEL_PRICE_AFTER_ACT //$(byXpath(String.format(HOTEL_PRICE_AFTER_ACT_IN_BLOCK, actualizedHotelCounter))).isDisplayed()
//                                        setHotelTourPriceActualizedOfferList($(byXpath(String.format(HOTEL_PRICE_BEFORE_ACT, hotelCounter))).getText().replaceAll("\\s", "")); //HOTEL_PRICE_AFTER_ACT
//                                    }
//                                    else
//                                        setHotelTourPriceActualizedOfferList("-");
//                                }
//                                catch (ElementNotInteractableException e){
//                                };
//                            }
//                            else
//                                setHotelTourPriceActualizedOfferList("-");
//                        }
//
//                        hotelOfferCounter ++;
                        //////////////////////////////////////////////////////
                    }
                    else
                        actualizeButtonCounter ++;

                    hotelCounter ++;
                }

                if($(byXpath(PAGINATION_NEXT_LINK)).isDisplayed())
                    $(byXpath(PAGINATION_NEXT_LINK)).waitUntil(Condition.visible, CONSTANT_2_SECONDS).click();
                else
                    break;

            }
            while ($(byXpath(String.format(HOTEL_BLOCK, 1))).isDisplayed());

        }

        else
            putEmptyHotelNamesOfferList(countryToValue, "-");

        setHotelTourOfferAmountList((double) hotelOfferCounter);

        hotelNamesOfferTempList.clear();

    }

    public void putHotelNamesOfferListByTO(String countryToValue, String resortValue, int hotelCounterValue, String toValue) throws Exception {

        ArrayList<String> hotelNamesOfferTempList = new ArrayList<String>();

        int hotelOfferCounter = 0;

        if(!ifEmptySearchResults()){
            if ($(byXpath(SHOW_ALL_TOURS_BUTTON)).isDisplayed())
                $(byXpath(SHOW_ALL_TOURS_BUTTON)).click();

            sortByPriceFromLess();
            clickBulletCellsIcon();

            do{
                int hotelCounter = 1;
                int actualizeButtonCounter = 1;
                while($(byXpath(String.format(HOTEL_BLOCK, hotelCounter))).isDisplayed() && hotelOfferCounter < hotelCounterValue){ // HOTEL_BLOCK
                    if(!hotelNamesOfferTempList.contains($(byXpath(String.format(HOTEL_NAME_IN_BLOCK, hotelCounter))).getText())){ //HOTEL_NAME
                        hotelNamesOfferTempList.add($(byXpath(String.format(HOTEL_NAME_IN_BLOCK, hotelCounter))).getText());
                        String fullHotelNameSpot = $(byXpath(String.format(HOTEL_NAME_IN_BLOCK, hotelCounter))).getText();
                        setHotelNamesOfferList(fullHotelNameSpot.substring(0, fullHotelNameSpot.indexOf(",")));//HOTEL_NAME
                        setRegionOfferList(fullHotelNameSpot.substring(fullHotelNameSpot.indexOf(", ") + 1).trim());
                        setHotelNameRoomOfferList($(byXpath(String.format(HOTEL_ROOM_NAME_IN_BLOCK, hotelCounter))).getText());
                        setHotelNameMealOfferList($(byXpath(String.format(HOTEL_MEAL_NAME_IN_BLOCK, hotelCounter))).getText());

                        if (resortValue.equals("-")){
                            setCountryResortOfferList(countryToValue);
                        }

                        else
                            setCountryResortOfferList(resortValue);
                        String oilTaxValue = "0";
                        if($(byXpath(String.format(HOTEL_OIL_TAX_BEFORE_ACT, hotelCounter))).isDisplayed())
                            oilTaxValue = $(byXpath(String.format(HOTEL_OIL_TAX_BEFORE_ACT, hotelCounter))).getText().replaceAll("\\s", "");
                        String priceSpotValue = $(byXpath(String.format(HOTEL_PRICE_BEFORE_ACT, hotelCounter))).getText().replaceAll("\\s", "");
                        String priceValue = String.valueOf(Integer.parseInt(oilTaxValue) + Integer.parseInt(priceSpotValue));
//                        if(toValue.equals("Coral Travel"))
//                            setHotelTourPriceOfferList(String.format("%.0f", Double.parseDouble(priceValue) * 0.965));
//                        else
                            setHotelTourPriceOfferList(priceValue);
                        setToNameOfferList(toValue);

                        //Actualization
                        if ($(byXpath(String.format(ACTUALIZE_TOUR_BUTTON, actualizeButtonCounter))).isDisplayed()){
                            try {
                                $(byXpath(String.format(ACTUALIZE_TOUR_BUTTON, actualizeButtonCounter))).click();
                                $(byXpath(ACTUALIZE_IN_PROGRESS_TOUR_LABEL_IN_BLOCk)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
                                if($(byXpath(String.format(HOTEL_PRICE_BEFORE_ACT, hotelCounter))).isDisplayed()){
                                    if($(byXpath(String.format(HOTEL_OIL_TAX_BEFORE_ACT, hotelCounter))).isDisplayed())
                                        oilTaxValue = $(byXpath(String.format(HOTEL_OIL_TAX_BEFORE_ACT, hotelCounter))).getText().replaceAll("\\s", "");
                                    priceSpotValue = $(byXpath(String.format(HOTEL_PRICE_BEFORE_ACT, hotelCounter))).getText().replaceAll("\\s", "");
                                    priceValue = String.valueOf(Integer.parseInt(oilTaxValue) + Integer.parseInt(priceSpotValue));
//                                    if(toValue.equals("Coral Travel"))
//                                        setHotelTourPriceActualizedOfferList(String.format("%.0f", Double.parseDouble(priceValue) * 0.965));
//                                    else
                                        setHotelTourPriceActualizedOfferList(priceValue);
                                }
                                else
                                    setHotelTourPriceActualizedOfferList("-");
                            }
                            catch (ElementNotInteractableException e){
                            };
                        }
                        else
                            setHotelTourPriceActualizedOfferList("-");

                        hotelOfferCounter ++;
                        //////////////////////////////////////////////////////
                    }
                    else
                        actualizeButtonCounter ++;

                    hotelCounter ++;
                }

                if($(byXpath(PAGINATION_NEXT_LINK)).isDisplayed() && hotelCounter <= hotelCounterValue)
                    $(byXpath(PAGINATION_NEXT_LINK)).waitUntil(Condition.visible, CONSTANT_2_SECONDS).click();
                else
                    break;
//                sleep(CONSTANT_1_SECOND);
//                moveToElement($(byXpath(POPULAR_FILTER_LABEL)));

            }
            while ($(byXpath(String.format(HOTEL_BLOCK, 1))).isDisplayed());

        }

        else{
            putEmptyHotelNamesOfferList(countryToValue, resortValue);
            setHotelNameRoomOfferList("-");
            setHotelNameMealOfferList("-");
            setToNameOfferList(toValue);
        }


        setHotelTourOfferAmountList((double) hotelOfferCounter);

        hotelNamesOfferTempList.clear();

    }

    public void putHotelNamesOfferListByTOWithHotel(String countryToValue, String resortValue, String toValue, String hotelIdValue) throws Exception {

        if(!ifEmptySearchResults() && driver.getCurrentUrl().contains(hotelIdValue) && !hotelIdValue.equals("0")){
            waitMoreTours();
//            if ($(byXpath(SHOW_ALL_TOURS_BUTTON)).isDisplayed())
//                $(byXpath(SHOW_ALL_TOURS_BUTTON)).click();

            sortByPriceFromLess();
            clickBulletCellsIcon();

            $(byXpath(ACTUALIZE_IN_PROGRESS_TOUR_LABEL_IN_BLOCk)).waitUntil(Condition.disappear, CONSTANT_420_SECONDS);

            String fullHotelNameSpotWithRegion = $(byXpath(String.format(HOTEL_NAME_IN_BLOCK, 1))).getText();
            String fullHotelNameSpot = "";
            if(!fullHotelNameSpotWithRegion.isEmpty())
                fullHotelNameSpot = fullHotelNameSpotWithRegion.substring(0, fullHotelNameSpotWithRegion.indexOf(", ")).trim();
            setHotelNamesOfferList(returnClearHotelNameValue(fullHotelNameSpot));
//            setHotelNamesOfferList(fullHotelNameSpot.substring(0, fullHotelNameSpot.indexOf(",")));//HOTEL_NAME
            if(!fullHotelNameSpotWithRegion.isEmpty()){
                setHotelStarsOfferList(returnHotelCategoryValue(fullHotelNameSpot));
                setRegionOfferList(fullHotelNameSpotWithRegion.substring(fullHotelNameSpotWithRegion.indexOf(", ") + 1).trim());
            }

            else{
                setHotelStarsOfferList(returnHotelCategoryValue(""));
                setRegionOfferList("");
            }
            setHotelNameRoomOfferList($(byXpath(String.format(HOTEL_ROOM_NAME_IN_BLOCK, 1))).getText());
            setHotelNameMealOfferList($(byXpath(String.format(HOTEL_MEAL_NAME_IN_BLOCK, 1))).getText());

            if (resortValue.equals("-")){
                setCountryResortOfferList(countryToValue);
            }

            else
                setCountryResortOfferList(resortValue);

            String oilTaxValue = "0";
            if($(byXpath(String.format(HOTEL_OIL_TAX_BEFORE_ACT, 1))).isDisplayed())
                oilTaxValue = $(byXpath(String.format(HOTEL_OIL_TAX_BEFORE_ACT, 1))).getText().replaceAll("\\s", "");
            String priceSpotValue = $(byXpath(String.format(HOTEL_PRICE_BEFORE_ACT, 1))).getText().replaceAll("\\s", "");
            String priceValue = String.valueOf(Integer.parseInt(oilTaxValue) + Integer.parseInt(priceSpotValue));
//            if(toValue.equals("Coral Travel"))
//                setHotelTourPriceOfferList(String.format("%.0f", Double.parseDouble(priceValue) * 0.965));
//            else
                setHotelTourPriceOfferList(priceValue);
            setToNameOfferList(toValue);

            //Actualization
            if ($(byXpath(String.format(ACTUALIZE_TOUR_BUTTON, 1))).isDisplayed()){
                try {
                    $(byXpath(String.format(ACTUALIZE_TOUR_BUTTON, 1))).click();
                    $(byXpath(ACTUALIZE_IN_PROGRESS_TOUR_LABEL_IN_BLOCk)).waitUntil(Condition.disappear, CONSTANT_420_SECONDS);
                    if($(byXpath(String.format(HOTEL_PRICE_BEFORE_ACT, 1))).isDisplayed()){
                        if($(byXpath(String.format(HOTEL_OIL_TAX_BEFORE_ACT, 1))).isDisplayed())
                            oilTaxValue = $(byXpath(String.format(HOTEL_OIL_TAX_BEFORE_ACT, 1))).getText().replaceAll("\\s", "");
                        priceSpotValue = $(byXpath(String.format(HOTEL_PRICE_BEFORE_ACT, 1))).getText().replaceAll("\\s", "");
                        priceValue = String.valueOf(Integer.parseInt(oilTaxValue) + Integer.parseInt(priceSpotValue));
//                        if(toValue.equals("Coral Travel"))
//                            setHotelTourPriceActualizedOfferList(String.format("%.0f", Double.parseDouble(priceValue) * 0.965));
//                        else
                            setHotelTourPriceActualizedOfferList(priceValue);
                    }
                    else
                        setHotelTourPriceActualizedOfferList("-");
                }
                catch (ElementNotInteractableException e){
                };
            }
            else
                setHotelTourPriceActualizedOfferList("-");

        }

        else{
            putEmptyHotelNamesOfferList(countryToValue, resortValue);
            setToNameOfferList(toValue);
        }

    }

    public OfferEmailSearchPage putHotelsAmountOfferList() throws Exception{
        if(!ifEmptySearchResults()) {
            waitMoreTours();
            if ($(byXpath(SHOW_ALL_TOURS_BUTTON)).isDisplayed())
                $(byXpath(SHOW_ALL_TOURS_BUTTON)).click();
            setHotelsAmountOfferList($(byXpath(OFFER_TOURS_VALUE)).waitUntil(Condition.visible, CONSTANT_360_SECONDS).getText().replaceAll("\\D", ""));
        }
        else{
            setHotelsAmountOfferList("0");
        }

        return this;
    }

    public boolean ifEmptySearchResults() throws Exception{
        return $(byXpath(EMPTY_SEARCH_LABEL)).isDisplayed();
    }

    public void putEmptyHotelNamesOfferList(String countryToValue, String resortValue) throws Exception{
        setHotelNamesOfferList("-");
        setRegionOfferList("-");
        setHotelStarsOfferList("-");
        setHotelNameRoomOfferList("-");
        setHotelNameMealOfferList("-");
        if (resortValue.equals("-"))
            setCountryResortOfferList(countryToValue);
        else
            setCountryResortOfferList(resortValue);
        setHotelTourPriceOfferList("-");
        setHotelTourPriceActualizedOfferList("-");
    }

}
