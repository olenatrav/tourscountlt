package pageobjects.LevelTravel;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import framework.Constants;
import framework.WebDriverCommands;
import org.openqa.selenium.logging.LogType;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.refresh;
import static framework.Constants.*;

public class LevelTravelHotelPage extends WebDriverCommands {

    private static LevelTravelHotelPage levelTravelHotelPage = null;
    public LevelTravelHotelPage(){

    }
    public static LevelTravelHotelPage getLevelTravelHotelPage(){
        if(levelTravelHotelPage == null)
            levelTravelHotelPage = new LevelTravelHotelPage();

        if ($(byText("500 Internal Server Error")).isDisplayed())
            refresh();

        if ($(byXpath("//div[@class = 'error']")).isDisplayed())
            refresh();

        checkServerErrorAndRefresh();

        return levelTravelHotelPage;
    }

    private final String HOTEL_PAGE_HEADER = "//h1[contains(text(), 'Туры в')]";
    private final String HOTEL_PAGE_TOURS_LOADER = "//div[contains(text(), 'Загружаем туры...')]";
    private final String HOTEL_PAGE_ALL_TOURS_BUTTON = "//button[text() = 'Все туры в этот отель']";
    private final String SLICK_DATES_PANEL = "//div[@class = 'slick-list']";
    private final String SHOW_PRICES_BUTTON = "//button[text() = 'Посмотреть цены']";
    private final String SHOW_PRICES_BUTTON_LOADER = "//button/div[contains(@class, 'Throbber__StyledThrobber')]"; ////div[contains(@class, 'SearcherDateCarouselItem') and contains(text(), '___________')]
    private final String SELECT_CONDITIONS_LABEL = "//h2[contains(text(), 'Выберите дату, номер, тип питания и количество ночей')]";
    private final String GOTO_TOUR_PAGE_NEW_BUTTON = "//div[contains(@class, 'HotelOfferCell__StyledNight') and text() = '%s ночей']/following-sibling::button[@type = 'button']";
    private final String NO_OFFERS_LABEL = "//div[contains(@class, 'HotelOfferCell__StyledNight') and text() = '%s ночей']/following-sibling::div[contains(@class, 'HotelOfferCell__StyledNoOffers')]";
    private final String TOURS_OLD_BLOCK = "//div[contains(@class, 'HotelSearchResults__StyledContainer')]";
    private final String GOTO_TOUR_PAGE_OLD_BUTTON = "//div[text() = '%s ночей']/following-sibling::button[contains(@class, 'HotelOfferCell__StyledBestOffer')]"; //"(//span[contains(@class, 'HotelOfferPrice__StyledPrice')])[1]";
    private final String GOTO_TOUR_PAGE_BY_NIGHTS_BUTTON = "(//div[contains(text(), '%s')]/following-sibling::button[contains(@data-link, 'package_details')])[%s]";
    private final String GOTO_TOUR_PAGE_BY_TO_BUTTON = "(//div[contains(@class, 'HotelOfferInfo') and contains(text(), '%s')]/ancestor::div[contains(@class, 'HotelOfferInfo')]/preceding-sibling::div[contains(text(), '%s')]/following-sibling::button[contains(@data-link, 'package_details')])[%s]";
    private final String GOTO_TOUR_PAGE_BY_TO_BUTTON_PRICE_VALUE = "/.//span[contains(@class, 'HotelOfferPrice')]";
    private final String TOURS_SELECT_TOP = "//div[@class = 'lt-hotel-results-top-content']";
    private final String OUTDATED_TOUR_POPUP = "similar-packages__title"; //classname
    private final String EMPTY_TOURS_ICON = "//i[contains(@class, 'empty-request')]";
    private final String EMPTY_TOURS_TITLE = "//div[contains(@class, 'SearcherEmptyRequest')]";
    private final String EMPTY_TOURS = "//div[contains(@class, 'SearcherEmpty')]";
    private final String EMPTY_TOURS_BLOCK = "//div[contains(@class, 'SearcherEmpty__StyledSearcherEmpty')]";
    private final String HOTEL_PAGE_HEADER_BLOCK = "//div[contains(@class, 'Header')]";
    private final String EMPTY_TOURS_BUTTON = "//button[contains(@class, 'SearcherEmptyRequest__StyledButton')]";
    private final String EMPTY_TOURS_SHOW_BUTTON = "//i[contains(@class, 'empty-request')]/following-sibling::button[contains(text(), 'Показать туры')]";
    private final String ERROR_LABEL = "//div[@class = 'error']";
    private final String FLAMINGO_BANNER = "//div[text() = 'Как вы обычно организуете отдых?']";
    private final String FLAMINGO_BANNER_CLOSE_BUTTON = "//div[text() = 'Как вы обычно организуете отдых?']/ancestor::div[1]/.//div[contains(@class, 'HeaderClose')]";

    public LevelTravelHotelPage closeFlamingoBanner() throws Exception{
        if($(byXpath(FLAMINGO_BANNER)).isDisplayed())
            $(byXpath(FLAMINGO_BANNER_CLOSE_BUTTON)).waitUntil(Condition.visible, CONSTANT_20_SECONDS).click();

        return this;
    }

    public LevelTravelHotelPage waitHotelPageTourLoaded(){
        $(byXpath(HOTEL_PAGE_HEADER)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS);
        $(byXpath(HOTEL_PAGE_TOURS_LOADER)).waitUntil(Condition.disappear, CONSTANT_420_SECONDS);

        return this;
    }

    public LevelTravelHotelPage waitHotelPageAllToursButton(){
        $(byXpath(HOTEL_PAGE_ALL_TOURS_BUTTON)).waitUntil(Condition.visible, CONSTANT_300_SECONDS);

        return this;
    }

    public boolean ifNoOffers (int nightsValue) throws Exception{
        if ($(byXpath(String.format(NO_OFFERS_LABEL, nightsValue))).isDisplayed())
            return true;
        else
            return false;
    }

    public boolean ifJSErrors() throws Exception{
        List jsErrors = Selenide.getWebDriverLogs(LogType.BROWSER, Level.SEVERE);
        if (jsErrors.isEmpty())
            return false;
        else
            return true;
    }

    public boolean ifHotelPageNoLoaded () throws Exception{
        if ($(byXpath(String.format(SHOW_PRICES_BUTTON_LOADER))).isDisplayed())
            return true;
        else
            return false;
    }

    public LevelTravelTourPage gotoLevelTravelTourPage(int nightsValue) throws Exception{
//        $(byXpath(HOTEL_PAGE_HEADER)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS);

        if($(byXpath(EMPTY_TOURS_ICON)).isDisplayed())
            $(byXpath(EMPTY_TOURS_SHOW_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).click();

        if($(byXpath(SLICK_DATES_PANEL)).isDisplayed()){
            $(byXpath(String.format(GOTO_TOUR_PAGE_NEW_BUTTON, nightsValue))).waitUntil(Condition.visible, Constants.CONSTANT_120_SECONDS);
            retryClickByXpath(String.format(GOTO_TOUR_PAGE_NEW_BUTTON, nightsValue));
        }
        else {
//            $(byXpath(TOURS_OLD_BLOCK)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).scrollTo();
            $(byXpath(String.format(GOTO_TOUR_PAGE_OLD_BUTTON, nightsValue))).waitUntil(Condition.visible, Constants.CONSTANT_120_SECONDS);
            retryClickByXpath(String.format(GOTO_TOUR_PAGE_OLD_BUTTON, nightsValue));
        }

        return new LevelTravelTourPage();
    }

    public LevelTravelTourPage gotoLevelTravelTourPageByTO(String toValue, int nightsValue) throws Exception{

        if($(byXpath(EMPTY_TOURS_ICON)).isDisplayed())
            $(byXpath(EMPTY_TOURS_SHOW_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_20_SECONDS).click();

        $(byXpath(String.format(GOTO_TOUR_PAGE_BY_TO_BUTTON, toValue, nightsValue, 1))).waitUntil(Condition.visible, CONSTANT_60_SECONDS);

//        if($(byXpath(SLICK_DATES_PANEL)).isDisplayed()){
        ArrayList<Integer> tourPriceValue = new ArrayList<Integer>();
        int toursCounter = 1;
        while($(byXpath(String.format(GOTO_TOUR_PAGE_BY_NIGHTS_BUTTON, nightsValue, toursCounter))).isDisplayed()){
            if($(byXpath(String.format(GOTO_TOUR_PAGE_BY_TO_BUTTON, toValue, nightsValue, toursCounter))).isDisplayed())
                tourPriceValue.add(Integer.parseInt($(byXpath(String.format(GOTO_TOUR_PAGE_BY_TO_BUTTON, toValue, nightsValue, toursCounter) + GOTO_TOUR_PAGE_BY_TO_BUTTON_PRICE_VALUE)).getText().replaceAll("\\D", "")));
            toursCounter ++;
        }
        int minPriceValueIndex = tourPriceValue.indexOf(Collections.min(tourPriceValue)) + 1;
        $(byXpath(String.format(GOTO_TOUR_PAGE_BY_TO_BUTTON, toValue, nightsValue, minPriceValueIndex))).waitUntil(Condition.visible, Constants.CONSTANT_120_SECONDS);
        retryClickByXpath(String.format(GOTO_TOUR_PAGE_BY_TO_BUTTON, toValue, nightsValue, minPriceValueIndex));
//        }
//        else {
//            $(byXpath(String.format(GOTO_TOUR_PAGE_OLD_BUTTON, nightsValue))).waitUntil(Condition.visible, Constants.CONSTANT_120_SECONDS);
//            retryClickByXpath(String.format(GOTO_TOUR_PAGE_OLD_BUTTON, nightsValue));
//        }

        return new LevelTravelTourPage();
    }

    public boolean ifTourOutdated() throws Exception{
        if ($(byClassName(OUTDATED_TOUR_POPUP)).isDisplayed())
            return true;
        else
            return false;

    }

    public boolean ifEmptyTours() throws Exception{
        if ($(byXpath(EMPTY_TOURS_BLOCK)).isDisplayed())
            return true;
        else
            return false;

    }

    public boolean ifEmptyHotelPage() throws Exception{
        if(!$(byXpath(HOTEL_PAGE_HEADER_BLOCK)).isDisplayed())
            return true;
        else
            return false;
    }

    public LevelTravelHotelPage loadToursIfButtonDisplayed() throws Exception{
        if ($(byXpath(EMPTY_TOURS_BUTTON)).isDisplayed())
            $(byXpath(EMPTY_TOURS_BUTTON)).click();

        return this;

    }

    public LevelTravelHotelPage reloadIfErrorDisplayed() throws Exception{
        if ($(byXpath(ERROR_LABEL)).isDisplayed())
            refreshPage();

        return this;

    }

}
