package pageobjects.LevelTravel;

import com.codeborne.selenide.Condition;
import framework.WebDriverCommands;
import org.openqa.selenium.By;

import java.util.ArrayList;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static framework.Constants.*;

public class LevelTravelHomePage extends WebDriverCommands {

    private LevelTravelHomePage levelTravelHomePage = null;
    private LevelTravelHomePage(){
        goToPage(HTTPS + LEVEL_TRAVEL_URL);
    }
    public LevelTravelHomePage getLevelTravelHomePage(){
        if(levelTravelHomePage == null)
            levelTravelHomePage = new LevelTravelHomePage();

        checkServerErrorAndRefresh();

        return levelTravelHomePage;
    }

    private final String LEVEL_TRAVEL_URL = "level.travel/";
    private final String DESTINATION_FIELD = "//div[contains(@class, 'search-form-destination')]/.//input";
    private final String DATE_FIELD = "//div[contains(@class, 'search-form-date')]/.//span";
    private final String CALENDAR_MONTH_YEAR_VALUE= "//div[@class = 'calendar-month']/div[text() = '%s']";
    private final String CALENDAR_DAY_VALUE= "//div[@class = 'calendar-month']/div[text() = '%s']/following::table/.//td[text() = '%s']";
    private final String CALENDAR_ANGLE_RIGHT_ELEMENT = "//i[@class = 'md-angle-right']";
//    private final String CALENDAR_ANGLE_LEFT_ELEMENT = "//i[@class = 'md-angle-left']";
    private final String FLEXIBLE_DATE_CHECKBOX = "//div[@class = 'flex-checkbox']";
    private final String NIGHTS_FIELD = "//div[contains(@class, 'search-form-nights')]/div";
    private final String NIGHTS_VALUE = "//div[contains(@class, 'search-form-nights-select')]/.//div[@class = 'value']";
    private final String NIGHTS_PLUS_BUTTON = "//div[contains(@class, 'search-form-nights-select')]/.//i[@class = 'md-plus']";
    private final String NIGHTS_MINUS_BUTTON = "//div[contains(@class, 'search-form-nights-select')]/.//i[@class = 'md-minus']";
    private final String FLEXIBLE_NIGHTS_CHECKBOX = "//div[@class = 'flex-check']";
    private final String TOURISTS_GROUP_FIELD = "//div[@class = 'search-form-tourists']";
    private final String ADULTS_VALUE = "//div[@class = 'search-form-tourists']/.//div[@class = 'value']";
    private final String ADULTS_PLUS_BUTTON = "//div[@class = 'search-form-tourists-select']/.//i[@class = 'md-plus']";
    private final String ADULTS_MINUS_BUTTON = "//div[@class = 'search-form-tourists-select']/.//i[@class = 'md-minus']";
    private final String ADD_KIDS_BUTTON = "//li[@class = 'add-kid']";
    private final String ADD_KIDS_OF_AGE_BUTTON = "//li[@class = 'add-kid']/.//option[@value = '%s']";
    private final String DEPARTURE_BUTTON = "//div[@class = 'search-form-departure']/input";
    private final String SEARCH_BUTTON = "//div[@class = 'search-form-submit']";


    public LevelTravelHomePage selectDestination (String destinationCountryValue) throws Exception
    {

            $(byXpath(DESTINATION_FIELD)).waitUntil(Condition.exist, CONSTANT_10_SECONDS)
                    .sendKeys(destinationCountryValue);

        return this;
    }

    public LevelTravelHomePage selectDate (String yearValue, String monthValue, String dayValue, boolean isFlexibleDate) throws Exception {

            $(byXpath(DATE_FIELD)).waitUntil(Condition.exist, CONSTANT_10_SECONDS).click();
            while (!($(byXpath(String.format(CALENDAR_MONTH_YEAR_VALUE, monthValue + " " + yearValue))).waitUntil(Condition.visible, CONSTANT_10_SECONDS).isDisplayed()))
                $(byXpath(CALENDAR_ANGLE_RIGHT_ELEMENT)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();
            $(byXpath(String.format(CALENDAR_DAY_VALUE, monthValue + " " + yearValue, dayValue))).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();
            $(byXpath(DATE_FIELD)).waitUntil(Condition.exist, CONSTANT_10_SECONDS).click();
            if (isFlexibleDate)
                $(byXpath(FLEXIBLE_DATE_CHECKBOX)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public LevelTravelHomePage selectNights (String nightsValue, boolean isFlexibleNights) throws Exception {

            $(byXpath(NIGHTS_FIELD)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();
            String defaultNightsValue = $(byXpath(NIGHTS_VALUE)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).getText().split(" ")[0];
            while (!nightsValue.equals(defaultNightsValue)){
                if (Integer.parseInt(nightsValue) > Integer.parseInt(defaultNightsValue))
                    $(byXpath(NIGHTS_PLUS_BUTTON)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();
                else
                    $(byXpath(NIGHTS_MINUS_BUTTON)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();

            }
            if (isFlexibleNights)
                $(byXpath(FLEXIBLE_NIGHTS_CHECKBOX)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();

        return this;
    }

    public LevelTravelHomePage selectTouristGroup (String adultsValue, ArrayList<String> kidsAges) throws Exception {

            $(By.xpath(TOURISTS_GROUP_FIELD)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();
            String defaultAdultsValue = $(byXpath(ADULTS_VALUE)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).getText().split(" ")[0];
            while (!adultsValue.equals(defaultAdultsValue)){
                if (Integer.parseInt(adultsValue) > Integer.parseInt(defaultAdultsValue))
                    $(byXpath(ADULTS_PLUS_BUTTON)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();
                else $(byXpath(ADULTS_MINUS_BUTTON)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();
            }
            for (int i=0; i < kidsAges.size(); i++){
                $(byXpath(ADD_KIDS_BUTTON)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();
                $(byXpath(String.format(ADD_KIDS_OF_AGE_BUTTON, kidsAges.get(i)))).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();
            }

        return this;

    }

    public LevelTravelHomePage selectDeparture (String departureValue) throws Exception {

            $(byXpath(DEPARTURE_BUTTON)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).sendKeys(departureValue);

        return this;
    }

    public LevelTravelSearchPage searchTours () throws Exception {

            $(byXpath(SEARCH_BUTTON)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();

        return new LevelTravelSearchPage();
    }

}
