package pageobjects.LevelTravel;

import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.WebDriverCommands;

import java.util.ArrayList;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static framework.Constants.CONSTANT_20_SECONDS;
import static pageobjects.LevelTravel.LevelTravelTourPage.getHotelTourPricesTourLTList;

public class LevelTravelTourPage extends WebDriverCommands {

    private static LevelTravelTourPage levelTravelTourPage = null;
    public LevelTravelTourPage(){

    }
    public static LevelTravelTourPage getLevelTravelTourPage(){
        if(levelTravelTourPage == null)
            levelTravelTourPage = new LevelTravelTourPage();

        if ($(byText("500 Internal Server Error")).isDisplayed())
            refresh();

        if ($(byXpath("//div[@class = 'error']")).isDisplayed())
            refresh();

        checkServerErrorAndRefresh();

        return levelTravelTourPage;
    }

    private final String FLIGHTS_LOADER = "//div[@class = 'checkout-flights__progress-text']";
    private final String CHECK_FLIGHTS_LOADER = "//div[@class = 'checkout-flights__progress-text']";
    private final String TOUR_BLOCK = "//div[@class = 'checkout-hotel']";
    private final String CHECKING_FLIGHTS_TITLE = "//h1[contains(text(), 'Проверяем наличие туров')]";
    private final String SIMILAR_PACKAGE_TITLE = "//h1[text() = 'Тур устарел!']";
    private final String TOUR_IS_ABSENT_POPUP = "global_notice"; //id
    private final String TOUR_DATA_MIGHT_OUTDATED_TEXT = "//div[text() = 'Данные тура могли устареть']";
    private final String TOUR_DATA_MIGHT_OUTDATED_LOADER = "//div[@class = 'lt-preloader-logo__sun-mask']";
    private final String TOUR_DATA_MIGHT_OUTDATED_CLASS = "similar-packages__title";  //className
    private final String TOUR_DATA_OUTDATED = "//div[text() = 'Чуть-чуть не успели!']";
    private final String TOUR_OUTDATED_MODAL = "//div[contains(@class, 'similar-packages__modal')]";
    private final String ROUTES_NOT_FOUND_LABEL = "//div[text() = 'Не удалось загрузить рейсы']";
    private final String ALL_TOURS_BUTTON = "//a[text() = 'Все туры в этот отель']";
    private final String TOUR_PAGE_TITLE = "//div[@class = 'checkout-hotel__title']";

    private final String FLIGHTS_BLOCK = "//div[contains(@class, 'checkout-flight__active')]";
    private final String BILL_INFO_BLOCK = "//div[@class = 'checkout-billinfo']";
    private final String TOTAL_PRICE_LABEL = "//span[@class = 'checkout-billinfo__value']";
    private final String FUEL_PRICE_LABEL = "//div[@title = 'Доплаты и сборы']/following-sibling::div/span";
    private final String TO_LABEL = "//span[@class = 'checkout-shortinfo__top-operator_text']";
    private final String ROOM_NAME_LABEL = "//h3[@class = 'checkout-packageinfo__roomtype']";
    private final String ORIGINAL_ROOM_NAME_LABEL = "//span[@class = 'checkout-packageinfo__originalroomtype']";
    private final String MEAL_NAME_LABEL = "(//span[@class = 'checkout-packageinfo__text'])[1]";

    private final String GOTO_FLIGHTS_BUTTON = "//button[contains(@class, 'checkout-packageinfo__selectflight')]";
    private final String GOTO_RESERVATION_BUTTON = "(//button[contains(@class, 'checkout-flight__submit')])[1]";
    private final String AUTH_FORM = "//div[contains(@class, 'checkout-client__auth')]";

    private final String ERROR_LABEL = "//div[@class = 'error']";

    private final String FLAMINGO_BANNER = "//div[text() = 'Как вы обычно организуете отдых?']";
    private final String FLAMINGO_BANNER_CLOSE_BUTTON = "//div[text() = 'Как вы обычно организуете отдых?']/ancestor::div[1]/.//div[contains(@class, 'HeaderClose')]";

    //////////////////////////////////////////////////////////////////////////////////////
    private static ArrayList<String> toNameTourLTList = new ArrayList<String>();
    public static ArrayList <String> getToNameTourLTList(){
        return toNameTourLTList;
    }
    public static void setToNameTourLTList (String toNameTourLTListValue){
        toNameTourLTList.add(toNameTourLTListValue);
    }

    private static ArrayList<String> roomNameTourLTList = new ArrayList<String>();
    public static ArrayList <String> getRoomNameTourLTList(){
        return roomNameTourLTList;
    }
    public static void setRoomNameTourLTList (String roomNameTourLTListValue){
        roomNameTourLTList.add(roomNameTourLTListValue);
    }

    private static ArrayList<String> mealNameTourLTList = new ArrayList<String>();
    public static ArrayList <String> getMealNameTourLTList(){
        return mealNameTourLTList;
    }
    public static void setMealNameTourLTList (String mealNameTourLTListValue){
        mealNameTourLTList.add(mealNameTourLTListValue);
    }

    private static ArrayList <String> hotelTourPricesTourLTList = new ArrayList<String>();
    public static ArrayList <String> getHotelTourPricesTourLTList(){
        return hotelTourPricesTourLTList;
    }
    public static void setHotelTourPricesTourLTList (String hotelTourPriceTourLTValue){
        hotelTourPricesTourLTList.add(hotelTourPriceTourLTValue);
    }

    private static   ArrayList <String> hotelTourPriceOilTaxTourLTList = new ArrayList<String>();
    public static ArrayList <String> getHotelTourPriceOilTaxTourLTList(){
        return hotelTourPriceOilTaxTourLTList;
    }
    public static void setHotelTourPriceOilTaxTourLTList (String hotelTourPriceOilTaxTourLTValue){
        hotelTourPriceOilTaxTourLTList.add(hotelTourPriceOilTaxTourLTValue);
    }

    private static ArrayList <String> hotelTourPriceWithoutOilTaxTourLTList = new ArrayList<String>();
    public static ArrayList <String> getHotelTourPriceWithoutOilTaxTourLTList() {
        return hotelTourPriceWithoutOilTaxTourLTList;
    }
    public static void setHotelTourPriceWithoutOilTaxTourLTList (String hotelTourPriceWithoutOilTaxTourLTValue){
        hotelTourPriceWithoutOilTaxTourLTList.add(hotelTourPriceWithoutOilTaxTourLTValue);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    public LevelTravelTourPage closeFlamingoBanner() throws Exception{
        if($(byXpath(FLAMINGO_BANNER)).isDisplayed())
            $(byXpath(FLAMINGO_BANNER_CLOSE_BUTTON)).waitUntil(Condition.visible, CONSTANT_20_SECONDS).click();

        return this;
    }

    public LevelTravelTourPage waitForLoaderDissapear() throws Exception {
        sleep(Constants.CONSTANT_2_SECONDS);
        $(byXpath(FLIGHTS_LOADER)).waitUntil(Condition.disappear, Constants.CONSTANT_540_SECONDS);

        return this;
    }

    public boolean checkIfTourLoaded() throws Exception{
        sleep(Constants.CONSTANT_2_SECONDS);
        return $(byXpath(TOUR_BLOCK)).isDisplayed();
    }

    public LevelTravelTourPage waitForCheckingFlights() throws Exception {
        $(byXpath(TOUR_PAGE_TITLE)).waitUntil(Condition.appear, Constants.CONSTANT_60_SECONDS);
        $(byXpath(CHECKING_FLIGHTS_TITLE)).waitUntil(Condition.disappear, Constants.CONSTANT_420_SECONDS);

        return this;
    }

    public boolean checkIfTourLTOutdated() throws Exception{
        boolean isTourOutdated = false;

        $(byXpath(TOUR_PAGE_TITLE)).waitUntil(Condition.appear, Constants.CONSTANT_60_SECONDS);

        if($(byXpath(TOUR_DATA_MIGHT_OUTDATED_TEXT)).isDisplayed())
            $(byXpath(TOUR_DATA_MIGHT_OUTDATED_LOADER)).waitUntil(Condition.disappear, Constants.CONSTANT_240_SECONDS);

        sleep(Constants.CONSTANT_3_SECONDS);
        if (    ($(byXpath(TOUR_OUTDATED_MODAL)).isDisplayed())
                || ($(byXpath(ROUTES_NOT_FOUND_LABEL)).isDisplayed())
                || ($(byXpath(SIMILAR_PACKAGE_TITLE)).isDisplayed())
                || $(byId(TOUR_IS_ABSENT_POPUP)).isDisplayed()
                || ($(byXpath(TOUR_DATA_OUTDATED)).isDisplayed())
        )
            isTourOutdated = true;

//        sleep(Constants.CONSTANT_2_SECONDS);
//        if ($(byXpath(SIMILAR_PACKAGE_TITLE)).exists())
//            isTourOutdated = true;
//
//        sleep(Constants.CONSTANT_2_SECONDS);
//        if ($(byId(TOUR_IS_ABSENT_POPUP)).exists())
//            isTourOutdated = true;
//
//        sleep(Constants.CONSTANT_2_SECONDS);
//        if($(byXpath(TOUR_DATA_OUTDATED)).exists())
//            isTourOutdated = true;

        return isTourOutdated;
    }

    public LevelTravelTourPage addEmptyTourTOPrice() throws Exception{
        setHotelTourPricesTourLTList("-");
        setHotelTourPriceOilTaxTourLTList("-");
        setHotelTourPriceWithoutOilTaxTourLTList("-");
        setToNameTourLTList("-");
        setRoomNameTourLTList("-");
        setMealNameTourLTList("-");

        return this;
    }

    public LevelTravelTourPage addTourPrice() throws Exception{
        String s = $(byXpath(TOTAL_PRICE_LABEL)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).getText().replaceAll("\\D", "");
        setHotelTourPricesTourLTList($(byXpath(TOTAL_PRICE_LABEL)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).getText().replaceAll("\\D", ""));
        setHotelTourPriceOilTaxTourLTList($(byXpath(FUEL_PRICE_LABEL)).waitUntil(Condition.visible, Constants.CONSTANT_10_SECONDS).getText().replaceAll("\\D", ""));
        setHotelTourPriceWithoutOilTaxTourLTList(Integer.toString((Integer.parseInt(getHotelTourPricesTourLTList().get(getHotelTourPricesTourLTList().size()-1))
                - Integer.parseInt(getHotelTourPriceOilTaxTourLTList().get(getHotelTourPriceOilTaxTourLTList().size()-1)))));

        return this;
    }

    public LevelTravelTourPage addTourTO() throws Exception{
//        $(byXpath(GOTO_FLIGHTS_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_180_SECONDS).click();
//        $(byXpath(GOTO_RESERVATION_BUTTON)).waitUntil(Condition.visible, Constants.CONSTANT_180_SECONDS).click();
//        $(byXpath(AUTH_FORM)).waitUntil(Condition.visible, Constants.CONSTANT_180_SECONDS);
//        driver.navigate().back();
        if ($(byXpath(TO_LABEL)).waitUntil(Condition.exist, Constants.CONSTANT_60_SECONDS).getText().isEmpty())
            setToNameTourLTList("Paks");
        else {
            String toNameFirstLetter = $(byXpath(TO_LABEL)).waitUntil(Condition.visible, Constants.CONSTANT_60_SECONDS).getText().substring(0, 1);
            setToNameTourLTList(toNameFirstLetter + $(byXpath(TO_LABEL)).waitUntil(Condition.visible, Constants.CONSTANT_10_SECONDS).getText().toLowerCase().substring(1));
        }

        return this;
    }

    public LevelTravelTourPage addTourRoomName() throws Exception{
        if($(byXpath(ORIGINAL_ROOM_NAME_LABEL)).exists())
            setRoomNameTourLTList($(byXpath(ORIGINAL_ROOM_NAME_LABEL)).waitUntil(Condition.visible, Constants.CONSTANT_10_SECONDS).getText());
        else
            setRoomNameTourLTList($(byXpath(ROOM_NAME_LABEL)).waitUntil(Condition.visible, Constants.CONSTANT_10_SECONDS).getText());

        return this;
    }

    public LevelTravelTourPage addTourMealName() throws Exception{
        setMealNameTourLTList($(byXpath(MEAL_NAME_LABEL)).waitUntil(Condition.visible, Constants.CONSTANT_10_SECONDS).getText().replaceAll("[а-яА-ЯёЁ()]", ""));

        return this;
    }

    public LevelTravelTourPage reloadIfErrorDisplayed() throws Exception{
        if ($(byXpath(ERROR_LABEL)).isDisplayed())
            refreshPage();

        return this;

    }

}
