package pageobjects.LevelTravel;

import com.codeborne.selenide.Condition;
import framework.WebDriverCommands;
import org.apache.poi.ss.usermodel.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.io.FileOutputStream;
import java.util.*;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.*;
import static framework.Constants.*;
import static framework.ReportGenerator.*;

public class LevelTravelSearchPage extends WebDriverCommands {

    private static LevelTravelSearchPage levelTravelSearchPage = null;
    public LevelTravelSearchPage(){

    }
    public static LevelTravelSearchPage getLevelTravelSearchPage(){
        if(levelTravelSearchPage == null)
            levelTravelSearchPage = new LevelTravelSearchPage();

        if ($(byText("500 Internal Server Error")).isDisplayed())
            refresh();

        if ($(byXpath("//div[@class = 'error']")).isDisplayed())
            refresh();

        checkServerErrorAndRefresh();

        return levelTravelSearchPage;
    }

    private final String LEVEL_TRAVEL_HEADER = "//div[@class = 'search-page-header']";
    private final String TOURS_PRELOADER = "//div[@class = 'lt-preloader-logo']";
    private static final String SELECT_TOUR_LABEL = "//h1[@class = 'search-page-title']/span[contains(text(), 'Выберите тур ')]";
    private static final String LOAD_TOURS_LABEL = "//h1[@class = 'search-page-title']/span[contains(text(), 'Загружаем туры ')]";
    private static final String LOAD_TOURS_PROGRESS_BAR = "//div[@class = 'search-page__progressbar-complete']";
    private static final String FILTER_BAY_HOTEL = "//div[text() = 'Расположен в бухте']";
    private final String FILTER_TO_CHECKBOX_CONTAINER = "(//div[contains(@class, 'filter-operators')]/.//div[contains(@class, 'FilterLabel__StyledItemNameContainer')])[%s]";
    private final String FILTER_TO_CHECKBOX_NAME = "(//div[contains(@class, 'filter-operators')]/.//div[contains(@class, 'FilterLabel__StyledItemNameContainer')]/div)[%s]"; //"//label[@for = 'filter-operator-checkbox-%s']/.//div[@class = 'min-price-label__item-label']";
    private final String FILTER_TO_CHECKBOX_INFO = "(//div[contains(@class, 'filter-operators')]/.//div[contains(@class, 'FilterLabel__StyledItemInfo')])[%s]";
    private final String FILTER_TO_CHECKBOX = "(//div[contains(@class, 'filter-operators')]/.//input[@type = 'checkbox'])[%s]";
    private final String FILTER_TO_CHECKBOX_CHECKED = "(//div[contains(@class, 'filter-operators')]/.//*[name()='svg']/following::div[contains(@class, 'FilterLabel__StyledFilterLabel')])[1]";
    private static final String BANNER_APPLY_FILTER = "//div[text() = 'Применяем фильтр']";
    private final String TOURS_SHOWN_COUNTER_LABEL = "//div[@class = 'search-page__hotels-counter']";
    private final String TOURS_COUNTER_LABEL = "(//div[contains(@class, 'StyledSwitcherLabel')])[2]/div[2]";
    private final String WIFI_TOURS_BLOCK = "//span[text() = 'Wi-Fi']";
    private final String WIFI_TOURS_COUNTER_LABEL = "(//input[@name = 'wifi'])[1]/following-sibling::div/div[contains(@class, 'FilterLabel__StyledItemInfo')]";
    private final String TO_LIST = "//div[contains(@class, 'filter-operator__checkbox-group')]";
    private final String TO_LIST_BLOCK = "//ul[contains(@class, 'filter-operator__list')]";
    private final String TO_LIST_LABEL = "//span[text() = 'Туроператор']";
    private final String SHOW_ALL_TO_BUTTON = "//button[text() = 'Показать всех туроператоров']";  //button[@class = 'checkbox-group__toggle-button filter-operator__toggle-button']
    private final String HIDE_ALL_TO_BUTTON = "//button[text() = 'Скрыть']";
    private final String SHOW_ALL_TO_ARROW = "svg[@name = 'smd-search-filter-show-all']";
    private final String HOTEL_FACILITIES_LABEL = "//div[@class = 'filters__filter-label' and text() = 'Удобства в отеле']";
    private static final String EMPTY_SEARCH = "//div[@class = 'empty-search-title']";
    private static final String EMPTY_SEARCH_TITLE = "//div[contains(@class, 'SearcherEmptyTitle')]";
    private static final String EMPTY_TOURS_BLOCK = "//div[contains(@class, 'SearcherEmpty__StyledSearcherEmpty')]";
    private static final String EMPTY_SEARCH_WITH_FILTERS = "//div[@class = 'no-results']";

    private final String PROMOCODE_BUNNER_CONTAINER = "//div[@class = 'lt-survey-banner__container']";
    private final String PROMOCODE_BUNNER_CLOSE_BUTTON = "//button[@class = 'lt-survey-banner__close']";

    private static final String SORT_BY_PRICE_BUTTON = "//button[contains(@class, 'sorting__link') and contains(text(), 'по цене')]";
    private final String SORT_BY_PRICE_BUTTON_ACTIVE = "//button[contains(@class, 'sorting__link--active') and contains(text(), 'по цене')]";
    private final String HOTEL_BLOCK = "//div[@class = 'hotels-list-item'][%s]";
    private final String HOTEL_STARS = "//div[@class = 'hotels-list-item'][%s]/.//div[@class = 'hotel-stars']";
    private static final String HOTEL_NAME_LINK = "(//div[contains(@class, 'HotelCard__StyledHotelCard')]/.//span[contains(@class, 'HotelCardTitle__StyledHotelName')])[%s]";
    private final String HOTEL_TOUR_PRICE = "(//div[contains(@class, 'HotelCard__StyledHotelCard')]/.//span[contains(@class, 'HotelCardPrice__StyledPrice')])[%s]";
    private final String HOTEL_NAME_LINK_PX = "//div[@class = 'hotels-list-item' and contains(@style, 'top: %spx;')]/.//span[contains(@class, 'HotelCardTitle__StyledHotelName')]"; //div[@class = 'hotels-list-item'][%s]/.//a[@class = 'hotel-explore-link']
    private final String HOTEL_RESORT_NAME_LINK_PX = "//div[@class = 'hotels-list-item' and contains(@style, 'top: %spx;')]/.//div[contains(@class, 'HotelCardMetaTitle__StyledLocationLabel')]";
    private final String HOTEL_STARS_PX = "(//div[@class = 'hotels-list-item' and contains(@style, 'top: %spx;')]/.//*[name() = 'svg' and contains(@class, 'HotelStars__StyledIconStar')])[%s]";
    private final String HOTEL_TOUR_PRICE_PX = "//div[@class = 'hotels-list-item' and contains(@style, 'top: %spx;')]/.//span[contains(@class, 'HotelCardPrice__StyledPrice')]"; //div[@class = 'hotels-list-item'][%s]/.//span[@class = 'hotel__price']
    private final String HOTEL_GOTO_HOTEL_PAGE_BUTTON = "//div[@class = 'hotels-list-item'][%s]/.//a[@class = 'hotel-explore-link']";
    private final String HOTEL_LINK = "(//a[contains(@href, '/hotels/')])[%s]";
    private static final String HOTEL_BLOCK_TOP_PX = "//div[@class = 'hotels-list-item' and contains(@style, 'top: %spx;')]";
    private final String ERROR_500 = "//div[@class = 'error']";

    private final String SWITCH = "(//div[contains(@class, 'ToggleSwitch__StyledIconContainer')])[1]";
    private final String SWITCH_URL = "?filter_instant_confirm=true";

    private static final String PAYLATE_BANNER = "//div[@class = 'paylateBan']";
    private static final String FREE_CANCELLATION_BANNER = "//a[@class = 'free-cancellation-banner']";
    private static final String HOTEL_BLOCK_PX = "//div[@class = 'hotels-list-item' and contains(@style, 'top: %spx;')]";
    private static final String HOTEL_BLOCK_HEADER_LABEL_PX = "//div[@class = 'hotels-list-item' and contains(@style, 'top: %spx;')]/.//div[contains(@class, 'HotelCardHeaderLabel')]";
    private static final String HOTEL_BLOCK_SIMILAR_TOURS_PX = "//div[@class = 'hotels-list-item' and contains(@style, 'top: %spx;')]/.//div[contains(@class, 'HotelCardSuggestedDates')]";
    private static final String HOTEL_BLOCK_CONDITIONS_PX = "(//div[@class = 'hotels-list-item' and contains(@style, 'top: %spx;')]/.//div[contains(@class, 'HotelCardFacts')])[1]";
    private static final String HOTEL_BLOCK_INSTANT_CONFIRM_PX = "//div[@class = 'hotels-list-item' and contains(@style, 'top: %spx;')]/.//div[contains(@type, 'instantConfirm')]";
    private static final String HOTEL_BLOCK_VARIOUS_LABEL_PX = "//div[@class = 'hotels-list-item' and contains(@style, 'top: %spx;')]/.//div[contains(@class, 'Label__StyledLabel')]";

    private final String LOTTERY_POPUP = "//div[contains(@class, 'js-lottery-dg')]";
    private final String LOTTERY_POPUP_CLOSE_BUTTON = "//div[contains(@class, 'js-lottery-dg')]/div[@class = 'popupClose']";

    private final String FLAMINGO_BANNER = "//div[text() = 'Как вы обычно организуете отдых?']";
    private final String FLAMINGO_BANNER_CLOSE_BUTTON = "//div[text() = 'Как вы обычно организуете отдых?']/ancestor::div[1]/.//div[contains(@class, 'HeaderClose')]";

    public static String getHotelNameLink(){ return HOTEL_NAME_LINK; }
    public static String getHotelBlockTopPx(){ return HOTEL_BLOCK_TOP_PX; }
    public static String getHotelBlockHeaderLabelPx(){ return HOTEL_BLOCK_HEADER_LABEL_PX; }
    public static String getHotelBlockSimilarToursPx(){ return HOTEL_BLOCK_SIMILAR_TOURS_PX; }
    public static String getHotelBlockConditionsPx(){ return HOTEL_BLOCK_CONDITIONS_PX; }
    public static String getHotelBlockInstantConfirmPx(){ return HOTEL_BLOCK_INSTANT_CONFIRM_PX; }
    public static String getHotelBlockVariousLabelPx(){ return HOTEL_BLOCK_VARIOUS_LABEL_PX; }

    public static int getHotelBlockHeightPx(int topPxValue){
        String styleValue = $(byXpath(String.format(HOTEL_BLOCK_PX, topPxValue))).waitUntil(Condition.visible, CONSTANT_300_SECONDS).getAttribute("style");
        return Integer.parseInt(styleValue.substring(0, styleValue.indexOf(";")).replaceAll("\\D", ""));
    }

    //////////////////////////////////////////////////////////////////////////////////////
    private static ArrayList <String> TOList = new ArrayList<String>();
    public static ArrayList <String> getTOList(){
        return TOList;
    }
    public static void setTOList (String TOListValue){
        TOList.add(TOListValue);
    }

//    private String currentDateValue = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
//    public String getCurrentDateValue() {
//        return currentDateValue;
//    }

    private int currentWeekNumberValue;
    public int getCurrentWeekNumberValue() {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(new Date());
        currentWeekNumberValue = calendar.get(Calendar.WEEK_OF_YEAR);
        return currentWeekNumberValue;
    }

    private static ArrayList <String> hotelNamesSearchLTList = new ArrayList<String>();
    public static ArrayList <String> getHotelNamesSearchLTList(){
        return hotelNamesSearchLTList; }
    public static void setHotelNamesSearchLTList (String hotelNamesSearchLTListValue){
        hotelNamesSearchLTList.add(hotelNamesSearchLTListValue);
    }

    private static ArrayList<String> regionLTList = new ArrayList<String>();
    public static ArrayList <String> getRegionLTList(){
        return regionLTList;
    }
    public static void setRegionLTList(String regionLTListValue){
        regionLTList.add(regionLTListValue);
    }

    private static ArrayList <String> hotelTourPricesSearchLTList = new ArrayList<String>();
    public static ArrayList <String> getHotelTourPricesSearchLTList(){
        return hotelTourPricesSearchLTList;
    }
    public static void setHotelTourPricesSearchLTList (String hotelTourPriceSearchLTValue){
        hotelTourPricesSearchLTList.add(hotelTourPriceSearchLTValue);
    }
    //////////////////////////////////////////////////////////////////////////////////////

    public LevelTravelSearchPage closeFlamingoBanner() throws Exception{
        if($(byXpath(FLAMINGO_BANNER)).isDisplayed())
            $(byXpath(FLAMINGO_BANNER_CLOSE_BUTTON)).waitUntil(Condition.visible, CONSTANT_20_SECONDS).click();

        return this;
    }

    public LevelTravelSearchPage getStatisticByTO (Workbook book, String countryValue, int dateCounter, boolean isFlexibleDate, int nightsValue, boolean isFlexibleNights,
                                                   String adultsValue, ArrayList<String> kidsAgesList, String departureValue, ArrayList<String> starsValues, String reportPathValue) throws Exception{

        Sheet sheet;
        if (book.getSheetIndex(departureValue + "-" + countryValue) == -1)
            sheet = book.createSheet(departureValue + "-" + countryValue);
        else
            sheet = book.getSheet(departureValue + "-" + countryValue);

        book.write(new FileOutputStream(reportPathValue));

        CellStyle style = book.createCellStyle();
        Font font = book.createFont();
        font.setBold(true);
        style.setFont(font);

        Row row = null;

        if(sheet.getLastRowNum() > 0){
            for (int i=0; i <= sheet.getLastRowNum(); i++){
                row = sheet.getRow(i);
                sheet.removeRow(row);
            }
        }

        book.write(new FileOutputStream(reportPathValue));

        int rowCounter = 0;
        int cellNum = 0;

        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0){
            row = sheet.getRow(rowCounter);
            cellNum = sheet.getRow(1).getLastCellNum();
        }
        else
            row = sheet.createRow(rowCounter);

        ////////////////////////
//        if (cellNum == 25){
//            for (int i=0; i<5; i++){
//                for (int j=0; j<=sheet.getLastRowNum(); j++){
//                    Cell c = sheet.getRow(j).getCell(0);
//                    if(!(c == null || c.getCellTypeEnum() == CellType.BLANK))
//                        sheet.getRow(j).removeCell(sheet.getRow(j).getCell(0));
//                }
//                deleteColumn(sheet, 0);
//                book.write(new FileOutputStream(reportPathValue));
//            }
//        }
//
//        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0){
//            row = sheet.getRow(rowCounter);
//            cellNum = sheet.getRow(1).getLastCellNum();
//        }
//        else
//            row = sheet.createRow(rowCounter);
        //////////////////////

        Cell statisticDate = row.createCell(cellNum);
        statisticDate.setCellValue(getCurrentDateValue() +  " " + getCurrentWeekNumberValue() + " " + getSearchDate(dateCounter));
        statisticDate.setCellStyle(style);

        rowCounter ++;

        if (rowCounter <= sheet.getLastRowNum()){
            row = sheet.getRow(rowCounter);
            cellNum = row.getLastCellNum();
        }
        else
            row = sheet.createRow(rowCounter);

        Cell resort = row.createCell(cellNum);
        resort.setCellValue("Страна/Курорт");
        resort.setCellStyle(style);

        Cell operator = row.createCell(cellNum + 1);
        operator.setCellValue("Operator");
        operator.setCellStyle(style);

        Cell tourCountLT = row.createCell(cellNum + 2);
        tourCountLT.setCellValue("LevelTravel");
        tourCountLT.setCellStyle(style);

        rowCounter++;

        String searchUrlByCountry = HTTPS + LEVEL_TRAVEL_URL + "search/" + departureLT.get(departureValue) + "-" + destinationCountryLT.get("Россия") +
                "-to-Any-" + destinationCountryLT.get(countryValue) + "-departure-";

        searchUrlByCountry += getSearchDate(dateCounter) + "-for-";

        if (isFlexibleNights)
            searchUrlByCountry += (nightsValue - 2) + ".." + (nightsValue + 2) + "-nights-";
        else
            searchUrlByCountry += (nightsValue) + "-nights-";

        searchUrlByCountry += adultsValue + "-adults-";

        if(!kidsAgesList.get(0).equals("0")){
            searchUrlByCountry += kidsAgesList.size() + "(";
            for (int i=0; i< kidsAgesList.size(); i++){
                searchUrlByCountry += kidsAgesList.get(i);
                if (i+1<kidsAgesList.size())
                    searchUrlByCountry += ",";
            }
            searchUrlByCountry += ")";
        }
        else
            searchUrlByCountry += "0";

        searchUrlByCountry +="-kids-1..5-stars?";

//        if (isFlexibleDate) {
//            if (departureValue.equals("Москва"))
//                searchUrlByCountry += "&flex_dates=3";
//            else
//                searchUrlByCountry += "&flex_dates=5";
//        }

        if (isFlexibleDate){
            if(searchUrlByCountry.endsWith("?"))
                searchUrlByCountry += "flex_dates=6";
            else
                searchUrlByCountry += "&flex_dates=6";
        }

        if(starsValues.size() > 0)
        {
            if(searchUrlByCountry.endsWith("?"))
                searchUrlByCountry += "filter_stars=";
            else
                searchUrlByCountry += "&filter_stars=";
            for(int i=0; i<starsValues.size(); i++)
                searchUrlByCountry +=starsValues.get(i) + ",";
        }

        //Country
        goToPage(searchUrlByCountry);
        if (!($(byXpath(LEVEL_TRAVEL_HEADER)).isDisplayed()) || $(byXpath(ERROR_500)).isDisplayed())
            refreshPage();

        $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//        $(byXpath(LOAD_TOURS_LABEL)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//        $(byXpath(SORT_BY_PRICE_BUTTON)).waitUntil(Condition.visible, CONSTANT_300_SECONDS).click();
//        $(byXpath(BANNER_APPLY_FILTER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
        $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
        sleep(CONSTANT_1_SECOND);
        if($(byXpath(ERROR_500)).isDisplayed()){
            refreshPage();
            $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//            $(byXpath(LOAD_TOURS_LABEL)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//            $(byXpath(SORT_BY_PRICE_BUTTON)).waitUntil(Condition.visible, CONSTANT_300_SECONDS).click();
//            $(byXpath(BANNER_APPLY_FILTER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
            $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
            sleep(CONSTANT_1_SECOND);
        }

        //Instant confirmation switch for Russia
//        if(countryValue.equals("Россия")){
//            $(byXpath(SWITCH)).waitUntil(Condition.visible,CONSTANT_20_SECONDS).click();
//            $(byXpath(BANNER_APPLY_FILTER)).waitUntil(Condition.disappear, CONSTANT_30_SECONDS);
//        }

        //Close bunner
        if($(byXpath(PROMOCODE_BUNNER_CONTAINER)).isDisplayed())
            $(byXpath(PROMOCODE_BUNNER_CLOSE_BUTTON)).waitUntil(Condition.visible, CONSTANT_5_SECONDS).click();

        if (rowCounter <= sheet.getLastRowNum()  && sheet.getLastRowNum() > 0)
            row = sheet.getRow(rowCounter);
        else
            row = sheet.createRow(rowCounter);
        resort = row.createCell(cellNum);
        resort.setCellValue(countryValue);
        operator = row.createCell(cellNum + 1);
        String currentTOValue = "Всего";
        operator.setCellValue(currentTOValue);
        setTOList(currentTOValue);

        tourCountLT = row.createCell(cellNum + 2);

        String currentToursCountValue = "0";
        if(!($(byXpath(EMPTY_SEARCH)).isDisplayed() || $(byXpath(EMPTY_SEARCH_WITH_FILTERS)).isDisplayed() || $(byXpath(EMPTY_TOURS_BLOCK)).isDisplayed())){
            $(byXpath(SELECT_TOUR_LABEL)).waitUntil(Condition.visible, CONSTANT_300_SECONDS);
            scrollUpOnPage();
            if($(byXpath(TOURS_SHOWN_COUNTER_LABEL)).isDisplayed()){
                currentToursCountValue = $(byXpath(TOURS_SHOWN_COUNTER_LABEL)).getText().replaceAll("[^0-9]", "");
                if(currentToursCountValue.equals(""))
                    currentToursCountValue = "0";
            }
            else
                currentToursCountValue = "0";
//            $(byXpath(WIFI_TOURS_COUNTER_LABEL)).scrollTo();
//            String fullHotelCounter = $(byXpath(WIFI_TOURS_COUNTER_LABEL)).waitUntil(Condition.visible, CONSTANT_60_SECONDS).getText();
//            currentToursCountValue = fullHotelCounter.substring(0, fullHotelCounter.indexOf(" ")).replaceAll("\\D", "");
            if ($(byXpath(ERROR_500)).isDisplayed())
                refreshPage();

//            if ($(byXpath(SHOW_ALL_TO_BUTTON)).isDisplayed()){
//                $(byXpath(TO_LIST_LABEL)).scrollTo();
//                $(byXpath(SHOW_ALL_TO_BUTTON)).waitUntil(Condition.visible, CONSTANT_60_SECONDS).click();
//            }

        }
        tourCountLT.setCellValue(currentToursCountValue);
        rowCounter++;

        //By TO
        for (int i=0; i<TOAll.length; i++)
            setTOList(TOAll[i]);

        int TOCounter = 1;
        while(TOCounter<getTOList().size()){
                if (rowCounter <= sheet.getLastRowNum())
                    row = sheet.getRow(rowCounter);
                else
                    row = sheet.createRow(rowCounter);
                resort = row.createCell(cellNum);
                resort.setCellValue(countryValue);
                operator = row.createCell(cellNum + 1);
                operator.setCellValue(getTOList().get(TOCounter));

                if ($(byXpath(SHOW_ALL_TO_BUTTON)).isDisplayed()){
//                    scrollToElement(SHOW_ALL_TO_BUTTON);
//                    scrollDownOnSomePxl(150);
                    $(byXpath(SHOW_ALL_TO_BUTTON)).click();
                    sleep(CONSTANT_1_SECOND);
//                    scrollToElement(HIDE_ALL_TO_BUTTON);
                }

                    int i=1;
                    while($(byXpath(String.format(FILTER_TO_CHECKBOX, i))).isDisplayed()){
                        if(($(byXpath(String.format(FILTER_TO_CHECKBOX_NAME, i))).waitUntil(Condition.visible, CONSTANT_60_SECONDS).getText()).contains(getTOList().get(TOCounter)) && !($(byXpath(EMPTY_TOURS_BLOCK)).isDisplayed())){
                            //check operator
                            $(byXpath(TO_LIST_LABEL)).scrollTo();
                            if ($(byXpath(SHOW_ALL_TO_BUTTON)).isDisplayed()){
//                                scrollToElement(SHOW_ALL_TO_BUTTON);
//                                scrollDownOnSomePxl(150);
                                $(byXpath(SHOW_ALL_TO_BUTTON)).click();
                                sleep(CONSTANT_1_SECOND);
//                                scrollToElement(HIDE_ALL_TO_BUTTON);
                            }
                            $(byXpath(String.format(FILTER_TO_CHECKBOX_NAME, i))).scrollTo();
                            $(byXpath(String.format(FILTER_TO_CHECKBOX_NAME, i))).click();
                            $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
                            sleep(CONSTANT_1_SECOND);
                            System.out.println(driver.getCurrentUrl());
                            //if operator still unchecked, check operator again
                            if(!driver.getCurrentUrl().contains("filter_operators=")){
                                $(byXpath(TO_LIST_LABEL)).scrollTo();
                                if ($(byXpath(SHOW_ALL_TO_BUTTON)).isDisplayed()){
//                                    scrollToElement(SHOW_ALL_TO_BUTTON);
//                                    scrollDownOnSomePxl(150);
                                    $(byXpath(SHOW_ALL_TO_BUTTON)).click();
                                    sleep(CONSTANT_1_SECOND);
//                                    scrollToElement(HIDE_ALL_TO_BUTTON);
                                }
                                $(byXpath(String.format(FILTER_TO_CHECKBOX_NAME, i))).scrollTo();
                                $(byXpath(String.format(FILTER_TO_CHECKBOX_NAME, i))).click();
                                $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
                                sleep(CONSTANT_1_SECOND);
                            }
                            /////////////////////////////////////////////////////////////////////////////////////
//                            $(byXpath(TO_LIST_LABEL)).scrollTo();
//                            if ($(byXpath(SHOW_ALL_TO_BUTTON)).isDisplayed())
//                                $(byXpath(SHOW_ALL_TO_BUTTON)).click();
//                            if(!$(byXpath(FILTER_TO_CHECKBOX_CHECKED)).isDisplayed()){
//                                $(byXpath(String.format(FILTER_TO_CHECKBOX_NAME, i))).click();
//                                $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//                                sleep(CONSTANT_1_SECOND);
//                            }
//                            else
//                                scrollUpOnPage();
                            /////////////////////////////////////////////////////////////////////////////////////

//                            $(byXpath(LOAD_TOURS_LABEL)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//                            $(byXpath(SORT_BY_PRICE_BUTTON)).waitUntil(Condition.visible, CONSTANT_300_SECONDS).click();
//                            $(byXpath(BANNER_APPLY_FILTER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
                            $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
                            sleep(CONSTANT_1_SECOND);
                            if(!($(byXpath(EMPTY_SEARCH)).isDisplayed() || $(byXpath(EMPTY_SEARCH_WITH_FILTERS)).isDisplayed() || $(byXpath(EMPTY_TOURS_BLOCK)).isDisplayed())){
//                                $(byXpath(SELECT_TOUR_LABEL)).waitUntil(Condition.visible, CONSTANT_300_SECONDS);
                                scrollUpOnPage();
                                if($(byXpath(TOURS_SHOWN_COUNTER_LABEL)).isDisplayed()){
                                    currentToursCountValue = $(byXpath(TOURS_SHOWN_COUNTER_LABEL)).getText().replaceAll("[^0-9]", "");
                                    if(currentToursCountValue.equals(""))
                                        currentToursCountValue = "0";
                                }
                                else
                                    currentToursCountValue = "0";
//                                $(byXpath(WIFI_TOURS_COUNTER_LABEL)).scrollTo();
//                                String fullHotelCounter = $(byXpath(WIFI_TOURS_COUNTER_LABEL)).waitUntil(Condition.visible, CONSTANT_60_SECONDS).getText();
//                                currentToursCountValue = fullHotelCounter.substring(0, fullHotelCounter.indexOf(" ")).replaceAll("\\D", "");

                                //uncheck operator
                                String currentUrl = driver.getCurrentUrl();
                                goToPage(currentUrl.substring(0, currentUrl.indexOf("?")));
                                $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
                                $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
                                sleep(CONSTANT_1_SECOND);
                                System.out.println(driver.getCurrentUrl());
                                /////////////////////////////////////////////////////////////////////////////
//                                $(byXpath(TO_LIST_LABEL)).scrollTo();
//                                if ($(byXpath(SHOW_ALL_TO_BUTTON)).isDisplayed())
//                                    $(byXpath(SHOW_ALL_TO_BUTTON)).click();
//                                $(byXpath(String.format(FILTER_TO_CHECKBOX_NAME, i))).waitUntil(Condition.visible, CONSTANT_20_SECONDS).click();
//                                $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//                                sleep(CONSTANT_1_SECOND);
//                                System.out.println(driver.getCurrentUrl());
//                                //if operator still checked, uncheck operator again
//                                if(driver.getCurrentUrl().contains("filter_operators=")){
//                                    $(byXpath(TO_LIST_LABEL)).scrollTo();
//                                    if ($(byXpath(SHOW_ALL_TO_BUTTON)).isDisplayed())
//                                        $(byXpath(SHOW_ALL_TO_BUTTON)).click();
//                                    $(byXpath(String.format(FILTER_TO_CHECKBOX_NAME, i))).click();
//                                    $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//                                    sleep(CONSTANT_1_SECOND);
//                                }
                                ///////////////////////////////////////////////////////////////////////////
//                                $(byXpath(TO_LIST_LABEL)).scrollTo();
//                                if ($(byXpath(SHOW_ALL_TO_BUTTON)).isDisplayed())
//                                    $(byXpath(SHOW_ALL_TO_BUTTON)).click();
//                                if($(byXpath(FILTER_TO_CHECKBOX_CHECKED)).isDisplayed()){
//                                    $(byXpath(String.format(FILTER_TO_CHECKBOX_NAME, i))).click();
//                                    $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//                                    sleep(CONSTANT_1_SECOND);
//                                }
//                                else
//                                    scrollUpOnPage();
                                ////////////////////////////////////////////////////////////////////////////

//                                $(byXpath(LOAD_TOURS_LABEL)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//                                $(byXpath(SORT_BY_PRICE_BUTTON)).waitUntil(Condition.visible, CONSTANT_300_SECONDS).click();
//                                $(byXpath(BANNER_APPLY_FILTER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
                                $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
                                sleep(CONSTANT_1_SECOND);
                            }
                            else{
                                currentToursCountValue = "0";
                                goToPage(searchUrlByCountry);
                                $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//                                $(byXpath(LOAD_TOURS_LABEL)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//                                $(byXpath(SORT_BY_PRICE_BUTTON)).waitUntil(Condition.visible, CONSTANT_300_SECONDS).click();
//                                $(byXpath(BANNER_APPLY_FILTER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
                                $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
                                sleep(CONSTANT_1_SECOND);
//                                if(countryValue.equals("Россия")){
//                                    $(byXpath(SWITCH)).waitUntil(Condition.visible,CONSTANT_20_SECONDS).click();
//                                    $(byXpath(BANNER_APPLY_FILTER)).waitUntil(Condition.disappear, CONSTANT_30_SECONDS);
//                                }
                                if ($(byXpath(SHOW_ALL_TO_BUTTON)).isDisplayed()){
//                                    scrollToElement(SHOW_ALL_TO_BUTTON);
//                                    scrollDownOnSomePxl(150);
                                    $(byXpath(SHOW_ALL_TO_BUTTON)).click();
                                    sleep(CONSTANT_1_SECOND);
//                                    scrollToElement(HIDE_ALL_TO_BUTTON);
                                }
                            }
                            break;
                        }
                        else
                            currentToursCountValue = "0";
                        i++;
                        if ($(byXpath(SHOW_ALL_TO_BUTTON)).isDisplayed()){
//                            scrollToElement(SHOW_ALL_TO_BUTTON);
//                            scrollDownOnSomePxl(150);
                            $(byXpath(SHOW_ALL_TO_BUTTON)).click();
                            sleep(CONSTANT_1_SECOND);
//                            scrollToElement(HIDE_ALL_TO_BUTTON);
                        }
                    }

                tourCountLT = row.createCell(cellNum + 2);
                tourCountLT.setCellValue(currentToursCountValue);
                TOCounter++;

                rowCounter++;

        }


        book.write(new FileOutputStream(reportPathValue));

        //Resorts
        for (int resortCounter=0; resortCounter < destinationResortLT.get(countryValue).length; resortCounter ++){
            String searchUrl = HTTPS + LEVEL_TRAVEL_URL + "search/" + departureLT.get(departureValue) + "-" + destinationCountryLT.get("Россия") +
                    "-to-" + destinationResortLT.get(countryValue)[resortCounter] + "-" + destinationCountryLT.get(countryValue) +
                    "-departure-";

            searchUrl += getSearchDate(dateCounter) + "-for-";

            if (isFlexibleNights)
                searchUrl += (nightsValue - 2) + ".." + (nightsValue + 2) + "-nights-";
            else
                searchUrl += (nightsValue) + "-nights-";

            searchUrl += adultsValue + "-adults-";

            if(!kidsAgesList.get(0).equals("0")){
                searchUrl += kidsAgesList.size() + "(";
                for (int i=0; i< kidsAgesList.size(); i++){
                    searchUrl += kidsAgesList.get(i);
                    if (i+1<kidsAgesList.size())
                        searchUrl += ",";
                }
                searchUrl += ")";
            }
            else
                searchUrl += "0";

            searchUrl +="-kids-1..5-stars?";

//            if (isFlexibleDate) {
//                if (departureValue.equals("Москва"))
//                    searchUrlByCountry += "&flex_dates=3";
//                else
//                    searchUrlByCountry += "&flex_dates=5";
//            }

            if (isFlexibleDate){
                if(searchUrl.endsWith("?"))
                    searchUrl += "flex_dates=6";
                else
                    searchUrl += "&flex_dates=6";
            }

            if(starsValues.size() > 0)
            {
                if(searchUrl.endsWith("?"))
                    searchUrl += "filter_stars=";
                else
                    searchUrl += "&filter_stars=";
                for(int i=0; i<starsValues.size(); i++)
                    searchUrl +=starsValues.get(i) + ",";
            }


            sleep(CONSTANT_2_SECONDS);
            goToPage(searchUrl);
            if (!($(byXpath(LEVEL_TRAVEL_HEADER)).isDisplayed()) || $(byXpath(ERROR_500)).isDisplayed())
                refreshPage();

            $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//            $(byXpath(LOAD_TOURS_LABEL)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//            $(byXpath(SORT_BY_PRICE_BUTTON)).waitUntil(Condition.visible, CONSTANT_300_SECONDS).click();
//            $(byXpath(BANNER_APPLY_FILTER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
            $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
            sleep(CONSTANT_1_SECOND);
            if($(byXpath(ERROR_500)).isDisplayed()){
                refreshPage();
                $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//                $(byXpath(LOAD_TOURS_LABEL)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//                $(byXpath(SORT_BY_PRICE_BUTTON)).waitUntil(Condition.visible, CONSTANT_300_SECONDS).click();
//                $(byXpath(BANNER_APPLY_FILTER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
                $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
                sleep(CONSTANT_1_SECOND);
            }

            //Instant confirmation switch for Russia
//            if(countryValue.equals("Россия")){
//                $(byXpath(SWITCH)).waitUntil(Condition.visible,CONSTANT_20_SECONDS).click();
//                $(byXpath(BANNER_APPLY_FILTER)).waitUntil(Condition.disappear, CONSTANT_30_SECONDS);
//            }

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);
            resort = row.createCell(cellNum);
            resort.setCellValue(resortLT.get(destinationResortLT.get(countryValue)[resortCounter]));
            operator = row.createCell(cellNum + 1);
            currentTOValue = "Всего";
            operator.setCellValue(currentTOValue);
            setTOList(currentTOValue);
            TOCounter ++;

            tourCountLT = row.createCell(cellNum + 2);
            if($(byXpath(EMPTY_SEARCH)).isDisplayed() || $(byXpath(EMPTY_SEARCH_WITH_FILTERS)).isDisplayed() || $(byXpath(EMPTY_TOURS_BLOCK)).isDisplayed())
                currentToursCountValue = "0";

            else{
//                $(byXpath(SELECT_TOUR_LABEL)).waitUntil(Condition.visible, CONSTANT_300_SECONDS);
//                $(byXpath(SORT_BY_PRICE_BUTTON)).waitUntil(Condition.visible, CONSTANT_300_SECONDS).click();
//                $(byXpath(BANNER_APPLY_FILTER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
                $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
                sleep(CONSTANT_1_SECOND);
                scrollUpOnPage();
                if($(byXpath(TOURS_SHOWN_COUNTER_LABEL)).isDisplayed()){
                    currentToursCountValue = $(byXpath(TOURS_SHOWN_COUNTER_LABEL)).getText().replaceAll("[^0-9]", "");
                    if(currentToursCountValue.equals(""))
                        currentToursCountValue = "0";
                }
                else
                    currentToursCountValue = "0";
//                $(byXpath(WIFI_TOURS_COUNTER_LABEL)).scrollTo();
//                String fullHotelCounter = $(byXpath(WIFI_TOURS_COUNTER_LABEL)).waitUntil(Condition.visible, CONSTANT_60_SECONDS).getText();
//                currentToursCountValue = fullHotelCounter.substring(0, fullHotelCounter.indexOf(" ")).replaceAll("\\D", "");
                if ($(byXpath(ERROR_500)).isDisplayed())
                    refreshPage();

//                if($(byXpath(SHOW_ALL_TO_BUTTON)).isDisplayed()){
//                    $(byXpath(TO_LIST_LABEL)).scrollTo();
//                    $(byXpath(SHOW_ALL_TO_BUTTON)).waitUntil(Condition.visible, CONSTANT_60_SECONDS).click();
//                }

            }

            //By TO
            tourCountLT.setCellValue(currentToursCountValue);
            rowCounter++;

            for (int i=0; i<TOAll.length; i++)
                setTOList(TOAll[i]);

            while(TOCounter<getTOList().size()){
                if(!getTOList().get(TOCounter).equals("Всего")){
                    if (rowCounter <= sheet.getLastRowNum())
                        row = sheet.getRow(rowCounter);
                    else
                        row = sheet.createRow(rowCounter);
                    resort = row.createCell(cellNum);
                    resort.setCellValue(resortLT.get(destinationResortLT.get(countryValue)[resortCounter]));
                    operator = row.createCell(cellNum + 1);
                    operator.setCellValue(getTOList().get(TOCounter));

                    if ($(byXpath(SHOW_ALL_TO_BUTTON)).isDisplayed()){
//                        scrollToElement(SHOW_ALL_TO_BUTTON);
//                        scrollDownOnSomePxl(150);
                        $(byXpath(SHOW_ALL_TO_BUTTON)).click();
                        sleep(CONSTANT_1_SECOND);
//                        scrollToElement(HIDE_ALL_TO_BUTTON);
                    }

                        int i=1;
                        while($(byXpath(String.format(FILTER_TO_CHECKBOX_NAME, i))).isDisplayed()){
                            if(($(byXpath(String.format(FILTER_TO_CHECKBOX_NAME, i))).waitUntil(Condition.visible, CONSTANT_60_SECONDS).getText()).contains(getTOList().get(TOCounter)) && !($(byXpath(EMPTY_TOURS_BLOCK)).isDisplayed())){
                                //check operator
                                $(byXpath(TO_LIST_LABEL)).scrollTo();
                                if ($(byXpath(SHOW_ALL_TO_BUTTON)).isDisplayed()){
//                                    scrollToElement(SHOW_ALL_TO_BUTTON);
//                                    scrollDownOnSomePxl(150);
                                    $(byXpath(SHOW_ALL_TO_BUTTON)).click();
                                    sleep(CONSTANT_1_SECOND);
//                                    scrollToElement(HIDE_ALL_TO_BUTTON);
                                }
                                $(byXpath(String.format(FILTER_TO_CHECKBOX_NAME, i))).scrollTo();
                                $(byXpath(String.format(FILTER_TO_CHECKBOX_NAME, i))).click();
                                $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_360_SECONDS);
                                sleep(CONSTANT_1_SECOND);
                                System.out.println(driver.getCurrentUrl());
                                //if operator still unchecked, check operator again
                                if(!driver.getCurrentUrl().contains("filter_operators=")){
                                    $(byXpath(TO_LIST_LABEL)).scrollTo();
                                    if ($(byXpath(SHOW_ALL_TO_BUTTON)).isDisplayed()){
//                                        scrollToElement(SHOW_ALL_TO_BUTTON);
//                                        scrollDownOnSomePxl(150);
                                        $(byXpath(SHOW_ALL_TO_BUTTON)).click();
                                        sleep(CONSTANT_1_SECOND);
//                                        scrollToElement(HIDE_ALL_TO_BUTTON);
                                    }
                                    $(byXpath(String.format(FILTER_TO_CHECKBOX_NAME, i))).scrollTo();
                                    $(byXpath(String.format(FILTER_TO_CHECKBOX_NAME, i))).click();
                                    $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
                                    sleep(CONSTANT_1_SECOND);
                                }
                                /////////////////////////////////////////////////////////////////////////////////////
//                                $(byXpath(TO_LIST_LABEL)).scrollTo();
//                                if ($(byXpath(SHOW_ALL_TO_BUTTON)).isDisplayed())
//                                    $(byXpath(SHOW_ALL_TO_BUTTON)).click();
//                                if(!$(byXpath(FILTER_TO_CHECKBOX_CHECKED)).isDisplayed()){
//                                    $(byXpath(String.format(FILTER_TO_CHECKBOX_NAME, i))).click();
//                                    $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_360_SECONDS);
//                                    sleep(CONSTANT_1_SECOND);
//                                }
//                                else
//                                    scrollUpOnPage();
//                                $(byXpath(LOAD_TOURS_LABEL)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//                                $(byXpath(SORT_BY_PRICE_BUTTON)).waitUntil(Condition.visible, CONSTANT_300_SECONDS).click();
//                                $(byXpath(BANNER_APPLY_FILTER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
                                $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
                                sleep(CONSTANT_1_SECOND);
                                if(!($(byXpath(EMPTY_SEARCH)).isDisplayed() || $(byXpath(EMPTY_SEARCH_WITH_FILTERS)).isDisplayed() || $(byXpath(EMPTY_TOURS_BLOCK)).isDisplayed())){
                                    scrollUpOnPage();
                                    if($(byXpath(TOURS_SHOWN_COUNTER_LABEL)).isDisplayed()){
                                        currentToursCountValue = $(byXpath(TOURS_SHOWN_COUNTER_LABEL)).getText().replaceAll("[^0-9]", "");
                                        if(currentToursCountValue.equals(""))
                                            currentToursCountValue = "0";
                                    }
                                    else
                                        currentToursCountValue = "0";
//                                    $(byXpath(WIFI_TOURS_COUNTER_LABEL)).scrollTo();
//                                    String fullHotelCounter = $(byXpath(WIFI_TOURS_COUNTER_LABEL)).waitUntil(Condition.visible, CONSTANT_60_SECONDS).getText();
//                                    currentToursCountValue = fullHotelCounter.substring(0, fullHotelCounter.indexOf(" ")).replaceAll("\\D", "");

                                    //uncheck operator
                                    String currentUrl = driver.getCurrentUrl();
                                    goToPage(currentUrl.substring(0, currentUrl.indexOf("?")));
                                    $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
                                    $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
                                    sleep(CONSTANT_1_SECOND);
                                    System.out.println(driver.getCurrentUrl());
                                    ///////////////////////////////////////////////////////////////////////////
//                                    $(byXpath(TO_LIST_LABEL)).scrollTo();
//                                    if ($(byXpath(SHOW_ALL_TO_BUTTON)).isDisplayed())
//                                        $(byXpath(SHOW_ALL_TO_BUTTON)).click();
//                                    $(byXpath(String.format(FILTER_TO_CHECKBOX_NAME, i))).click();
//                                    $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//                                    sleep(CONSTANT_1_SECOND);
//                                    System.out.println(driver.getCurrentUrl());
//                                    //if operator still checked, uncheck operator again
//                                    if(driver.getCurrentUrl().contains("filter_operators=")){
//                                        $(byXpath(TO_LIST_LABEL)).scrollTo();
//                                        if ($(byXpath(SHOW_ALL_TO_BUTTON)).isDisplayed())
//                                            $(byXpath(SHOW_ALL_TO_BUTTON)).click();
//                                        $(byXpath(String.format(FILTER_TO_CHECKBOX_NAME, i))).click();
//                                        $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//                                        sleep(CONSTANT_1_SECOND);
//                                    }
                                    //////////////////////////////////////////////////////////////////////////
//                                    $(byXpath(TO_LIST_LABEL)).scrollTo();
//                                    if ($(byXpath(SHOW_ALL_TO_BUTTON)).isDisplayed())
//                                        $(byXpath(SHOW_ALL_TO_BUTTON)).click();
//                                    if($(byXpath(FILTER_TO_CHECKBOX_CHECKED)).isDisplayed()){
//                                        $(byXpath(String.format(FILTER_TO_CHECKBOX_NAME, i))).click();
//                                        $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//                                        sleep(CONSTANT_1_SECOND);
//                                    }
//                                    else
//                                        scrollUpOnPage();
                                    ///////////////////////////////////////////////////////////////////////////

//                                    $(byXpath(LOAD_TOURS_LABEL)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//                                    $(byXpath(SORT_BY_PRICE_BUTTON)).waitUntil(Condition.visible, CONSTANT_300_SECONDS).click();
//                                    $(byXpath(BANNER_APPLY_FILTER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
                                    $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
                                    sleep(CONSTANT_1_SECOND);
                                }
                                else{
                                    currentToursCountValue = "0";
                                    goToPage(searchUrl);
                                    $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//                                    if(countryValue.equals("Россия")){
//                                        $(byXpath(SWITCH)).waitUntil(Condition.visible,CONSTANT_20_SECONDS).click();
//                                        $(byXpath(BANNER_APPLY_FILTER)).waitUntil(Condition.disappear, CONSTANT_30_SECONDS);
//                                    }
                                    if ($(byXpath(SHOW_ALL_TO_BUTTON)).isDisplayed()){
//                                        scrollToElement(SHOW_ALL_TO_BUTTON);
//                                        scrollDownOnSomePxl(150);
                                        $(byXpath(SHOW_ALL_TO_BUTTON)).click();
                                        sleep(CONSTANT_1_SECOND);
//                                        scrollToElement(HIDE_ALL_TO_BUTTON);
                                    }
                                }
//                                String currentTOInfoValue = $(byXpath(String.format(FILTER_TO_CHECKBOX_INFO, i))).waitUntil(Condition.visible, CONSTANT_60_SECONDS).getText();
//                                currentToursCountValue = currentTOInfoValue.substring(0, currentTOInfoValue.indexOf(" "));
//                                if (currentToursCountValue.equals("нет"))
//                                    currentToursCountValue = "0";
                                break;
                            }
                            else
                                currentToursCountValue = "0";

                            i++;
                            if ($(byXpath(SHOW_ALL_TO_BUTTON)).isDisplayed()){
//                                scrollToElement(SHOW_ALL_TO_BUTTON);
//                                scrollDownOnSomePxl(150);
                                $(byXpath(SHOW_ALL_TO_BUTTON)).click();
                                sleep(CONSTANT_1_SECOND);
//                                scrollToElement(HIDE_ALL_TO_BUTTON);
                            }
                        }

                    tourCountLT = row.createCell(cellNum + 2);
                    tourCountLT.setCellValue(currentToursCountValue);
                    TOCounter++;

                    rowCounter++;
                }
            }
          book.write(new FileOutputStream(reportPathValue));
        }

//        book.close();

        return this;
    }

    public LevelTravelSearchPage getStatisticByTOFromFilter (Workbook book, String countryValue, int dateCounter, boolean isFlexibleDate, int nightsValue, boolean isFlexibleNights,
                                                   String adultsValue, ArrayList<String> kidsAgesList, String departureValue, ArrayList<String> starsValues, String reportPathValue) throws Exception{

        Sheet sheet;
        if (book.getSheetIndex(departureValue + "-" + countryValue) == -1)
            sheet = book.createSheet(departureValue + "-" + countryValue);
        else
            sheet = book.getSheet(departureValue + "-" + countryValue);

        book.write(new FileOutputStream(reportPathValue));

        CellStyle style = book.createCellStyle();
        Font font = book.createFont();
        font.setBold(true);
        style.setFont(font);

        Row row = null;

        if(sheet.getLastRowNum() > 0){
            for (int i=0; i <= sheet.getLastRowNum(); i++){
                row = sheet.getRow(i);
                sheet.removeRow(row);
            }
        }

        book.write(new FileOutputStream(reportPathValue));

        int rowCounter = 0;
        int cellNum = 0;

        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0){
            row = sheet.getRow(rowCounter);
            cellNum = sheet.getRow(1).getLastCellNum();
        }
        else
            row = sheet.createRow(rowCounter);

        Cell statisticDate = row.createCell(cellNum);
        statisticDate.setCellValue(getCurrentDateValue() +  " " + getCurrentWeekNumberValue() + " " + getSearchDate(dateCounter));
        statisticDate.setCellStyle(style);

        rowCounter ++;

        if (rowCounter <= sheet.getLastRowNum()){
            row = sheet.getRow(rowCounter);
            cellNum = row.getLastCellNum();
        }
        else
            row = sheet.createRow(rowCounter);

        Cell resort = row.createCell(cellNum);
        resort.setCellValue("Страна/Курорт");
        resort.setCellStyle(style);

        Cell operator = row.createCell(cellNum + 1);
        operator.setCellValue("Operator");
        operator.setCellStyle(style);

        Cell tourCountLT = row.createCell(cellNum + 2);
        tourCountLT.setCellValue("LevelTravel");
        tourCountLT.setCellStyle(style);

        rowCounter++;

        String searchUrlByCountry = HTTPS + LEVEL_TRAVEL_URL + "search/" + departureLT.get(departureValue) + "-" + destinationCountryLT.get("Россия") +
                "-to-Any-" + destinationCountryLT.get(countryValue) + "-departure-";

        searchUrlByCountry += getSearchDate(dateCounter) + "-for-";

        if (isFlexibleNights)
            searchUrlByCountry += (nightsValue - 2) + ".." + (nightsValue + 2) + "-nights-";
        else
            searchUrlByCountry += (nightsValue) + "-nights-";

        searchUrlByCountry += adultsValue + "-adults-";

        if(!kidsAgesList.get(0).equals("0")){
            searchUrlByCountry += kidsAgesList.size() + "(";
            for (int i=0; i< kidsAgesList.size(); i++){
                searchUrlByCountry += kidsAgesList.get(i);
                if (i+1<kidsAgesList.size())
                    searchUrlByCountry += ",";
            }
            searchUrlByCountry += ")";
        }
        else
            searchUrlByCountry += "0";

        searchUrlByCountry +="-kids-1..5-stars?";

//        if (isFlexibleDate) {
//            if (departureValue.equals("Москва"))
//                searchUrlByCountry += "&flex_dates=3";
//            else
//                searchUrlByCountry += "&flex_dates=5";
//        }

        if(starsValues.size() > 0)
        {
            if(searchUrlByCountry.endsWith("?"))
                searchUrlByCountry += "filter_stars=";
            else
                searchUrlByCountry += "&filter_stars=";
            for(int i=0; i<starsValues.size(); i++)
                searchUrlByCountry +=starsValues.get(i) + ",";
        }

        if (isFlexibleDate){
            if(searchUrlByCountry.endsWith("?"))
                searchUrlByCountry += "flex_dates=6";
            else
                searchUrlByCountry += "&flex_dates=6";
        }

        //Country
        goToPage(searchUrlByCountry);
        System.out.println(searchUrlByCountry);
        if (!($(byXpath(LEVEL_TRAVEL_HEADER)).isDisplayed()) || $(byXpath(ERROR_500)).isDisplayed())
            refreshPage();

        $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
        $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
        sleep(CONSTANT_1_SECOND);
        if($(byXpath(ERROR_500)).isDisplayed()){
            refreshPage();
            $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
            $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
            sleep(CONSTANT_1_SECOND);
        }

        //Close bunner
        if($(byXpath(PROMOCODE_BUNNER_CONTAINER)).isDisplayed())
            $(byXpath(PROMOCODE_BUNNER_CLOSE_BUTTON)).waitUntil(Condition.visible, CONSTANT_5_SECONDS).click();

        if (rowCounter <= sheet.getLastRowNum()  && sheet.getLastRowNum() > 0)
            row = sheet.getRow(rowCounter);
        else
            row = sheet.createRow(rowCounter);
        resort = row.createCell(cellNum);
        resort.setCellValue(countryValue);
        operator = row.createCell(cellNum + 1);
        String currentTOValue = "Всего";
        operator.setCellValue(currentTOValue);
        setTOList(currentTOValue);

        tourCountLT = row.createCell(cellNum + 2);

        String currentToursCountValue = "0";
        if(!($(byXpath(EMPTY_SEARCH)).isDisplayed() || $(byXpath(EMPTY_SEARCH_WITH_FILTERS)).isDisplayed() || $(byXpath(EMPTY_TOURS_BLOCK)).isDisplayed())){
            $(byXpath(SELECT_TOUR_LABEL)).waitUntil(Condition.visible, CONSTANT_300_SECONDS);
            scrollUpOnPage();
            if($(byXpath(TOURS_SHOWN_COUNTER_LABEL)).isDisplayed()){
                currentToursCountValue = $(byXpath(TOURS_SHOWN_COUNTER_LABEL)).getText().replaceAll("[^0-9]", "");
                if(currentToursCountValue.equals(""))
                    currentToursCountValue = "0";
            }
            else
                currentToursCountValue = "0";
//            $(byXpath(WIFI_TOURS_COUNTER_LABEL)).scrollTo();
//            String fullHotelCounter = $(byXpath(WIFI_TOURS_COUNTER_LABEL)).waitUntil(Condition.visible, CONSTANT_60_SECONDS).getText();
//            currentToursCountValue = fullHotelCounter.substring(0, fullHotelCounter.indexOf(" ")).replaceAll("\\D", "");

            if ($(byXpath(ERROR_500)).isDisplayed())
                refreshPage();
        }
        else
            currentToursCountValue = "0";

        tourCountLT.setCellValue(currentToursCountValue);
        rowCounter++;

        //By TO
        for (int i=0; i<TOAll.length; i++)
            setTOList(TOAll[i]);

        int TOCounter = 1;
        while(TOCounter<getTOList().size()){
            if (rowCounter <= sheet.getLastRowNum())
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);
            resort = row.createCell(cellNum);
            resort.setCellValue(countryValue);
            operator = row.createCell(cellNum + 1);
            operator.setCellValue(getTOList().get(TOCounter));

            if ($(byXpath(SHOW_ALL_TO_BUTTON)).isDisplayed() && !($(byXpath(EMPTY_SEARCH)).isDisplayed() || $(byXpath(EMPTY_SEARCH_WITH_FILTERS)).isDisplayed() || $(byXpath(EMPTY_TOURS_BLOCK)).isDisplayed())){
//                scrollToElement(SHOW_ALL_TO_BUTTON);
//                scrollDownOnSomePxl(150);
                $(byXpath(SHOW_ALL_TO_BUTTON)).click();
                sleep(CONSTANT_1_SECOND);
//                scrollToElement(HIDE_ALL_TO_BUTTON);
            }

            int i=1;
            while($(byXpath(String.format(FILTER_TO_CHECKBOX, i))).isDisplayed()){
                if(($(byXpath(String.format(FILTER_TO_CHECKBOX_NAME, i))).waitUntil(Condition.visible, CONSTANT_60_SECONDS).getText()).contains(getTOList().get(TOCounter)) && !($(byXpath(EMPTY_TOURS_BLOCK)).isDisplayed())){
                    $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
                    sleep(CONSTANT_1_SECOND);
                    if(!($(byXpath(EMPTY_SEARCH)).isDisplayed() || $(byXpath(EMPTY_SEARCH_WITH_FILTERS)).isDisplayed() || $(byXpath(EMPTY_TOURS_BLOCK)).isDisplayed())){
                        String fullHotelCounter = $(byXpath(String.format(FILTER_TO_CHECKBOX_INFO, i))).waitUntil(Condition.visible, CONSTANT_60_SECONDS).getText();
                        currentToursCountValue = fullHotelCounter.substring(0, fullHotelCounter.indexOf(" ")).replaceAll("\\D", "");
                        if(currentToursCountValue.equals(""))
                            currentToursCountValue = "0";
                    }
                    else{
                        currentToursCountValue = "0";
                        goToPage(searchUrlByCountry);
                        System.out.println(searchUrlByCountry);
                        $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
                        $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
                        sleep(CONSTANT_1_SECOND);
                    }
                    break;
                }
                else
                    currentToursCountValue = "0";
                i++;
            }

            tourCountLT = row.createCell(cellNum + 2);
            tourCountLT.setCellValue(currentToursCountValue);
            TOCounter++;

            rowCounter++;

        }

        book.write(new FileOutputStream(reportPathValue));

        //Resorts
        for (int resortCounter=0; resortCounter < destinationResortLT.get(countryValue).length; resortCounter ++){
            String searchUrl = HTTPS + LEVEL_TRAVEL_URL + "search/" + departureLT.get(departureValue) + "-" + destinationCountryLT.get("Россия") +
                    "-to-" + destinationResortLT.get(countryValue)[resortCounter] + "-" + destinationCountryLT.get(countryValue) +
                    "-departure-";

            searchUrl += getSearchDate(dateCounter) + "-for-";

            if (isFlexibleNights)
                searchUrl += (nightsValue - 2) + ".." + (nightsValue + 2) + "-nights-";
            else
                searchUrl += (nightsValue) + "-nights-";

            searchUrl += adultsValue + "-adults-";

            if(!kidsAgesList.get(0).equals("0")){
                searchUrl += kidsAgesList.size() + "(";
                for (int i=0; i< kidsAgesList.size(); i++){
                    searchUrl += kidsAgesList.get(i);
                    if (i+1<kidsAgesList.size())
                        searchUrl += ",";
                }
                searchUrl += ")";
            }
            else
                searchUrl += "0";

            searchUrl +="-kids-1..5-stars?";

//            if (isFlexibleDate) {
//                if (departureValue.equals("Москва"))
//                    searchUrlByCountry += "&flex_dates=3";
//                else
//                    searchUrlByCountry += "&flex_dates=5";
//            }

            if(starsValues.size() > 0)
            {
                if(searchUrl.endsWith("?"))
                    searchUrl += "filter_stars=";
                else
                    searchUrl += "&filter_stars=";
                for(int i=0; i<starsValues.size(); i++)
                    searchUrl +=starsValues.get(i) + ",";
            }

            if (isFlexibleDate){
                if(searchUrl.endsWith("?"))
                    searchUrl += "flex_dates=6";
                else
                    searchUrl += "&flex_dates=6";
            }

            sleep(CONSTANT_2_SECONDS);
            goToPage(searchUrl);
            System.out.println(searchUrl);
            if (!($(byXpath(LEVEL_TRAVEL_HEADER)).isDisplayed()) || $(byXpath(ERROR_500)).isDisplayed())
                refreshPage();

            $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
            $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
            sleep(CONSTANT_1_SECOND);
            if($(byXpath(ERROR_500)).isDisplayed()){
                refreshPage();
                $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
                $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
                sleep(CONSTANT_1_SECOND);
            }

            if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0)
                row = sheet.getRow(rowCounter);
            else
                row = sheet.createRow(rowCounter);
            resort = row.createCell(cellNum);
            resort.setCellValue(resortLT.get(destinationResortLT.get(countryValue)[resortCounter]));
            operator = row.createCell(cellNum + 1);
            currentTOValue = "Всего";
            operator.setCellValue(currentTOValue);
            setTOList(currentTOValue);
            TOCounter ++;

            tourCountLT = row.createCell(cellNum + 2);
            if($(byXpath(EMPTY_SEARCH)).isDisplayed() || $(byXpath(EMPTY_SEARCH_WITH_FILTERS)).isDisplayed() || $(byXpath(EMPTY_TOURS_BLOCK)).isDisplayed())
                currentToursCountValue = "0";

            else{
                $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
                sleep(CONSTANT_1_SECOND);
                scrollUpOnPage();
                if($(byXpath(TOURS_SHOWN_COUNTER_LABEL)).isDisplayed()){
                    currentToursCountValue = $(byXpath(TOURS_SHOWN_COUNTER_LABEL)).getText().replaceAll("[^0-9]", "");
                    if(currentToursCountValue.equals(""))
                        currentToursCountValue = "0";
                }
                else
                    currentToursCountValue = "0";
//                $(byXpath(WIFI_TOURS_COUNTER_LABEL)).scrollTo();
//                String fullHotelCounter = $(byXpath(WIFI_TOURS_COUNTER_LABEL)).waitUntil(Condition.visible, CONSTANT_60_SECONDS).getText();
//                currentToursCountValue = fullHotelCounter.substring(0, fullHotelCounter.indexOf(" ")).replaceAll("\\D", "");
                if ($(byXpath(ERROR_500)).isDisplayed())
                    refreshPage();
            }

            //By TO
            tourCountLT.setCellValue(currentToursCountValue);
            rowCounter++;

            for (int i=0; i<TOAll.length; i++)
                setTOList(TOAll[i]);

            while(TOCounter<getTOList().size()){
                if(!getTOList().get(TOCounter).equals("Всего")){
                    if (rowCounter <= sheet.getLastRowNum())
                        row = sheet.getRow(rowCounter);
                    else
                        row = sheet.createRow(rowCounter);
                    resort = row.createCell(cellNum);
                    resort.setCellValue(resortLT.get(destinationResortLT.get(countryValue)[resortCounter]));
                    operator = row.createCell(cellNum + 1);
                    operator.setCellValue(getTOList().get(TOCounter));

                    if ($(byXpath(SHOW_ALL_TO_BUTTON)).isDisplayed() && !($(byXpath(EMPTY_SEARCH)).isDisplayed() || $(byXpath(EMPTY_SEARCH_WITH_FILTERS)).isDisplayed() || $(byXpath(EMPTY_TOURS_BLOCK)).isDisplayed())){
//                        scrollToElement(SHOW_ALL_TO_BUTTON);
//                        scrollDownOnSomePxl(150);
                        $(byXpath(SHOW_ALL_TO_BUTTON)).click();
                        sleep(CONSTANT_1_SECOND);
//                        scrollToElement(HIDE_ALL_TO_BUTTON);
                    }

                    int i=1;
                    while($(byXpath(String.format(FILTER_TO_CHECKBOX_NAME, i))).isDisplayed()){
                        if(($(byXpath(String.format(FILTER_TO_CHECKBOX_NAME, i))).waitUntil(Condition.visible, CONSTANT_60_SECONDS).getText()).contains(getTOList().get(TOCounter)) && !($(byXpath(EMPTY_TOURS_BLOCK)).isDisplayed())){
                            $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
                            sleep(CONSTANT_1_SECOND);
                            if(!($(byXpath(EMPTY_SEARCH)).isDisplayed() || $(byXpath(EMPTY_SEARCH_WITH_FILTERS)).isDisplayed() || $(byXpath(EMPTY_TOURS_BLOCK)).isDisplayed())){
                                String fullHotelCounter = $(byXpath(String.format(FILTER_TO_CHECKBOX_INFO, i))).waitUntil(Condition.visible, CONSTANT_60_SECONDS).getText();
                                currentToursCountValue = fullHotelCounter.substring(0, fullHotelCounter.indexOf(" ")).replaceAll("\\D", "");
                                if(currentToursCountValue.equals(""))
                                    currentToursCountValue = "0";
                            }
                            else{
                                currentToursCountValue = "0";
                                goToPage(searchUrl);
                                System.out.println(searchUrl);
                                $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
                            }
                            break;
                        }
                        else
                            currentToursCountValue = "0";

                        i++;
                    }

                    tourCountLT = row.createCell(cellNum + 2);
                    tourCountLT.setCellValue(currentToursCountValue);
                    TOCounter++;

                    rowCounter++;
                }
            }
            book.write(new FileOutputStream(reportPathValue));
        }

        return this;
    }

    public LevelTravelSearchPage gotoLTSearchListByCriteria(String countryValue, int dateCounter, boolean isFlexibleDate, int nightsValue, boolean isFlexibleNights,
                                                            String adultsValue, ArrayList<String> kidsAgesList, String departureValue, ArrayList<String> starsValues, String toValue) throws Exception{

        String searchUrlByCountry = HTTPS + LEVEL_TRAVEL_URL + "search/" + departureLT.get(departureValue) + "-" + destinationCountryLT.get("Россия") +
                "-to-Any-" + destinationCountryLT.get(countryValue) + "-departure-";

        searchUrlByCountry += getSearchDate(dateCounter) + "-for-";

        if (isFlexibleNights)
            searchUrlByCountry += (nightsValue - 2) + ".." + (nightsValue + 2) + "-nights-";
        else
            searchUrlByCountry += (nightsValue) + "-nights-";

        searchUrlByCountry += adultsValue + "-adults-";

        if(!kidsAgesList.get(0).equals("0")){
            searchUrlByCountry += kidsAgesList.size() + "(";
            for (int i=0; i< kidsAgesList.size(); i++){
                searchUrlByCountry += kidsAgesList.get(i);
                if (i+1<kidsAgesList.size())
                    searchUrlByCountry += ",";
            }
            searchUrlByCountry += ")";
        }
        else
            searchUrlByCountry += "0";

        searchUrlByCountry +="-kids-1..5-stars?filter_operators=";

        searchUrlByCountry += toValue;

//        if (isFlexibleDate) {
//            if (departureValue.equals("Москва"))
//                searchUrlByCountry += "&flex_dates=3";
//            else
//                searchUrlByCountry += "&flex_dates=5";
//        }

        if(starsValues.size() > 0)
        {
            if(searchUrlByCountry.endsWith("?"))
                searchUrlByCountry += "filter_stars=";
            else
                searchUrlByCountry += "&filter_stars=";
            for(int i=0; i<starsValues.size(); i++)
                searchUrlByCountry +=starsValues.get(i) + ",";
        }

        if (isFlexibleDate){
            if(searchUrlByCountry.endsWith("?"))
                searchUrlByCountry += "flex_dates=6";
            else
                searchUrlByCountry += "&flex_dates=6";
        }

        goToPage(searchUrlByCountry);
        if (!($(byXpath(LEVEL_TRAVEL_HEADER)).isDisplayed()) || $(byXpath(ERROR_500)).isDisplayed())
            refreshPage();

        $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//        $(byXpath(LOAD_TOURS_LABEL)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//        $(byXpath(SORT_BY_PRICE_BUTTON)).waitUntil(Condition.visible, CONSTANT_300_SECONDS).click();
//        $(byXpath(BANNER_APPLY_FILTER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
        $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
        sleep(CONSTANT_1_SECOND);
//        $(byXpath(SELECT_TOUR_LABEL)).waitUntil(Condition.visible, CONSTANT_120_SECONDS);
        if(!($(byXpath(SELECT_TOUR_LABEL)).isDisplayed()) || $(byXpath(ERROR_500)).isDisplayed()){
            refreshPage();
            $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//            $(byXpath(LOAD_TOURS_LABEL)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//            $(byXpath(SORT_BY_PRICE_BUTTON)).waitUntil(Condition.visible, CONSTANT_300_SECONDS).click();
//            $(byXpath(BANNER_APPLY_FILTER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
            $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
            sleep(CONSTANT_1_SECOND);
//            $(byXpath(SELECT_TOUR_LABEL)).waitUntil(Condition.visible, CONSTANT_120_SECONDS);
        }

        return this;
    }



    public LevelTravelSearchPage clickSortByPriceLink() throws Exception{
        $(byXpath(LOAD_TOURS_LABEL)).waitUntil(Condition.disappear, CONSTANT_240_SECONDS);
//        $(byXpath(SORT_BY_PRICE_BUTTON)).waitUntil(Condition.visible, CONSTANT_300_SECONDS).click();
//        $(byXpath(BANNER_APPLY_FILTER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
        $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
        sleep(CONSTANT_1_SECOND);
//        $(byXpath(SELECT_TOUR_LABEL)).waitUntil(Condition.visible, CONSTANT_120_SECONDS);
        if (!$(byXpath(SORT_BY_PRICE_BUTTON_ACTIVE)).isDisplayed()){
            $(byXpath(SORT_BY_PRICE_BUTTON)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();
            $(byXpath(BANNER_APPLY_FILTER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
        }
        sleep(CONSTANT_1_SECOND);
//        if(!driver.getCurrentUrl().contains("&sort_by=price,asc")){
//            $(byXpath(SORT_BY_PRICE_BUTTON)).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();
//            $(byXpath(BANNER_APPLY_FILTER)).waitUntil(Condition.disappear, CONSTANT_120_SECONDS);
//        }

        return this;
    }

    public LevelTravelSearchPage clickBayHotelFilter() throws Exception{
        $(byXpath(FILTER_BAY_HOTEL)).waitUntil(Condition.visible, CONSTANT_360_SECONDS).click();
        $(byXpath(BANNER_APPLY_FILTER)).waitUntil(Condition.disappear, CONSTANT_120_SECONDS);
        sleep(CONSTANT_1_SECOND);
        scrollUpOnPage();

        return this;
    }

    //Old method
//    public LevelTravelSearchPage getHotelNameTourPrice(String countryValue, String departureValue, String reportPathValue) throws Exception{
//
//        FileInputStream fsIP= new FileInputStream(new File(reportPathValue));
//        Workbook book = new HSSFWorkbook(fsIP);
//        Sheet sheet = null;
//        if (book.getSheetIndex(departureValue + "-" + countryValue) == -1)
//            sheet = book.createSheet(departureValue + "-" + countryValue);
//        else
//            sheet = book.getSheet(departureValue + "-" + countryValue);
//        CellStyle style = book.createCellStyle();
//        Font font = book.createFont();
//        font.setBold(true);
//        style.setFont(font);
//        Row row = null;
//
//        int rowCounter = 0;
//        int cellNum = 0;
//
//        if (rowCounter <= sheet.getLastRowNum() && sheet.getLastRowNum() > 0){
//            row = sheet.getRow(rowCounter);
//            cellNum = sheet.getRow(1).getLastCellNum();
//        }
//        else
//            row = sheet.createRow(rowCounter);
//
//        Cell statisticDate = row.createCell(cellNum);
//        statisticDate.setCellValue(getCurrentDateValue() + " " + getCurrentWeekNumberValue());
//        statisticDate.setCellStyle(style);
//
//        rowCounter ++;
//
//        if (rowCounter <= sheet.getLastRowNum()){
//            row = sheet.getRow(rowCounter);
//            cellNum = row.getLastCellNum();
//        }
//        else
//            row = sheet.createRow(rowCounter);
//
//        Cell hotel = row.createCell(cellNum);
//        hotel.setCellValue("Отель");
//        hotel.setCellStyle(style);
//
//        Cell showcase = row.createCell(cellNum + 1);
//        showcase.setCellValue("Витрина");
//        showcase.setCellStyle(style);
//
//        Cell priceWithOilTax = row.createCell(cellNum + 2);
//        priceWithOilTax.setCellValue("Цена с топливом");
//        priceWithOilTax.setCellStyle(style);
//
//        Cell oilTax = row.createCell(cellNum + 3);
//        oilTax.setCellValue("Топливо");
//        oilTax.setCellStyle(style);
//
//        Cell priceWithoutOilTax = row.createCell(cellNum + 4);
//        priceWithoutOilTax.setCellValue("Цена без топлива");
//        priceWithoutOilTax.setCellStyle(style);
//
//        rowCounter++;
//
//        int i=0;
//        while (i<20){
//            int hotelBlockCounter = 1;
//            while ($(byXpath(String.format(HOTEL_BLOCK, hotelBlockCounter))).isDisplayed()){
//                boolean isUniqueHotel = true;
//                if (i>0){
//                    for (int hotelCounter = 0; hotelCounter < i; hotelCounter ++){
//                        if (getHotelNamesSearchLTList().get(hotelCounter).equals($(byXpath(String.format(HOTEL_NAME_LINK_PX, hotelBlockCounter))).getText()))
//                            isUniqueHotel = false;
//                    }
//                }
//                if (isUniqueHotel){
//                    setHotelNamesSearchLTList($(byXpath(String.format(HOTEL_NAME_LINK_PX, hotelBlockCounter))).getText());
//                    setHotelTourPricesSearchLTList($(byXpath(String.format(HOTEL_TOUR_PRICE_PX, hotelBlockCounter))).getText().replaceAll("\\D", ""));
//
//                    if (rowCounter <= sheet.getLastRowNum()  && sheet.getLastRowNum() > 0)
//                        row = sheet.getRow(rowCounter);
//                    else
//                        row = sheet.createRow(rowCounter);
//
//                    hotel = row.createCell(cellNum);
//                    hotel.setCellValue(getHotelNamesSearchLTList().get(i));
//
//                    showcase = row.createCell(cellNum + 1);
//                    showcase.setCellValue("LevelTravel");
//
//                    priceWithOilTax = row.createCell(cellNum + 2);
//                    priceWithOilTax.setCellValue(getHotelTourPricesSearchLTList().get(i));
//
//                    oilTax = row.createCell(cellNum + 3);
//                    oilTax.setCellValue("0");
//
//                    priceWithoutOilTax = row.createCell(cellNum + 4);
//                    priceWithoutOilTax.setCellValue(getHotelTourPricesSearchLTList().get(i));
//
//                    rowCounter ++;
//                    i++;
//                }
//                hotelBlockCounter ++;
//            }
//
//            WebElement currentHotelBlock = driver.findElement(By.xpath(String.format(HOTEL_STARS, hotelBlockCounter - 1)));
//            Actions actions = new Actions(driver);
//            actions.moveToElement(currentHotelBlock);
//            actions.perform();
//        }
//
//        book.write(new FileOutputStream(reportPathValue));
//        book.close();
//
//        return this;
//    }

    public LevelTravelSearchPage addCurrentHotelNameTourPricePx(int hotelBlockPixelCounter) throws Exception{
        int hotelStarsCounter = 1;
        while($(byXpath(String.format(HOTEL_STARS_PX, hotelBlockPixelCounter, hotelStarsCounter))).isDisplayed())
            hotelStarsCounter ++;
        setHotelNamesSearchLTList($(byXpath(String.format(HOTEL_NAME_LINK_PX, hotelBlockPixelCounter))).getText() + " " + (hotelStarsCounter-1) + "*");
        String resortValue = $(byXpath(String.format(HOTEL_RESORT_NAME_LINK_PX, hotelBlockPixelCounter))).getText();
        setRegionLTList(resortValue.substring(0, resortValue.lastIndexOf(",")));
        setHotelTourPricesSearchLTList($(byXpath(String.format(HOTEL_TOUR_PRICE_PX, hotelBlockPixelCounter))).getText().replaceAll("\\D", ""));

        return this;
    }

    public LevelTravelSearchPage addCurrentHotelNameTourPrice(int hotelBlockCounter) throws Exception{
        setHotelNamesSearchLTList($(byXpath(String.format(HOTEL_NAME_LINK, hotelBlockCounter))).getText());
        setHotelTourPricesSearchLTList($(byXpath(String.format(HOTEL_TOUR_PRICE, hotelBlockCounter))).getText().replaceAll("\\D", ""));

        return this;
    }

    public LevelTravelSearchPage clearLastHotelNameTourPrice() throws Exception{
        if (getHotelNamesSearchLTList().size()>0)
            getHotelNamesSearchLTList().remove(getHotelNamesSearchLTList().size()-1);
        if (getHotelTourPricesSearchLTList().size()>0)
            getHotelTourPricesSearchLTList().remove(getHotelTourPricesSearchLTList().size()-1);

        return this;
    }

    public LevelTravelSearchPage gotoSearchPageWithCriteria (String countryValue, int dateCounter, boolean isFlexibleDate, int nightsValue, boolean isFlexibleNights,
                                                   String adultsValue, ArrayList<String> kidsAgesList, String departureValue, ArrayList<String> starsValues) throws Exception{

        String searchUrlByCountry = HTTPS + LEVEL_TRAVEL_URL + "search/" + departureLT.get(departureValue) + "-" + destinationCountryLT.get("Россия") +
                "-to-Any-" + destinationCountryLT.get(countryValue) + "-departure-"; //Dombay, Sheregesh, Arkhyz, Elbrusskiy.District, Krasnaya.Polyana

        searchUrlByCountry += getSearchDate(dateCounter) + "-for-";

        if (isFlexibleNights)
            searchUrlByCountry += (nightsValue - 2) + ".." + (nightsValue + 2) + "-nights-";
        else
            searchUrlByCountry += (nightsValue) + "-nights-";

        searchUrlByCountry += adultsValue + "-adults-";

        if(!kidsAgesList.get(0).equals("0")){
            searchUrlByCountry += kidsAgesList.size() + "(";
            for (int i=0; i< kidsAgesList.size(); i++){
                searchUrlByCountry += kidsAgesList.get(i);
                if (i+1<kidsAgesList.size())
                    searchUrlByCountry += ",";
            }
            searchUrlByCountry += ")";
        }
        else
            searchUrlByCountry += "0";

        searchUrlByCountry +="-kids-1..5-stars?";

//        if (isFlexibleDate) {
//            if (departureValue.equals("Москва"))
//                searchUrlByCountry += "&flex_dates=3";
//            else
//                searchUrlByCountry += "&flex_dates=5";
//        }

        if(starsValues.size() > 0)
        {
            if(searchUrlByCountry.endsWith("?"))
                searchUrlByCountry += "filter_stars=";
            else
                searchUrlByCountry += "&filter_stars=";
            for(int i=0; i<starsValues.size(); i++)
                searchUrlByCountry +=starsValues.get(i) + ",";
        }

        if (isFlexibleDate){
            if(searchUrlByCountry.endsWith("?"))
                searchUrlByCountry += "flex_dates=6";
            else
                searchUrlByCountry += "&flex_dates=6";
        }

        if ($(byXpath(LOTTERY_POPUP)).isDisplayed())
            $(byXpath(LOTTERY_POPUP_CLOSE_BUTTON)).waitUntil(Condition.visible, CONSTANT_20_SECONDS).click();

        //Country
        goToPage(searchUrlByCountry);
        if (!($(byXpath(LEVEL_TRAVEL_HEADER)).isDisplayed()) || $(byXpath(ERROR_500)).isDisplayed())
            refreshPage();

        if(!($(byXpath(EMPTY_SEARCH)).isDisplayed() || $(byXpath(EMPTY_SEARCH_WITH_FILTERS)).isDisplayed() || $(byXpath(EMPTY_SEARCH_TITLE)).isDisplayed() || $(byXpath(EMPTY_TOURS_BLOCK)).isDisplayed())){
            $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//            $(byXpath(LOAD_TOURS_LABEL)).waitUntil(Condition.disappear, CONSTANT_420_SECONDS);
//            $(byXpath(SORT_BY_PRICE_BUTTON)).waitUntil(Condition.visible, CONSTANT_300_SECONDS).click();
//            $(byXpath(BANNER_APPLY_FILTER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
            $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
            sleep(CONSTANT_1_SECOND);
//            $(byXpath(SELECT_TOUR_LABEL)).waitUntil(Condition.visible, CONSTANT_240_SECONDS);
            if(!($(byXpath(SELECT_TOUR_LABEL)).isDisplayed()) || $(byXpath(ERROR_500)).isDisplayed()){
                refreshPage();
                $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//                $(byXpath(LOAD_TOURS_LABEL)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//                $(byXpath(SORT_BY_PRICE_BUTTON)).waitUntil(Condition.visible, CONSTANT_300_SECONDS).click();
//                $(byXpath(BANNER_APPLY_FILTER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
                $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
                sleep(CONSTANT_1_SECOND);
//                $(byXpath(SELECT_TOUR_LABEL)).waitUntil(Condition.visible, CONSTANT_240_SECONDS);
            }

            //Instant confirmation switch for Russia
//        if(countryValue.equals("Россия")){
//            $(byXpath(SWITCH)).waitUntil(Condition.visible,CONSTANT_20_SECONDS).click();
//            $(byXpath(BANNER_APPLY_FILTER)).waitUntil(Condition.disappear, CONSTANT_30_SECONDS);
//        }

            //Close bunner
            if($(byXpath(PROMOCODE_BUNNER_CONTAINER)).isDisplayed())
                $(byXpath(PROMOCODE_BUNNER_CLOSE_BUTTON)).waitUntil(Condition.visible, CONSTANT_5_SECONDS).click();
        }

        return this;
    }

    public LevelTravelSearchPage gotoSearchPageWithTOCriteria (String countryValue, int dateCounter, boolean isFlexibleDate, int nightsValue, boolean isFlexibleNights,
                                                             String adultsValue, ArrayList<String> kidsAgesList, String departureValue, ArrayList<String> starsValues, ArrayList<String> mealValues, String operatorValue) throws Exception{

        String searchUrlByCountry = HTTPS + LEVEL_TRAVEL_URL + "search/" + departureLT.get(departureValue) + "-" + destinationCountryLT.get("Россия") +
                "-to-Any-" + destinationCountryLT.get(countryValue) + "-departure-"; //Dombay, Sheregesh, Arkhyz, Elbrusskiy.District, Krasnaya.Polyana

        searchUrlByCountry += getSearchDate(dateCounter) + "-for-";

        if (isFlexibleNights)
            searchUrlByCountry += (nightsValue - 2) + ".." + (nightsValue + 2) + "-nights-";
        else
            searchUrlByCountry += (nightsValue) + "-nights-";
        searchUrlByCountry += adultsValue + "-adults-";

        if(!kidsAgesList.get(0).equals("0")){
            searchUrlByCountry += kidsAgesList.size() + "(";
            for (int i=0; i< kidsAgesList.size(); i++){
                searchUrlByCountry += kidsAgesList.get(i);
                if (i+1<kidsAgesList.size())
                    searchUrlByCountry += ",";
            }
            searchUrlByCountry += ")";
        }
        else
            searchUrlByCountry += "0";

        searchUrlByCountry +="-kids-1..5-stars?";

//        if (isFlexibleDate) {
//            if (departureValue.equals("Москва"))
//                searchUrlByCountry += "&flex_dates=3";
//            else
//                searchUrlByCountry += "&flex_dates=5";
//        }

        if (isFlexibleDate){
            if(searchUrlByCountry.endsWith("?"))
                searchUrlByCountry += "flex_dates=6";
            else
                searchUrlByCountry += "&flex_dates=6";
        }

        if(starsValues.size() > 0)
            {
                if(searchUrlByCountry.endsWith("?"))
                    searchUrlByCountry += "filter_stars=";
                else
                    searchUrlByCountry += "&filter_stars=";
                for(int i=0; i<starsValues.size(); i++)
                    searchUrlByCountry +=starsValues.get(i) + ",";
            }

        if(mealValues.size() > 0){
            if(searchUrlByCountry.endsWith("?"))
                searchUrlByCountry += "filter_meals=";
            else
                searchUrlByCountry += "&filter_meals=";
            for(int i=0; i<mealValues.size(); i++)
                searchUrlByCountry +=mealValues.get(i) + ",";
        }

        if(!operatorValue.isEmpty()){
            if(searchUrlByCountry.endsWith("?"))
                searchUrlByCountry += "filter_operators=" + operatorValue;
            else
                searchUrlByCountry += "&filter_operators=" + operatorValue;
        }

        if ($(byXpath(LOTTERY_POPUP)).isDisplayed())
            $(byXpath(LOTTERY_POPUP_CLOSE_BUTTON)).waitUntil(Condition.visible, CONSTANT_20_SECONDS).click();

        //Country
        System.out.println(searchUrlByCountry);
        goToPage(searchUrlByCountry);
        if (!($(byXpath(LEVEL_TRAVEL_HEADER)).isDisplayed()) || $(byXpath(ERROR_500)).isDisplayed())
            refreshPage();

        $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//        $(byXpath(LOAD_TOURS_LABEL)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//        $(byXpath(SORT_BY_PRICE_BUTTON)).waitUntil(Condition.visible, CONSTANT_300_SECONDS).click();
//        $(byXpath(BANNER_APPLY_FILTER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
        $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
        sleep(CONSTANT_1_SECOND);
//        $(byXpath(SELECT_TOUR_LABEL)).waitUntil(Condition.visible, CONSTANT_240_SECONDS);
        if(!($(byXpath(SELECT_TOUR_LABEL)).isDisplayed()) || $(byXpath(ERROR_500)).isDisplayed()){
            refreshPage();
            $(byXpath(TOURS_PRELOADER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//            $(byXpath(LOAD_TOURS_LABEL)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
//            $(byXpath(SORT_BY_PRICE_BUTTON)).waitUntil(Condition.visible, CONSTANT_300_SECONDS).click();
//            $(byXpath(BANNER_APPLY_FILTER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
            $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
            sleep(CONSTANT_1_SECOND);
//            $(byXpath(SELECT_TOUR_LABEL)).waitUntil(Condition.visible, CONSTANT_240_SECONDS);
        }

        //Instant confirmation switch for Russia
//        if(countryValue.equals("Россия")){
//            $(byXpath(SWITCH)).waitUntil(Condition.visible,CONSTANT_20_SECONDS).click();
//            $(byXpath(BANNER_APPLY_FILTER)).waitUntil(Condition.disappear, CONSTANT_30_SECONDS);
//        }

        //Close bunner
        if($(byXpath(PROMOCODE_BUNNER_CONTAINER)).isDisplayed())
            $(byXpath(PROMOCODE_BUNNER_CLOSE_BUTTON)).waitUntil(Condition.visible, CONSTANT_5_SECONDS).click();

        return this;
    }

    public LevelTravelHotelPage gotoLevelTravelHotelPagePx(int hotelBlockPixelCounter) throws Exception{
        WebElement currentHotelBlock = driver.findElement(By.xpath(String.format(HOTEL_TOUR_PRICE_PX, hotelBlockPixelCounter)));
        Actions actions = new Actions(driver);
        actions.moveToElement(currentHotelBlock);
        actions.perform();
        scrollDownOnSomePxl(150);
        $(byXpath(String.format(HOTEL_TOUR_PRICE_PX, hotelBlockPixelCounter))).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();

        return new LevelTravelHotelPage();
    }

    public LevelTravelHotelPage gotoLevelTravelHotelPage(int hotelBlockCounter) throws Exception{
        WebElement currentHotelBlock = driver.findElement(By.xpath(String.format(HOTEL_TOUR_PRICE, hotelBlockCounter)));
        Actions actions = new Actions(driver);
        actions.moveToElement(currentHotelBlock);
        actions.perform();
        $(byXpath(String.format(HOTEL_TOUR_PRICE, hotelBlockCounter))).waitUntil(Condition.visible, CONSTANT_10_SECONDS).click();

        return new LevelTravelHotelPage();
    }

    public static boolean ifEmptyLTSearch() throws Exception{
//        $(byXpath(LOAD_TOURS_LABEL)).waitUntil(Condition.disappear, CONSTANT_240_SECONDS);
//        $(byXpath(SORT_BY_PRICE_BUTTON)).waitUntil(Condition.visible, CONSTANT_300_SECONDS).click();
//        $(byXpath(BANNER_APPLY_FILTER)).waitUntil(Condition.disappear, CONSTANT_300_SECONDS);
        $(byXpath(LOAD_TOURS_PROGRESS_BAR)).waitUntil(Condition.disappear, CONSTANT_480_SECONDS);
        sleep(CONSTANT_1_SECOND);
//        $(byXpath(SELECT_TOUR_LABEL)).waitUntil(Condition.visible, CONSTANT_120_SECONDS);
        if ($(byXpath(EMPTY_SEARCH)).isDisplayed() || $(byXpath(EMPTY_SEARCH_WITH_FILTERS)).isDisplayed() || $(byXpath(EMPTY_SEARCH_TITLE)).isDisplayed() || $(byXpath(EMPTY_TOURS_BLOCK)).isDisplayed())
            return true;
        else
            return false;
    }

    public static Integer getHotelBlockHeight() throws Exception{
        int hotelBlockHeight = 0;
        if($(byXpath(PAYLATE_BANNER)).isDisplayed())
            hotelBlockHeight = 286;
        if($(byXpath(FREE_CANCELLATION_BANNER)).isDisplayed())
            hotelBlockHeight = 286;

        return hotelBlockHeight;
    }

}
