import framework.Constants;
import framework.ReportGenerator;
import framework.SeleniumTestCase;
import framework.Sender;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.Travelata.OfferEmailAuthPage;
import pageobjects.Travelata.OfferEmailSearchPage;
import pageobjects.operators.Pegast.PegastMainPage;
import pageobjects.operators.Pegast.PegastSearchPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import static com.codeborne.selenide.Selenide.sleep;
import static framework.JDBCPostgreSQLConnection.getPegastOfferHotelId;
import static framework.ReportGenerator.getSearchRussianDate;
import static pageobjects.Travelata.OfferEmailSearchPage.getHotelIdOfferList;

public class PegastTourPricesTest extends SeleniumTestCase {

    @Test(dataProvider = "tourCriteria", enabled = true)
    public void PegastTourPricesTest(String operatorValue, boolean isFlexibleNights, String kidsAges,
                                  String offerNameValue, String offerPwdValue, String reportName) throws Exception {

        //Jenkins
        String cityFromValue =      System.getenv("CITY_PARAM");
        String countryToValue =     System.getenv("COUNTRY_PARAM");
        int dateCounter =           Integer.parseInt(System.getenv("DATE_COUNT_PARAM"));
        boolean isFlexibleDate =    Boolean.parseBoolean(System.getenv("FLEXIBLE_DATE_PARAM"));
        int nightsValue =           Integer.parseInt(System.getenv("NIGHT_COUNT_PARAM"));
        String adultsValue =        System.getenv("ADULTS_PARAM");
        String starsValues =        System.getenv("STARS_PARAM");
        String mealValues =         System.getenv("MEAL_PARAM");
        String recipientValue =     System.getenv("RECIPIENTS_PARAM");

//        //Local
//        String cityFromValue = "Москва";
//        String countryToValue = "Турция";
//        int dateCounter = 14;
//        boolean isFlexibleDate = false;
//        int nightsValue = 7;
//        String adultsValue = "2";
//        String starsValues = "5";
//        String mealValues = "AI";
//        String recipientValue = "olena.tyshchenko@yandex.ru";

        FileInputStream fsIP = new FileInputStream(new File(reportName));
        Workbook book = new HSSFWorkbook(fsIP);

        while (book.getNumberOfSheets() > 0)
            book.removeSheetAt(0);

        ArrayList<String> kidsAgesList = new ArrayList<>(Arrays.asList(kidsAges.split("\\s*,\\s*")));
        ArrayList<String> starsList = new ArrayList<>(Arrays.asList(starsValues.split("\\s*,\\s*")));
        ArrayList<String> mealList = new ArrayList<>(Arrays.asList(mealValues.split("\\s*,\\s*")));

        //Pegast
        PegastMainPage
                .getPegastMainPage()
                .gotoPegastMainPage()
                .selectCityFrom(cityFromValue)
                .selectCountryTo(countryToValue)
                .selectDateNights(getSearchRussianDate(dateCounter), nightsValue)
                .gotoPegastSearchPage();

        //By country
        PegastSearchPage
                .getPegastSearchPage()
                .waitForSearchResults()
                .closeJivoBanner()
                .selectStars(starsList)
                .selectMeal(mealList)
                .clickSearchButton()
                .waitForSearchResults()
                .putHotelNamesPegastList(adultsValue);

        sleep(Constants.CONSTANT_2_SECONDS);
        getPegastOfferHotelId(starsList, countryToValue);

        //Offer Part
        OfferEmailAuthPage
                .getOfferEmailAuthPage()
                .gotoOfferEmailPage()
                .enterOfferEmail(offerNameValue, offerPwdValue);

        //By country
        if (!getHotelIdOfferList().isEmpty()) {
            for (int hotelCounter = 0; hotelCounter < getHotelIdOfferList().size(); hotelCounter++) {

                if ((hotelCounter % 10) == 0){
                    refreshPage();
                    System.out.println("Page reloading");
                    OfferEmailAuthPage
                            .getOfferEmailAuthPage()
                            .enterOfferEmail(offerNameValue, offerPwdValue);
                }

                OfferEmailSearchPage
                        .getOfferEmailSearchPage()
                        .gotoSearchCriteriaByTOWithHotel(countryToValue, dateCounter, isFlexibleDate, nightsValue, isFlexibleNights, adultsValue, kidsAgesList, cityFromValue, starsList, mealList, "-", operatorValue, getHotelIdOfferList().get(hotelCounter), hotelCounter)
                        .putHotelNamesOfferListByTOWithHotel(countryToValue, "-", operatorValue, getHotelIdOfferList().get(hotelCounter))
                ;
            }
        }

        ReportGenerator.getReportGenerator().generateReportPegastOffer(book, reportName, countryToValue, cityFromValue, dateCounter);

        book.write(new FileOutputStream(reportName));
        book.close();

        Sender.sendMail(recipientValue, reportName, "Pegast prices report + " + dateCounter, countryToValue);

    }

    @AfterTest

    @DataProvider
    public Object[][] tourCriteria(){
        return new Object[][]{
                {
                        "40",
                        false,
                        "0",
                        "olena.tyshchenko@travelata.ru",
                        "7174043502F70664",
                        "C:\\Reports\\PegastTourPrices.xls",
                }
        };
    }

}
