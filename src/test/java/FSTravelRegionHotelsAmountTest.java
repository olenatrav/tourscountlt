import com.codeborne.selenide.Condition;
import framework.ReportGenerator;
import framework.SeleniumTestCase;
import framework.Sender;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.Travelata.OfferEmailAuthPage;
import pageobjects.Travelata.OfferEmailSearchPage;
import pageobjects.operators.FSTravel.FSTravelMainPage;
import pageobjects.operators.FSTravel.FSTravelSearchPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import static framework.Constants.*;
import static framework.ReportGenerator.getSearchDateDashFormat;
import static pageobjects.operators.FSTravel.FSTravelSearchPage.*;

public class FSTravelRegionHotelsAmountTest extends SeleniumTestCase {

    @Test(dataProvider = "tourCriteria", enabled = true)
    public void FSTravelRegionHotelsAmountTest(String operatorValue, boolean isFlexibleNights,
                                         String kidsAgesValue,
                                         String offerNameValue, String offerPwdValue, String reportName) throws Exception{

        //Jenkins
        String countryToValue =           System.getenv("COUNTRY_PARAM");
        int dateCounter =                 Integer.parseInt(System.getenv("DATE_COUNT_PARAM"));
        int nightsValue =                 Integer.parseInt(System.getenv("NIGHT_COUNT_PARAM"));
        String adultsValue =              System.getenv("ADULTS_PARAM");
        String starsValues =              System.getenv("STARS_PARAM");
        String mealValues =               System.getenv("MEAL_PARAM");
        String recipientValue =           System.getenv("RECIPIENTS_PARAM");

//        //Local
//        String countryToValue =         "Турция";
//        int dateCounter =               86;
//        int nightsValue =               7;
//        String adultsValue =            "2";
//        String starsValues =            "5";
//        String mealValues =             "AI";
//        String recipientValue =         "olena.tyshchenko@yandex.ru, konstantin.prilepa@travelata.ru";

        FileInputStream fsIP= new FileInputStream(new File(reportName));
        Workbook book = new HSSFWorkbook(fsIP);

//        while(book.getNumberOfSheets()>0)
//            book.removeSheetAt(0);

        ArrayList<String> kidsAgesList = new ArrayList<>(Arrays.asList(kidsAgesValue.split("\\s*,\\s*")));
        ArrayList<String> starsList = new ArrayList<>();
        ArrayList<String> mealList = new ArrayList<>();
        if(!starsValues.equals("-"))
            starsList = new ArrayList<>(Arrays.asList(starsValues.split("\\s*,\\s*")));
        if(!mealValues.equals("-"))
            mealList = new ArrayList<>(Arrays.asList(mealValues.split("\\s*,\\s*")));

        String countryEngValue = "";

        ArrayList<String> countriesList = new ArrayList<String>(Arrays.asList(countryToValue.split("\\s*,\\s*")));

        for (int countryCounter = 0; countryCounter < countriesList.size(); countryCounter++) {

            countryEngValue += destinationCountryEng.get(countriesList.get(countryCounter)) + " ";

            for (int departureCounter = 0; departureCounter < departureFSTravel.length; departureCounter ++) {

                //FSTravel part
                FSTravelMainPage
                        .getFSTravelMainPage()
                        .gotoFSTravelMainPage()
                        .selectCityFrom(departureFSTravel[departureCounter]);

                if(!FSTravelMainPage.isCountryDisplayed(countriesList.get(countryCounter))){
                    setHotelsAmountFSTravelList("0");
                    setDepartureCityFSTravelList(departureFSTravel[departureCounter]);
                    setActualDateCounterList(dateCounter);
                    setActualNightsCounterList(nightsValue);
                }
                else{
                    FSTravelMainPage
                            .getFSTravelMainPage()
                            .selectCountryTo(countriesList.get(countryCounter))
                            .selectFlightDate(getSearchDateDashFormat(dateCounter))
                            .selectNightsValue(nightsValue)
                            .clickSearchButton();

                    FSTravelSearchPage
                            .getFSTravelSearchPage()
                            .waitForSearchResults();

                    if(isNoToursResult() || isEmptySearchResult()){
                        setHotelsAmountFSTravelList("0");
                        setDepartureCityFSTravelList(departureFSTravel[departureCounter]);
                        setActualDateCounterList(dateCounter);
                        setActualNightsCounterList(nightsValue);
                    }
                    else{
                        FSTravelSearchPage
                                .getFSTravelSearchPage()
                                .selectStars(starsList)
                                .selectMeal(mealList)
                                .waitForSearchResults()
                                .putHotelsAmountFSTravelList(departureFSTravel[departureCounter], dateCounter, nightsValue);
                    }
                }

                //Offer part
                OfferEmailAuthPage
                        .getOfferEmailAuthPage()
                        .gotoOfferEmailPage()
                        .enterOfferEmail(offerNameValue, offerPwdValue);

                OfferEmailSearchPage
                        .getOfferEmailSearchPage()
                        .gotoSearchCriteriaForDepartureCity(countriesList.get(countryCounter), dateCounter, false, nightsValue, isFlexibleNights, adultsValue, kidsAgesList, departureFSTravel[departureCounter], starsList, mealList, "-", operatorValue)
                        .putHotelsAmountOfferList();
                ;

            }

            ReportGenerator.getReportGenerator().generateRegionFSTravelOfferHotelAmount(book, reportName, countriesList.get(countryCounter));

            getHotelsAmountFSTravelList().clear();
            getDepartureCityFSTravelList().clear();
            getActualDateCounterList().clear();
            getActualNightsCounterList().clear();

        }


        book.write(new FileOutputStream(reportName));
        book.close();

        Sender.sendMail(recipientValue, reportName, "FSTravel region amount report + " + dateCounter + " " + countryEngValue, countryEngValue);

    }

    @AfterTest

    @DataProvider
    public Object[][] tourCriteria(){
        return new Object[][]{
                {
                        "55",
                        false,
                        "0",
                        "olena.tyshchenko@travelata.ru",
                        "7174043502F70664",
                        "C:\\Reports\\FSTravelRegionHotelsAmount.xls",
                }
        };
    }

}
