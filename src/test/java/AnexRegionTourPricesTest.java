import com.codeborne.selenide.Condition;
import framework.Constants;
import framework.ReportGenerator;
import framework.SeleniumTestCase;
import framework.Sender;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.Travelata.OfferEmailAuthPage;
import pageobjects.Travelata.OfferEmailSearchPage;
import pageobjects.operators.Anex_new.AnexAuthPage;
import pageobjects.operators.Anex_new.AnexSearchPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static framework.Constants.*;
import static framework.JDBCPostgreSQLConnection.getAnexOfferHotelId;
import static framework.ReportGenerator.getSearchRussianDate;
import static pageobjects.Travelata.OfferEmailSearchPage.*;
import static pageobjects.operators.Anex_new.AnexSearchPage.*;
import static pageobjects.operators.Anex_new.AnexTourPage.setHotelTourPriceActualizedAnexList;

public class AnexRegionTourPricesTest extends SeleniumTestCase {

    @Test(dataProvider = "tourCriteria", enabled = true)
    public void AnexRegionTourPricesTest(String operatorValue, boolean isFlexibleNights, String kidsAges, String anexNameValue, String anexPwdValue,
                                 String offerNameValue, String offerPwdValue, String reportName) throws Exception{

        //Jenkins
        String cityFromValue =            System.getenv("CITY_PARAM");
        String countryToValue =           System.getenv("COUNTRY_PARAM");
        int dateCounter =                 Integer.parseInt(System.getenv("DATE_COUNT_PARAM"));
        int nightsValue =                 Integer.parseInt(System.getenv("NIGHT_COUNT_PARAM"));
        String adultsValue =              System.getenv("ADULTS_PARAM");
        String starsValues =              System.getenv("STARS_PARAM");
        String mealValues =               System.getenv("MEAL_PARAM");
        boolean isInstantConfirmation =   Boolean.parseBoolean(System.getenv("INSTANT_CONFIRMATION_PARAM"));
        String recipientValue =           System.getenv("RECIPIENTS_PARAM");

//        //Local
//        String cityFromValue =          "Санкт-Петербург";
//        String countryToValue =         "Египет";
//        int dateCounter =               13;
//        int nightsValue =               7;
//        String adultsValue =            "2";
//        String starsValues =            "5";
//        String mealValues =             "AI";
//        boolean isInstantConfirmation = false;
//        String recipientValue =         "olena.tyshchenko@yandex.ru";

        FileInputStream fsIP= new FileInputStream(new File(reportName));
        Workbook book = new HSSFWorkbook(fsIP);

        while(book.getNumberOfSheets()>0)
            book.removeSheetAt(0);

        ArrayList<String> departuresList = new ArrayList<>(Arrays.asList(cityFromValue.split("\\s*,\\s*")));
        ArrayList<String> kidsAgesList = new ArrayList<>(Arrays.asList(kidsAges.split("\\s*,\\s*")));
        ArrayList<String> starsList = new ArrayList<>();
        ArrayList<String> mealList = new ArrayList<>();
        if(!starsValues.equals("-"))
            starsList = new ArrayList<>(Arrays.asList(starsValues.split("\\s*,\\s*")));
        if(!mealValues.equals("-"))
            mealList = new ArrayList<>(Arrays.asList(mealValues.split("\\s*,\\s*")));

        int actualDateCounterValue = 0;
        boolean emptySearch = true;
        int actualNightsCounterValue = 0;

        AnexAuthPage
                .getAnexAuthPage()
                .gotoAnexAuthPage()
                .loginAnexLKPage(anexNameValue, anexPwdValue);

        for (int departureCounter = 0; departureCounter < departuresList.size(); departureCounter ++) {

            actualDateCounterValue = 0;
            emptySearch = true;
            actualNightsCounterValue = 0;

            //Anex Part
            AnexAuthPage
                    .getAnexAuthPage()
                    .gotoAnexAuthPage()
                    .gotoAnexSearchPage();
            AnexSearchPage
                    .getAnexSearchPage()
                    .selectCityFrom(departuresList.get(departureCounter))
                    .openCountryToSelect(countryToValue);
            if (isCountryActive(countryToValue)) {
                emptySearch = false;
                AnexSearchPage
                        .getAnexSearchPage()
                        .selectCountryTo(countryToValue)
                        .openCalendar();
                actualDateCounterValue = getFirstActiveDateCounterValue(dateCounter);

                if (actualDateCounterValue != 0) {
                    AnexSearchPage
                            .getAnexSearchPage()
                            .selectFlightDate(getSearchRussianDate(actualDateCounterValue));
                    actualNightsCounterValue = getFirstActiveNightsCounterValue(nightsValue);

                    AnexSearchPage
                            .getAnexSearchPage()
                            .selectNightsTo(actualNightsCounterValue)
                            .selectNightsFrom(actualNightsCounterValue)
                            .selectMeal(mealList)
                            .selectStars(starsList)
                            .clickInstantConfirmButton(isInstantConfirmation)
                            .clickSubmitSearchButton()
                            .waitForSearchResult();

                    //By country
                    if (!ifNoToursResult()) {
                        AnexSearchPage
                                .getAnexSearchPage()
                                .putHotelNamesAnexList(true);
                    } else
                        emptySearch = true;
                } else
                    emptySearch = true;
            } else
                emptySearch = true;

            if (emptySearch) {
                setHotelsAmountAnexList("-");
                setDepartureCityAnexList(departuresList.get(departureCounter));
                setHotelNamesAnexList("-");
                setHotelStarsAnexList("-");
                setCountryResortAnexList(countryToValue);
                setHotelTourPriceAnexList("-");
                setHotelTourPriceActualizedAnexList("-");
            }


            sleep(Constants.CONSTANT_2_SECONDS);
            getAnexOfferHotelId(starsList, countryToValue);

            if ((getHotelIdOfferList().size() == 1) && (getHotelIdOfferList().get(0).equals("0"))) {
                OfferEmailSearchPage
                        .getOfferEmailSearchPage()
                        .putEmptyHotelNamesOfferList(countryToValue, "-");
            } else {
                //Offer Part
                OfferEmailAuthPage
                        .getOfferEmailAuthPage()
                        .gotoOfferEmailPage()
                        .enterOfferEmail(offerNameValue, offerPwdValue);

                //By country
                for (int hotelCounter = 0; hotelCounter < getHotelIdOfferList().size(); hotelCounter++) {
                    OfferEmailSearchPage
                            .getOfferEmailSearchPage()
                            .gotoSearchCriteriaByTOWithHotel(countryToValue, actualDateCounterValue, false, actualNightsCounterValue, isFlexibleNights, adultsValue, kidsAgesList, departuresList.get(departureCounter), starsList, mealList, "-", operatorValue, getHotelIdOfferList().get(hotelCounter), hotelCounter)
                            .putHotelNamesOfferListByTOWithHotel(countryToValue, "-", operatorValue, getHotelIdOfferList().get(hotelCounter))
                    ;
                }
            }

            ReportGenerator.getReportGenerator().generateReportAnexOffer(book, reportName, countryToValue, departuresList.get(departureCounter), actualDateCounterValue, actualNightsCounterValue);

//            book.write(new FileOutputStream(reportName));
//            book.close();
//
//            Sender.sendMail(recipientValue, reportName, "Anex region report + " + dateCounter, countryToValue);

        }

//        ReportGenerator.getReportGenerator().generateRegionAnexOfferHotelAmount(book, reportName, countryToValue);

        book.write(new FileOutputStream(reportName));
        book.close();

        Sender.sendMail(recipientValue, reportName, "Anex region prices report " + dateCounter + " " + countryToValue, countryToValue);

    }

    @AfterTest

    @DataProvider
    public Object[][] tourCriteria(){
        return new Object[][]{
                {
                        "5",
                        false,
                        "0",
                        "Ttechnology",
                        "jT68$LyJ",
                        "olena.tyshchenko@travelata.ru",
                        "7174043502F70664",
                        "C:\\Reports\\AnexRegionTourPrices.xls",
                }
        };
    }

}
