import framework.Constants;
import framework.ReportGenerator;
import framework.SeleniumTestCase;
import framework.Sender;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.LevelTravel.LevelTravelHotelPage;
import pageobjects.LevelTravel.LevelTravelSearchPage;
import pageobjects.LevelTravel.LevelTravelTourPage;
import pageobjects.Travelata.TravelataHotelPage;
import pageobjects.Travelata.TravelataSearchPage;
import pageobjects.Travelata.TravelataTourPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static framework.Constants.departureLevelTravel;
import static framework.Constants.destinationCountryEng;
import static framework.ReportGenerator.getFinalWithOilTaxSerpWithOilTaxLTRatioList;
import static framework.ReportGenerator.getFinalWithOilTaxSerpWithOilTaxTravRatioList;
import static pageobjects.LevelTravel.LevelTravelSearchPage.*;
import static pageobjects.LevelTravel.LevelTravelTourPage.*;
import static pageobjects.LevelTravel.LevelTravelTourPage.getHotelTourPriceWithoutOilTaxTourLTList;
import static pageobjects.Travelata.TravelataHotelPage.checkIfEmptyPage;
import static pageobjects.Travelata.TravelataHotelPage.checkIfEmptySearch;
import static pageobjects.Travelata.TravelataSearchPage.*;
import static pageobjects.Travelata.TravelataSearchPage.getHotelTourPriceWithoutOilTaxSearchTravList;
import static pageobjects.Travelata.TravelataTourPage.*;
import static pageobjects.Travelata.TravelataTourPage.getHotelTourPriceWithoutOilTaxTourTravList;

public class LTRegionTourPricesTest extends SeleniumTestCase {

    @Test(dataProvider = "tourCriteria", enabled = true)
    public void LTRegionTourPricesTest(boolean isFlexibleNights, String reportPathValue) throws Exception {

        //Jenkins
        String countryValue =       System.getenv("COUNTRY_PARAM");
        String dateCounterValue =   System.getenv("DATE_COUNT_PARAM");
        boolean isFlexibleDate =    Boolean.parseBoolean(System.getenv("FLEXIBLE_DATE_PARAM"));
        int nightsValue =           Integer.parseInt(System.getenv("NIGHT_COUNT_PARAM"));
        String adultsValue =        System.getenv("ADULTS_PARAM");
        String kidsAges =           System.getenv("KIDS_AGES_PARAM");
        String starsValues =        System.getenv("STARS_PARAM");
        String recipientValue =     System.getenv("RECIPIENTS_PARAM");

//        //Local
//        String countryValue =       "Турция"; //Египет, Турция, Россия, ОАЭ, Таиланд, Доминикана, Куба, Мексика, Венесуэла, Мальдивы, Шри-Ланка, Кипр, Греция, Абхазия
//        String dateCounterValue =   "15"; //15, 15, 15, 15, 15, 14, 14, 14, 16, 15, 15, 15, 15, 15
//        boolean isFlexibleDate =    false;
//        int nightsValue =           7;
//        String adultsValue =        "2";
//        String kidsAges =           "0";
//        String starsValues =        "3";
//        String recipientValue =     "olena.tyshchenko@yandex.ru"; //konstantin.prilepa@travelata.ru

        Workbook book;
        File file = new File(reportPathValue);
        FileInputStream fileInputStream = new FileInputStream(file);
        if (file.exists())
            book = WorkbookFactory.create(fileInputStream);
        else {
            if (file.getName().endsWith(".xls"))
                book = new HSSFWorkbook(fileInputStream);
            else if (file.getName().endsWith(".xlsx"))
                book = new XSSFWorkbook(fileInputStream);
            else
                throw new IllegalArgumentException("Unknown file extension");
        }

//        while(book.getNumberOfSheets()>0)
//            book.removeSheetAt(0);

        ArrayList<String> dateCounterList = new ArrayList<>(Arrays.asList(dateCounterValue.split("\\s*,\\s*")));
        ArrayList<String> kidsAgesList = new ArrayList<>(Arrays.asList(kidsAges.split("\\s*,\\s*")));
        ArrayList<String> starsList = new ArrayList<>();
        if(!starsValues.equals("-"))
            starsList = new ArrayList<>(Arrays.asList(starsValues.split("\\s*,\\s*")));

        String countryEngValue = "";

        ArrayList<String> countriesList = new ArrayList<String>(Arrays.asList(countryValue.split("\\s*,\\s*")));

        for (int countryCounter=0; countryCounter<countriesList.size(); countryCounter++) {

            countryEngValue += destinationCountryEng.get(countriesList.get(countryCounter)) + " ";

            for (int departureCounter = 0; departureCounter < departureLevelTravel.length; departureCounter ++){

                int hotelBlockCounter = 1;
                int hotelBlockPixelCounter = 0;
                int i=0;

                System.out.println(destinationCountryEng.get(countriesList.get(countryCounter)));

                //Level Travel part
                LevelTravelSearchPage
                        .getLevelTravelSearchPage()
                        .gotoSearchPageWithCriteria(countriesList.get(countryCounter), Integer.parseInt(dateCounterList.get(countryCounter)),
                                isFlexibleDate, nightsValue, isFlexibleNights, adultsValue, kidsAgesList, departureLevelTravel[departureCounter], starsList);

                if (ifEmptyLTSearch()) {
                    setHotelNamesSearchLTList("-");
                    setHotelTourPricesSearchLTList("0");
//                    setHotelTourPricesTourLTList("0");
//                    setHotelTourPriceOilTaxTourLTList("0");
//                    setHotelTourPriceWithoutOilTaxTourLTList("0");
//                    setToNameTourLTList("-");
                }

                else {
                    LevelTravelSearchPage
                            .getLevelTravelSearchPage()
                            .clickSortByPriceLink();

                    while (($(byXpath(String.format(getHotelBlockTopPx(), hotelBlockPixelCounter))).isDisplayed())) {

//                        boolean isUniqueHotel = true;
                        String oldTab = driver.getWindowHandle();
//                        if (i > 0) {
//                            for (int hotelCounter = 0; hotelCounter < i; hotelCounter++) {
//                                if (getHotelNamesSearchLTList().get(hotelCounter).equals($(byXpath(String.format(getHotelBlockTopPx(), hotelBlockPixelCounter))).getText()))
//                                    isUniqueHotel = false;
//                            }
//                        }
//                        if (isUniqueHotel) {

                            scrollToElement(String.format(getHotelBlockTopPx(), hotelBlockPixelCounter));
//                            WebElement currentHotelBlock = driver.findElement(By.xpath(String.format(getHotelBlockTopPx(), hotelBlockPixelCounter))); //getHotelStars(), hotelBlockCounter - 1
//                            Actions actions = new Actions(driver);
//                            actions.moveToElement(currentHotelBlock);
//                            actions.perform();

                            LevelTravelSearchPage
                                    .getLevelTravelSearchPage()
                                    .addCurrentHotelNameTourPricePx(hotelBlockPixelCounter);

                            ////////////////////////////////////////////////////////////////Actualization LT

//                            LevelTravelSearchPage
//                                    .getLevelTravelSearchPage()
//                                    .gotoLevelTravelHotelPagePx(hotelBlockPixelCounter);
//                            String hotelTab = driver.getWindowHandle();
//                            ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
//                            newTab.remove(hotelTab);
//                            driver.switchTo().window(newTab.get(0));
//
//                            LevelTravelHotelPage
//                                    .getLevelTravelHotelPage()
//                                    .reloadIfErrorDisplayed();
//                            if(LevelTravelHotelPage.getLevelTravelHotelPage().ifEmptyHotelPage()){
//                                LevelTravelTourPage
//                                        .getLevelTravelTourPage()
//                                        .addEmptyTourTOPrice();
//                            }
//                            else
//                                LevelTravelHotelPage.getLevelTravelHotelPage()
//                                        .waitHotelPageTourLoaded();
//
//                            if(LevelTravelHotelPage.getLevelTravelHotelPage().ifEmptyTours() ||
//                                    LevelTravelHotelPage.getLevelTravelHotelPage().ifNoOffers(nightsValue) ||
//                                    LevelTravelHotelPage.getLevelTravelHotelPage().ifEmptyHotelPage() ||
//                                    LevelTravelHotelPage.getLevelTravelHotelPage().ifHotelPageNoLoaded()){
//                                LevelTravelTourPage
//                                        .getLevelTravelTourPage()
//                                        .addEmptyTourTOPrice();
//                            }
//                            else{
//                                LevelTravelHotelPage
//                                        .getLevelTravelHotelPage()
//                                        .waitHotelPageAllToursButton()
//                                        .loadToursIfButtonDisplayed()
//                                        .gotoLevelTravelTourPage(nightsValue);
//
//                                if (LevelTravelHotelPage.getLevelTravelHotelPage().ifTourOutdated())
//                                {
//                                    LevelTravelTourPage
//                                            .getLevelTravelTourPage()
//                                            .addEmptyTourTOPrice();
//                                }
//                                else{
//                                    driver.close();
//                                    newTab = new ArrayList<String>(driver.getWindowHandles());
//                                    sleep(Constants.CONSTANT_1_SECOND);
//                                    newTab.remove(oldTab);
//                                    driver.switchTo().window(newTab.get(0));
//
//                                    LevelTravelTourPage
//                                            .getLevelTravelTourPage()
//                                            .reloadIfErrorDisplayed()
//                                            .waitForLoaderDissapear();
//                                    if(!LevelTravelTourPage.getLevelTravelTourPage().checkIfTourLoaded()){
//                                        LevelTravelTourPage.getLevelTravelTourPage().addEmptyTourTOPrice();
//                                    }
//                                    else{
//                                        LevelTravelTourPage.getLevelTravelTourPage().waitForCheckingFlights();
//                                        if (LevelTravelTourPage.getLevelTravelTourPage().checkIfTourLTOutdated()) {
//                                            LevelTravelTourPage.getLevelTravelTourPage().addEmptyTourTOPrice();
//                                        }
//                                        else {
//                                            LevelTravelTourPage
//                                                    .getLevelTravelTourPage()
//                                                    .addTourPrice()
//                                                    .addTourTO();
//                                        }
//                                    }
//                                }
//                            }
//
//                            sleep(Constants.CONSTANT_1_SECOND);
//                            driver.close();
//                            driver.switchTo().window(oldTab);

                            //////////////////////////////////////////////////////////////////////

                            i++;

                            hotelBlockPixelCounter += getHotelBlockHeightPx(hotelBlockPixelCounter);
//                        }
                    }
                }


                // Travelata part

                i = 0;
                hotelBlockCounter = 1;

                TravelataSearchPage
                        .getTravelataSearchPage()
                        .gotoTravelataMain()
                        .closeLotteryPopup()
                        .closeHRPopup()
                        .closePaylatePopup()
                        .closeCashBackPopup()
                        .gotoSearchPageWithCriteria(countriesList.get(countryCounter), "",
                                Integer.parseInt(dateCounterList.get(countryCounter)), isFlexibleDate, nightsValue,
                                isFlexibleNights, adultsValue, kidsAgesList, departureLevelTravel[departureCounter], starsList);

                if (ifEmptyTravSearch()) {
                    setHotelNamesSearchTravList("-");
                    setHotelTourPricesSearchTravList("0");
//                    setHotelTourPriceOilTaxSearchTravList("0");
//                    setHotelTourPriceWithoutOilTaxSearchTravList("0");
//                    setHotelTourPricesTourTravList("0");
//                    setHotelTourPriceOilTaxTourTravList("0");
//                    setHotelTourPriceWithoutOilTaxTourTravList("0");
//                    setToNameTourTravList("-");
                }

                else {
                    TravelataSearchPage
                            .getTravelataSearchPage()
                            .closeLotteryPopup()
                            .closeHRPopup()
                            .closeExpiredPopup()
                            .closeCashBackPopup()
                            .clickSortByPriceLink();

                    while (($(byXpath(String.format(getTravHotelBlock(), hotelBlockCounter))).isDisplayed())) {
//                        boolean isUniqueHotel = true;
//                        String oldTab = driver.getWindowHandle();
//                        if (i > 0) {
//                            for (int hotelCounter = 0; hotelCounter < i; hotelCounter++) {
//                                if (getHotelNamesSearchTravList().get(hotelCounter).equals($(byXpath(String.format(getTravHotelNameLink(), hotelBlockCounter))).getText()))
//                                    isUniqueHotel = false;
//                            }
//                        }
//                        if (isUniqueHotel) {
                            scrollToElement(String.format(getTravHotelNameLink(), hotelBlockCounter));
//                            WebElement currentHotelBlock = driver.findElement(By.xpath(String.format(getTravHotelNameLink(), hotelBlockCounter)));
//                            Actions actions = new Actions(driver);
//                            actions.moveToElement(currentHotelBlock);
//                            actions.perform();

                            TravelataSearchPage
                                    .getTravelataSearchPage()
                                    .closeLotteryPopup()
                                    .closeExpiredPopup()
                                    .closeCashBackPopup()
                                    .addHotelNameTourPriceOilTax(hotelBlockCounter);

                            /////////////////////////////////////////////////////////////////////// Actualization Travelata
//
//                            TravelataSearchPage
//                                    .getTravelataSearchPage()
//                                    .gotoTravelataHotelPage(hotelBlockCounter);
//
//                            String hotelTab = driver.getWindowHandle();
//                            ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
//                            newTab.remove(hotelTab);
//                            driver.switchTo().window(newTab.get(0));
//
//                            TravelataHotelPage
//                                    .getTravelataHotelPage()
//                                    .closeCashBackPopup()
//                                    .closeLotteryPopup()
//                                    .reloadIfErrorDisplayed();
//
//                            if (!checkIfEmptyPage()){
//
//                                TravelataHotelPage
//                                        .waitForLoaderDisappear();
//                                if (!checkIfEmptySearch()) {
//                                    TravelataHotelPage
//                                            .getTravelataHotelPage()
//                                            .closeCashBackPopup()
//                                            .closeLotteryPopup()
////                                .closeFaqBanner()
//                                            .gotoTravelataTourPage();
//
//                                    TravelataTourPage
//                                            .getTravelataTourPage()
//                                            .waitForFlightsLoaded();
//                                    if (TravelataTourPage.getTravelataTourPage().checkIfTourOutdated()) {
//                                        TravelataTourPage
//                                                .getTravelataTourPage()
//                                                .addEmptyTourTOPrice();
//                                    } else {
//                                        TravelataTourPage
//                                                .getTravelataTourPage()
//                                                .addCheapestTourPrice();
//                                    }
//
//                                    sleep(Constants.CONSTANT_1_SECOND);
//                                    driver.close();
//                                    driver.switchTo().window(oldTab);
//                                    i++;
//                                } else {
//                                    TravelataSearchPage
//                                            .getTravelataSearchPage()
//                                            .clearLastHotelNameTourPrice();
//                                    sleep(Constants.CONSTANT_1_SECOND);
//                                    driver.close();
//                                    driver.switchTo().window(oldTab);
//                                }
//
//                            }
//
//                            else {
//                                TravelataSearchPage
//                                        .getTravelataSearchPage()
//                                        .clearLastHotelNameTourPrice();
//                                sleep(Constants.CONSTANT_1_SECOND);
//                                driver.close();
//                                driver.switchTo().window(oldTab);
//                            }

                            /////////////////////////////////////////////////////////////////

//                        }
                        hotelBlockCounter++;
                    }

                }

                ReportGenerator.getReportGenerator().generateReportRegionPricesLTTrav(book, reportPathValue, countriesList.get(countryCounter), departureLevelTravel[departureCounter], Integer.parseInt(dateCounterList.get(countryCounter)));

                getHotelNamesSearchLTList().clear();
                getHotelTourPricesSearchLTList().clear();

//                getToNameTourLTList().clear();
//                getHotelTourPricesTourLTList().clear();
//                getHotelTourPriceOilTaxTourLTList().clear();
//                getHotelTourPriceWithoutOilTaxTourLTList().clear();

                getHotelNamesSearchTravList().clear();
                getHotelTourPricesSearchTravList().clear();

//                getHotelTourPriceOilTaxSearchTravList().clear();
//                getHotelTourPriceWithoutOilTaxSearchTravList().clear();
//                getToNameTourTravList().clear();
//                getHotelTourPricesTourTravList().clear();
//                getHotelTourPriceOilTaxTourTravList().clear();
//                getHotelTourPriceWithoutOilTaxTourTravList().clear();
//                getFinalWithOilTaxSerpWithOilTaxLTRatioList().clear();
//                getFinalWithOilTaxSerpWithOilTaxTravRatioList().clear();

                book.write(new FileOutputStream(reportPathValue));

                refreshPage();
            }

            book.close();
            fileInputStream.close();

            Sender.sendMail(recipientValue, reportPathValue, "LT prices region report " + countryEngValue + " ", countryEngValue);
        }

    }

    @AfterTest

    @DataProvider
    public Object[][] tourCriteria(){
        return new Object[][]{
                {
                        false,
                        "C:\\Reports\\LTRegionTourPrices.xls",
                }
        };
    }

}
