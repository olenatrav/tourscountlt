import framework.SeleniumTestCase;
import framework.Sender;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.Register.FederalRegisterPage;

public class FederalRegisterTest extends SeleniumTestCase {

    @Test(dataProvider = "parameters", enabled = true)
    public void GetFederalRegisterHotelsTest(String fileName, String deltaFileName) throws Exception{

        //Jenkins
        String recipientValue = System.getenv("RECIPIENTS_PARAM");

//        //Local
//        String recipientValue = "olena.tyshchenko@yandex.ru, vladimir.kalinin@travelata.ru, anna.kozhaka@travelata.ru";

        FederalRegisterPage
                .getFederalRegisterPage()
                .gotoFederalRegisterPage()
//                .getHotelsList(fileName)
                .getDeltaHotelsList(fileName, deltaFileName)
                .addDeltaHotelsToFile(fileName, deltaFileName)
        ;

        Sender.sendHotelsListMail(recipientValue, fileName, deltaFileName, "Delta hotels report");

    }

    @DataProvider
    public Object[][] parameters(){
        return new Object[][]{
                {
                        "C:\\Reports\\hotels.txt",
                        "C:\\Reports\\deltaHotels.txt",
                }
        };
    }

}
