import framework.Constants;
import framework.ReportGenerator;
import framework.SeleniumTestCase;
import framework.Sender;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.Travelata.OfferEmailAuthPage;
import pageobjects.Travelata.OfferEmailSearchPage;
import pageobjects.operators.BiblioGlobus.BGMainPage;
import pageobjects.operators.BiblioGlobus.BGTourListPage;
import pageobjects.operators.BiblioGlobus.BGTourPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static framework.Constants.*;
import static framework.ReportGenerator.getSearchRussianDate;
import static pageobjects.operators.BiblioGlobus.BGTourListPage.*;
import static pageobjects.operators.BiblioGlobus.BGTourListPage.getIsNightCorrespond;
import static pageobjects.operators.BiblioGlobus.BGTourPage.*;
import static pageobjects.operators.BiblioGlobus.BGTourPage.ifSeveralPlacesListLink;

public class BGRegionHotelsAmountTest extends SeleniumTestCase {

    @Test(dataProvider = "tourCriteria", enabled = true)
    public void BGRegionTourPricesTest(String operatorValue, boolean isFlexibleNights, String kidsAges,
                                       String offerNameValue, String offerPwdValue, String reportName) throws Exception {

        //Jenkins
        String countryToValue =         System.getenv("COUNTRY_PARAM");
        int dateCounter =               Integer.parseInt(System.getenv("DATE_COUNT_PARAM"));
        int nightsValue =               Integer.parseInt(System.getenv("NIGHT_COUNT_PARAM"));
        String adultsValue =            System.getenv("ADULTS_PARAM");
        String starsValues =            System.getenv("STARS_PARAM");
        String mealValues =             System.getenv("MEAL_PARAM");
        boolean isInstantConfirmation = Boolean.parseBoolean(System.getenv("INSTANT_CONFIRMATION_PARAM"));
        String recipientValue =         System.getenv("RECIPIENTS_PARAM");

//        //Local
//        String countryToValue = "Турция";
//        int dateCounter = 90;
//        int nightsValue = 7;
//        String adultsValue = "2";
//        String starsValues = "-";
//        String mealValues = "-";
//        boolean isInstantConfirmation = false;
//        String recipientValue = "olena.tyshchenko@yandex.ru, konstantin.prilepa@travelata.ru";

        FileInputStream fsIP = new FileInputStream(new File(reportName));
        Workbook book = new HSSFWorkbook(fsIP);

//        while (book.getNumberOfSheets() > 0)
//            book.removeSheetAt(0);

        ArrayList<String> kidsAgesList = new ArrayList<>(Arrays.asList(kidsAges.split("\\s*,\\s*")));
        ArrayList<String> starsList = new ArrayList<>();
        ArrayList<String> mealList = new ArrayList<>();
        if(!starsValues.equals("-"))
            starsList = new ArrayList<>(Arrays.asList(starsValues.split("\\s*,\\s*")));
        if(!mealValues.equals("-"))
            mealList = new ArrayList<>(Arrays.asList(mealValues.split("\\s*,\\s*")));

        int currentDateCounter = 0;
        int actualNightsCounterValue = 0;
        String countryEngValue = "";

        ArrayList<String> countriesList = new ArrayList<String>(Arrays.asList(countryToValue.split("\\s*,\\s*")));

        for (int countryCounter = 0; countryCounter < countriesList.size(); countryCounter++) {

            countryEngValue += destinationCountryEng.get(countriesList.get(countryCounter)) + " ";

            for (int departureCounter = 0; departureCounter < departureBG.length; departureCounter ++) {

                //BG part
                int listCounter;

                BGMainPage
                        .getBGMainPage()
                        .gotoBGMainPage()
                        .gotoBGTourPage();


                BGTourPage
                        .getBGTourPage()
                        .selectCityFrom(departureBG[departureCounter])
                        .selectCountryTo(countriesBG.get(countriesList.get(countryCounter)));

                currentDateCounter = 1;
                if (dateCounter > 7)
                    currentDateCounter = dateCounter - 7;
                int finishDateCounter = dateCounter + 7;
                while (currentDateCounter <= finishDateCounter) {
                    BGTourPage
                            .getBGTourPage()
                            .selectDateFrom(getSearchRussianDate(currentDateCounter))
                            .selectDateTo(getSearchRussianDate(currentDateCounter))
                            .clickWithFlightButton()
                            .waitForTourListsLoad();
                    if ($(byXpath(getEmptyListLink())).isDisplayed())
                        currentDateCounter++;
                    else
                        break;
                }

                if (currentDateCounter > finishDateCounter) {
                    setHotelsAmountBGList("0");
                    setDepartureCityBGList(departureBG[departureCounter]);
                    setActualDateCounterList(currentDateCounter);
                    setActualNightsCounterList(nightsValue);
                } else {
                    //By country
                    listCounter = 1;
                    while ($(byXpath(String.format(getListLink(), listCounter))).isDisplayed()) {

                        if (!ifSeveralPlacesListLink(listCounter) && ifBestPriceTourListLink(listCounter)) {
                            String oldTab = driver.getWindowHandle();

                            BGTourPage
                                    .getBGTourPage()
                                    .clickTourListLink(listCounter); // clickBestPriceTourListLink(listCounter);

                            String listTab = driver.getWindowHandle();
                            ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
                            newTab.remove(listTab);
                            driver.switchTo().window(newTab.get(0));

                            if(isEmptyPriceList()){
                                setHotelsAmountBGList("0");
                                setDepartureCityBGList(departureBG[departureCounter]);
                                setActualDateCounterList(currentDateCounter);
                                setActualNightsCounterList(nightsValue);
                            }
                            else{

                                actualNightsCounterValue = getFirstActiveNightsCounterValue(nightsValue);

                                BGTourListPage
                                        .getBGTourListPage()
                                        .selectNights(actualNightsCounterValue)
                                        .selectStars(starsList)
                                        .selectMeal(mealList)
                                        .clickInstantConfirm(isInstantConfirmation)
                                        .clickSubmitButton();
                                sleep(CONSTANT_1_SECOND);
                                if (getIsStarCorrespond() && getIsNightCorrespond())
                                    BGTourListPage
                                            .getBGTourListPage()
                                            .putHotelsAmountBGList();

                                driver.close();
                                driver.switchTo().window(oldTab);

                            }
                        }

                        listCounter++;
                    }

                    setHotelsAmountBGList(String.valueOf(getHotelNamesBGList().size()));
                    setDepartureCityBGList(departureBG[departureCounter]);
                    setActualDateCounterList(currentDateCounter);
                    setActualNightsCounterList(actualNightsCounterValue);

                    getHotelNamesBGList().clear();

                }

                sleep(Constants.CONSTANT_2_SECONDS);

                //Offer part
                OfferEmailAuthPage
                        .getOfferEmailAuthPage()
                        .gotoOfferEmailPage()
                        .enterOfferEmail(offerNameValue, offerPwdValue);

                OfferEmailSearchPage
                        .getOfferEmailSearchPage()
                        .gotoSearchCriteriaForDepartureCity(countriesList.get(countryCounter), currentDateCounter, false, actualNightsCounterValue, isFlexibleNights, adultsValue, kidsAgesList, departureBG[departureCounter], starsList, mealList, "-", operatorValue)
                        .putHotelsAmountOfferList();

                sleep(CONSTANT_1_SECOND);

            }

            ReportGenerator.getReportGenerator().generateRegionBGOfferHotelAmount(book, reportName, countriesList.get(countryCounter));

            getHotelsAmountBGList().clear();
            getDepartureCityBGList().clear();
            getActualDateCounterList().clear();
            getActualNightsCounterList().clear();

        }

        book.write(new FileOutputStream(reportName));
        book.close();

        Sender.sendMail(recipientValue, reportName, "BG region amount report " + dateCounter + " " + countryEngValue, countryEngValue);

    }

    @AfterTest

    @DataProvider
    public Object[][] tourCriteria(){
        return new Object[][]{
                {
                        "11",
                        false,
                        "0",
                        "olena.tyshchenko@travelata.ru",
                        "7174043502F70664",
                        "C:\\Reports\\BGRegionHotelsAmount.xls",
                }
        };
    }

}
