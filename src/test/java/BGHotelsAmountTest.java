import framework.ReportGenerator;
import framework.SeleniumTestCase;
import framework.Sender;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.Travelata.OfferEmailAuthPage;
import pageobjects.Travelata.OfferEmailSearchPage;
import pageobjects.operators.BiblioGlobus.BGMainPage;
import pageobjects.operators.BiblioGlobus.BGTourListPage;
import pageobjects.operators.BiblioGlobus.BGTourPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static framework.Constants.*;
import static framework.PercentageCounter.*;
import static framework.ReportGenerator.*;
import static pageobjects.operators.BiblioGlobus.BGTourListPage.*;
import static pageobjects.operators.BiblioGlobus.BGTourPage.*;

public class BGHotelsAmountTest extends SeleniumTestCase {

    @Test(dataProvider = "tourCriteria", enabled = true)
    public void BGHotelsAmountTest(String operatorValue, boolean isFlexibleDate, int nightsValue, boolean isFlexibleNights,
                                   String adultsValue, String kidsAgesValue,
                                   String offerNameValue, String offerPwdValue, String reportName) throws Exception{

        //Jenkins
        String cityFromValue =          System.getenv("CITY_PARAM");
        String countryToValue =         System.getenv("COUNTRY_PARAM");
        int dateCounter =               Integer.parseInt(System.getenv("DATE_COUNT_PARAM"));
        String starsValues =            System.getenv("STARS_PARAM");
        boolean isInstantConfirmation = Boolean.parseBoolean(System.getenv("INSTANT_CONFIRMATION_PARAM"));
        String recipientValue =         System.getenv("RECIPIENTS_PARAM");

//        //Local
//        String cityFromValue =            "Москва";
//        String countryToValue =           "Турция";
//        int dateCounter =                 15;
//        String starsValues =              "4";
//        boolean isInstantConfirmation =   false;
//        String recipientValue =           "olena.tyshchenko@yandex.ru";

        FileInputStream fsIP= new FileInputStream(new File(reportName));
        Workbook book = new HSSFWorkbook(fsIP);

        while(book.getNumberOfSheets()>0)
            book.removeSheetAt(0);

        ArrayList<String> kidsAgesList = new ArrayList<>(Arrays.asList(kidsAgesValue.split("\\s*,\\s*")));
        ArrayList<String> starsList = new ArrayList<>();
        if(!starsValues.equals("-"))
            starsList = new ArrayList<>(Arrays.asList(starsValues.split("\\s*,\\s*")));

        //BG part
        int listCounter;
        BGMainPage
                .getBGMainPage()
                .gotoBGMainPage()
                .gotoBGTourPage();


        BGTourPage
                .getBGTourPage()
                .selectCityFrom(cityFromValue)
                .selectCountryTo(countryToValue)
                .selectDateFrom(getSearchRussianDate(dateCounter))
                .selectDateTo(getSearchRussianDate(dateCounter))
                .clickWithFlightButton()
                .waitForTourListsLoad();

        //By country
        if($(byXpath(getEmptyListLink())).isDisplayed()){
            setHotelNamesBGList("-");
            setCountryResortBGList(countryToValue);
            setHotelTourPriceBGList("-");
        }
        else{
            listCounter = 1;
            while ($(byXpath(String.format(getListLink(), listCounter))).isDisplayed()){

                if(ifBestPriceTourListLink(listCounter)){
                    String oldTab = driver.getWindowHandle();

                    BGTourPage
                            .getBGTourPage()
                            .clickBestPriceTourListLink(listCounter);

                    String listTab = driver.getWindowHandle();
                    ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
                    newTab.remove(listTab);
                    driver.switchTo().window(newTab.get(0));

                    if(isEmptyPriceList()){
                        setHotelNamesBGList("-");
                        setCountryResortBGList(countryToValue);
                        setHotelTourPriceBGList("-");
                    }

                    else{

                        BGTourListPage
                                .getBGTourListPage()
                                .selectNights(nightsValue)
                                .clickInstantConfirm(isInstantConfirmation)
                                .clickSubmitButton()
                                .putHotelNamesBGList();

                        driver.close();
                        driver.switchTo().window(oldTab);

                    }
                }

                listCounter ++;
            }
        }


//        //By resort
//        for(int resortCounter = 0; resortCounter < resortsBGTurkey.length; resortCounter ++){
//            int hotelBGCounter = 0;
//            BGTourPage
//                    .getBGTourPage()
//                    .selectResortTo(countryToValue, resortsBGTurkey[resortCounter])
//                    .clickWithFlightButton();
//
//            if($(byXpath(getEmptyListLink())).isDisplayed()){
//                setHotelNamesBGList("-");
//                setCountryResortBGList("-");
//                setHotelTourPriceBGList("-");
//            }
//            else{
//                listCounter = 1;
//                while ($(byXpath(String.format(getListLink(), listCounter))).isDisplayed()){
//
//                    if(ifBestPriceTourListLink(listCounter)){
//                        String oldTab = driver.getWindowHandle();
//
//                        BGTourPage
//                                .getBGTourPage()
//                                .clickBestPriceTourListLink(listCounter);
//
//                        String listTab = driver.getWindowHandle();
//                        ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
//                        newTab.remove(listTab);
//                        driver.switchTo().window(newTab.get(0));
//
//                        hotelBGCounter =
//                        BGTourListPage
//                                .getBGTourListPage()
//                                .selectNights(nightsValue)
//                                .clickSubmitButton()
//                                .putHotelNamesBGList(countryToValue, resortsBGOfferTurkey.get(resortsBGTurkey[resortCounter])); //
//
//                        driver.close();
//                        driver.switchTo().window(oldTab);
//                    }
//
//                    listCounter ++;
//                }
//            }
//            setHotelTourBGAmountList((double) hotelBGCounter);
//        }

        //Offer Part
        //By country
        OfferEmailAuthPage
                .getOfferEmailAuthPage()
                .gotoOfferEmailPage()
                .enterOfferEmail(offerNameValue, offerPwdValue);
        OfferEmailSearchPage
                .getOfferEmailSearchPage()
                .gotoSearchCriteria(countryToValue, dateCounter, isFlexibleDate, nightsValue, isFlexibleNights, adultsValue, kidsAgesList, cityFromValue, starsList, "-", operatorValue)
                .putHotelNamesOfferListWithResort(countryToValue) //putHotelNamesOfferList(countryToValue, "-")
                ;

        //By resort
//        for(int resortCounter = 0; resortCounter < resortsBGTurkey.length; resortCounter ++){
//            OfferEmailSearchPage
//                    .getOfferEmailSearchPage()
//                    .gotoSearchCriteria(countryToValue, dateCounter, isFlexibleDate, nightsValue, isFlexibleNights, adultsValue, kidsAges, cityFromValue, starsValues, resortOffer.get(resortsBGOfferTurkey.get(resortsBGTurkey[resortCounter])), operatorValue)
//                    .putHotelNamesOfferList(countryToValue, resortsBGOfferTurkey.get(resortsBGTurkey[resortCounter]))
//                    ;
//        }

        ReportGenerator.getReportGenerator().generateReportBGOffer(book, reportName, countryToValue, cityFromValue, dateCounter, nightsValue);

        book.write(new FileOutputStream(reportName));
        book.close();

        Sender.sendMail(recipientValue, reportName, "BG report + " + dateCounter, countryToValue);

    }


    @AfterTest

    @DataProvider
    public Object[][] tourCriteria(){
        return new Object[][]{
                {
                        "11",
                        false,
                        7,
                        false,
                        "2",
                        "0",
                        "olena.tyshchenko@travelata.ru",
                        "7174043502F70664",
                        "C:\\Reports\\BGHotelsAmount.xls",
                }
        };
    }

}
