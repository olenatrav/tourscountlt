import framework.ReportGenerator;
import framework.SeleniumTestCase;
import framework.Sender;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.Travelata.OfferEmailAuthPage;
import pageobjects.Travelata.OfferEmailSearchPage;
import pageobjects.operators.Anex.AnexMainPage;
import pageobjects.operators.Anex_new.AnexAuthPage;
import pageobjects.operators.Anex_new.AnexSearchPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import static com.codeborne.selenide.Selenide.sleep;
import static framework.Constants.*;
import static framework.ReportGenerator.getSearchRussianDate;
import static pageobjects.Travelata.OfferEmailSearchPage.getHotelsAmountOfferList;
import static pageobjects.operators.Anex_new.AnexSearchPage.*;
import static pageobjects.operators.Anex_new.AnexSearchPage.setActualNightsCounterList;

public class AnexRegionHotelsAmountTest extends SeleniumTestCase {

    @Test(dataProvider = "tourCriteria", enabled = true)
    public void AnexRegionHotelsAmountTest(String operatorValue, boolean isFlexibleNights, String kidsAges,
                                         String offerNameValue, String offerPwdValue, String reportName) throws Exception{ //String anexNameValue, String anexPwdValue,

        //Jenkins
        String countryToValue =           System.getenv("COUNTRY_PARAM");
        int dateCounter =                 Integer.parseInt(System.getenv("DATE_COUNT_PARAM"));
        int nightsValue =                 Integer.parseInt(System.getenv("NIGHT_COUNT_PARAM"));
        String adultsValue =              System.getenv("ADULTS_PARAM");
        String starsValues =              System.getenv("STARS_PARAM");
        String mealValues =               System.getenv("MEAL_PARAM");
        boolean isInstantConfirmation =   Boolean.parseBoolean(System.getenv("INSTANT_CONFIRMATION_PARAM"));
        String recipientValue =           System.getenv("RECIPIENTS_PARAM");

//        //Local
//        String countryToValue =         "Египет, Турция";
//        int dateCounter =               15;
//        int nightsValue =               7;
//        String adultsValue =            "2";
//        String starsValues =            "-";
//        String mealValues =             "-";
//        boolean isInstantConfirmation = false;
//        String recipientValue =         "olena.tyshchenko@yandex.ru, konstantin.prilepa@travelata.ru";

        FileInputStream fsIP= new FileInputStream(new File(reportName));
        Workbook book = new HSSFWorkbook(fsIP);

        while(book.getNumberOfSheets()>0)
            book.removeSheetAt(0);

        ArrayList<String> kidsAgesList = new ArrayList<>(Arrays.asList(kidsAges.split("\\s*,\\s*")));
        ArrayList<String> starsList = new ArrayList<>();
        ArrayList<String> mealList = new ArrayList<>();
        if(!starsValues.equals("-"))
            starsList = new ArrayList<>(Arrays.asList(starsValues.split("\\s*,\\s*")));
        if(!mealValues.equals("-"))
            mealList = new ArrayList<>(Arrays.asList(mealValues.split("\\s*,\\s*")));

        String countryEngValue = "";

//////////Anex lk
//        AnexAuthPage
//                .getAnexAuthPage()
//                .gotoAnexAuthPage()
//                .loginAnexLKPage(anexNameValue, anexPwdValue);

        ArrayList<String> countriesList = new ArrayList<String>(Arrays.asList(countryToValue.split("\\s*,\\s*")));

        for (int countryCounter = 0; countryCounter < countriesList.size(); countryCounter++) {

            countryEngValue += destinationCountryEng.get(countriesList.get(countryCounter)) + " ";

            for (int departureCounter = 0; departureCounter < departureAnex.length; departureCounter++) {

                AnexMainPage
                        .getAnexMainPage()
                        .gotoAnexCountryMainPage(countriesList.get(countryCounter))
                        .selectCityFrom(departureAnex[departureCounter])
                        .selectCountryTo(countriesList.get(countryCounter))
                        .selectFlightDate(getSearchRussianDate(dateCounter))
                        .selectNights(String.valueOf(nightsValue))
                        .clickSearchButton();

                pageobjects.operators.Anex.AnexSearchPage
                        .getAnexSearchPage()
                        .putHotelsAmountAnexList(departureAnex[departureCounter], dateCounter, nightsValue);

//////////////Anex lk
//            actualDateCounterValue = 0;
//            emptySearch = true;
//            actualNightsCounterValue = 0;
//
//            //Anex Part
//            AnexAuthPage
//                    .getAnexAuthPage()
//                    .gotoAnexAuthPage()
//                    .gotoAnexSearchPage();
//            AnexSearchPage
//                    .getAnexSearchPage()
//                    .selectCityFrom(departureAnex[departureCounter])
//                    .openCountryToSelect(countryToValue);
//            if (isCountryActive(countryToValue)) {
//                emptySearch = false;
//                AnexSearchPage
//                        .getAnexSearchPage()
//                        .selectCountryTo(countryToValue)
//                        .openCalendar();
//                actualDateCounterValue = getFirstActiveDateCounterValue(dateCounter);
//
//                if (actualDateCounterValue != 0) {
//                    AnexSearchPage
//                            .getAnexSearchPage()
//                            .selectFlightDate(getSearchRussianDate(actualDateCounterValue));
//                    actualNightsCounterValue = getFirstActiveNightsCounterValue(nightsValue);
//
//                    AnexSearchPage
//                            .getAnexSearchPage()
//                            .selectNightsTo(actualNightsCounterValue)
//                            .selectNightsFrom(actualNightsCounterValue)
//                            .selectMeal(mealList)
//                            .selectStars(starsList)
//                            .clickInstantConfirmButton(isInstantConfirmation)
//                            .clickSubmitSearchButton()
//                            .waitForSearchResult();
//
//                    //By country
//                    if (!ifNoToursResult()) {
//                        AnexSearchPage
//                                .getAnexSearchPage()
//                                .clickGroupByHotelCheckbox()
//                                .putHotelsAmountAnexList(departureAnex[departureCounter], actualDateCounterValue, actualNightsCounterValue);
//                    } else
//                        emptySearch = true;
//                } else
//                    emptySearch = true;
//            } else
//                emptySearch = true;
//
//            if (emptySearch) {
//                setHotelsAmountAnexList("-");
//                setDepartureCityAnexList(departureAnex[departureCounter]);
//                setActualDateCounterList(dateCounter);
//                setActualNightsCounterList(nightsValue);
//            }

                //Offer part
                OfferEmailAuthPage
                        .getOfferEmailAuthPage()
                        .gotoOfferEmailPage()
                        .enterOfferEmail(offerNameValue, offerPwdValue);

                OfferEmailSearchPage
                        .getOfferEmailSearchPage()
                        .gotoSearchCriteriaForDepartureCity(countriesList.get(countryCounter), dateCounter, false, nightsValue, isFlexibleNights, adultsValue, kidsAgesList, departureAnex[departureCounter], starsList, mealList, "-", operatorValue)
                        .putHotelsAmountOfferList();

                sleep(CONSTANT_1_SECOND);

            }

            ReportGenerator.getReportGenerator().generateRegionAnexOfferHotelAmount(book, reportName, countriesList.get(countryCounter));

            AnexSearchPage.getActualDateCounterList().clear();
            AnexSearchPage.getActualNightsCounterList().clear();
            getDepartureCityAnexList().clear();
            getHotelsAmountAnexList().clear();
            getHotelsAmountOfferList().clear();

        }

        book.write(new FileOutputStream(reportName));
        book.close();

        Sender.sendMail(recipientValue, reportName, "Anex region amount report " + dateCounter + " " + countryEngValue, countryEngValue);

    }

    @AfterTest

    @DataProvider
    public Object[][] tourCriteria(){
        return new Object[][]{
                {
                        "5",
                        false,
                        "0",
                        "olena.tyshchenko@travelata.ru",
                        "7174043502F70664",
                        "C:\\Reports\\AnexRegionHotelsAmount.xls",
                }
        };
    }

}
