import framework.PercentageCounter;
import framework.SeleniumTestCase;
import framework.Sender;
import net.lightbody.bmp.proxy.CaptureType;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.LevelTravel.LevelTravelSearchPage;
import pageobjects.Travelata.OfferEmailAuthPage;
import pageobjects.Travelata.OfferEmailSearchPage;
import pageobjects.Travelata.TravelataSearchPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import static framework.Constants.destinationCountryEng;
import static framework.PercentageCounter.getPercentageCounterList;
import static pageobjects.LevelTravel.LevelTravelSearchPage.*;
import static pageobjects.Travelata.OfferEmailSearchPage.*;

public class LTHotelsAmountTest extends SeleniumTestCase {

    @Test (dataProvider = "tourCriteria", enabled = true)
    public void GetToursStatisticTest(boolean isFlexibleNights, String reportPathValue) throws Exception{

        //Jenkins
        String departureValue =     System.getenv("CITY_PARAM");
        String countryValue =       System.getenv("COUNTRY_PARAM");
        String dateCounterValue =   System.getenv("DATE_COUNT_PARAM");
        boolean isFlexibleDate =    Boolean.parseBoolean(System.getenv("FLEXIBLE_DATE_PARAM"));
        int nightsValue =           Integer.parseInt(System.getenv("NIGHT_COUNT_PARAM"));
        String adultsValue =        System.getenv("ADULTS_PARAM");
        String kidsAges =           System.getenv("KIDS_AGES_PARAM");
        String starsValues =        System.getenv("STARS_PARAM");
        String recipientValue =     System.getenv("RECIPIENTS_PARAM");

//        //Local
//        String departureValue =     "Москва";
//        String countryValue =       "Тунис, Вьетнам"; //Египет, Турция, Россия, ОАЭ, Таиланд, Доминикана, Куба, Мексика, Венесуэла, Мальдивы, Шри-Ланка, Кипр, Греция, Абхазия
//        String dateCounterValue =   "60, 60"; //15, 15, 15, 15, 15, 14, 14, 14, 16, 15, 15, 15, 15, 15
//        boolean isFlexibleDate =    false;
//        int nightsValue =           7;
//        String adultsValue =        "2";
//        String kidsAges =           "0";
//        String starsValues =        "-";
//        String recipientValue =     "olena.tyshchenko@yandex.ru"; //konstantin.prilepa@travelata.ru

        FileInputStream fsIP= new FileInputStream(new File(reportPathValue));
        Workbook book = new HSSFWorkbook(fsIP);

//        while(book.getNumberOfSheets()>0)
//            book.removeSheetAt(0);

        ArrayList<String> departuresList = new ArrayList<>(Arrays.asList(departureValue.split("\\s*,\\s*")));
        ArrayList<String> dateCounterList = new ArrayList<>(Arrays.asList(dateCounterValue.split("\\s*,\\s*")));
        ArrayList<String> kidsAgesList = new ArrayList<>(Arrays.asList(kidsAges.split("\\s*,\\s*")));
        ArrayList<String> starsList = new ArrayList<>();
        if(!starsValues.equals("-"))
            starsList = new ArrayList<>(Arrays.asList(starsValues.split("\\s*,\\s*")));

        for (int departureCounter = 0; departureCounter < departuresList.size(); departureCounter ++){

            ArrayList<String> countriesList = new ArrayList<String>(Arrays.asList(countryValue.split("\\s*,\\s*")));
            String countryEngValue = "";
            for (int countryCounter=0; countryCounter<countriesList.size(); countryCounter++){

                try{
//                    if (book.getSheetIndex(departureValue + "-" + countriesList.get(countryCounter)) != -1)
//                        book.removeSheetAt(book.getSheetIndex(departureValue + "-" + countriesList.get(countryCounter)));

                    countryEngValue += destinationCountryEng.get(countriesList.get(countryCounter)) + " ";

                    LevelTravelSearchPage
                            .getLevelTravelSearchPage()
                            .getStatisticByTOFromFilter(book, countriesList.get(countryCounter), Integer.parseInt(dateCounterList.get(countryCounter)), isFlexibleDate,
                                    nightsValue, isFlexibleNights, adultsValue, kidsAgesList, departuresList.get(departureCounter), starsList, reportPathValue);
                    TravelataSearchPage
                            .getTravelataSearchPage()
                            .closeLotteryPopup()
                            .closeHRPopup()
                            .closeCashBackPopup()
                            .closePaylatePopup()
                            .getTravStatisticByTO(book, countriesList.get(countryCounter), Integer.parseInt(dateCounterList.get(countryCounter)), isFlexibleDate, nightsValue, isFlexibleNights,
                                    adultsValue, kidsAgesList, departuresList.get(departureCounter), starsList, reportPathValue);
                    PercentageCounter
                            .getPercentageCounter()
                            .countPercentage(book, countriesList.get(countryCounter), reportPathValue, departuresList.get(departureCounter));

                    getTOList().clear();
                    getHotelNamesSearchLTList().clear();
                    getHotelTourPricesSearchLTList().clear();
                    getHotelNamesOfferList().clear();
                    getCountryResortOfferList().clear();
                    getHotelTourPriceOfferList().clear();
                    getHotelTourPriceActualizedOfferList().clear();
                    getPercentageCounterList().clear();
                }
                catch (Exception e){
                    System.out.println("Error with country " + destinationCountryEng.get(countriesList.get(countryCounter)));
                }

                refreshPage();

            }

//            ArrayList<Integer> sheetsToDelList = new ArrayList<>();
//            for(int sheetCounter = 0; sheetCounter < book.getNumberOfSheets(); sheetCounter ++){
//                boolean isSheetToDel = true;
//                for(int countryCounter=0; countryCounter<countriesList.size(); countryCounter++){
//                    String s = book.getSheetAt(sheetCounter).getSheetName();
//                    s = departureValue + "-" + countriesList.get(countryCounter);
//                    if(book.getSheetAt(sheetCounter).getSheetName().equals(departureValue + "-" + countriesList.get(countryCounter)))
//                        isSheetToDel = false;
//                }
//                if(isSheetToDel)
//                    sheetsToDelList.add(sheetCounter);
//            }
//            for(int i=0; i<sheetsToDelList.size(); i++){
//                book.removeSheetAt(sheetsToDelList.get(i));
//            }

            book.write(new FileOutputStream(reportPathValue));
            book.close();

            Sender.sendMail(recipientValue, reportPathValue, "SER quality report " + countryEngValue + " ", countryEngValue);

        }

    }


    @AfterTest

    @DataProvider
    public Object[][] tourCriteria(){
        return new Object[][]{
                {
                     false,
                     "C:\\Reports\\TourStatistics.xls",
                }
        };
    }

}
