import framework.Constants;
import framework.ReportGenerator;
import framework.SeleniumTestCase;
import framework.Sender;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.Travelata.OfferEmailAuthPage;
import pageobjects.Travelata.OfferEmailSearchPage;
import pageobjects.operators.Alean.AleanHotelPage;
import pageobjects.operators.Alean.AleanMainPage;
import pageobjects.operators.Alean.AleanTourPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static framework.Constants.*;
import static framework.ReportGenerator.getSearchDateNoDots;
import static pageobjects.operators.Alean.AleanMainPage.*;
import static pageobjects.operators.Alean.AleanMainPage.getAleanToursCounterValue;
import static pageobjects.operators.Alean.AleanTourPage.setHotelTourPriceActualizedAleanList;

public class AleanHotelsAmountTest extends SeleniumTestCase {

    @Test(dataProvider = "tourCriteria", enabled = true)
    public void AleanHotelsAmountTest(String operatorValue, boolean isFlexibleDate, int nightsValue, boolean isFlexibleNights,
                                      String adultsValue, String kidsAgesValue,
                                      String offerNameValue, String offerPwdValue, String reportName) throws Exception{

        //Jenkins
        String cityFromValue =          System.getenv("CITY_PARAM");
        String countryToValue =         System.getenv("COUNTRY_PARAM");
        int dateCounter =               Integer.parseInt(System.getenv("DATE_COUNT_PARAM"));
        String starsValues =            System.getenv("STARS_PARAM");
        boolean isInstantConfirmation = Boolean.parseBoolean(System.getenv("INSTANT_CONFIRMATION_PARAM"));
        String recipientValue =         System.getenv("RECIPIENTS_PARAM");

//        //Local
//        String cityFromValue =            "Москва";
//        String countryToValue =           "Россия";
//        int dateCounter =                 15;
//        String starsValues =              "4";
//        boolean isInstantConfirmation =   false;
//        String recipientValue =           "olena.tyshchenko@yandex.ru";

//        reportName += dateCounter + ".xls";

        FileInputStream fsIP= new FileInputStream(new File(reportName));
        Workbook book = new HSSFWorkbook(fsIP);

        while(book.getNumberOfSheets()>0)
            book.removeSheetAt(0);

        ArrayList<String> kidsAgesList = new ArrayList<>(Arrays.asList(kidsAgesValue.split("\\s*,\\s*")));
        ArrayList<String> starsList = new ArrayList<>();
        if(!starsValues.equals("-"))
            starsList = new ArrayList<>(Arrays.asList(starsValues.split("\\s*,\\s*")));

        //Alean part
        //By resort
        AleanMainPage
                .getAleanMainPage()
                .gotoAleanMainPage()
                .closeMailingPopup()
                .clickTourTab()
                ;

        for (int resortCounter = 0; resortCounter < countryResortsAlean.get(countryToValue).length; resortCounter ++){

            int tourCounter = 0;

            AleanMainPage
                    .getAleanMainPage()
                    .getToursAmountByTourCriteria(cityFromValue, countryResortsAlean.get(countryToValue)[resortCounter], getSearchDateNoDots(dateCounter), getSearchDateNoDots(dateCounter + nightsValue))
                    .clickInstantConfirmationCheckbox(isInstantConfirmation)
            ;

            if (ifEmptyAleanSearch()){
                setHotelNamesAleanList("-");
                setCountryResortAleanList(countryToValue);
                setHotelTourPriceAleanList("-");
                setHotelTourPriceActualizedAleanList("-");
            }
            else{
                String oldTab = driver.getWindowHandle();
                clickSortByAsc();
                int hotelCounterSpot = 0;
                do{
                    int hotelCounter = 1;
                    while ($(byXpath(String.format(getTourBlock(), hotelCounter))).isDisplayed()){
                        putHotelNamePriceAleanList(countryResortsAlean.get(countryToValue)[resortCounter], hotelCounter);
                        tourCounter ++;
                        hotelCounterSpot ++;

                        //Actualization
//                        AleanMainPage
//                                .getAleanMainPage()
//                                .gotoAleanHotelPage(hotelCounter)
//                        ;
//
//                        String hotelTab = driver.getWindowHandle();
//                        ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
//                        newTab.remove(hotelTab);
//                        driver.switchTo().window(newTab.get(0));
//
//                        AleanHotelPage
//                                .getAleanHotelPage()
//                                .waitForProgressBarDisappear()
//                        ;
//
//                        if (ifNoToursFound())
//                            setHotelTourPriceActualizedAleanList("-");
//                        else if(ifNoPossibleToOrder())
//                            setHotelTourPriceActualizedAleanList("-");
//                        else{
//                            AleanHotelPage
//                                    .getAleanHotelPage()
//                                    .gotoAleanTourPage()
//                            ;
//
//                            driver.close();
//                            newTab = new ArrayList<String>(driver.getWindowHandles());
//                            sleep(Constants.CONSTANT_1_SECOND);
//                            newTab.remove(oldTab);
//                            driver.switchTo().window(newTab.get(0));
//
//                            AleanTourPage
//                                    .getAleanTourPage()
//                                    .waitForActualizationFinish()
//                                    .putFinalAleanPrice();
//
//                        }
//
//                        sleep(Constants.CONSTANT_1_SECOND);
//                        driver.close();
//                        driver.switchTo().window(oldTab);
                        //////////////////////////////////////////////

                        hotelCounter ++;
                    }

                    if($(byXpath(getNextPageButton())).isDisplayed())
                        $(byXpath(getNextPageButton())).click();
                }
                while ((!(Integer.parseInt(getAleanToursCounterValue()) >= hotelCounterSpot && $(byXpath(getNextPageButtonDisabled())).isDisplayed())) && ($(byXpath(getPaginationPanel())).exists()));
            }
            setHotelTourAleanAmountList((double) tourCounter);

        }

        //Offer Part
        //By resort
        for (int resortCounter = 0; resortCounter < countryResortsAlean.get(countryToValue).length; resortCounter ++){
            OfferEmailAuthPage
                    .getOfferEmailAuthPage()
                    .gotoOfferEmailPage()
                    .enterOfferEmail(offerNameValue, offerPwdValue);
            OfferEmailSearchPage
                    .getOfferEmailSearchPage()
                    .gotoSearchCriteria(countryToValue, dateCounter, isFlexibleDate, nightsValue, isFlexibleNights, adultsValue, kidsAgesList, cityFromValue, starsList, resortOffer.get(countryResortsAlean.get(countryToValue)[resortCounter]), operatorValue)
                    .putHotelNamesOfferList(countryToValue, countryResortsAlean.get(countryToValue)[resortCounter])
            ;
        }

        ReportGenerator.getReportGenerator().generateReportAleanOffer(book, reportName, countryToValue, cityFromValue, dateCounter, countryResortsAlean.get(countryToValue));

        book.write(new FileOutputStream(reportName));
        book.close();

        Sender.sendMail(recipientValue, reportName, "Alean amount report + " + dateCounter, countryToValue);

    }

    @DataProvider
    public Object[][] tourCriteria(){
        return new Object[][]{
                {
                        "122",
                        false,
                        7,
                        false,
                        "2",
                        "0",
                        "olena.tyshchenko@travelata.ru",
                        "7174043502F70664",
                        "C:\\Reports\\AleanHotelsAmount.xls",
                }
        };
    }

}
