import framework.Constants;
import framework.ReportGenerator;
import framework.SeleniumTestCase;
import framework.Sender;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.Travelata.OfferEmailAuthPage;
import pageobjects.Travelata.OfferEmailSearchPage;
import pageobjects.operators.BiblioGlobus.BGMainPage;
import pageobjects.operators.BiblioGlobus.BGTourListPage;
import pageobjects.operators.BiblioGlobus.BGTourPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static framework.Constants.CONSTANT_1_SECOND;
import static framework.Constants.countriesBG;
import static framework.JDBCPostgreSQLConnection.*;
import static framework.ReportGenerator.getSearchRussianDate;
import static pageobjects.Travelata.OfferEmailSearchPage.getHotelIdOfferList;
import static pageobjects.operators.BiblioGlobus.BGTourListPage.*;
import static pageobjects.operators.BiblioGlobus.BGTourPage.*;

public class BGTourPricesTest extends SeleniumTestCase {

    @Test(dataProvider = "tourCriteria", enabled = true)
    public void BGTourPricesTest(String operatorValue, boolean isFlexibleNights, String kidsAges,
                                 String offerNameValue, String offerPwdValue, String reportName) throws Exception{

        //Jenkins
        String cityFromValue =          System.getenv("CITY_PARAM");
        String countryToValue =         System.getenv("COUNTRY_PARAM");
        int dateCounter =               Integer.parseInt(System.getenv("DATE_COUNT_PARAM"));
        boolean isFlexibleDate =        Boolean.parseBoolean(System.getenv("FLEXIBLE_DATE_PARAM"));
        int nightsValue =               Integer.parseInt(System.getenv("NIGHT_COUNT_PARAM"));
        String adultsValue =            System.getenv("ADULTS_PARAM");
        String starsValues =            System.getenv("STARS_PARAM");
        String mealValues =             System.getenv("MEAL_PARAM");
        boolean isInstantConfirmation = Boolean.parseBoolean(System.getenv("INSTANT_CONFIRMATION_PARAM"));
        String recipientValue =         System.getenv("RECIPIENTS_PARAM");

//        //Local
//        String cityFromValue =            "Москва";
//        String countryToValue =           "Турция";
//        int dateCounter =                 90;
//        boolean isFlexibleDate =          false;
//        int nightsValue =                 7;
//        String adultsValue =              "2";
//        String starsValues =              "-";
//        String mealValues =               "AI";
//        boolean isInstantConfirmation =   false;
//        String recipientValue =           "olena.tyshchenko@yandex.ru";

        FileInputStream fsIP= new FileInputStream(new File(reportName));
        Workbook book = new HSSFWorkbook(fsIP);

        while(book.getNumberOfSheets()>0)
            book.removeSheetAt(0);

        ArrayList<String> kidsAgesList = new ArrayList<>(Arrays.asList(kidsAges.split("\\s*,\\s*")));
        ArrayList<String> starsList = new ArrayList<>();
        ArrayList<String> mealList = new ArrayList<>();
        if(!starsValues.equals("-"))
            starsList = new ArrayList<>(Arrays.asList(starsValues.split("\\s*,\\s*")));
        if(!mealValues.equals("-"))
            mealList = new ArrayList<>(Arrays.asList(mealValues.split("\\s*,\\s*")));

            //BG part
            int listCounter;

            BGMainPage
                    .getBGMainPage()
                    .gotoBGMainPage()
                    .gotoBGTourPage();

            BGTourPage
                    .getBGTourPage()
                    .selectCityFrom(cityFromValue)
                    .selectCountryTo(countriesBG.get(countryToValue))
                    .selectDateFrom(getSearchRussianDate(dateCounter))
                    .selectDateTo(getSearchRussianDate(dateCounter))
                    .clickWithFlightButton()
                    .waitForTourListsLoad();

            //By country
            if ($(byXpath(getEmptyListLink())).isDisplayed()) {
                setHotelNamesBGList("-");
                setHotelStarsBGList("-");
                setCountryResortBGList(countryToValue);
                setHotelTourPriceBGList("-");
                setIsBestPriceBGList("-");
            } else {
                listCounter = 1;
                while ($(byXpath(String.format(getListLink(), listCounter))).isDisplayed()) {

                    String isPriceListValue = String.valueOf(ifBestPriceTourListLink(listCounter));
                    if ((!ifSeveralPlacesListLink(listCounter)) && (ifBestPriceTourListLink(listCounter))) { // ifBestPriceTourListLink(listCounter)
                        String oldTab = driver.getWindowHandle();

                        BGTourPage
                                .getBGTourPage()
                                .clickTourListLink(listCounter); // clickBestPriceTourListLink(listCounter);

                        String listTab = driver.getWindowHandle();
                        ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
                        newTab.remove(listTab);
                        driver.switchTo().window(newTab.get(0));

                        if(isEmptyPriceList()){

                            setHotelNamesBGList("-");
                            setHotelStarsBGList("-");
                            setCountryResortBGList(countryToValue);
                            setHotelTourPriceBGList("-");
                            setIsBestPriceBGList("-");

                        }
                        else{

                            BGTourListPage
                                    .getBGTourListPage()
                                    .selectNights(nightsValue)
                                    .selectStars(starsList)
                                    .selectMeal(mealList)
                                    .clickInstantConfirm(isInstantConfirmation)
                                    .clickSubmitButton();
                            sleep(CONSTANT_1_SECOND);
                            if (getIsStarCorrespond() && getIsNightCorrespond())
                                BGTourListPage
                                        .getBGTourListPage()
                                        .putHotelClearNamesBGList(isPriceListValue);

                            driver.close();
                            driver.switchTo().window(oldTab);

                        }
                    }

                    listCounter++;
                }
            }

            sleep(Constants.CONSTANT_2_SECONDS);
            getBGOfferHotelId(starsList, countryToValue);

            //Offer Part
            OfferEmailAuthPage
                    .getOfferEmailAuthPage()
                    .gotoOfferEmailPage()
                    .enterOfferEmail(offerNameValue, offerPwdValue);

            //By country
            if (!getHotelIdOfferList().isEmpty()) {
                for (int hotelCounter = 0; hotelCounter < getHotelIdOfferList().size(); hotelCounter++) {

                    if ((hotelCounter % 10) == 0){
                        refreshPage();
                        System.out.println("Page reloading");
                        OfferEmailAuthPage
                                .getOfferEmailAuthPage()
                                .enterOfferEmail(offerNameValue, offerPwdValue);
                    }

                    OfferEmailSearchPage
                            .getOfferEmailSearchPage()
                            .gotoSearchCriteriaByTOWithHotel(countryToValue, dateCounter, isFlexibleDate, nightsValue, isFlexibleNights, adultsValue, kidsAgesList, cityFromValue, starsList, mealList, "-", operatorValue, getHotelIdOfferList().get(hotelCounter), hotelCounter)
                            .putHotelNamesOfferListByTOWithHotel(countryToValue, "-", operatorValue, getHotelIdOfferList().get(hotelCounter))
                    ;
                }
            }

            ReportGenerator.getReportGenerator().generateReportBGOffer(book, reportName, countryToValue, cityFromValue, dateCounter, nightsValue);

            book.write(new FileOutputStream(reportName));
            book.close();

            Sender.sendMail(recipientValue, reportName, "BG prices report + " + dateCounter, countryToValue);

    }

    @AfterTest

    @DataProvider
    public Object[][] tourCriteria(){
        return new Object[][]{
                {
                        "11",
                        false,
                        "0",
                        "olena.tyshchenko@travelata.ru",
                        "7174043502F70664",
                        "C:\\Reports\\BGTourPrices.xls",
                }
        };
    }

}
