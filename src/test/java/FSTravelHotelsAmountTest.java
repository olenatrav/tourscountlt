import framework.ReportGenerator;
import framework.SeleniumTestCase;
import framework.Sender;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.Travelata.OfferEmailAuthPage;
import pageobjects.Travelata.OfferEmailSearchPage;
import pageobjects.operators.FSTravel.FSTravelMainPage;
import pageobjects.operators.FSTravel.FSTravelSearchPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import static framework.ReportGenerator.getSearchDateDashFormat;
import static pageobjects.operators.FSTravel.FSTravelSearchPage.*;

public class FSTravelHotelsAmountTest extends SeleniumTestCase {

    @Test(dataProvider = "tourCriteria", enabled = true)
    public void FSTravelHotelsAmountTest(String operatorValue, boolean isFlexibleDate, int nightsValue, boolean isFlexibleNights,
                                     String adultsValue, String kidsAgesValue,
                                     String offerNameValue, String offerPwdValue, String reportName) throws Exception{

        //Jenkins
        String cityFromValue =          System.getenv("CITY_PARAM");
        String countryToValue =         System.getenv("COUNTRY_PARAM");
        int dateCounter =               Integer.parseInt(System.getenv("DATE_COUNT_PARAM"));
        String starsValues =            System.getenv("STARS_PARAM");
        String recipientValue =         System.getenv("RECIPIENTS_PARAM");

//        //Local
//        String cityFromValue =            "Москва";
//        String countryToValue =           "Турция";
//        int dateCounter =                 15;
//        String starsValues =              "4";
//        String recipientValue =           "olena.tyshchenko@yandex.ru";

        FileInputStream fsIP= new FileInputStream(new File(reportName));
        Workbook book = new HSSFWorkbook(fsIP);

        while(book.getNumberOfSheets()>0)
            book.removeSheetAt(0);

        ArrayList<String> kidsAgesList = new ArrayList<>(Arrays.asList(kidsAgesValue.split("\\s*,\\s*")));
        ArrayList<String> starsList = new ArrayList<>();
        if(!starsValues.equals("-"))
            starsList = new ArrayList<>(Arrays.asList(starsValues.split("\\s*,\\s*")));

        //FSTravel part
        FSTravelMainPage
                .getFSTravelMainPage()
                .gotoFSTravelMainPage()
                .selectCityFrom(cityFromValue);

        if(!FSTravelMainPage.isCountryDisplayed(countryToValue)){
            setHotelNamesFSTravelList("-");
            setCountryResortFSTravelList("-");
            setHotelTourPriceFSTravelList("-");
        }
        else{
            FSTravelMainPage
                    .getFSTravelMainPage()
                    .selectCountryTo(countryToValue)
                    .selectFlightDate(getSearchDateDashFormat(dateCounter))
                    .selectNightsValue(nightsValue)
                    .clickSearchButton();

            if(isEmptySearchResult())
            {
                setHotelNamesFSTravelList("-");
                setCountryResortFSTravelList("-");
                setHotelTourPriceFSTravelList("-");
            }
            else
                FSTravelSearchPage
                        .getFSTravelSearchPage()
                        .waitForSearchResults()
                        .selectStars(starsList)
                        .waitForSearchResults()
                        .putHotelNamesFSTravelList(countryToValue, false);
        }

        //Offer part
        OfferEmailAuthPage
                .getOfferEmailAuthPage()
                .gotoOfferEmailPage()
                .enterOfferEmail(offerNameValue, offerPwdValue);

            OfferEmailSearchPage
                    .getOfferEmailSearchPage()
                    .gotoSearchCriteria(countryToValue, dateCounter, isFlexibleDate, nightsValue, isFlexibleNights, adultsValue, kidsAgesList, cityFromValue, starsList, "-", operatorValue)
                    .putHotelNamesOfferListWithResort(countryToValue)
            ;


        ReportGenerator.getReportGenerator().generateReportFSTravelOfferAmount(book, reportName, countryToValue, cityFromValue, dateCounter);

        book.write(new FileOutputStream(reportName));
        book.close();

        Sender.sendMail(recipientValue, reportName, "FSTravel amount report + " + dateCounter, countryToValue);

    }

    @AfterTest

    @DataProvider
    public Object[][] tourCriteria(){
        return new Object[][]{
                {
                        "55",
                        false,
                        7,
                        false,
                        "2",
                        "0",
                        "olena.tyshchenko@travelata.ru",
                        "7174043502F70664",
                        "C:\\Reports\\FSTravelHotelsAmount.xls",
                }
        };
    }

}
