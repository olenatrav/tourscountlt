import framework.Constants;
import framework.ReportGenerator;
import framework.SeleniumTestCase;
import framework.Sender;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.LevelTravel.LevelTravelHotelPage;
import pageobjects.LevelTravel.LevelTravelSearchPage;
import pageobjects.LevelTravel.LevelTravelTourPage;
import pageobjects.Travelata.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static framework.Constants.*;
import static framework.ReportGenerator.getFinalWithOilTaxSerpWithOilTaxLTRatioList;
import static framework.ReportGenerator.getFinalWithOilTaxSerpWithOilTaxTravRatioList;
import static pageobjects.LevelTravel.LevelTravelSearchPage.*;
import static pageobjects.LevelTravel.LevelTravelTourPage.*;
import static pageobjects.LevelTravel.LevelTravelTourPage.getHotelTourPriceWithoutOilTaxTourLTList;
import static pageobjects.Travelata.OfferEmailSearchPage.*;

public class LTTourPriceByTOTest extends SeleniumTestCase {

    @Test(dataProvider = "tourCriteria", enabled = true)
    public void ComparePricesTest(boolean isFlexibleNights, String offerNameValue,
                                  String offerPwdValue, String reportPathValue) throws Exception {

        //Jenkins
        String departureValue =     System.getenv("CITY_PARAM");
        String countryValue =       System.getenv("COUNTRY_PARAM");
        String dateCounterValue =   System.getenv("DATE_COUNT_PARAM");
        boolean isFlexibleDate =    Boolean.parseBoolean(System.getenv("FLEXIBLE_DATE_PARAM"));
        int nightsValue =           Integer.parseInt(System.getenv("NIGHT_COUNT_PARAM"));
        String adultsValue =        System.getenv("ADULTS_PARAM");
        String kidsAges =           System.getenv("KIDS_AGES_PARAM");
        String starsValues =        System.getenv("STARS_PARAM");
        String mealValues =         System.getenv("MEAL_PARAM");
        int hotelsCounter =         Integer.parseInt(System.getenv("HOTEL_COUNT_PARAM"));
        String recipientValue =     System.getenv("RECIPIENTS_PARAM");

//        //Local
//        String departureValue =     "Москва";
//        String countryValue =       "Турция";
//        String dateCounterValue =   "7";
//        boolean isFlexibleDate =    false;
//        int nightsValue =           7;
//        String adultsValue =        "2";
//        String kidsAges =           "0";
//        String starsValues =        "4";
//        String mealValues =         "AI";
//        int hotelsCounter =         10;
//        String recipientValue =     "olena.tyshchenko@yandex.ru";

        Workbook book;
        File file = new File(reportPathValue);
        FileInputStream fileInputStream = new FileInputStream(file);
        if (file.exists())
            book = WorkbookFactory.create(fileInputStream);
        else {
            if (file.getName().endsWith(".xls"))
                book = new HSSFWorkbook(fileInputStream);
            else if (file.getName().endsWith(".xlsx"))
                book = new XSSFWorkbook(fileInputStream);
            else
                throw new IllegalArgumentException("Unknown file extension");
        }

        while(book.getNumberOfSheets()>0)
            book.removeSheetAt(0);

        ArrayList<String> departuresList = new ArrayList<>(Arrays.asList(departureValue.split("\\s*,\\s*")));
        ArrayList<String> dateCounterList = new ArrayList<>(Arrays.asList(dateCounterValue.split("\\s*,\\s*")));
        ArrayList<String> kidsAgesList = new ArrayList<>(Arrays.asList(kidsAges.split("\\s*,\\s*")));
        ArrayList<String> starsList = new ArrayList<>();
        ArrayList<String> mealList = new ArrayList<>();
        if(!starsValues.equals("-"))
            starsList = new ArrayList<>(Arrays.asList(starsValues.split("\\s*,\\s*")));
        if(!mealValues.equals("-"))
            mealList = new ArrayList<>(Arrays.asList(mealValues.split("\\s*,\\s*")));

        for (int departureCounter = 0; departureCounter<departuresList.size(); departureCounter ++){

            ArrayList<String> countriesList = new ArrayList<String>(Arrays.asList(countryValue.split("\\s*,\\s*")));
            String countryEngValue = "";

            for (int countryCounter=0; countryCounter<countriesList.size(); countryCounter++) {

                try{
                    countryEngValue += destinationCountryEng.get(countriesList.get(countryCounter)) + " ";


                    for(int toCounter = 0; toCounter< TOPriceBy.length; toCounter++){

                        //Level Travel part
                        int hotelBlockPixelCounter = 0;
                        int i = 0;

                        LevelTravelSearchPage
                                .getLevelTravelSearchPage()
                                .gotoSearchPageWithTOCriteria(countriesList.get(countryCounter), Integer.parseInt(dateCounterList.get(countryCounter)), isFlexibleDate, nightsValue, isFlexibleNights, adultsValue, kidsAgesList, departuresList.get(departureCounter), starsList, mealList, TOLT.get(TOPriceBy[toCounter]));

                        if (ifEmptyLTSearch()) {
                            setHotelNamesSearchLTList("-");
                            setRegionLTList("-");
                            setHotelTourPricesSearchLTList("0");
                            setHotelTourPricesTourLTList("0");
                            setHotelTourPriceOilTaxTourLTList("0");
                            setHotelTourPriceWithoutOilTaxTourLTList("0");
                            setToNameTourLTList("-");
                            setRoomNameTourLTList("-");
                            setMealNameTourLTList("-");
                        }

                        else {
                            LevelTravelSearchPage
                                    .getLevelTravelSearchPage()
                                    .clickSortByPriceLink();

                            while (($(byXpath(String.format(getHotelBlockTopPx(), hotelBlockPixelCounter))).isDisplayed()) && (i < hotelsCounter)) {

                                boolean isUniqueHotel = true;
                                String oldTab = driver.getWindowHandle();
                                if (i > 0) {
                                    for (int hotelCounter = 0; hotelCounter < i; hotelCounter++) {
                                        if (getHotelNamesSearchLTList().get(hotelCounter).equals($(byXpath(String.format(getHotelBlockTopPx(), hotelBlockPixelCounter))).getText()))
                                            isUniqueHotel = false;
                                    }
                                }
                                if (isUniqueHotel) {

                                    WebElement currentHotelBlock = driver.findElement(By.xpath(String.format(getHotelBlockTopPx(), hotelBlockPixelCounter))); //getHotelStars(), hotelBlockCounter - 1
                                    Actions actions = new Actions(driver);
                                    actions.moveToElement(currentHotelBlock);
                                    actions.perform();
                                    scrollDownOnSomePxl(150);

                                    LevelTravelSearchPage
                                            .getLevelTravelSearchPage()
                                            .addCurrentHotelNameTourPricePx(hotelBlockPixelCounter)
                                            .gotoLevelTravelHotelPagePx(hotelBlockPixelCounter);

                                    String hotelTab = driver.getWindowHandle();
                                    ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
                                    newTab.remove(hotelTab);
                                    driver.switchTo().window(newTab.get(0));

                                    LevelTravelHotelPage
                                            .getLevelTravelHotelPage()
                                            .reloadIfErrorDisplayed()
                                            .waitHotelPageTourLoaded();

                                    if (LevelTravelHotelPage.getLevelTravelHotelPage().ifEmptyTours() ||
                                            LevelTravelHotelPage.getLevelTravelHotelPage().ifNoOffers(nightsValue)) {
                                        LevelTravelTourPage
                                                .getLevelTravelTourPage()
                                                .addEmptyTourTOPrice();
                                    } else {
                                        LevelTravelHotelPage
                                                .getLevelTravelHotelPage()
                                                .loadToursIfButtonDisplayed()
                                                .gotoLevelTravelTourPageByTO(TOPriceBy[toCounter], nightsValue);

                                        if (LevelTravelHotelPage.getLevelTravelHotelPage().ifTourOutdated()) {
                                            LevelTravelTourPage
                                                    .getLevelTravelTourPage()
                                                    .addEmptyTourTOPrice();
                                        } else {
                                            driver.close();
                                            newTab = new ArrayList<String>(driver.getWindowHandles());
                                            sleep(Constants.CONSTANT_1_SECOND);
                                            newTab.remove(oldTab);
                                            driver.switchTo().window(newTab.get(0));

                                            LevelTravelTourPage
                                                    .getLevelTravelTourPage()
                                                    .reloadIfErrorDisplayed()
                                                    .waitForLoaderDissapear()
                                                    .waitForCheckingFlights();
                                            if (LevelTravelTourPage.getLevelTravelTourPage().checkIfTourLTOutdated()) {
                                                LevelTravelTourPage
                                                        .getLevelTravelTourPage()
                                                        .addEmptyTourTOPrice();

                                            } else {
                                                LevelTravelTourPage
                                                        .getLevelTravelTourPage()
                                                        .addTourPrice()
                                                        .addTourTO()
                                                        .addTourRoomName()
                                                        .addTourMealName();
                                            }
                                        }
                                    }

                                    i++;
                                    sleep(Constants.CONSTANT_1_SECOND);
                                    driver.close();
                                    driver.switchTo().window(oldTab);

                                    scrollDownOnSomePxl(200);
                                    scrollUpOnSomePxl(200);
                                    hotelBlockPixelCounter += getHotelBlockHeightPx(hotelBlockPixelCounter);
                                }
                            }
                        }


                        //Offer part
                        OfferEmailAuthPage
                                .getOfferEmailAuthPage()
                                .gotoOfferEmailPage()
                                .enterOfferEmail(offerNameValue, offerPwdValue);

                        OfferEmailSearchPage
                                .getOfferEmailSearchPage()
                                .gotoSearchCriteriaByTO(countryValue, Integer.parseInt(dateCounterList.get(countryCounter)), isFlexibleDate, nightsValue, isFlexibleNights, adultsValue, kidsAgesList, departureValue, starsList, mealList, "-", TOOfferId.get(TOPriceBy[toCounter]))
                                .putHotelNamesOfferListByTO(countryValue, "-", hotelsCounter, TOOffer.get(TOPriceBy[toCounter]))
                        ;

                    }

                    ReportGenerator.getReportGenerator().generateReportLTOffer(book, countriesList.get(countryCounter), departuresList.get(departureCounter), Integer.parseInt(dateCounterList.get(countryCounter)), kidsAges);

                    getHotelNamesSearchLTList().clear();
                    getHotelTourPricesSearchLTList().clear();

                    getToNameTourLTList().clear();
                    getRoomNameTourLTList().clear();
                    getMealNameTourLTList().clear();
                    getHotelTourPricesTourLTList().clear();
                    getHotelTourPriceOilTaxTourLTList().clear();
                    getHotelTourPriceWithoutOilTaxTourLTList().clear();

                    getHotelNamesOfferList().clear();
                    getHotelNameRoomOfferList().clear();
                    getHotelNameMealOfferList().clear();
                    getCountryResortOfferList().clear();
                    getToNameOfferList().clear();
                    getHotelTourPriceOfferList().clear();
                    getHotelTourPriceActualizedOfferList().clear();

                    getFinalWithOilTaxSerpWithOilTaxLTRatioList().clear();
                    getFinalWithOilTaxSerpWithOilTaxTravRatioList().clear();

                    book.write(new FileOutputStream(reportPathValue));
                }
                catch (Exception e){
                    System.out.println("Error for country " + destinationCountryEng.get(countriesList.get(countryCounter)));
                }

                refreshPage();

            }

            book.close();
            fileInputStream.close();

            Sender.sendMail(recipientValue, reportPathValue, "Prices comparison by TO report " + countryEngValue + dateCounterValue + " ", countryEngValue);

        }

    }

    @AfterTest

    @DataProvider
    public Object[][] tourCriteria(){
        return new Object[][]{
                {
                        false,
                        "olena.tyshchenko@travelata.ru",
                        "7174043502F70664",
                        "C:\\Reports\\ComparePricesByTO.xls",
                }
        };
    }

}
