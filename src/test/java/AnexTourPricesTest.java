import framework.Constants;
import framework.ReportGenerator;
import framework.SeleniumTestCase;
import framework.Sender;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.Travelata.OfferEmailAuthPage;
import pageobjects.Travelata.OfferEmailSearchPage;
import pageobjects.operators.Anex_new.AnexAuthPage;
import pageobjects.operators.Anex_new.AnexSearchPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import static com.codeborne.selenide.Selenide.sleep;
import static framework.JDBCPostgreSQLConnection.getAnexOfferHotelId;
import static framework.ReportGenerator.getSearchRussianDate;
import static pageobjects.Travelata.OfferEmailSearchPage.getHotelIdOfferList;
import static pageobjects.operators.Anex_new.AnexSearchPage.*;
import static pageobjects.operators.Anex_new.AnexSearchPage.setHotelTourPriceAnexList;
import static pageobjects.operators.Anex_new.AnexTourPage.setHotelTourPriceActualizedAnexList;

public class AnexTourPricesTest extends SeleniumTestCase {

    @Test(dataProvider = "tourCriteria", enabled = true)
    public void AnexTourPricesTest(String operatorValue, boolean isFlexibleDate, boolean isFlexibleNights, String kidsAges, String anexNameValue, String anexPwdValue,
                                 String offerNameValue, String offerPwdValue, String reportName) throws Exception{

        //Jenkins
        String cityFromValue =          System.getenv("CITY_PARAM");
        String countryToValue =         System.getenv("COUNTRY_PARAM");
        int dateCounter =               Integer.parseInt(System.getenv("DATE_COUNT_PARAM"));
        int nightsValue =               Integer.parseInt(System.getenv("NIGHT_COUNT_PARAM"));
        String adultsValue =            System.getenv("ADULTS_PARAM");
        String starsValues =            System.getenv("STARS_PARAM");
        String mealValues =             System.getenv("MEAL_PARAM");
        boolean isInstantConfirmation = Boolean.parseBoolean(System.getenv("INSTANT_CONFIRMATION_PARAM"));
        String recipientValue =         System.getenv("RECIPIENTS_PARAM");

//        //Local
//        String cityFromValue =            "Москва";
//        String countryToValue =           "Египет";
//        int dateCounter =                 15;
//        int nightsValue =                 7;
//        String adultsValue =              "2";
//        String starsValues =              "4";
//        String mealValues =               "AI";
//        boolean isInstantConfirmation =   false;
//        String recipientValue =           "olena.tyshchenko@yandex.ru, konstantin.prilepa@travelata.ru";

        FileInputStream fsIP= new FileInputStream(new File(reportName));
        Workbook book = new HSSFWorkbook(fsIP);

        while(book.getNumberOfSheets()>0)
            book.removeSheetAt(0);

        ArrayList<String> kidsAgesList = new ArrayList<>(Arrays.asList(kidsAges.split("\\s*,\\s*")));
        ArrayList<String> starsList = new ArrayList<>();
        ArrayList<String> mealList = new ArrayList<>();
        if(!starsValues.equals("-"))
            starsList = new ArrayList<>(Arrays.asList(starsValues.split("\\s*,\\s*")));
        if(!mealValues.equals("-"))
            mealList = new ArrayList<>(Arrays.asList(mealValues.split("\\s*,\\s*")));

        //Anex Part
        AnexAuthPage
                .getAnexAuthPage()
                .gotoAnexAuthPage()
                .loginAnexLKPage(anexNameValue, anexPwdValue)
                .gotoAnexSearchPage();
        AnexSearchPage
                .getAnexSearchPage()
                .selectCityFrom(cityFromValue)
                .openCountryToSelect(countryToValue)
                .selectCountryTo(countryToValue)
                .openCalendar()
                .selectFlightDate(getSearchRussianDate(dateCounter))
                .selectNightsTo(nightsValue)
                .selectNightsFrom(nightsValue)
                .selectMeal(mealList)
                .selectStars(starsList)
                .clickInstantConfirmButton(isInstantConfirmation)
                .clickSubmitSearchButton()
                .waitForSearchResult();

        //By country
        if(ifNoToursResult())
        {
            setHotelNamesAnexList("-");
            setHotelStarsAnexList("-");
            setCountryResortAnexList("-");
            setHotelTourPriceAnexList("-");
            setHotelTourPriceActualizedAnexList("-");
        }
        else{
            AnexSearchPage
                    .getAnexSearchPage()
                    .putHotelNamesAnexList(true);
        }

        sleep(Constants.CONSTANT_2_SECONDS);
        getAnexOfferHotelId(starsList, countryToValue);

        //Offer Part
        OfferEmailAuthPage
                .getOfferEmailAuthPage()
                .gotoOfferEmailPage()
                .enterOfferEmail(offerNameValue, offerPwdValue);

        //By country
        if (!getHotelIdOfferList().isEmpty()) {
            for (int hotelCounter = 0; hotelCounter < getHotelIdOfferList().size(); hotelCounter++) {
                if ((hotelCounter % 10) == 0){
                    refreshPage();
                    System.out.println("Page reloading");
                    OfferEmailAuthPage
                            .getOfferEmailAuthPage()
                            .enterOfferEmail(offerNameValue, offerPwdValue);
                }
                OfferEmailSearchPage
                        .getOfferEmailSearchPage()
                        .gotoSearchCriteriaByTOWithHotel(countryToValue, dateCounter, isFlexibleDate, nightsValue, isFlexibleNights, adultsValue, kidsAgesList, cityFromValue, starsList, mealList, "-", operatorValue, getHotelIdOfferList().get(hotelCounter), hotelCounter)
                        .putHotelNamesOfferListByTOWithHotel(countryToValue, "-", operatorValue, getHotelIdOfferList().get(hotelCounter))
                ;
            }
        }

        ReportGenerator.getReportGenerator().generateReportAnexOffer(book, reportName, countryToValue, cityFromValue, dateCounter, nightsValue);

        book.write(new FileOutputStream(reportName));
        book.close();

        Sender.sendMail(recipientValue, reportName, "Anex report + " + dateCounter, countryToValue);

    }

    @AfterTest

    @DataProvider
    public Object[][] tourCriteria(){
        return new Object[][]{
                {
                        "5",
                        false,
                        false,
                        "0",
                        "Ttechnology",
                        "jT68$LyJ",
                        "olena.tyshchenko@travelata.ru",
                        "7174043502F70664",
                        "C:\\Reports\\AnexTourPrices.xls",
                }
        };
    }

}
