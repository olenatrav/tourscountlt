import framework.ReportGenerator;
import framework.SeleniumTestCase;
import framework.Sender;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.Travelata.OfferEmailAuthPage;
import pageobjects.Travelata.OfferEmailSearchPage;
import pageobjects.operators.Anex_new.AnexSearchPage;
import pageobjects.operators.Anex_new.AnexAuthPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import static framework.Constants.*;
import static framework.ReportGenerator.getSearchRussianDate;
import static pageobjects.operators.Anex_new.AnexSearchPage.*;

public class AnexHotelsAmountTest extends SeleniumTestCase {

    @Test(dataProvider = "tourCriteria", enabled = true)
    public void AnexHotelsAmountTest(String anexNameValue, String anexPwdValue, String operatorValue, boolean isFlexibleDate, int nightsValue, boolean isFlexibleNights,
                                    String adultsValue, String kidsAgesValue,
                                    String offerNameValue, String offerPwdValue, String reportName) throws Exception{

        //Jenkins
        String cityFromValue =          System.getenv("CITY_PARAM");
        String countryToValue =         System.getenv("COUNTRY_PARAM");
        int dateCounter =               Integer.parseInt(System.getenv("DATE_COUNT_PARAM"));
        String starsValues =            System.getenv("STARS_PARAM");
        boolean isInstantConfirmation = Boolean.parseBoolean(System.getenv("INSTANT_CONFIRMATION_PARAM"));
        String recipientValue =         System.getenv("RECIPIENTS_PARAM");

//        //Local
//        String cityFromValue =            "Москва";
//        String countryToValue =           "Турция";
//        int dateCounter =                 15;
//        String starsValues =              "4";
//        boolean isInstantConfirmation =   false;
//        String recipientValue =           "olena.tyshchenko@yandex.ru";

        FileInputStream fsIP= new FileInputStream(new File(reportName));
        Workbook book = new HSSFWorkbook(fsIP);

        while(book.getNumberOfSheets()>0)
            book.removeSheetAt(0);

        ArrayList<String> kidsAgesList = new ArrayList<>(Arrays.asList(kidsAgesValue.split("\\s*,\\s*")));
        ArrayList<String> starsList = new ArrayList<>();
        if(!starsValues.equals("-"))
            starsList = new ArrayList<>(Arrays.asList(starsValues.split("\\s*,\\s*")));

        //Anex part
        AnexAuthPage
                .getAnexAuthPage()
                .gotoAnexAuthPage()
                .loginAnexLKPage(anexNameValue, anexPwdValue)
                .gotoAnexSearchPage();
        AnexSearchPage
                .getAnexSearchPage()
                .selectCityFrom(cityFromValue)
                .openCountryToSelect(countryToValue)
                .selectCountryTo(countryToValue)
                .openCalendar()
                .selectFlightDate(getSearchRussianDate(dateCounter))
                .selectNightsTo(nightsValue)
                .selectNightsFrom(nightsValue)
                .clickInstantConfirmButton(isInstantConfirmation);

        for(int resortCounter = 0; resortCounter < countryResortsAnex.get(countryToValue).length; resortCounter ++){

            if(ifNoToursResult())
            {
                setHotelNamesAnexList("-");
                setCountryResortAnexList("-");
                setHotelTourPriceAnexList("-");
//                setHotelTourPriceActualizedAnexList("-");
//                setHotelTourOfferAmountList((double) 0);
            }
            else
                AnexSearchPage
                        .getAnexSearchPage()
                        .selectResortTo(countryResortsAnex.get(countryToValue)[resortCounter])
                        .clickSubmitSearchButton()
                        .waitForSearchResult()
                        .putHotelNamesAnexList(false);
            scrollUpOnPage();

        }


        //Offer part
            OfferEmailAuthPage
                    .getOfferEmailAuthPage()
                    .gotoOfferEmailPage()
                    .enterOfferEmail(offerNameValue, offerPwdValue);

        for(int resortCounter = 0; resortCounter < countryResortsAnex.get(countryToValue).length; resortCounter ++) {
            OfferEmailSearchPage
                    .getOfferEmailSearchPage()
                    .gotoSearchCriteria(countryToValue, dateCounter, isFlexibleDate, nightsValue, isFlexibleNights, adultsValue, kidsAgesList, cityFromValue, starsList, resortOffer.get(countryResortsAnex.get(countryToValue)[resortCounter]), operatorValue)
                    .putHotelNamesOfferListWithResort(countryToValue)
            ;

        }
        ReportGenerator.getReportGenerator().generateReportAnexOfferAmount(book, reportName, countryToValue, cityFromValue, dateCounter);

        book.write(new FileOutputStream(reportName));
        book.close();

        Sender.sendMail(recipientValue, reportName, "Anex amount report + " + dateCounter, countryToValue);

    }

    @AfterTest

    @DataProvider
    public Object[][] tourCriteria(){
        return new Object[][]{
                {
                        "Ttechnology",
                        "jT68$LyJ",
                        "5",
                        false,
                        7,
                        false,
                        "2",
                        "0",
                        "olena.tyshchenko@travelata.ru",
                        "7174043502F70664",
                        "C:\\Reports\\AnexHotelsAmount.xls",
                }
        };
    }

}
