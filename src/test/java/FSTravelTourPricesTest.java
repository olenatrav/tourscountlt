import framework.Constants;
import framework.ReportGenerator;
import framework.SeleniumTestCase;
import framework.Sender;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.Travelata.OfferEmailAuthPage;
import pageobjects.Travelata.OfferEmailSearchPage;
import pageobjects.operators.FSTravel.FSTravelMainPage;
import pageobjects.operators.FSTravel.FSTravelSearchPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import static com.codeborne.selenide.Selenide.sleep;
import static framework.JDBCPostgreSQLConnection.getFSTravelOfferHotelId;
import static framework.ReportGenerator.getSearchDateDashFormat;
import static pageobjects.Travelata.OfferEmailSearchPage.getHotelIdOfferList;
import static pageobjects.operators.FSTravel.FSTravelSearchPage.isEmptySearchResult;
import static pageobjects.operators.FSTravel.FSTravelSearchPage.isNoToursResult;

public class FSTravelTourPricesTest extends SeleniumTestCase {

    @Test(dataProvider = "tourCriteria", enabled = true)
    public void FSTravelTourPriceTest(String operatorValue, boolean isFlexibleDate, boolean isFlexibleNights, String kidsAges,
                                  String offerNameValue, String offerPwdValue, String reportName) throws Exception{

        //Jenkins
        String cityFromValue =      System.getenv("CITY_PARAM");
        String countryToValue =     System.getenv("COUNTRY_PARAM");
        int dateCounter =           Integer.parseInt(System.getenv("DATE_COUNT_PARAM"));
        int nightsValue =           Integer.parseInt(System.getenv("NIGHT_COUNT_PARAM"));
        String adultsValue =        System.getenv("ADULTS_PARAM");
        String starsValues =        System.getenv("STARS_PARAM");
        String mealValues =         System.getenv("MEAL_PARAM");
        String recipientValue =     System.getenv("RECIPIENTS_PARAM");

//        //Local
//        String cityFromValue =      "Анапа";
//        String countryToValue =     "Турция";
//        int dateCounter =           15;
//        int nightsValue =           7;
//        String adultsValue =        "2";
//        String starsValues =        "4";
//        String mealValues =         "AI";
//        String recipientValue =     "olena.tyshchenko@yandex.ru";

        FileInputStream fsIP= new FileInputStream(new File(reportName));
        Workbook book = new HSSFWorkbook(fsIP);

        while(book.getNumberOfSheets()>0)
            book.removeSheetAt(0);

        ArrayList<String> kidsAgesList = new ArrayList<>(Arrays.asList(kidsAges.split("\\s*,\\s*")));
        ArrayList<String> starsList = new ArrayList<>();
        ArrayList<String> mealList = new ArrayList<>();
        if(!starsValues.equals("-"))
            starsList = new ArrayList<>(Arrays.asList(starsValues.split("\\s*,\\s*")));
        if(!mealValues.equals("-"))
            mealList = new ArrayList<>(Arrays.asList(mealValues.split("\\s*,\\s*")));

        //FSTravel part
        FSTravelMainPage
                .getFSTravelMainPage()
                .gotoFSTravelMainPage()
                .selectCityFrom(cityFromValue);

        if(!FSTravelMainPage.isCountryDisplayed(countryToValue))
            FSTravelSearchPage.putEmptyHotelNamesFSTravelList(countryToValue);

        else {
            FSTravelMainPage
                    .getFSTravelMainPage()
                    .selectCountryTo(countryToValue)
                    .selectFlightDate(getSearchDateDashFormat(dateCounter))
                    .selectNightsValue(nightsValue)
                    .clickSearchButton();

            //By country
            FSTravelSearchPage
                    .getFSTravelSearchPage()
                    .waitForSearchResults();

            if (isNoToursResult() || isEmptySearchResult())
                FSTravelSearchPage.putEmptyHotelNamesFSTravelList(countryToValue);

            else {
                FSTravelSearchPage
                        .getFSTravelSearchPage()
                        .selectStars(starsList)
                        .selectMeal(mealList)
                        .waitForSearchResults()
                        .putHotelNamesFSTravelList(countryToValue, true);
            }
        }

        sleep(Constants.CONSTANT_2_SECONDS);
        getFSTravelOfferHotelId(starsList, countryToValue);

        //Offer Part
        OfferEmailAuthPage
                .getOfferEmailAuthPage()
                .gotoOfferEmailPage()
                .enterOfferEmail(offerNameValue, offerPwdValue);

        //By country
        if (!getHotelIdOfferList().isEmpty()) {
            for (int hotelCounter = 0; hotelCounter < getHotelIdOfferList().size(); hotelCounter++) {

                if ((hotelCounter % 10) == 0){
                    refreshPage();
                    System.out.println("Page reloading");
                    OfferEmailAuthPage
                            .getOfferEmailAuthPage()
                            .enterOfferEmail(offerNameValue, offerPwdValue);
                }

                OfferEmailSearchPage
                        .getOfferEmailSearchPage()
                        .gotoSearchCriteriaByTOWithHotel(countryToValue, dateCounter, isFlexibleDate, nightsValue, isFlexibleNights, adultsValue, kidsAgesList, cityFromValue, starsList, mealList, "-", operatorValue, getHotelIdOfferList().get(hotelCounter), hotelCounter)
                        .putHotelNamesOfferListByTOWithHotel(countryToValue, "-", operatorValue, getHotelIdOfferList().get(hotelCounter))
                ;
            }
        }

        ReportGenerator.getReportGenerator().generateReportFSTravelOffer(book, reportName, countryToValue, cityFromValue, dateCounter);

        book.write(new FileOutputStream(reportName));
        book.close();

        Sender.sendMail(recipientValue, reportName, "FSTravel prices report + " + dateCounter, countryToValue);

    }

    @AfterTest

    @DataProvider
    public Object[][] tourCriteria(){
        return new Object[][]{
                {
                        "55",
                        false,
                        false,
                        "0",
                        "olena.tyshchenko@travelata.ru",
                        "7174043502F70664",
                        "C:\\Reports\\FSTravelTourPrices.xls",
                }
        };
    }

}
