import framework.Constants;
import framework.ReportGenerator;
import framework.SeleniumTestCase;
import framework.Sender;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.LevelTravel.LevelTravelHotelPage;
import pageobjects.LevelTravel.LevelTravelSearchPage;
import pageobjects.LevelTravel.LevelTravelTourPage;
import pageobjects.Travelata.TravelataHotelPage;
import pageobjects.Travelata.TravelataSearchPage;
import pageobjects.Travelata.TravelataTourPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static framework.Constants.destinationCountryEng;
import static framework.ReportGenerator.getFinalWithOilTaxSerpWithOilTaxLTRatioList;
import static framework.ReportGenerator.getFinalWithOilTaxSerpWithOilTaxTravRatioList;
import static pageobjects.LevelTravel.LevelTravelSearchPage.*;
import static pageobjects.LevelTravel.LevelTravelTourPage.*;
import static pageobjects.LevelTravel.LevelTravelTourPage.getHotelTourPriceWithoutOilTaxTourLTList;
import static pageobjects.Travelata.TravelataHotelPage.checkIfEmptyPage;
import static pageobjects.Travelata.TravelataHotelPage.checkIfEmptySearch;
import static pageobjects.Travelata.TravelataSearchPage.*;
import static pageobjects.Travelata.TravelataSearchPage.getHotelTourPriceWithoutOilTaxSearchTravList;
import static pageobjects.Travelata.TravelataTourPage.*;
import static pageobjects.Travelata.TravelataTourPage.getHotelTourPriceWithoutOilTaxTourTravList;

public class LTBayHotelsFilterTest extends SeleniumTestCase {

    @Test(dataProvider = "tourCriteria", enabled = true)
    public void BayHotelsFilterTest(boolean isFlexibleNights, String reportPathValue) throws Exception {

        //Jenkins
        String departureValue =     System.getenv("CITY_PARAM");
        String countryValue =       System.getenv("COUNTRY_PARAM");
        String dateCounterValue =   System.getenv("DATE_COUNT_PARAM");
        boolean isFlexibleDate =    Boolean.parseBoolean(System.getenv("FLEXIBLE_DATE_PARAM"));
        int nightsValue =           Integer.parseInt(System.getenv("NIGHT_COUNT_PARAM"));
        String adultsValue =        System.getenv("ADULTS_PARAM");
        String kidsAges =           System.getenv("KIDS_AGES_PARAM");
        String starsValues =        System.getenv("STARS_PARAM");
        String recipientValue =     System.getenv("RECIPIENTS_PARAM");

//        //Local
//        String departureValue =     "Москва";
//        String countryValue =       "Египет";
//        String dateCounterValue =   "56";
//        boolean isFlexibleDate =    false;
//        int nightsValue =           7;
//        String adultsValue =        "2";
//        String kidsAges =           "0";
//        String starsValues =        "4";
//        String recipientValue =     "olena.tyshchenko@yandex.ru"; //konstantin.prilepa@travelata.ru, tatyana.logacheva@travelata.ru

        Workbook book;
        File file = new File(reportPathValue);
        FileInputStream fileInputStream = new FileInputStream(file);
        if (file.exists())
            book = WorkbookFactory.create(fileInputStream);
        else {
            if (file.getName().endsWith(".xls"))
                book = new HSSFWorkbook(fileInputStream);
            else if (file.getName().endsWith(".xlsx"))
                book = new XSSFWorkbook(fileInputStream);
            else
                throw new IllegalArgumentException("Unknown file extension");
        }

//        while(book.getNumberOfSheets()>0)
//            book.removeSheetAt(0);

        ArrayList<String> departuresList = new ArrayList<>(Arrays.asList(departureValue.split("\\s*,\\s*")));
        ArrayList<String> dateCounterList = new ArrayList<>(Arrays.asList(dateCounterValue.split("\\s*,\\s*")));
        ArrayList<String> kidsAgesList = new ArrayList<>(Arrays.asList(kidsAges.split("\\s*,\\s*")));
        ArrayList<String> starsList = new ArrayList<>();
        if(!starsValues.equals("-"))
            starsList = new ArrayList<>(Arrays.asList(starsValues.split("\\s*,\\s*")));

        for (int departureCounter = 0; departureCounter<departuresList.size(); departureCounter ++){

            ArrayList<String> countriesList = new ArrayList<String>(Arrays.asList(countryValue.split("\\s*,\\s*")));
            String countryEngValue = "";

            for (int countryCounter=0; countryCounter<countriesList.size(); countryCounter++) {

                try{
                    countryEngValue += destinationCountryEng.get(countriesList.get(countryCounter)) + " ";

                    int hotelBlockCounter = 1;
                    int hotelBlockPixelCounter = 0;
                    int i=0;

                    //Level Travel part
                    LevelTravelSearchPage
                            .getLevelTravelSearchPage()
                            .gotoSearchPageWithCriteria(countriesList.get(countryCounter), Integer.parseInt(dateCounterList.get(countryCounter)), isFlexibleDate, nightsValue, isFlexibleNights, adultsValue, kidsAgesList, departuresList.get(departureCounter), starsList);

                    if (ifEmptyLTSearch()) {
                        setHotelNamesSearchLTList("-");
                        setRegionLTList("-");
                        setHotelTourPricesSearchLTList("0");
                    }

                    else {
                        LevelTravelSearchPage
                                .getLevelTravelSearchPage()
                                .clickSortByPriceLink()
                                .clickBayHotelFilter();

                        while (($(byXpath(String.format(getHotelBlockTopPx(), hotelBlockPixelCounter))).isDisplayed())) { // && (i < hotelsCounter)

                            boolean isUniqueHotel = true;
                            if (i > 0) {
                                for (int hotelCounter = 0; hotelCounter < i; hotelCounter++) {
                                    if (getHotelNamesSearchLTList().get(hotelCounter).equals($(byXpath(String.format(getHotelBlockTopPx(), hotelBlockPixelCounter))).getText()))
                                        isUniqueHotel = false;
                                }
                            }
                            if (isUniqueHotel) {

                                WebElement currentHotelBlock = driver.findElement(By.xpath(String.format(getHotelBlockTopPx(), hotelBlockPixelCounter))); //getHotelStars(), hotelBlockCounter - 1
                                Actions actions = new Actions(driver);
                                actions.moveToElement(currentHotelBlock);
                                actions.perform();

                                LevelTravelSearchPage
                                        .getLevelTravelSearchPage()
                                        .addCurrentHotelNameTourPricePx(hotelBlockPixelCounter);

                                hotelBlockPixelCounter += getHotelBlockHeightPx(hotelBlockPixelCounter);
                            }
                        }
                    }

                    ReportGenerator.getReportGenerator().generateReportLTBayFilter(book, reportPathValue, countriesList.get(countryCounter), departuresList.get(departureCounter), Integer.parseInt(dateCounterList.get(countryCounter)));

                    book.write(new FileOutputStream(reportPathValue));

                    getHotelNamesSearchLTList().clear();
                    getRegionLTList().clear();
                    getHotelTourPricesSearchLTList().clear();
                }
                catch (Exception e){
                    System.out.println("Error for country " + destinationCountryEng.get(countriesList.get(countryCounter)));
                }

            }

            book.close();
            fileInputStream.close();

            Sender.sendMail(recipientValue, reportPathValue, "LT Bay filter report " + countryEngValue + " ", countryEngValue);

        }

    }

    @AfterTest

    @DataProvider
    public Object[][] tourCriteria(){
        return new Object[][]{
                {
                        false,
                        new String[]{}, //"4", "5"
                        "C:\\Reports\\BayHotelsFilter.xls",
                }
        };
    }

}
