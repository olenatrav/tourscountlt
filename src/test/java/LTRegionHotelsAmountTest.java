import framework.PercentageCounter;
import framework.SeleniumTestCase;
import framework.Sender;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.LevelTravel.LevelTravelSearchPage;
import pageobjects.Travelata.TravelataSearchPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import static framework.Constants.departureLevelTravel;
import static framework.Constants.destinationCountryEng;
import static framework.PercentageCounter.getPercentageCounterList;
import static pageobjects.LevelTravel.LevelTravelSearchPage.*;
import static pageobjects.Travelata.OfferEmailSearchPage.*;
import static pageobjects.Travelata.OfferEmailSearchPage.getHotelTourPriceActualizedOfferList;

public class LTRegionHotelsAmountTest extends SeleniumTestCase {

    @Test(dataProvider = "tourCriteria", enabled = true)
    public void LTRegionHotelsAmountTest (boolean isFlexibleNights, String reportPathValue) throws Exception{

        //Jenkins
        String countryValue =       System.getenv("COUNTRY_PARAM");
        String dateCounterValue =   System.getenv("DATE_COUNT_PARAM");
        boolean isFlexibleDate =    Boolean.parseBoolean(System.getenv("FLEXIBLE_DATE_PARAM"));
        int nightsValue =           Integer.parseInt(System.getenv("NIGHT_COUNT_PARAM"));
        String adultsValue =        System.getenv("ADULTS_PARAM");
        String kidsAgesValue =           System.getenv("KIDS_AGES_PARAM");
        String starsValues =        System.getenv("STARS_PARAM");
        String recipientValue =     System.getenv("RECIPIENTS_PARAM");

//        //Local
//        String countryValue =       "Турция"; //Египет, Турция, Россия, ОАЭ, Таиланд, Доминикана, Куба, Мексика, Венесуэла, Мальдивы, Шри-Ланка, Кипр, Греция, Абхазия
//        String dateCounterValue =   "7"; //15, 15, 15, 15, 15, 14, 14, 14, 16, 15, 15, 15, 15, 15
//        boolean isFlexibleDate =    false;
//        int nightsValue =           7;
//        String adultsValue =        "2";
//        String kidsAgesValue =      "0";
//        String starsValues =        "4";
//        String recipientValue =     "olena.tyshchenko@yandex.ru"; //konstantin.prilepa@travelata.ru

        FileInputStream fsIP= new FileInputStream(new File(reportPathValue));
        Workbook book = new HSSFWorkbook(fsIP);

//        while(book.getNumberOfSheets()>0)
//            book.removeSheetAt(0);

        ArrayList<String> kidsAgesList = new ArrayList<>(Arrays.asList(kidsAgesValue.split("\\s*,\\s*")));
        ArrayList<String> dateCounterList = new ArrayList<>(Arrays.asList(dateCounterValue.split("\\s*,\\s*")));
        ArrayList<String> starsList = new ArrayList<>();
        if(!starsValues.equals("-"))
            starsList = new ArrayList<>(Arrays.asList(starsValues.split("\\s*,\\s*")));

        String countryEngValue = "";

        ArrayList<String> countriesList = new ArrayList<String>(Arrays.asList(countryValue.split("\\s*,\\s*")));

        for (int countryCounter=0; countryCounter<countriesList.size(); countryCounter++){

            countryEngValue += destinationCountryEng.get(countriesList.get(countryCounter)) + " ";

            for (int departureCounter = 0; departureCounter < departureLevelTravel.length; departureCounter ++){

                LevelTravelSearchPage
                        .getLevelTravelSearchPage()
                        .getStatisticByTOFromFilter(book, countriesList.get(countryCounter), Integer.parseInt(dateCounterList.get(countryCounter)), isFlexibleDate,
                                nightsValue, isFlexibleNights, adultsValue, kidsAgesList, departureLevelTravel[departureCounter], starsList, reportPathValue);
                TravelataSearchPage
                        .getTravelataSearchPage()
                        .closeLotteryPopup()
                        .closeHRPopup()
                        .closeCashBackPopup()
                        .closePaylatePopup()
                        .getTravStatisticByTO(book, countriesList.get(countryCounter), Integer.parseInt(dateCounterList.get(countryCounter)), isFlexibleDate, nightsValue, isFlexibleNights,
                                adultsValue, kidsAgesList, departureLevelTravel[departureCounter], starsList, reportPathValue);

                PercentageCounter
                        .getPercentageCounter()
                        .countPercentage(book, countriesList.get(countryCounter), reportPathValue, departureLevelTravel[departureCounter]);

                refreshPage();

            }

            getTOList().clear();
            getHotelNamesSearchLTList().clear();
            getHotelTourPricesSearchLTList().clear();
            getHotelNamesOfferList().clear();
            getCountryResortOfferList().clear();
            getHotelTourPriceOfferList().clear();
            getHotelTourPriceActualizedOfferList().clear();
            getPercentageCounterList().clear();

        }

        book.write(new FileOutputStream(reportPathValue));
        book.close();

        Sender.sendMail(recipientValue, reportPathValue, "LT region amount report " + countryEngValue + " ", countryEngValue);

    }


    @AfterTest

    @DataProvider
    public Object[][] tourCriteria(){
        return new Object[][]{
                {
                        false,
                        "C:\\Reports\\LTRegionHotelsAmount.xls",
                }
        };
    }

}
