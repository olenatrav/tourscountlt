import framework.Constants;
import framework.ReportGenerator;
import framework.SeleniumTestCase;
import framework.Sender;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.LevelTravel.LevelTravelHotelPage;
import pageobjects.LevelTravel.LevelTravelSearchPage;
import pageobjects.LevelTravel.LevelTravelTourPage;
import pageobjects.Travelata.TravelataHotelPage;
import pageobjects.Travelata.TravelataSearchPage;
import pageobjects.Travelata.TravelataTourPage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static framework.Constants.destinationCountryEng;
import static pageobjects.LevelTravel.LevelTravelSearchPage.*;
import static pageobjects.LevelTravel.LevelTravelTourPage.*;
import static framework.ReportGenerator.getFinalWithOilTaxSerpWithOilTaxLTRatioList;
import static framework.ReportGenerator.getFinalWithOilTaxSerpWithOilTaxTravRatioList;
import static pageobjects.Travelata.TravelataHotelPage.checkIfEmptyPage;
import static pageobjects.Travelata.TravelataHotelPage.checkIfEmptySearch;
import static pageobjects.Travelata.TravelataSearchPage.*;
import static pageobjects.Travelata.TravelataTourPage.*;

public class LTTourPricesTest extends SeleniumTestCase {

    @Test(dataProvider = "tourCriteria", enabled = true)
    public void ComparePricesTest(boolean isFlexibleNights, String reportPathValue) throws Exception {

        //Jenkins
        String departureValue =     System.getenv("CITY_PARAM");
        String countryValue =       System.getenv("COUNTRY_PARAM");
        String dateCounterValue =   System.getenv("DATE_COUNT_PARAM");
        boolean isFlexibleDate =    Boolean.parseBoolean(System.getenv("FLEXIBLE_DATE_PARAM"));
        int nightsValue =           Integer.parseInt(System.getenv("NIGHT_COUNT_PARAM"));
        String adultsValue =        System.getenv("ADULTS_PARAM");
        String kidsAges =           System.getenv("KIDS_AGES_PARAM");
        String starsValues =        System.getenv("STARS_PARAM");
        int hotelsCounter =         Integer.parseInt(System.getenv("HOTEL_COUNT_PARAM"));
        String recipientValue =     System.getenv("RECIPIENTS_PARAM");

//        //Local
//        String departureValue =     "Москва";
//        String countryValue =       "Кипр, Греция, Абхазия"; //Египет, Турция, Россия, ОАЭ, Таиланд, Доминикана, Куба, Мексика, Венесуэла, Мальдивы, Шри-Ланка, Кипр, Греция, Абхазия
//        String dateCounterValue =   "15"; //15, 15, 15, 15, 15, 14, 14, 14, 16, 15, 15, 15, 15, 15
//        boolean isFlexibleDate =    false;
//        int nightsValue =           7;
//        String adultsValue =        "2";
//        String kidsAges =           "0";
//        String starsValues =        "-";
//        int hotelsCounter =         15;
//        String recipientValue =     "olena.tyshchenko@yandex.ru"; //konstantin.prilepa@travelata.ru

        Workbook book;
        File file = new File(reportPathValue);
        FileInputStream fileInputStream = new FileInputStream(file);
        if (file.exists())
            book = WorkbookFactory.create(fileInputStream);
        else {
            if (file.getName().endsWith(".xls"))
                book = new HSSFWorkbook(fileInputStream);
            else if (file.getName().endsWith(".xlsx"))
                book = new XSSFWorkbook(fileInputStream);
            else
                throw new IllegalArgumentException("Unknown file extension");
        }

//        while(book.getNumberOfSheets()>0)
//            book.removeSheetAt(0);

        ArrayList<String> departuresList = new ArrayList<>(Arrays.asList(departureValue.split("\\s*,\\s*")));
        ArrayList<String> dateCounterList = new ArrayList<>(Arrays.asList(dateCounterValue.split("\\s*,\\s*")));
        ArrayList<String> kidsAgesList = new ArrayList<>(Arrays.asList(kidsAges.split("\\s*,\\s*")));
        ArrayList<String> starsList = new ArrayList<>();
        if(!starsValues.equals("-"))
            starsList = new ArrayList<>(Arrays.asList(starsValues.split("\\s*,\\s*")));

        for (int departureCounter = 0; departureCounter<departuresList.size(); departureCounter ++){

            ArrayList<String> countriesList = new ArrayList<String>(Arrays.asList(countryValue.split("\\s*,\\s*")));
            String countryEngValue = "";

            for (int countryCounter=0; countryCounter<countriesList.size(); countryCounter++) {

                try{
                    countryEngValue += destinationCountryEng.get(countriesList.get(countryCounter)) + " ";

                    int hotelBlockCounter = 1;
                    int hotelBlockPixelCounter = 0;
                    int i=0;

                    System.out.println(destinationCountryEng.get(countriesList.get(countryCounter)));

                    //Level Travel part
                    LevelTravelSearchPage
                            .getLevelTravelSearchPage()
                            .gotoSearchPageWithCriteria(countriesList.get(countryCounter), Integer.parseInt(dateCounterList.get(countryCounter)),
                                    isFlexibleDate, nightsValue, isFlexibleNights, adultsValue, kidsAgesList, departuresList.get(departureCounter), starsList);

                    if (ifEmptyLTSearch()) {
                        setHotelNamesSearchLTList("-");
                        setHotelTourPricesSearchLTList("0");
                        setHotelTourPricesTourLTList("0");
                        setHotelTourPriceOilTaxTourLTList("0");
                        setHotelTourPriceWithoutOilTaxTourLTList("0");
                        setToNameTourLTList("-");
                    }

                    else {
                        LevelTravelSearchPage
                                .getLevelTravelSearchPage()
                                .closeFlamingoBanner()
                                .clickSortByPriceLink();

                        while (($(byXpath(String.format(getHotelBlockTopPx(), hotelBlockPixelCounter))).isDisplayed()) && (i < hotelsCounter)) {
//                    while (($(byXpath(String.format(getHotelNameLink(), hotelBlockCounter))).isDisplayed()) && (i < hotelsCounter)) {

                            boolean isUniqueHotel = true;
                            String oldTab = driver.getWindowHandle();
                            if (i > 0) {
                                for (int hotelCounter = 0; hotelCounter < i; hotelCounter++) {
                                    if (getHotelNamesSearchLTList().get(hotelCounter).equals($(byXpath(String.format(getHotelBlockTopPx(), hotelBlockPixelCounter))).getText()))
//                                if (getHotelNamesSearchLTList().get(hotelCounter).equals($(byXpath(String.format(getHotelNameLink(), hotelBlockCounter))).getText()))
                                        isUniqueHotel = false;
                                }
                            }
                            if (isUniqueHotel) {

                                WebElement currentHotelBlock = driver.findElement(By.xpath(String.format(getHotelBlockTopPx(), hotelBlockPixelCounter))); //getHotelStars(), hotelBlockCounter - 1
//                            WebElement currentHotelBlock = driver.findElement(By.xpath(String.format(getHotelNameLink(), hotelBlockCounter)));
                                Actions actions = new Actions(driver);
                                actions.moveToElement(currentHotelBlock);
                                actions.perform();

                                LevelTravelSearchPage
                                        .getLevelTravelSearchPage()
                                        .addCurrentHotelNameTourPricePx(hotelBlockPixelCounter)
//                                    .addCurrentHotelNameTourPrice(hotelBlockCounter)
                                        .gotoLevelTravelHotelPagePx(hotelBlockPixelCounter);
//                                    .gotoLevelTravelHotelPage(hotelBlockCounter);

                                String hotelTab = driver.getWindowHandle();
                                ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
                                newTab.remove(hotelTab);
                                driver.switchTo().window(newTab.get(0));

                                LevelTravelHotelPage
                                        .getLevelTravelHotelPage()
                                        .reloadIfErrorDisplayed();
                                if(LevelTravelHotelPage.getLevelTravelHotelPage().ifEmptyHotelPage()){
                                    LevelTravelTourPage
                                            .getLevelTravelTourPage()
                                            .addEmptyTourTOPrice();
                                }
                                else
                                    LevelTravelHotelPage.getLevelTravelHotelPage()
                                        .waitHotelPageTourLoaded()
                                        .closeFlamingoBanner();

                                if(LevelTravelHotelPage.getLevelTravelHotelPage().ifEmptyTours() ||
                                        LevelTravelHotelPage.getLevelTravelHotelPage().ifNoOffers(nightsValue) ||
                                        LevelTravelHotelPage.getLevelTravelHotelPage().ifEmptyHotelPage() ||
                                        LevelTravelHotelPage.getLevelTravelHotelPage().ifHotelPageNoLoaded()){
                                    LevelTravelTourPage
                                            .getLevelTravelTourPage()
                                            .addEmptyTourTOPrice();
                                }
                                else{
                                    LevelTravelHotelPage
                                            .getLevelTravelHotelPage()
                                            .waitHotelPageAllToursButton()
                                            .loadToursIfButtonDisplayed()
                                            .gotoLevelTravelTourPage(nightsValue);

                                    if (LevelTravelHotelPage.getLevelTravelHotelPage().ifTourOutdated())
                                    {
                                        LevelTravelTourPage
                                                .getLevelTravelTourPage()
                                                .addEmptyTourTOPrice();
                                    }
                                    else{
                                        driver.close();
                                        newTab = new ArrayList<String>(driver.getWindowHandles());
                                        sleep(Constants.CONSTANT_1_SECOND);
                                        newTab.remove(oldTab);
                                        driver.switchTo().window(newTab.get(0));

                                        LevelTravelTourPage
                                                .getLevelTravelTourPage()
                                                .reloadIfErrorDisplayed()
                                                .waitForLoaderDissapear()
                                                .closeFlamingoBanner();
                                        if(!LevelTravelTourPage.getLevelTravelTourPage().checkIfTourLoaded()){
                                            LevelTravelTourPage.getLevelTravelTourPage().addEmptyTourTOPrice();
                                        }
                                        else{
                                            LevelTravelTourPage.getLevelTravelTourPage().waitForCheckingFlights();
                                            if (LevelTravelTourPage.getLevelTravelTourPage().checkIfTourLTOutdated()) {
                                                LevelTravelTourPage.getLevelTravelTourPage().addEmptyTourTOPrice();
                                            }
                                            else {
                                                LevelTravelTourPage
                                                        .getLevelTravelTourPage()
                                                        .addTourPrice()
                                                        .addTourTO();
                                            }
                                        }
                                    }
                                }

                                i++;
                                sleep(Constants.CONSTANT_1_SECOND);
                                driver.close();
                                driver.switchTo().window(oldTab);

//                            hotelBlockCounter++;
//                            int hotelBlockPixelCounterSpot = 0;
//                            if(!$(byXpath(String.format(getHotelBlockConditionsPx(), hotelBlockPixelCounter))).isDisplayed())
//                                hotelBlockPixelCounterSpot += 37; //hotel conditions block not displayed
//                            else if($(byXpath(String.format(getHotelBlockSimilarToursPx(), hotelBlockPixelCounter))).isDisplayed())
//                                hotelBlockPixelCounterSpot += 88; //similar tours block on hotel block displayed
//                            if($(byXpath(String.format(getHotelBlockHeaderLabelPx(), hotelBlockPixelCounter))).isDisplayed())
//                                hotelBlockPixelCounterSpot += 24; //mir label on hotel block displayed
//                            if($(byXpath(String.format(getHotelBlockInstantConfirmPx(), hotelBlockPixelCounter))).isDisplayed() ||
//                                    $(byXpath(String.format(getHotelBlockVariousLabelPx(), hotelBlockPixelCounter))).isDisplayed())
//                                hotelBlockPixelCounterSpot += 28; //instant confirm label / another label on hotel block displayed
//                            if (hotelBlockCounter == 3)
//                                hotelBlockPixelCounter += getHotelBlockHeight();
//                            hotelBlockPixelCounter += 216;
//                            hotelBlockPixelCounter += hotelBlockPixelCounterSpot;

                                hotelBlockPixelCounter += getHotelBlockHeightPx(hotelBlockPixelCounter);
                            }
                        }
                    }


                    // Travelata part

                    i = 0;
                    hotelBlockCounter = 1;

                    TravelataSearchPage
                            .getTravelataSearchPage()
                            .gotoTravelataMain()
                            .closeLotteryPopup()
                            .closeHRPopup()
                            .closePaylatePopup()
                            .closeCashBackPopup()
                            .gotoSearchPageWithCriteria(countriesList.get(countryCounter), "",
                                    Integer.parseInt(dateCounterList.get(countryCounter)), isFlexibleDate, nightsValue,
                                    isFlexibleNights, adultsValue, kidsAgesList, departuresList.get(departureCounter), starsList);

                    if (ifEmptyTravSearch()) {
                        setHotelNamesSearchTravList("-");
                        setHotelTourPricesSearchTravList("0");
                        setHotelTourPriceOilTaxSearchTravList("0");
                        setHotelTourPriceWithoutOilTaxSearchTravList("0");
                        setHotelTourPricesTourTravList("0");
                        setHotelTourPriceOilTaxTourTravList("0");
                        setHotelTourPriceWithoutOilTaxTourTravList("0");
                        setToNameTourTravList("-");
                    }

                    else {
                        TravelataSearchPage
                                .getTravelataSearchPage()
                                .closeLotteryPopup()
                                .closeHRPopup()
                                .closeExpiredPopup()
                                .closeCashBackPopup()
                                .clickSortByPriceLink();

                        while (($(byXpath(String.format(getTravHotelBlock(), hotelBlockCounter))).isDisplayed()) && (i < hotelsCounter)) {
                            boolean isUniqueHotel = true;
                            String oldTab = driver.getWindowHandle();
                            if (i > 0) {
                                for (int hotelCounter = 0; hotelCounter < i; hotelCounter++) {
                                    if (getHotelNamesSearchTravList().get(hotelCounter).equals($(byXpath(String.format(getTravHotelNameLink(), hotelBlockCounter))).getText()))
                                        isUniqueHotel = false;
                                }
                            }
                            if (isUniqueHotel) {

                                WebElement currentHotelBlock = driver.findElement(By.xpath(String.format(getTravHotelNameLink(), hotelBlockCounter)));
                                Actions actions = new Actions(driver);
                                actions.moveToElement(currentHotelBlock);
                                actions.perform();

                                TravelataSearchPage
                                        .getTravelataSearchPage()
                                        .closeLotteryPopup()
                                        .closeExpiredPopup()
                                        .closeCashBackPopup()
                                        .addHotelNameTourPriceOilTax(hotelBlockCounter)
                                        .gotoTravelataHotelPage(hotelBlockCounter);

                                String hotelTab = driver.getWindowHandle();
                                ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
                                newTab.remove(hotelTab);
                                driver.switchTo().window(newTab.get(0));

                                TravelataHotelPage
                                        .getTravelataHotelPage()
                                        .closeCashBackPopup()
                                        .closeLotteryPopup()
                                        .reloadIfErrorDisplayed();

                                if (!checkIfEmptyPage()){

                                    TravelataHotelPage
                                            .waitForLoaderDisappear();
                                    if (!checkIfEmptySearch()) {
                                        TravelataHotelPage
                                                .getTravelataHotelPage()
                                                .closeCashBackPopup()
                                                .closeLotteryPopup()
//                                .closeFaqBanner()
                                                .gotoTravelataTourPage();

                                        ////Old Tour Page
//                                        TravelataTourPage
//                                                .getTravelataTourPage()
//                                                .reloadIfErrorDisplayed()
//                                                .waitForLoaderDisappear();
//                                        if (TravelataTourPage.getTravelataTourPage().checkIfTourTravOutdated()) {
//                                            TravelataTourPage
//                                                    .getTravelataTourPage()
//                                                    .addEmptyTourTOPrice();
//                                        } else {
//                                            TravelataTourPage
//                                                    .getTravelataTourPage()
//                                                    .addTourTOPrice();
//                                        }
                                        /////////////////////////////

                                        ////Constructor Tour Page
                                        TravelataTourPage
                                                .getTravelataTourPage()
                                                .waitForFlightsLoaded();
                                        if (TravelataTourPage.getTravelataTourPage().checkIfTourOutdated()) {
                                            TravelataTourPage
                                                    .getTravelataTourPage()
                                                    .addEmptyTourTOPrice();
                                        } else {
                                            TravelataTourPage
                                                    .getTravelataTourPage()
                                                    .addCheapestTourPrice();
                                        }
                                        /////////////////////////////

                                        sleep(Constants.CONSTANT_1_SECOND);
                                        driver.close();
                                        driver.switchTo().window(oldTab);
                                        i++;
                                    } else {
                                        TravelataSearchPage
                                                .getTravelataSearchPage()
                                                .clearLastHotelNameTourPrice();
                                        sleep(Constants.CONSTANT_1_SECOND);
                                        driver.close();
                                        driver.switchTo().window(oldTab);
                                    }

                                }

                                else {
                                    TravelataSearchPage
                                            .getTravelataSearchPage()
                                            .clearLastHotelNameTourPrice();
                                    sleep(Constants.CONSTANT_1_SECOND);
                                    driver.close();
                                    driver.switchTo().window(oldTab);
                                }

                            }
                            hotelBlockCounter++;
                        }

                    }

                    ReportGenerator.getReportGenerator().generateReportLTTrav(book, reportPathValue, countriesList.get(countryCounter), departuresList.get(departureCounter), Integer.parseInt(dateCounterList.get(countryCounter)));

                    getHotelNamesSearchLTList().clear();
                    getHotelTourPricesSearchLTList().clear();

                    getToNameTourLTList().clear();
                    getHotelTourPricesTourLTList().clear();
                    getHotelTourPriceOilTaxTourLTList().clear();
                    getHotelTourPriceWithoutOilTaxTourLTList().clear();

                    getHotelNamesSearchTravList().clear();
                    getHotelTourPricesSearchTravList().clear();
                    getHotelTourPriceOilTaxSearchTravList().clear();
                    getHotelTourPriceWithoutOilTaxSearchTravList().clear();

                    getToNameTourTravList().clear();
                    getHotelTourPricesTourTravList().clear();
                    getHotelTourPriceOilTaxTourTravList().clear();
                    getHotelTourPriceWithoutOilTaxTourTravList().clear();

                    getFinalWithOilTaxSerpWithOilTaxLTRatioList().clear();
                    getFinalWithOilTaxSerpWithOilTaxTravRatioList().clear();

                    book.write(new FileOutputStream(reportPathValue));
                }
                catch (Exception e){
                    System.out.println("Error for country " + destinationCountryEng.get(countriesList.get(countryCounter)));
                }

                refreshPage();

            }

            book.close();
            fileInputStream.close();

            Sender.sendMail(recipientValue, reportPathValue, "Prices comparison report " + countryEngValue + " ", countryEngValue);

        }

    }

    @AfterTest

    @DataProvider
    public Object[][] tourCriteria(){
        return new Object[][]{
                {
                        false,
                        "C:\\Reports\\ComparePrices.xls",
                }
        };
    }

}
